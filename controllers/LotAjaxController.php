<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\filters\AuthFilter;
use app\components\filters\LotAccessFilter;
use app\components\filters\CreateDealFilter;
use app\components\filters\TenderAccessFilter;
use app\components\filters\DelegateLotFilter;
use app\components\filters\RiskIsNotActive;
use app\components\filters\SpecialistFilter;

use app\models\Lot;
use app\models\LotDep;
use app\models\LotFile;
use app\models\LotParticipant;
use app\models\LotPriceOffer;
use app\models\LotRisk;
use app\models\AppProvision;
use app\models\AgreementProvision;
use app\models\ManagerHistory;
use app\models\TenderCompetitor;


class LotAjaxController extends Controller
{
	public function behaviors()
	{
		return [
			[
				'class' => AuthFilter::className(),
			],
			[
				'class' => TenderAccessFilter::className(),
				'only' => ['remove-file', 'add-participant', 'remove-participant', 'create-risk', 'app-is-need', 'app-is-exist', 'app-is-return', 'agree-is-need', 'agree-is-exist', 'agree-is-return'],
			],
			[
				'class' => DelegateLotFilter::className(),
			  'only' => ['delegate-to-dep']
		  ],
		  [
		  	'class' => SpecialistFilter::className(),
		  	'only' => ['delegate-to-dep', 'app-is-need', 'app-is-exist', 'app-is-return', 'agree-is-need', 'agree-is-exist', 'agree-is-return', 'change-provision-amount']
		  ]  
		];
	}

	public function actionDelegateToDep($tenderid)
	{
		$lot = new Lot();
		return $this->asJson($lot->delegateToDep());
	}

	public function getLinkToDeal($csid, $lotid)
	{
		$soap='xreq=
            <soapenv:Envelope">
            <soapenv:Header>
            <heads:credentials">
            <heads:key>69av92us40jn</heads:key>
            </heads:credentials>
            </soapenv:Header>
            <soapenv:Body>
            <get_ecsid_by_csid>'.$csid.'</get_ecsid_by_csid>
            </soapenv:Body>
            </soapenv:Envelope>';

       	$headers = array(
            'POST /apihub/index.php',
            'Host: kcrm.kaztranscom.kz',
            'Content-Length: '.strlen($soap));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://kcrm.kaztranscom.kz/apihub/index.php');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, "Opera/9.63 (Windows NT 5.1; U; ru) Presto/2.1.1");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
        curl_setopt($ch, CURLOPT_POST, true );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $soap);
        $result = curl_exec($ch);
        curl_close($ch);
      
        $ecsid = $result;
       
      	if (Lot::linkToDeal($ecsid, $lotid)) {
      		return true;
		}

		return false;     	
	}

	public function actionDelegateFromDep()
	{
		return Lot::delegateFromDep();
	}

	public function actionUpdateFile()
	{
		$file = new LotFile();
		if ($file->ajaxUpdateFile()) {
			return $this->asJson($file->response);
		} else {
			return false;
		}
	}

	public function actionAssignManager()
	{
		$lot = new Lot();

		if ($lot->assignManager()) {
			return true;
		}
		return false;
	}

	public function actionReAssignManager($lotid)
	{
		return Lot::reAssignManager($lotid);
	}

	public function actionAssignDep()
	{
		$lot_dep = new LotDep();

		if ($lot_dep->assignDepartment()) {
			return $this->asJson($lot_dep->response);
		}
		return false; 
	}

	public function actionReAssignDep()
	{
		$lot_dep = new LotDep();

		if ($lot_dep->reAssignDepartment()) {
			return $this->asJson($lot_dep->response);
		}

		return $this->asJson(['status' => 0]);
	}

	public function actionRemoveFile($id)
	{
		$file = new LotFile();

		if ($file->ajaxRemoveFile($id)) {
			return $this->asJson($file->response);
		} else {
			return false;
		}
	}
	public function actionUploadFile(){

		$file = new LotFile();
		if ($file->ajaxUploadFile()) {
			return $this->asJson($file->response);
		} else {
			return false;
		}
	}

	public function actionAddParticipant()
	{	
		$participant = new LotParticipant();


		$participant->lp_title = Yii::$app->request->post('lp_title');
		$participant->lp_competitor_id = Yii::$app->request->post('lp_competitor_id');
		$participant->lp_price = Yii::$app->request->post('lp_price');
		$participant->lp_discount = Yii::$app->request->post('lp_discount');
		$participant->lp_lotid = Yii::$app->request->post('lp_lotid');

		if ($participant->lp_competitor_id == NULL) {
				
				try {
					$competitor = new TenderCompetitor();

					$competitor->competitor_title = $participant->lp_title;

					$competitor->add();

					$participant->lp_competitor_id = $competitor->competitor_id;			
							
				} catch (Exception $e) {
					return $e->getMessage();
				}
		}

		if ($participant->add()) {
			return $this->asJson($participant->response);
		} else {
			return false;
		}
	}

	public function actionRemoveParticipant($id)
	{
		return LotParticipant::remove($id);
	}

	public function actionLotWinner($id)
	{
		return LotParticipant::winner($id);
	}

	public function actionChangePriceOffer($id)
	{
		$price_offer = new LotPriceOffer();

		if ($price_offer->change($id)) {
			return $this->asJson($price_offer->response);
		} else {
			return false;
		}
	}

	public function actionAppIsNeed($tenderid)
	{

		if (AppProvision::isNeed()) {
			return $this->asJson(AppProvision::$response);
		} else {
			return false;
		}
	}

	public function actionAppIsExist($tenderid)
	{
		if (AppProvision::isExist()) {
			return true;
		} else {
			return false;
		}
	}

	public function actionAppIsReturn($tenderid)
	{
		if (AppProvision::isReturn()) {
			return true;
		} else {
			return false;
		}
	}


	public function actionAgreeIsNeed($tenderid)
	{
		if (AgreementProvision::isNeed()) {
			return true;
		} else {
			return false;
		}
	}

	public function actionAgreeIsExist($tenderid)
	{
		if (AgreementProvision::isExist()) {
			return true;
		} else {
			return false;
		}
	}

	public function actionAgreeIsReturn($tenderid)
	{
		if (AgreementProvision::isReturn()) {
			return true;
		} else {
			return false;
		}
	}

	public function actionChangeProvisionAmount($tenderid)
	{
		if (AppProvision::changeAmount() && AgreementProvision::changeAmount()) {
			return $this->asJson(['ap_amount' => AppProvision::$response, 'lap_amount' => AgreementProvision::$response]);
		} else {
			return false;
		}
	}

	public function actionIsParticipate()
	{
			$lot = new Lot();

			if ($lot->isParticipate()) {

				// if ($lot->is_participate == 0) {
				// 	Yii::$app->MailComponent->sendDepMail($this->depid, $this->l_tenderid['l_tenderid'], $this->l_tenderid['lot_number'], $this->l_tenderid['lot_name']);
				// }

				return $this->asJson($lot->response);
			}

			return $this->asJson(['status' => 404]);
	}

	public function actionChangeManager()
	{
		$history = new ManagerHistory();
		if ($history->changeManager()) {
				return $this->asJson($history->response);
		}
	}

	public function actionApproveChangeManager()
	{
		$history = new ManagerHistory();

		if ($history->approveChangeManager()) {
				return $this->asJson($history->response);
		}
	}
}