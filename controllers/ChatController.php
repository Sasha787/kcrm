<?php 

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\filters\AuthFilter;
use app\components\filters\LotAccessFilter;
use app\components\filters\CreateDealFilter;
use app\components\filters\TenderAccessFilter;
use app\components\filters\SpecialistFilter;

use app\models\LotChat;

class ChatController extends Controller
{
	public function behaviors()
	{
		return [
			[
				'class' => AuthFilter::className(),
			],
			[
				'class' => LotAccessFilter::className(),
				'only' => ['view']
 			],
			[
				'class' => TenderAccessFilter::className(),
				'only' => ['edit', 'create']
			],
			[
				'class' => CreateDealFilter::className(),
				'only' => ['create-deal', 'create-new-deal']
			],
			[
				'class' => SpecialistFilter::className(),
				'only' => ['create', 'store', 'update']
			],
		];
	}

	public function actionMessages($lotId)
	{	
        $lot_messages = LotChat::getLotMessages($lotId);
        return $this->asJson(['messages' => $lot_messages]);
	}
	
	public function actionNotifications($lotId)
	{
		$managers = Yii::$app->request->post('data');

		$result = Yii::$app->MailComponent->notifyRiskManagers($lotId, $managers);

		return json_encode($result);
	}

	public function actionNotifyChatUser($userId, $lotId) {
		$result = Yii::$app->MailComponent->sendChatMail($userId, $lotId);
		return json_encode(['userid' => $userId, 'lotId' => $lotId, 'result' => $result]);
	}

    public function actionMessage()
    {
        $message = new LotChat();

		$message->lotid = Yii::$app->request->post('lotId');
		$message->userid = Yii::$app->request->post('userId');
        $message->message = Yii::$app->request->post('message');
        
		if ($message->add()) {

            return $this->asJson(['success' => true]);

        } else {
            
            return $this->asJson(['success' => false]);
        }
    }
}