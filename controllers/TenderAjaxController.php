<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

use app\components\filters\AuthFilter;
use app\components\filters\TenderAccessFilter;
use yii\data\ArrayDataProvider;

use app\models\Tender;
use app\models\TenderFile;
use app\models\TenderManager;

/**
 * 
 */
class TenderAjaxController extends Controller
{
	public function behaviors()
	{
		return [
			[
				'class' => AuthFilter::className(),
			],
			[
				'class' => TenderAccessFilter::className(),
				'only' => ['remove-file', 'to-specialist', 'toggle-tender-status']
			]
		];
	}

	public function actionUploadFile()
	{
		$file = new TenderFile();
		if ($file->ajaxUploadFile()) {
			return $this->asJson($file->response);
		} else {
			return false;
		}
	}
	public function actionUpdateFile()
	{
		$file = new TenderFile();
		if ($file->ajaxUpdateFile()) {
			return $this->asJson($file->response);
		} else {
			return false;
		}
	}

	public function actionRemoveFile($id)
	{
		$file = new TenderFile();

		if ($file->ajaxRemoveFile($id)) {
			return $this->asJson($file->response);
		} else {
			return false;
		}
	}

	public function actionToSpecialist()
	{
		$tender_to_manager = new TenderManager();

		if ($tender_to_manager->ajaxToSpecialist()) {

			Yii::$app->MailComponent->sendToSpecialist($tender_to_manager->tm_tenderid, $tender_to_manager->tm_managerid);

			return true;
		} else {
			return false;
		}
	}

	public function actionToggleTenderStatus()
	{
		if (Tender::toggleTenderStatus()) {
			return true;
		}
		return false;
	}

	public function actionSearchBin($q = null, $id = null)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$out = ['results' => ['id' => '', 'text' => '']];
		if (!is_null($q)) {

			$query1 = (new \yii\db\Query())
				->select(['csid as id', 'csbin as text'])
				->from('kcrm.csinfo')
				->where(['like', 'csbin', $q])
				->andWhere(['cscactive' => 1])
				->limit(10);

			$query2 = (new \yii\db\Query())
				->select(['csid as id', 'csbin as text'])
				->from(Yii::$app->params['cfg_sandbox_db'] . '.csinfo')
				->where(['like', 'csbin', $q])
				->andWhere(['cscactive' => 1])
				->limit(10);

			$unionQuery = $query1->union($query2);

			$command = $unionQuery->createCommand();
			$data = $command->queryAll();
			$out['results'] = array_values($data);
		} elseif ($id > 0) {
			$out['results'] = ['id' => $id, 'text' => City::find($id)->name];
		}
		return $out;
	}

	public function actionSearchUser($q = null, $id = null)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		$out = ['results' => ['id' => '', 'name' => '', 'q' => $q, 'pid' => $id]];
		
		if (!is_null($q)) {

			$query1 = (new \yii\db\Query())
				->select(['userid as id', 'fullname as name', 'email'])
				->from('contracts.users')
				->where(['like', 'fullname', $q]) //'%'.$q.'%'])
				->andWhere(['blocked' => 0])
				->limit(10);

			$command = $query1->createCommand();
			$data = $command->queryAll();
			$out['results'] = array_values($data);

		} /* elseif ($id > 0) {
			$out['results'] = ['id' => $id, 'name' => City::find($id)->name];
		} */

		return $out;
	}

	public function actionSearchCs($q = null, $id = null)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$out = ['results' => ['id' => '', 'text' => '']];
		if (!is_null($q)) {
			$query1 = (new \yii\db\Query())
				->select(['csid as id', 'csname as text'])
				->from('kcrm.csinfo')
				->where(['like', 'csname', $q])
				->andWhere(['cscactive' => 1])
				->limit(10);

			$query2 = (new \yii\db\Query())
				->select(['csid as id', 'csname as text'])
				->from(Yii::$app->params['cfg_sandbox_db'] . '.csinfo')
				->where(['like', 'csname', $q])
				->andWhere(['cscactive' => 1])
				->limit(10);

			$unionQuery = $query1->union($query2);

			$command = $unionQuery->createCommand();
			$data = $command->queryAll();

			$out['results'] = array_values($data);
		} elseif ($id > 0) {
			$out['results'] = ['id' => $id, 'text' => City::find($id)->name];
		}
		return $out;
	}

	public function actionFindCsByid($id)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		if (!is_null($id)) {

			$query1 = (new \yii\db\Query())
				->select(['csid', 'csname', 'csbin'])
				->from('kcrm.csinfo')
				->where(['csid' => $id])
				->andWhere(['cscactive' => 1])
				->limit(1);

			$query2 = (new \yii\db\Query())
				->select(['csid', 'csname', 'csbin'])
				->from(Yii::$app->params['cfg_sandbox_db'] . '.csinfo')
				->where(['csid' => $id])
				->andWhere(['cscactive' => 1])
				->limit(1);

			$unionQuery = $query1->union($query2);

			$command = $unionQuery->createCommand();
			$data = $command->queryOne();

			return $data;
		}
	}

	public function actionSearchCompetitor($q = null, $id = null)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$out = ['results' => ['id' => '', 'text' => '']];
		if (!is_null($q)) {

			$query = (new \yii\db\Query())
				->select(['competitor_id as id', 'competitor_title as text'])
				->from('tender_competitors')
				->where(['like', 'competitor_title', $q])
				->limit(10);



			$command = $query->createCommand();
			$data = $command->queryAll();

			$out['results'] = array_values($data);
		} elseif ($id > 0) {
			$out['results'] = ['id' => $id, 'text' => City::find($id)->name];
		}
		return $out;
	}

	public function actionSearch($q)
	{
		$userlist = Yii::$app->CacheComponent->getDrtpSpecialists();

		if ($q == null) {
			return $this->redirect(['tender/index']);
		}

		$provider = new ArrayDataProvider([
			'allModels' => Tender::search($q),
			'sort' => [
				'attributes' => [
					'tenderid',
					'senddate',
					'source_title',
					't_ccsid',
					't_csbin',
					't_csname',
					'accept_solution',
					'purchase_title',
					'tender_userid',
					'tender_username',
					'purchase_number',
					'purchase_name',
					'actid',
					'aid_signed_agreements',
					'updated_at',
					'lot_counts_tv',
					'lot_participate_count',
				],
				'defaultOrder' => [
					'lot_participate_count' => SORT_DESC,
					'lot_counts_tv' => SORT_DESC,
					'updated_at' => SORT_DESC,
				]
			],
			'pagination' => [
				'pageSize' => 25,
			],
		]);

		$tenders = $provider->getModels();

		return $this->render('/tender/index', compact('provider', 'tenders', 'userlist'));
	}
}
