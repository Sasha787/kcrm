<?php 

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\filters\AuthFilter;
use app\components\filters\LotAccessFilter;
use app\components\filters\CheckListAccessFilter;
use app\components\filters\SpecialistFilter;

use app\models\Document;
use app\models\PrivUser;


/**
 * 
 */
class CheckListAjaxController extends Controller
{
	public function behaviors()
	{
		return [
			[
				'class' => AuthFilter::className(),
			],
			[
				'class' => LotAccessFilter::className(),
				'only' => ['create-own-doc', 'remove-checklist', 'create']
			],
			[
				'class' => CheckListAccessFilter::className(),
				'only' => ['upload-checklist-file', 'comment', 'remove-file']
			],
			[
				'class' => SpecialistFilter::className(),
				'only' => ['remove-checklist', 'remove-file', 'upload-checklist-file', 'comment', 'create']
			]
		];
	}

	public function actionCreate($lotid, $tenderid)
	{
		$doc = new Document();

		if ($doc->create($lotid)) {

			if ($doc->depid == -1) {

				$users = [
					0 => ['userid' => Yii::$app->LotComponent->getCurManager($lotid)],
				];

				Yii::$app->MailComponent->sendCheckListMail($lotid, $users);
			} else {

				$users = PrivUser::getByDepid($doc->depid);
				Yii::$app->MailComponent->sendCheckListMail($lotid, $users);
			}
			
			return $this->asJson($doc->response);
		}
	}
	public function actionGetDocs()
	{
		$doc = new Document();

		return $this->asJson($doc->getDocuments());
	}

	public function actionCreateOwnDoc($lotid)
	{
		$doc = new Document();
		if ($doc->createOwnDoc($lotid)) {
			return $this->asJson($doc->response);
		}
	}

	public function actionUploadChecklistFile($lotid,$tenderid)
	{
		$doc = new Document();

		if ($doc->ajaxUploadFile()) {
			return $this->asJson($doc->response);
		}
	}

	public function actionDownloadFile($id)
	{
		return Document::download($id);
	}

	public function actionRemoveFile($id, $lotid, $tenderid)
	{
		return Document::removeFile($id);
	}

	public function actionComment($tenderid)
	{
		$doc = new Document();

		if ($doc->comment()) {
			return $this->asJson($doc->response);
		}
	}

	public function actionRemoveChecklist($id, $tenderid)
	{
		return Document::removeChecklist($id);
	}

}