<?php 

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\filters\AuthFilter;
use app\components\filters\SpecialistFilter;
use app\components\filters\TenderAccessFilter;
use yii\data\ArrayDataProvider;

use app\models\Auth;
use app\models\Tender;
use app\models\Lot;
use app\models\Csinfo;
use app\models\TenderSource;
use app\models\TenderSector;
use app\models\TenderFile;
use app\models\DepList;
use app\models\User;
use app\models\TenderOrig;
use app\models\TenderProtocol;
use app\models\ProtocolFile;



/**
 * 
 */
class TenderController extends Controller
{

	public function behaviors()
	{
		return [
			// [
			//  'class' => AuthFilter::className(),
			// ],
            [
             'class' => TenderAccessFilter::className(),
             'only' => ['create', 'edit', 'update']
            ],
            [
                'class' => SpecialistFilter::className(),
                'only' => ['edit', 'update']
            ]
		];
	}

	public function actionIndex()
    {   
        $userlist = Yii::$app->CacheComponent->getDrtpSpecialists();

        $isParticipate = [
            ['value' => 0, 'title' => 'Не Выбрано'], 
            ['value' => 1, 'title' => 'Отказ'], 
            ['value' => 2, 'title' => 'Участвуем']
        ];

    	$provider = new ArrayDataProvider([
			'allModels' => Tender::getAll(),
			'sort' => [
				'attributes' => [
					'tenderid',
					'senddate',
					'source_title',
					't_ccsid',
					't_csbin',
					't_csname',
                    'accept_solution',
					'purchase_title',
                    'tender_userid',
                    'tender_username',
					'purchase_number',
					'purchase_name',
                    'actid',
                    'aid_signed_agreements',
                    'updated_at',
                    'lot_counts_tv', 
                    'lot_participate_count_true',
				],
                'defaultOrder' => [ 
                    //'lot_participate_count_true' => SORT_DESC,
                    //'lot_counts_tv'=>SORT_DESC,
                    'updated_at'=> SORT_DESC, 
                ]
			],
			'pagination' => [
				'pageSize' => 25,
			],
		]);

		$tenders = $provider->getModels();

		return $this->render('/tender/index', compact('provider', 'tenders', 'userlist', 'isParticipate'));

    }

    public function actionFiled()
    {
        $userlist = Yii::$app->CacheComponent->getDrtpSpecialists();

        $provider = new ArrayDataProvider([
            'allModels' => Tender::getFiled(),
            'sort' => [
                'attributes' => [
                    'tenderid',
                    'senddate',
                    'source_title',
                    't_ccsid',
                    't_csbin',
                    't_csname',
                    'accept_solution',
                    'purchase_title',
                    'tender_userid',
                    'tender_username',
                    'purchase_number',
                    'purchase_name',
                    'actid',
                    'aid_signed_agreements',
                    'updated_at',
                    'lot_counts_tv', 
                    'lot_participate_count',
                ],
                'defaultOrder' => [ 
                    'lot_participate_count' => SORT_DESC,
                    'lot_counts_tv'=>SORT_DESC,
                    'updated_at'=>SORT_DESC, 
                ]
            ],
            'pagination' => [
                'pageSize' => 25,
            ],
        ]);

        $tenders = $provider->getModels();

        return $this->render('/tender/filed', compact('provider', 'tenders', 'userlist'));
    }

    public function actionWorked()
    {
        $userlist = Yii::$app->CacheComponent->getDrtpSpecialists();

        $provider = new ArrayDataProvider([
            'allModels' => Tender::getWorked(),
            'sort' => [
                'attributes' => [
                    'tenderid',
                    'senddate',
                    'source_title',
                    't_ccsid',
                    't_csbin',
                    't_csname',
                    'accept_solution',
                    'purchase_title',
                    'tender_userid',
                    'tender_username',
                    'purchase_number',
                    'purchase_name',
                    'actid',
                    'aid_signed_agreements',
                    'updated_at',
                    'lot_counts_tv', 
                    'lot_participate_count',
                ],
                'defaultOrder' => [ 
                    'lot_participate_count' => SORT_DESC,
                    'lot_counts_tv'=>SORT_DESC,
                    'updated_at'=>SORT_DESC, 
                ]
            ],
            'pagination' => [
                'pageSize' => 25,
            ],
        ]);

        $tenders = $provider->getModels();

        return $this->render('/tender/worked', compact('provider', 'tenders', 'userlist'));
    }

    public function actionUnWorked()
    {
        $userlist = Yii::$app->CacheComponent->getDrtpSpecialists();

        $provider = new ArrayDataProvider([
            'allModels' => Tender::getUnWorked(),
            'sort' => [
                'attributes' => [
                    'tenderid',
                    'senddate',
                    'source_title',
                    't_ccsid',
                    't_csbin',
                    't_csname',
                    'accept_solution',
                    'purchase_title',
                    'tender_userid',
                    'tender_username',
                    'purchase_number',
                    'purchase_name',
                    'actid',
                    'aid_signed_agreements',
                    'updated_at',
                    'lot_counts_tv', 
                    'lot_participate_count',
                ],
                'defaultOrder' => [ 
                    'lot_participate_count' => SORT_DESC,
                    'lot_counts_tv'=>SORT_DESC,
                    'updated_at'=>SORT_DESC, 
                ]
            ],
            'pagination' => [
                'pageSize' => 25,
            ],
        ]);

        $tenders = $provider->getModels();

        return $this->render('/tender/unworked', compact('provider', 'tenders', 'userlist'));
    }

    public function actionCreate()
    {
        $purchases = Yii::$app->CacheComponent->getPurchases();
        $sources = Yii::$app->CacheComponent->getSources();
        $sectors = Yii::$app->CacheComponent->getSectors();
        $origlist = Yii::$app->CacheComponent->getOrigList();
        $ognlist = Yii::$app->CacheComponent->getOgnList();

    	return $this->render('/tender/new', compact('purchases', 'sources', 'sectors', 'origlist', 'ognlist'));
    }

    public function actionEdit($tenderid)
    {
        $purchases = Yii::$app->CacheComponent->getPurchases();
        $sources = Yii::$app->CacheComponent->getSources();
        $sectors = Yii::$app->CacheComponent->getSectors();
        $origlist = Yii::$app->CacheComponent->getOrigList();
        $ognlist = Yii::$app->CacheComponent->getOgnList();


        $tender = Tender::info($tenderid);
    	return $this->render('/tender/edit', compact('purchases', 'sources', 'sectors', 'tender', 'origlist', 'ognlist'));
    }

    public function actionUpdate($id)
    {
    	$tender_obj = new Tender();
    	if ($tender_obj->edit($id)) {

    		Yii::$app->session->setFlash('success', 'Тендер успешно изменен.');
    		return $this->redirect(['tender/view', 'id'=>$id]);

    	} else {

            $purchases = Yii::$app->CacheComponent->getPurchases();
            $sources = Yii::$app->CacheComponent->getSources();
            $sectors = Yii::$app->CacheComponent->getSectors();
            $origlist = Yii::$app->CacheComponent->getOrigList();
            $ognlist = Yii::$app->CacheComponent->getOgnList();

            $tender = $tender_obj->info($id);

    		Yii::$app->session->setFlash('error', $tender_obj->error);
    		return $this->render('/tender/edit', compact('purchases', 'sources', 'sectors', 'tender', 'origlist', 'ognlist'));
    	}
    }

    public function actionTest()
    {
        //$lots = Yii::$app->UserComponent->getTenderLotManagers(1745);

        $result = Yii::$app->MailComponent->notifyLotManagers(1745);

        return json_encode($result);
    }

    public function actionView($id)
    {
    	$deplist = Yii::$app->CacheComponent->getDepList();
        $origlist = Yii::$app->CacheComponent->getOrigList();
        $ognlist = Yii::$app->CacheComponent->getOgnList();
        $protocols = Yii::$app->CacheComponent->getProtocols();

        $managers = Yii::$app->CacheComponent->getOnlyManagers();
        $tender = Tender::info($id);

        $tender_protocols = TenderProtocol::getAll($id);
        $tender_protocol_files = ProtocolFile::getAll($id);
    	$provider = new ArrayDataProvider([
			'allModels' => Lot::infoByTenderId($id),
			'pagination' => [
				'pageSize' => 25,
			],
        ]);
        
        $procurementList = [ '0' => 'N/A', '1' => 'ТО', '2' => 'ТК', '3' => 'СИ'];

		$lots = $provider->getModels();
    	return $this->render('/tender/info', compact('protocols', 'tender_protocols', 'tender_protocol_files', 'tender', 'lots', 'provider', 'deplist', 'userlist', 'managers', 'origlist', 'ognlist', 'procurementList'));
    }

    public function actionStore()
    {
    	$tender = new Tender();
        $tender_file = new TenderFile();

    	if ($tender->store()) {

            $tender_file->uploadFile($tender->id);

            Yii::$app->session->setFlash('success', 'Тендер успешно создан');
    		return $this->redirect(['tender/view', 'id'=>$tender->id]);

    	} else {

    		$purchases = Yii::$app->CacheComponent->getPurchases();
            $sources = Yii::$app->CacheComponent->getSources();
            $sectors = Yii::$app->CacheComponent->getSectors();
            $origlist = Yii::$app->CacheComponent->getOrigList();
            $ognlist = Yii::$app->CacheComponent->getOgnList();
			
            Yii::$app->session->setFlash('error', $tender->error);

    		return $this->render('/tender/new', compact('purchases', 'sources', 'sectors', 'origlist', 'ognlist'));
    	}
    }
    
    public function actionDownloadFile($id)
    {
       return TenderFile::download($id);
    }

    public function actionCurrentTime()
    {
        return var_dump(Yii::$app->params['currentTime']);
    }
}