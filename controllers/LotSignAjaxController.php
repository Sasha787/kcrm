<?php 

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\filters\AuthFilter;
use app\components\LotAccessFilter;
use app\components\filters\RiskIsNotActive;
use app\components\filters\SpecialistFilter;
use yii\web\Response;

use app\models\LotSign;

class LotSignAjaxController extends Controller
{

	public function behaviors()
	{
		return [
			[
				'class' => AuthFilter::className(),
			],
			[
				'class' => RiskIsNotActive::className(),
				'only' => ['remove-user', 'add-sign-user'], 
			],
			[
				'class' => SpecialistFilter::className(),
				'only' => ['remove-user', 'add-sign-user'], 
			],
		];
	}

	public function actionAddSignUser($risk_id, $tenderid)
	{
		$sign = new LotSign();
		if ($sign->addUser()) {
			return $this->asJson($sign->response);
		}
	}

	public function actionRemoveUser($sid, $risk_id, $tenderid)
	{	
		$sign = new LotSign();

		if ($sign->rmUser($sid)) {
			return $this->asJson($sign->response);
		} else {
			return false;
		}
	}
}