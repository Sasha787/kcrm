<?php 

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\filters\AuthFilter;
use app\components\filters\LotAccessFilter;
use app\components\filters\CreateDealFilter;
use app\components\filters\TenderAccessFilter;
use app\components\filters\RiskIsActive;
use app\components\filters\SpecialistFilter;


use app\models\Lot;
use app\models\LotStatus;
use app\models\LotIndication;
use app\models\LotPercent;
use app\models\LotConclusion;
use app\models\User;
use app\models\LotSign;
use app\models\LotFile;
use app\models\DepList;
use app\models\Document;
use app\models\LotRisk;
use app\models\AppProvision;
use app\models\AgreementProvision;
use app\models\LotProtocol;
use app\models\ManagerHistory;
use app\models\ParticipateHistory;
use app\models\RiskFile;
use app\models\LotChat;



/**
 * 
 */
class LotController extends Controller
{

	public function behaviors()
	{
		return [
			[
				'class' => AuthFilter::className(),
			],
			[
				'class' => LotAccessFilter::className(),
				'only' => ['view']
 			],
			[
				'class' => TenderAccessFilter::className(),
				'only' => ['edit', 'create']
			],
			[
				'class' => CreateDealFilter::className(),
				'only' => ['create-deal', 'create-new-deal']
			],
			[
				'class' => SpecialistFilter::className(),
				'only' => ['create', 'store', 'update']
			],
		];
	}

	public function actionView($id)
	{	
		$userlist = Yii::$app->CacheComponent->getUsers();
		$deplist = Yii::$app->CacheComponent->getDepList();
		$protocols = Yii::$app->CacheComponent->getProtocols();

		$lot_info = Lot::infoByLotId($id);

		//return json_encode($lot_info);

		$lot_info['depids'] = explode(',', $lot_info['depids']);

		$managers = User::getByDepid();
		
		$documents = Document::all();
		$need_docs = Document::getById($id);

		$lot_risk = LotRisk::get($id);
		$signed_users = LotSign::all($id);
		$riskFiles = RiskFile::get($id);

		$risks = Yii::$app->RiskComponent->get($lot_risk, $signed_users, $riskFiles);

		$application_provision = AppProvision::getById($id);
		$agreement_provision = AgreementProvision::getById($id);
		
		$isNeedMakeSolution = ParticipateHistory::isNeedMakeSolution($id);
		$allSolutions = ParticipateHistory::getAllSolutions($id);
		$isNeedApproveSolution = ParticipateHistory::isNeedApproveSolution($id);
		
		$is_has_tv = Lot::isHasTv($id);
		$doc_files = Document::getDocFiles($id);
		$lot_participants = Lot::getParticipants($id);
		
		$lot_protocols = LotProtocol::getLotProtocols($id);
		$lot_protocol_files = LotProtocol::getFiles($id);
		$lot_deals = Lot::getDeals($id);
		$manager_history = ManagerHistory::get($id);

		$reasonsList = [ '0' => 'N/A', '1' => 'По квалификационной части', '2' => 'По технической части', '3' => 'По ценовой', '4' => 'Другое'];

		return $this->render('/lot/info', compact('managers', 'lot_deals', 'lot_info', 'userlist', 
			'is_has_tv', 'deplist', 'documents', 'need_docs', 'doc_files', 'lot_participants', 
			'application_provision', 'agreement_provision', 'protocols', 
			'lot_protocols', 'lot_protocol_files', 'manager_history', 'isNeedApproveSolution',
			'allSolutions', 'isNeedMakeSolution', 'risks', 'reasonsList'));
	}

	public function actionMessages($lotId)
	{	
		$lot_messages = LotChat::getLotMessages($lotId);

		return $this->asJson(['messages' => $lot_messages]);
	}

	public function actionEdit($id, $tenderid)
	{
		$lot_info = Lot::infoByLotId($id);
		$lot_statuses = Yii::$app->CacheComponent->getLotStatuses();
		$lot_indications = Yii::$app->CacheComponent->getLotIndications();
		$percents = Yii::$app->CacheComponent->getLotPercents();
		$conclusions = Yii::$app->CacheComponent->getLotConclusions();

		return $this->render('/lot/edit', compact('lot_info', 'lot_statuses', 'lot_indications', 'percents', 'conclusions'));
	}

	function get_type($elem)
	{
		return $elem['userid'];
	}

	public function actionNotify()
	{
		$lots = Lot::getLotsForNotifications();
		$final = [];
		$managers = [];

		//return json_encode($lots);

		foreach($lots as $index => $lot) {
			if(($lot['ap_is_need'] == 1 && $lot['ap_is_exist'] == 0) || ($lot['lap_is_need'] == 1 && $lot['lap_is_exist'] == 0)) {
				$final[] = $lot;
				//$managers[] = $lot['m_managerid'];
			}
		}

		if(count($final)) {

			$departments = []; 

			foreach($final as $k => $lot) {
				$query = (new \yii\db\Query())
				->select('u.userid, u.login, u.fullname, u.position, u.depid, u.email')
				->from('contracts.users u')
				->where(['blocked'=> 0])
				->andWhere(['!=', 'userid', 0])
				->andWhere(['=', 'userid', $lot['m_managerid']])
				->orderBy(['u.fullname' => SORT_ASC])
				->one();
	
				$final[$k]['depId'] = $query['depid'];
				$departments[] = (int) trim($query['depid']);
				$managers[] = [
					'userid' => $lot['m_managerid'],
					'depid' => $query['depid']
				];
			}
	
			$managers = array_unique($managers, SORT_REGULAR);
			$departments = array_unique($departments);
	
			// Form user list
	
			$users = (new \yii\db\Query())
			->select(['privusers.userid', 'privusers.depid'])
			->from('privusers')
			->where(['privusers.depid' => $departments])
			->andWhere(['privusers.tender' => [3] ])
			->all();

			// Арман Маралов 240
			// Эркин Орозакунов 1460

			$users = array_merge($users, $managers);

			//->andWhere(['=', 'privusers.tender', 1])
			//->all();
	
			/* if(count($departments)) {
	
				$users = $users->where(['privusers.depid' => $departments])->all();
				$users = array_merge($users, $managers);
	
			} else {
	
				$users = $users->all();
				$users = array_merge($users, $managers);
			} */
	
			//return json_encode($users);
			//return json_encode($final);

			foreach($final as $key => $lot) {
				foreach($users as $j => $user) {
					if($user['depid'] == $lot['depId']) {
						//return json_encode(['userid' => $user['userid'], 'lotid' => $lot['lotid']]);
						Yii::$app->MailComponent->sendToUser($user['userid'], $lot['lotid']);
					}
				}
			}

			Yii::$app->MailComponent->sendToUser(1810, $lot['lotid']);
	
			//return json_encode(['lots' => $final, 'users' => $users]);
			return json_encode(['success' => true]);
		}
	}

	public function actionUpdate($id, $tenderid)
	{
		$lot = new Lot();

		//return $lot->edit($id);

		if ($lot->edit($id)) {
			Yii::$app->session->setFlash('success', "Лот изменен успешно.");
		} else {
			Yii::$app->session->setFlash('error', "Лот не изменен.");
		}

		return $this->redirect(['lot/view', 'id'=>$id]);
	}

	public function actionCreate($tenderid)
	{
		$lot_statuses = Yii::$app->CacheComponent->getLotStatuses();
		$lot_indications = Yii::$app->CacheComponent->getLotIndications();
		$percents = Yii::$app->CacheComponent->getLotPercents();
		$conclusions = Yii::$app->CacheComponent->getLotConclusions();

		return $this->render('/lot/create', compact('lot_info', 'lot_statuses', 'lot_indications', 'percents', 'conclusions'));
	}

	public function actionStore($tenderid)
	{
		$lot = new Lot();
		$lot_file = new LotFile();
		
		if ($lot->store()) {

			$lot_file->uploadFile($lot->lotid);
			Yii::$app->session->setFlash('success', "Лот успешно создан.");
		} else {
			Yii::$app->session->setFlash('error', "Не удалось создать лот");
		}

		return $this->redirect(['lot/view', 'id'=> $lot->lotid]);
			
	}

	public function actionCreateDeal($id, $dealid)
	{
		$lot = new Lot();

		if ($lot->assignLotToDeal($id, $dealid)) {

				$this->getLinkToDeal($lot->csid, $id);
					
				Yii::$app->session->setFlash('success', "Лот был привязан к сделки из kCRM");
		} else {
			Yii::$app->session->setFlash('error', "Упс :(. Ошибка!");
		}

		return $this->redirect(['lot/view', 'id'=>$id]);
	}

	
	public function actionCreateNewDeal($id)
	{
		error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);
		$lot = new Lot();

		if ($lot->createDeal($id)) {

			$this->getLinkToDeal($lot->csid, $id);
		
				Yii::$app->session->setFlash('success', "Сделка была успешно создана в kCRM");
		} else {
				Yii::$app->session->setFlash('error', "Сделка не была создана в KCRM");
		}
		
		return $this->redirect(['lot/view', 'id'=>$id]);
	}

	public function getLinkToDeal($csid, $lotid)
	{
		$soap='xreq=
            <soapenv:Envelope">
            <soapenv:Header>
            <heads:credentials">
            <heads:key>69av92us40jn</heads:key>
            </heads:credentials>
            </soapenv:Header>
            <soapenv:Body>
            <get_ecsid_by_csid>'.$csid.'</get_ecsid_by_csid>
            </soapenv:Body>
            </soapenv:Envelope>';

       	$headers = array(
            'POST /apihub/index.php',
            'Host: kcrm.kaztranscom.kz',
            'Content-Length: '.strlen($soap));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://kcrm.kaztranscom.kz/apihub/index.php');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, "Opera/9.63 (Windows NT 5.1; U; ru) Presto/2.1.1");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
        curl_setopt($ch, CURLOPT_POST, true );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $soap);
        $result = curl_exec($ch);
        curl_close($ch);
      
        $ecsid = $result;
       
      	if (Lot::linkToDeal($ecsid, $lotid)) {
      		Yii::$app->session->setFlash('success', "Вы успешно создали сделку в KCRM. Ссылку была сгенерирована и будет доступна в информации о лоте.");
      		return true;
		}

		return false;     	
	}

	public function actionSign($risk_id, $lotid)
	{
		if (LotSign::sign($risk_id, $lotid)) {
			Yii::$app->session->setFlash('success', "Вы успешно подписали лот, поздравляем.");
		} else {
			Yii::$app->session->setFlash('error', "К сожалению, не удалось подписать лот. Попробуйте снова.");
		}

		return $this->redirect(['lot/view', 'id'=>$lotid]);
	}

	public function actionDownloadFile($id)
	{
		return LotFile::download($id);
	}

	public function actionAddRisk($lotid)
	{
		if (LotRisk::create($lotid)) {
			Yii::$app->session->setFlash('success', "Новый риск был добавлен");
		} else {
			Yii::$app->session->setFlash('error', "К сожалению, не удалось добавить новый риск. Попробуйте снова.");
		}

		return $this->redirect(['lot/view', 'id'=>$lotid]);
	}

}