<?php 
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\filters\AuthFilter;
use app\components\filters\TenderAccessFilter;
use yii\data\ArrayDataProvider;

use app\models\Tender;
use app\models\Lot;
use app\models\TenderSector;
use app\models\TenderSource;
use app\models\LotParticipant;
use app\models\TenderCompetitor;

class ReportController extends Controller
{
		public function behaviors()
		{
			return [
				[
					'class' => AuthFilter::className(),
				],
				[
					'class' => TenderAccessFilter::className(),
				], 
			];
		}

		public function actionIndex()
		{
			return $this->render('/report/index');
		}

		public function actionTender()
		{

			$source = Yii::$app->CacheComponent->getSources();
      $sectors = Yii::$app->CacheComponent->getSectors();

			$provider = new ArrayDataProvider([
				'allModels' => [],
				'pagination' => false,
			]);

			$tenders = $provider->getModels();
			$contragents = Tender::getContragents();
	
			return $this->render('/report/tender', compact('sectors', 'provider', 'tenders', 'source', 'contragents'));
		}

		public function actionReportTender()
		{
			$source = Yii::$app->CacheComponent->getSources();
      $sectors = Yii::$app->CacheComponent->getSectors();

			$provider = new ArrayDataProvider([
				'allModels' => Tender::reportAll(),
				'pagination' => false,
				'sort' => [
					'attributes' => [
						'tenderid',
						'senddate',
						'source_title',
						't_ccsid',
						't_csbin',
						't_csname',
						'accept_solution',
						'purchase_title',
						'tender_userid',
						'tender_username',
						'purchase_number',
						'purchase_name',
						'actid',
						'aid_signed_agreements',
						'updated_at',
						'lot_counts_tv', 
						'lot_participate_count_true',
					],
				],
			]);

			$tenders = $provider->getModels();
			$contragents = Tender::getContragents();
	
			return $this->render('/report/tender', compact('sectors', 'provider', 'tenders', 'source', 'contragents'));
		}

		public function actionLot()
		{
			$conclusions = Yii::$app->CacheComponent->getLotConclusions();
			$deplist = Yii::$app->CacheComponent->getDepList();
			$contragents = Tender::getContragents();


			$deplist[0]['dname'] = 'Все';

			$provider = new ArrayDataProvider([
				'allModels' => [],
				'pagination' => false,
				'sort' => [
					'attributes' => [
						'lotid',
						't_csbin',
						't_csname',
						'purchase_number',
						'lot_number',
						'budget',
						'date_purchase',
						'accept_solution',
						'lot_conclusion',
						'depnames',
					],
				],
			]);

			$lots = $provider->getModels();
	
			return $this->render('/report/lot', compact('deplist', 'provider', 'lots', 'conclusions', 'contragents'));
		}

		public function actionReportLot()
		{
			$conclusions = Yii::$app->CacheComponent->getLotConclusions();
			$deplist = Yii::$app->CacheComponent->getDepList();
			$contragents = Tender::getContragents();

			$deplist[0]['dname'] = 'Все';

			$provider = new ArrayDataProvider([
				'allModels' => Lot::reportAll(),
				'pagination' => false,
				'sort' => [
					'attributes' => [
						'lotid',
						't_csbin',
						't_csname',
						'purchase_number',
						'lot_number',
						'budget',
						'date_purchase',
						'lot_conclusion',
						'depnames',
					],
				],
			]);

			$lots = $provider->getModels();
	
			return $this->render('/report/lot', compact('deplist', 'provider', 'lots', 'conclusions', 'contragents'));
		}

		public function actionCompetitors()
		{
			$competitors = TenderCompetitor::getAll();
			$contragents = Tender::getContragents();

			$provider = new ArrayDataProvider([
				'allModels' => [],
				'pagination' => false,
				'sort' => [
					'attributes' => [
						'lotid',
						't_csbin',
						't_csname',
						'purchase_number',
						'lot_number',
						'budget',
						'date_purchase',
						'accept_solution',
						'lot_conclusion',
						'depnames',
					],
				],
			]);

			$lots = $provider->getModels();
	
			return $this->render('/report/competitors', compact('contragents', 'provider', 'lots', 'competitors'));
		}

		public function actionReportCompetitors()
		{
			
			$competitors = TenderCompetitor::getAll();
			$contragents = Tender::getContragents();
			
			$provider = new ArrayDataProvider([
				'allModels' => TenderCompetitor::reportAll(),
				'pagination' => false,
				'sort' => [
					'attributes' => [
						'lotid',
						't_csbin',
						't_csname',
						'purchase_number',
						'lot_number',
						'budget',
						'date_purchase',
						'accept_solution',
						'lot_conclusion',
						'depnames',
					],
				],
			]);

			$lots = $provider->getModels();
	
			return $this->render('/report/competitors', compact('contragents', 'provider', 'lots', 'competitors'));
		}

		public function actionFull()
		{
			$conclusions = Yii::$app->CacheComponent->getLotConclusions();
			$deplist = Yii::$app->CacheComponent->getDepList();
			$contragents = Tender::getContragents();
			$competitors = TenderCompetitor::getAll();

			//$unixPeriodFrom = strtotime('05-05-2019');
			//return json_encode(['message' => $unixPeriodFrom]);

			$deplist[0]['dname'] = 'Все';

			$isParticipate = [
				['value' => 0, 'title' => 'Не Выбрано'], 
				['value' => 1, 'title' => 'Отказ'], 
				['value' => 2, 'title' => 'Участвуем']
			];

			//$reasonsList = [ '0' => 'По квалификационной части', '1' => 'По технической части', '2' => 'По ценовой'];
			$reasonsList = [ '0' => 'N/A', '1' => 'По квалификационной части', '2' => 'По технической части', '3' => 'По ценовой'];
			
			$reasonsOptions = [ 
				[ 'value' => 0, 'title' => 'Не Выбрано'], 
				[ 'value' => 1, 'title' => 'По квалификационной части'], 
				[ 'value' => 2, 'title' => 'По технической части'], 
				[ 'value' => 3, 'title' => 'По ценовой']
			];

			$provider = new ArrayDataProvider([
				'allModels' => Lot::reportFull(),
				'pagination' => false,
				'sort' => [
					'attributes' => [
						'lotid',
						't_csbin',
						't_csname',
						'purchase_number',
						'lot_number',
						'budget',
						'date_purchase',
						'lot_conclusion',
						'depnames',
					],
				],
			]);

			$lots = $provider->getModels();
			
			//$lots['reasonsList'] = $reasonsList;
			//return json_encode($lots);
	
			return $this->render('/report/full', compact('deplist', 'provider', 'lots', 'conclusions', 'contragents', 'reasonsList', 'reasonsOptions', 'isParticipate', 'competitors'));
		}

		public function actionExport() {
			
			$file = \Yii::createObject([
				'class' => 'codemix\excelexport\ExcelFile',
				'sheets' => [
					'Users' => [
						'class' => 'codemix\excelexport\ActiveExcelSheet',
						'query' => Lot::find(),
					]
				]
			]);
			$file->send('user.xlsx');
		}
}