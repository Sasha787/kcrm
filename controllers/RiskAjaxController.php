<?php 

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\filters\AuthFilter;
use app\components\filters\LotAccessFilter;
use app\components\filters\TenderAccessFilter;
use app\components\filters\RiskIsNotActive;
use app\components\filters\RiskIsActive;
use app\components\filters\SpecialistFilter;

use app\models\RiskFile;
use app\models\LotRisk;
use app\models\LotSign;
use app\models\Lot;


/**
 * 
 */
class RiskAjaxController extends Controller
{
	public function behaviors()
	{
		return [
			[
				'class' => AuthFilter::className(),
			],
			[
				'class' => TenderAccessFilter::className(),
				'only' => ['upload-risk-file', 'remove-file']
			],
			[
				'class' => RiskIsNotActive::className(),
				'only' => ['remove-file', 'upload-risk-file', 'create-risk'],
			],
			[
				'class' => SpecialistFilter::className(),
				'only' => ['remove-file', 'upload-risk-file', 'create-risk', 'risk-is-active'],
			]
		];
	}

	public function actionUploadRiskFile($risk_id)
	{
		$riskFile = new RiskFile();

		if ($riskFile->upload()) {
			return $this->asJson($riskFile->response);
		}

		return $this->asJson(['status' => 404]);
	}
	// id файла и id лота
	public function actionRemoveFile($fileid, $risk_id)
	{
		return RiskFile::remove($fileid);
	}

	public function actionDownloadFile($id)
	{
		return RiskFile::download($id);
	}

	public function actionRiskIsActive()
	{
		$risk = new LotRisk();

		if ($risk->isActive()) {

			if ($risk->lr_isActive == 1) {

					$users = LotSign::allOfRisk($risk->lr_lotid, $risk->lr_id);
					$infoForMail = Lot::getInfoForMail($risk->lr_lotid);

					Yii::$app->MailComponent->sentRiskSigners($users, $infoForMail);
			}

			return $this->asJson($risk->response);
		}

		return $this->asJson(['status' => 303, 'res' => 'Действие не удалось!']);
	}


	public function actionTest()
	{	
		$data = 'another test';
		return $this->renderPartial('/lot/partials/_risk',  compact('data'));
	}


	public function actionCreateRisk($risk_id)
	{
		$risk = new LotRisk();
	
		if ($risk->add($risk_id)) {
			return $this->asJson($risk->response);
		} else {
			return false;
		}
	}

}