<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\filters\AuthFilter;
use app\components\filters\LotAccessFilter;
use app\components\filters\CreateDealFilter;
use app\components\filters\TenderAccessFilter;
use app\components\filters\DelegateLotFilter;

use app\models\Lot;
use app\models\ParticipateHistory;
use app\models\LotDep;


class ParticipateAjaxController extends Controller
{
	public function behaviors()
	{
		return [
			[
				'class' => AuthFilter::className(),
			],
		];
	}

	public function actionIsParticipate()
	{
			$participate = new ParticipateHistory();

			if ($participate->isParticipate()) {

				$infoForMail = Lot::getInfoForMail($participate->lotid);

				//отправить на почту привилегированным пользователям департамента уведомление для подтверждения отказа от участия в тендере
				if ($participate->isParticipate == 0) {

					$lotDepId = LotDep::getLotDepId($participate->lotid);
					Yii::$app->MailComponent->sendDepApproveSolution($lotDepId['depid'], $infoForMail['lotid'], $infoForMail['lot_number'], $infoForMail['lot_name']);
				
				}

				//отправить на почту специалистам ДРТП, о том что участвуем в тендере
				if ($participate->isParticipate == 1) {

					Yii::$app->MailComponent->sendNotifyToSpecialist($infoForMail['lotid'], $infoForMail['purchase_number'], $infoForMail['purchase_name'], $infoForMail['lot_name'], $infoForMail['t_csname']);
				
				}

				$response = [
					'status' => 200, 
					'res' => [
						'isParticipate' => $participate->isParticipate,
						'reason' => $participate->reason,
						'manager' => Yii::$app->UserComponent->getById($participate->managerId)['fullname'],
						'date' => date('d-m-Y H:i:s', $participate->date)
					]
				];
				return $this->asJson($response);
			}

			return $this->asJson(['status' => 404]);
	}

	public function actionAllowParticipate()
	{
		$participate = new ParticipateHistory();
		if ($participate->allowParticipate()) {
			return true;
		} 

		return false;
	}

	public function actionCancelSolution($id, $idButton)
	{	
		return $this->renderPartial('/lot/partials/_make_solution', compact('id', 'idButton'));
	}
}