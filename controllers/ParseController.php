<?php 
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\filters\AuthFilter;
use yii\data\ArrayDataProvider;


use app\models\LotParse;

class ParseController extends Controller
{
	// public function behaviors()
	// {
	// 	return [
	// 		[
	// 			'class' => AuthFilter::className(),
	// 		], 
	// 	];
	// }

	public function actionCompleteLot()
	{
		$lotParse = new LotParse();
		if ($lotParse->complete()) {
			return $this->asJson(['username' => Yii::$app->UserComponent->getById($lotParse->userid)['fullname']]);
		} 
		
		return false;
	}

	public function actionMitwork()
	{	
		$data = Yii::$app->MitworkParseComponent->parse();

		LotParse::addFromMitwork($data);

		return true;
	}

	public function actionIndex()
	{
			$provider = new ArrayDataProvider([
				'allModels' => LotParse::getAll(),
				'sort' => [
					'attributes' => [
						'id',
						'parsed_from',
						'lot_title',
						'add_info',
						'total_amount',
						'lot_status',
						'lot_link',
						'cs_bin',
						'cs_title',
						'cs_link',
						'is_completed',
						'id_from_site'
					],
					'defaultOrder' => ['id_from_site'=>SORT_DESC]
				],
				'pagination' => [
					'pageSize' => 25,
				],
			]);
        // return var_dump(LotParse::getAll());
			$lots = $provider->getModels();
			return $this->render('/parse/index', compact('provider', 'lots'));
	}

	public function actionTest()
	{	
		$str = " s s ";
		echo strlen($str);
		echo preg_replace('/\s+/','',$str);
		echo strlen($str);
	}
}