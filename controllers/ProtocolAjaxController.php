<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\filters\AuthFilter;
use app\components\filters\LotAccessFilter;
use app\components\filters\TenderAccessFilter;
use app\components\filters\SpecialistFilter;

use app\models\TenderProtocol;
use app\models\ProtocolFile;


class ProtocolAjaxController extends Controller
{
	public function behaviors()
	{
		return [
			[
				'class' => AuthFilter::className(),
			],
			[
				'class' => TenderAccessFilter::className(),
				'only' => ['create', 'upload-protocol-file', 'remove-file', 'remove-protocol']
			],
			[
				'class' => SpecialistFilter::className(),
				'only' => ['create', 'upload-protocol-file', 'remove-file', 'remove-protocol']
			]  
		];
	}

	public function actionCreate($tenderid)
	{
		$protocol = new TenderProtocol();

		if ($protocol->create($tenderid)) {
			return $this->asJson($protocol->response);
		} else {
			return false;
		}
	}

	public function actionUploadProtocolFile($tenderid)
	{
		$protocolFile = new ProtocolFile();

		if ($protocolFile->uploadAjaxFile()) {
			return $this->asJson($protocolFile->response);
		}

		return false;
	}

	public function actionDownloadFile($id)
	{
		return ProtocolFile::download($id);
	}

	public function actionRemoveFile($id, $tenderid)
	{
		return ProtocolFile::removeFile($id);
	}

	public function actionRemoveProtocol($id, $tenderid)
	{
		return TenderProtocol::removeProtocol($id);
	}
	
}