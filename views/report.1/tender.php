<?php
use yii\helpers\Url;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

$this->title = "Отчет";
$tender_permission = Yii::$app->auth->tenderPermission()['tender'];
if ($tender_permission != 1 || $tender_permission != 2) {
    $disabled = true;
} else {
    $disabled = false;
}

date_default_timezone_set('Asia/Almaty');
?>
<div class="row">
   <div class="col-md-12" style="background-color: white"> <br>
            <div class="col-md-5 col-md-offset-3">
            <div class="col-md-4">
                <span>Сектор организации</span>
                <div class="form-group">
                    <?= 
                    Select2::widget([
                        'name' => 'sectors',
                        'value' => 0,
                        'data' =>  ArrayHelper::map($sectors, 'sectorid', 'sector_title'),
                        'options' => ['multiple'=>false, 'class' => 'inp01', 'placeholder' => 'Выберите сектор', 'id' => 'sector'],
                        'pluginOptions' => [
                            'width' => '200px',
                        ],
                    ]);
                    ?>
                </div>
            </div>
            
            <div class="col-md-4">
                <span>Заказчик</span>
                <div class="form-group">
                    <?= 
                    Select2::widget([
                        'name' => 'contragent',
                        'value' => 0,
                        'data' =>  ArrayHelper::merge(['0'=>'Не выбран'], ArrayHelper::map($contragents, 't_csbin', 't_csname')),
                        'options' => ['multiple'=>false, 'class' => 'inp01', 'placeholder' => 'Наименование заказчика', 'id' => 'contragent'],
                        'pluginOptions' => [
                            'width' => '200px',
                        ],
                    ]);
                    ?>
                </div>
            </div>

            <div class="col-md-4">
                <span>Решение об участии</span>
                <div class="form-group">
                    <?= 
                    Select2::widget([
                        'name' => 'solution',
                        'value' => 0,
                        'data' =>  ['0'=> 'Все', '1' => 'Участвуем', '2' => 'Не участвуем', '3' => 'Не принято'],
                        'options' => ['multiple'=>false, 'class' => 'inp01', 'placeholder' => 'Решение об участии', 'id' => 'solution'],
                        'pluginOptions' => [
                            'width' => '200px',
                        ],
                    ]);
                    ?>
                </div>
            </div>

            <div class="col-md-4">
                <span>Источник получения</span>
                <div class="form-group">
                    <?= 
                    Select2::widget([
                        'name' => 'source',
                        'value' => 0,
                        'data' =>  ArrayHelper::map($source, 'sourceid', 'source_title'),
                        'options' => ['multiple'=>false, 'class' => 'inp01', 'placeholder' => 'Источник получения', 'id' => 'source'],
                        'pluginOptions' => [
                            'width' => '200px',
                        ],
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-3 col-md-offset-8">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" onclick="report();">Применить фильтр</button>
                    </div>
                </div>
            </div>
            </div>
   </div>
    <div class="col-md-12" style="background-color: white;"> 
        <div>
            <div class="table-responsive">
                <?php Pjax::begin(['id' => 'report']) ?>
                <?php 
                echo GridView::widget([
                    'dataProvider' => $provider,
                    'tableOptions' => [
                        'id' => 'main_table',
                        'class' => 'table table-striped table-bordered',
                    ],
                    'summary' => false,
                    'rowOptions' => function($tenders)
                    {
                        return ['id' => 'tender_'.$tenders['tenderid']];
                    },
                    'columns' => [

                    [                                       
                        'label' => 'id',
                        'encodeLabel' => false,
                        'value' => function () {
                            static $i = 1;
                            return (string)$i++;
                        },
                    ],
                    [ 
                        'attribute' => 't_csbin',                                               
                        'label' => 'БИН',
                        'encodeLabel' => false,
                        'value' => function ($tenders) {
                            return $tenders['t_csbin'];
                        },
                        'contentOptions' => function($tenders)
                        {
                          return ['onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'];
                        },
                    ],
                    [ 
                        'attribute' => 't_csname',                                               
                        'label' => 'Название',
                        'encodeLabel' => false,
                        'headerOptions' => ['style' => 'width:20px;'],
                        'value' => function ($tenders) {
                            return $tenders['t_csname'];
                        },  
                        'contentOptions' => function($tenders)
                        {
                          return ['onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'];
                      },        
                    ],
                    [ 
                        'attribute' => 'lot_participate_count_true',                                               
                        'label' => 'Решение',
                        'headerOptions' => ['style' => 'width: 100px;'],
                        'value' => function ($tenders) {
                            if ($tenders['lot_participate_count_true'] > 0) {
                                return 'Участвуем';
                            } elseif ($tenders['lot_participate_count_false'] > 0) {
                                return 'Не участвуем';
                            } else {
                                return 'Решение не принято';
                            }
                        },
                    ],
                    [ 
                        'attribute' => 'purchase_number',                                               
                        'label' => '№ Закупки',
                        'value' => function ($tenders) {
                            return $tenders['purchase_number'];
                        },
                        'contentOptions' => function($tenders)
                        {
                          return ['onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'];
                        },
                    ],
                    [ 
                        'attribute' => 'date_purchase',                                              
                        'label' => 'Дата вскрытия',
                        'encodeLabel' => false,
                        'value' => function ($tenders) {
                            return date('d-m-Y', $tenders['tender_date_purchase']);
                        },  
                        'contentOptions' => function($tenders)
                        {  
                             return [
                                'onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'
                            ];
                        },        
                    ],
                    [                             
                        'header' => 'Ответственный специалист',
                        'format' => 'raw',
                        'value' => function ($tenders) {
                            return ($tenders['tm_managerid'] != NULL || $tenders['tm_managerid'] != 0) ? 
                                Yii::$app->UserComponent->getById($tenders['tm_managerid'])['fullname'] : 
                                'Не выбран';
                        }
                    ],
                    [ 
                        'attribute' => 'source_title',                                               
                        'label' => 'Источник получения',
                        'value' => function ($tenders) {
                            return $tenders['source_title'];
                        },
                        'contentOptions' => function($tenders)
                        {
                          return ['onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'];
                        },
                    ],
                    [ 
                        'attribute' => 'depnames',                                               
                        'label' => 'Филиал',
                        'value' => function ($tenders) {
                            return $tenders['depnames'];
                        },
                        'contentOptions' => function($tenders)
                        {
                          return ['onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'];
                        },
                    ],
                ],
            ]);
?>
<?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>

<script>
    
    function download(){
        var today = new Date();
        var dd = today.getDate();

        var mm = today.getMonth()+1; 
        var yyyy = today.getFullYear();
        if(dd<10) 
        {
            dd='0'+dd;
        } 

        if(mm<10) 
        {
            mm='0'+mm;
        } 
        today = mm+'-'+dd+'-'+yyyy;

        $("#main_table").tableExport({

          headers: true,                   

          footers: true,

          formats: ["xlsx", "csv"],          

          fileName: today + "Отчёт по тендерам",                        

          bootstrap: false,

          exportButtons: true,                         

          position: "bottom",                  

          ignoreRows: null,                            

          ignoreCols: null,                  

          trimWhitespace: true, 

          type: 'pdf',    

      });
    }
    function report()
    {   
        var sector = btoa($('#sector').val());
        var solution = btoa($('#solution').val());
        var source = btoa($('#source').val());
        var contragent = btoa($('#contragent').val());
        // console.log(atob(sectors));
        // return 0;
        $.pjax.reload({
            type: 'GET',
            container: "#report", 
            url: "index.php?r=report/report-tender",
            data: {
                sector: sector,
                solution: solution,
                source: source,
                contragent: contragent,
            }
        });
    }

</script>

<script>
    // $(document).ready(function(){
    //     $("#main_table").tableExport({

    //       headers: true,                   

    //       footers: true,

    //       formats: ["xlsx", "csv", "txt"],          

    //       fileName: "id",                        

    //       bootstrap: false,

    //       exportButtons: true,                         

    //       position: "bottom",                  

    //       ignoreRows: null,                            

    //       ignoreCols: null,                  

    //       trimWhitespace: false        

    //   });
    // });
</script>