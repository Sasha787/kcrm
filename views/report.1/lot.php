<?php
use yii\helpers\Url;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

$this->title = "Отчет";
$tender_permission = Yii::$app->auth->tenderPermission()['tender'];
if ($tender_permission != 1 || $tender_permission != 2) {
    $disabled = true;
} else {
    $disabled = false;
}

date_default_timezone_set('Asia/Almaty');
?>
<div class="row">
  <div class="col-md-12" style="background-color: white"> <br>
            <div class="col-md-5 col-md-offset-3">
    
            <div class="col-md-4">
                <span>Заказчик</span>
                <div class="form-group">
                    <?= 
                    Select2::widget([
                        'name' => 'contragent',
                        'value' => 0,
                        'data' =>  ArrayHelper::merge(['0'=>'Не выбрано'], ArrayHelper::map($contragents, 't_csbin', 't_csname')),
                        'options' => ['multiple'=>false, 'class' => 'inp01', 'placeholder' => 'Заказчик', 'id' => 'contragent'],
                        'pluginOptions' => [
                            'width' => '200px',
                        ],
                    ]);
                    ?>
                </div>
            </div>

            <div class="col-md-4">
                <span>Итоги закупки</span>
                <div class="form-group">
                    <?= 
                    Select2::widget([
                        'name' => 'conclusions',
                        'value' => 0,
                        'data' =>  ArrayHelper::map($conclusions, 'concid', 'conclusion_title'),
                        'options' => ['multiple'=>false, 'class' => 'inp01', 'placeholder' => 'Итоги закупки', 'id' => 'conclusion'],
                        'pluginOptions' => [
                            'width' => '200px',
                        ],
                    ]);
                    ?>
                </div>
            </div>

            <div class="col-md-4">
                <span>Филиал</span>
                <div class="form-group">
                    <?= 
                    Select2::widget([
                        'name' => 'depid',
                        'value' => 0,
                        'data' =>  ArrayHelper::map($deplist, 'depid', 'dname'),
                        'options' => ['multiple'=>false, 'class' => 'inp01', 'placeholder' => 'Филиал', 'id' => 'depid'],
                        'pluginOptions' => [
                            'width' => '200px',
                        ],
                    ]);
                    ?>
                </div>
            </div>

            <!--  -->
            <div class="col-md-12">
                <div class="col-md-3 col-md-offset-8">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" onclick="report();">Применить фильтр</button>
                    </div>
                </div>
            </div>
            </div>
   </div>
    <div class="col-md-12" style="background-color: white;"> 
   
        <div>
            <div class="table-responsive">
                <?php Pjax::begin(['id' => 'report']) ?>
                <?php 
                echo GridView::widget([
                    'dataProvider' => $provider,
                    'tableOptions' => [
                        'id' => 'main_table',
                        'class' => 'table table-striped table-bordered',
                    ],
                    'summary' => false,
                    'rowOptions' => function($lots)
                    {
                        return ['id' => 'tender_'.$lots['lotid']];
                    },
                    'columns' => [
                    [                                       
                        'label' => 'id',
                        'encodeLabel' => false,
                        'value' => function () {
                            static $i = 1;
                            return (string)$i++;
                        },
                    ],
                    [ 
                        'attribute' => 't_csbin',                                               
                        'label' => 'БИН',
                        'encodeLabel' => false,
                        'value' => function ($lots) {
                            return $lots['t_csbin'];
                        },
                        'contentOptions' => function($lots)
                        {
                          return ['onclick' => 'window.open("' . Url::toRoute(['lot/view', 'id'=>$lots['lotid']]). '")'];
                        },
                    ],
                    [ 
                        'attribute' => 't_csname',                                               
                        'label' => 'Название',
                        'encodeLabel' => false,
                        'headerOptions' => ['style' => 'width:20px;'],
                        'value' => function ($lots) {
                            return $lots['t_csname'];
                        },  
                        'contentOptions' => function($lots)
                        {
                          return ['onclick' => 'window.open("' . Url::toRoute(['lot/view', 'id'=>$lots['lotid']]). '")'];
                      },        
                    ],
                    [ 
                        'attribute' => 'purchase_number',                                               
                        'label' => '№ закупки',
                        'encodeLabel' => false,
                        'headerOptions' => ['style' => 'width:20px;'],
                        'value' => function ($lots) {
                            return $lots['purchase_number'];
                        },  
                        'contentOptions' => function($lots)
                        {
                          return ['onclick' => 'window.open("' . Url::toRoute(['lot/view', 'id'=>$lots['lotid']]). '")'];
                      },        
                    ],
                    [ 
                        'attribute' => 'lot_number',                                               
                        'label' => '№ лота',
                        'headerOptions' => ['style' => 'width: 100px;'],
                        'value' => function ($lots) {
                            return $lots['lot_number'];
                        },
                    ],
                    [ 
                        'attribute' => 'budget',                                               
                        'label' => 'Бюджет, без НДС',
                        'value' => function ($lots) {
                            return number_format($lots['budget'], 2, ',', ' ').' тг';
                        },
                        'contentOptions' => function($lots)
                        {
                          return ['onclick' => 'window.open("' . Url::toRoute(['lot/view', 'id'=>$lots['lotid']]). '")'];
                        },
                    ],
                    [ 
                        'attribute' => 'date_purchase',                                              
                        'label' => 'Дата вскрытия',
                        'encodeLabel' => false,
                        'value' => function ($lots) {
                            return date('d-m-Y', $lots['date_purchase']);
                        },  
                        'contentOptions' => function($lots)
                        {  
                             return [
                                'onclick' => 'window.open("' . Url::toRoute(['lot/view', 'id'=>$lots['lotid']]). '")'
                            ];
                        },        
                    ],
                    [                             
                        'header' => 'Итоги закупки',
                        'format' => 'raw',
                        'value' => function ($lots) {
                            return $lots['conclusion_title'];
                        }
                    ],
                    [ 
                        'attribute' => 'depnames',                                               
                        'label' => 'Филиал',
                        'value' => function ($lots) {
                            return $lots['depnames'];
                        },
                        'contentOptions' => function($lots)
                        {
                          return ['onclick' => 'window.open("' . Url::toRoute(['lot/view', 'id'=>$lots['lotid']]). '")'];
                        },
                    ],
                ],
            ]);
?>
<?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>

<script>
    
    function download(){
        var today = new Date();
        var dd = today.getDate();

        var mm = today.getMonth()+1; 
        var yyyy = today.getFullYear();
        if(dd<10) 
        {
            dd='0'+dd;
        } 

        if(mm<10) 
        {
            mm='0'+mm;
        } 
        today = mm+'-'+dd+'-'+yyyy;

        $("#main_table").tableExport({

          headers: true,                   

          footers: true,

          formats: ["xlsx"],          

          fileName: today + "Отчёт по тендерам",                        

          bootstrap: false,

          exportButtons: true,                         

          position: "bottom",                  

          ignoreRows: null,                            

          ignoreCols: null,                  

          trimWhitespace: false        

      });
    }
    function report()
    {   
        var conclusion = btoa($('#conclusion').val());
        var depid = btoa($('#depid').val());
        var contragent = btoa($('#contragent').val());
        $.pjax.reload({
            type: 'GET',
            container: "#report", 
            url: "index.php?r=report/report-lot",
            data: {
                conclusion: conclusion,
                depid: depid,
                contragent: contragent,
            }
        });
    }

</script>

<script>
    // $(document).ready(function(){
    //     $("#main_table").tableExport({

    //       headers: true,                   

    //       footers: true,

    //       formats: ["xlsx", "csv", "txt"],          

    //       fileName: "id",                        

    //       bootstrap: false,

    //       exportButtons: true,                         

    //       position: "bottom",                  

    //       ignoreRows: null,                            

    //       ignoreCols: null,                  

    //       trimWhitespace: false        

    //   });
    // });
</script>