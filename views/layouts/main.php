<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

require Yii::$app->basePath.'/../config.php';
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <meta name="description" content="kaztranscom - CRM">
    <meta name="keywords" content="клиент, продажа, сделка">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <script src="<?= Yii::$app->params['module_folder_name'] ?>js/apps.js?v=3"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<div class="container-fluid">

<body style="background: rgb(229, 229, 229);" onload="animatebg(); modal();">
<?php $this->beginBody() ?>
<div class="row">
    <div class="col-md-12">
    <table id="mainbg" width="100%" border="0" cellpadding="1" cellspacing="0" style="background:url('/<?= Yii::$app->auth->userConfig()['bgimg'] ? Yii::$app->auth->userConfig()['bgimg'] : $cfg_bcg_image?>'); border:1px solid #cccccc;">
        <tr style="height:30px;"><td colspan="3">&nbsp;</td></tr>
        <tr><td style="width:30px;height:70px;">&nbsp;</td><td rowspan="3" style="width:150px;height:150px;background:#eeeeea;border:3px solid #ffffff;vertical-align:middle;text-align:center;"><img id="bgimg2" src="/<?= Yii::$app->auth->userConfig()['pimg'] ? Yii::$app->auth->userConfig()['pimg'] : $cfg_profile_image?>" style="width:147px;border:0px;"></td><td>&nbsp;</td></tr>
        <tr><td style="width:30px;height:50px;">&nbsp;</td>
            <td NOWRAP>
                <div id="topmenu">
                </div>
            </td></tr>
            <tr style="height:30px;">
                <td style="width:30px;background:#ffffff;">&nbsp;</td>
                <td style="background:#ffffff;vertical-align:middle;text-align:left;">
                    <table style="width:100%;" cellpadding="3" cellspacing="1" border="0">
                        <tr>
                            <td style="width:300px;">&nbsp;&nbsp;&nbsp;<a id="fullname" class="cntlist2"><b><?= Yii::$app->auth->user()['dname'].'/'.Yii::$app->auth->user()['fullname'] ?></b></a>&nbsp;</td>
                            <td><div id="topmenu2">
                            </div></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="height:5px;"><td colspan="3" style="background:#ffffff;"></td></tr>
        </table>
        </div>
    </div>
<div class="wrap">
    <div class="container-fluid">
        <?= $content ?>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</div>
</html>
<?php $this->endPage() ?>
