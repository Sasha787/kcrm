<?php 
use kartik\select2\Select2; 
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>

<?php foreach ($risks as $i => $risk): ?>

<div id="<?='risk_'.$risk['lr_id']?>" class="one_risk">
<div class="panel-body">
	<div class="row">
		<div class="col-md-12">
				<?php if ($risk['lr_title'] == NULL): ?>
					<h4 id="<?='risk_title_'.$risk['lr_id']?>" style="color:red;">Риски не описаны.</h4>
				<?php else: ?>
				<h4 id="<?='risk_title_'.$risk['lr_id']?>"><?= nl2br($risk['lr_title']) ?></h4>
				<?php endif ?>

				<textarea class="form-control" id="<?='risk_textarea_'.$risk['lr_id']?>" cols="30" rows="5" style="display: none;"><?= $risk['lr_title'] ?></textarea>
		</div>
			<div class="col-md-6 col-md-offset-6" style="margin-top: 15px;">
				<div class="col-md-6">
					<div id="<?='risk_info_'.$risk['lr_id']?>" style="display: block;">
						<b>Последние изменения от:</b><br>
						<i id="<?='author_of_risk_'.$risk['lr_id']?>"><?= $risk['lr_fullname']?></i><br>
						<i id="<?='date_of_risk_'.$risk['lr_id']?>"><?= date('d-m-Y H:i', $risk['lr_updated_at']) ?></i>
					</div>
				</div>
				<div class="col-md-6">
					<?php if ($user_tender_permission == 1 || $user_tender_permission == 2): ?>
						<button class="btn btn-default btn-xs" id="<?='risk_create_button_'.$risk['lr_id']?>" style="float: right;" onclick="createRisk(<?=$risk['lr_id']?>, <?= $lot_info['l_tenderid'] ?>)">Описать риски</button>
						<button class="btn btn-success btn-xs" id="<?='risk_save_button_'.$risk['lr_id']?>" style="float: right; display: none" onclick="saveRisk(<?=$risk['lr_id']?>, <?= $lotid ?>, <?= $lot_info['l_tenderid'] ?>)">Сохранить</button>
					<?php endif ?>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<b>Вложения:</b><br><br>
					<div id="<?='risk_files_'.$risk['lr_id']?>">
					<?php if (isset($risk['files'])): ?>
							<?php foreach ($risk['files'] as $k => $file): ?>
								<span id="<?='risk_file_'.$file['rf_id']?>">
									<a target="_blank" href="<?=Url::toRoute(['risk-ajax/download-file', 'id' => $file['rf_id']])?>"><?=$file['rf_filename']?></a><button type="button"  class="btn btn-outline-danger btn-xs" title="Удалить файл" onclick="rmRiskFile(<?=$file['rf_id']?>, <?=$risk['lr_id']?>)"><i class="fa fa-remove" aria-hidden="true" style="color:red;"></i></button><br>
								</span>
							<?php endforeach ?>
					<?php endif ?>
					</div>
					<?php if ($user_tender_permission == 1 || $user_tender_permission == 2): ?>
						<input name="risk-file" type="file" id="<?='risk-file-input-'.$risk['lr_id']?>" class="inp01" accept=".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" onchange="uploadRiskFile(<?=$risk['lr_id']?>, <?=$lotid?>, <?= $lot_info['l_tenderid'] ?>);">
					<?php endif ?>		
				</div>


			</div>
		</div>
</div>



<div class="table-responsive">
    <table class="table table-striped table-condensed" id="<?='table_signed_'.$risk['lr_id']?>">
        <thead>
            <tr>
                <th>ФИО</th>
                <th>Подразделение</th>
                <th>Дата получения</th>
                <th>Подписал</th>
                <th>Дата подписания</th>
                <th>Комментарий</th>                                          
            </tr>
        </thead>   
        <tbody>
        <?php if (isset($risk['signers'])): ?>
        	<?php foreach ($risk['signers'] as $j => $signer): ?>
            <tr id="<?= 'signed_user_'.$signer['sid'] ?>">
                <td><?= $signer['s_username'] ?></td>
                <td><?= $signer['s_dname'] ?></td>
                <td><?= date('d-m-Y', $signer['s_date_received']) ?></td>
                <td><?= $signer['is_signed'] ? '<span class="glyphicon glyphicon-ok" style="color:green">' : '<span class="glyphicon glyphicon-remove" style="color:red">' ?></td>
                <td><?= $signer['s_date_signed'] ? date('d-m-Y', $signer['s_date_signed']) : 'Еще не подписал' ?> </td>
                <td><?= $signer['s_comment'] ?></td>
                <?php if ($user_tender_permission == 1 || $user_tender_permission == 2): ?>
                    <td><button type="button" id="remove" class="btn btn-danger btn-xs" onclick="rmUser(<?=$signer['sid']?>, <?=$risk['lr_id']?>)" style="margin-left: 20px;">Удалить</button></td>
                <?php endif ?>                                   
            </tr>  
        	<?php endforeach ?>	
        <?php endif ?>                               
        </tbody>
    </table>
</div>
<?php if (isset($risk['is_need_sign']) && $risk['lr_isActive'] == 1): ?>
	<div class="col-md-6">
        <form class="form" role="form" method="POST" action="<?=Url::toRoute(['lot/sign', 'risk_id'=>$risk['lr_id'], 'lotid'=>$lotid])?>">
        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
            <div class="form-group">
                <textarea class="form-control" cols="4" type="text" name="comment" placeholder="Комментарий" ></textarea>
            </div>
            <div class="form-group">
            
            </div>
            <div class="form-group">
                <button type="submit" id="btn-submit" name="is_sign" class="btn btn-success btn-xs" value="1">Принять</button>
                <button type="submit" id="btn-submit" name="is_sign" class="btn btn-danger btn-xs" value="0">Отклонить</button>
            </div>
        </form>
    </div>	
<?php endif ?>
<?php if ($user_tender_permission == 1 || $user_tender_permission == 2): ?>
    <div class="col-md-12" >
        <div class="col-md-6">
            <div class="controls"><br>
                <b>Выберите подписантов, которые должны согласовать данный лот.</b><br><br>
                <?= Select2::widget([
                    'name' => 'is_affiliated',
                    'value' => 0,
                    'data' => ArrayHelper::merge(['0' => 'Не выбрано'], ArrayHelper::map($userlist, 'userid', 'fullname')),
                    'options' => ['multiple'=>false, 'class' => 'inp01'],
                    'pluginOptions' => [
                        'width' => '200px',
                    ],
                    'pluginEvents' => [
                        'select2:select' => 'function() { addUser(this.value, '.$risk['lr_id'].', '.$lotid.') }'
                    ],
                ]);
                ?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="controls"><br>
                <b>Запустив на согласование, изменить риски и вложения нельзя!</b>
                <br><br>

                <?php if ($risk['lr_isActive'] == 0): ?>
                    <button id="<?='risk-is-active-0-'.$risk['lr_id']?>" class="btn btn-danger" onclick="riskIsActive(0, <?=$risk['lr_id']?>, <?= $lotid ?>, <?= $lot_info['l_tenderid'] ?>)" style="display:none;">Отозвать согласование</button>
                    <button id="<?='risk-is-active-1-'.$risk['lr_id']?>" class="btn btn-default" onclick="riskIsActive(1,<?=$risk['lr_id']?>, <?= $lotid ?>, <?= $lot_info['l_tenderid'] ?>)">Запустить на согласование</button>
                <?php else: ?>
                    <button id="<?='risk-is-active-1-'.$risk['lr_id']?>" class="btn btn-default" onclick="riskIsActive(1, <?=$risk['lr_id']?>, <?= $lotid ?>, <?= $lot_info['l_tenderid'] ?>)" style="display:none;">Запустить на согласование</button>
                    <button id="<?='risk-is-active-0-'.$risk['lr_id']?>" class="btn btn-danger" onclick="riskIsActive(0, <?=$risk['lr_id']?>, <?= $lotid ?>, <?= $lot_info['l_tenderid'] ?>)">Отозвать согласование</button>
                <?php endif ?>
            </div>
        </div>
    </div>
<?php endif ?>
</div>
<div style="height: 150px; border-bottom: 1px solid black"></div>
<?php endforeach ?>
