<div class="form-group" id="participate-block">
	<div class="row">
		<div class="col-md-6">
			<textarea class="form-control" cols="2" type="text" id="participate-reason-textarea" placeholder="Причина, которая стала основанием для не участия в тендере" ></textarea>
		</div>
		<div class="col-md-12" style="margin-top: 20px;">
			<button type="submit" id="btn-submit" name="is_sign" class="btn btn-success btn-xs" onclick="isParticipate(1, <?=$id?>, <?=$idButton?>)">Участвуем</button>
			<button type="submit" id="btn-submit" name="is_sign" class="btn btn-danger btn-xs" onclick="isParticipate(0, <?=$id?>, <?=$idButton?>)">Не участвуем</button>
		</div>
	</div>
</div>