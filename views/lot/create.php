<?php
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
?>
<!-- 
<div class="row">
	<div class="col-md-6">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Общая информация</a></li>
				</ul>
			</div>
		</nav>
	</div>
</div> -->

<div class="row">
	<div class="col-md-6">
		<form action="<?= Url::toRoute(['lot/store', 'tenderid' => Yii::$app->request->get('tenderid')])?>" method="POST" onkeypress="return event.keyCode!=13">
			<input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" >
			<input type="hidden" name="tender_id" value="<?= Yii::$app->request->get('tenderid'); ?>">
			<input type="hidden" id="tmp_code" name="tmp_code"  value="<?= Yii::$app->params['tmp_code']?>">
			<table class="table" style="width:100%;background:#ffffff;border:10px solid #ffffff;">
				<tbody>
					<tr>
						<td colspan="2"><div class="alert alert-info haction1" >Создание лота</div><br><br>
						</td>
					</tr>
					<tr>
					<td class="td01">Участие аффилированных лиц</td>
					<td width="50%" class="td02">
						<?=
								Select2::widget([
									'name' => 'is_affiliated',
									'value' => 0,
									'hideSearch' => true,
									'data' => ['0'=>'Нет', '1'=>'Да'],
									'options' => ['multiple'=>false, 'class' => 'inp01']
								]); 
							?>
					</td>
				</tr>
					<tr>
						<td class="td01">№ (номер) Лота</td><td class="td02">
							<input name="lot_number" type="text" id="ELE_CSBIN" value="" class="inp01" autocomplete="off">
						</td>
					</tr>
					<tr>
						<td width="50%" class="td01">№ (номер), наименование Лота (ов) закупки</td><td width="50%" class="td02">
							<textarea name="lot_name" type="text" id="ELE_CSBIN" value="" class="inp01" autocomplete="off"></textarea> 
						</td>
					</tr>
					<tr>
						<td class="td01">Место поставки товара / выполнения работ / оказания услуг</td><td class="td02">
							<input name="place" type="text" id="ELE_CSBIN" value="" class="inp01" autocomplete="off">
						</td>
					</tr>
					<tr>
						<td class="td01">Дата проведения закупки (вскрытия)</td><td class="td02">
							<?=
							DatePicker::widget([
								'name' => 'date_purchase',
								'type' => DatePicker::TYPE_INPUT,
								'value' => date('d-m-Y'),
								'pluginOptions' => [
									'autoclose'=>true,
									'format' => 'dd-mm-yyyy',
								],
								'options' => ['class' => 'inp01']
							]);
							?>
						</td>
					</tr>
					<tr>
						<td class="td01">Бюджет закупки, в тенге, без учета НДС</td><td class="td02">
							<input name="budget" type="number" id="budget" value="" class="inp01" autocomplete="off" onkeyup="countVat();" step="any">
						</td>
					</tr>
					<tr>
						<td class="td01">Бюджет закупки, в тенге, с учетом НДС</td><td class="td02">
							<input name="budget_vat" type="number" id="budget_vat" value="" class="inp01" autocomplete="off" step="any">
						</td>
					</tr>
					<tr>
						<td class="td01">Статус объявленной закупки</td><td class="td02">
							<?=
								Select2::widget([
									'name' => 'lot_status',
									'value' => 0,
									'hideSearch' => true,
									'data' => ArrayHelper::map($lot_statuses, 'lid', 'status_title'),
									'options' => ['multiple'=>false, 'class' => 'inp01']
								]); 
							?>
						</td>
					</tr>
					<tr>
						<td class="td01">Признак закупки ("Существующий" / "Новый" объем услуг АО KazTransCom)</td><td class="td02">
							<?=
							Select2::widget([
								'name' => 'purchase_indication',
								'value' => 0,
								'data' => ArrayHelper::map($lot_indications, 'indicationid', 'indication_title'),
								'options' => ['multiple'=>false, 'class' => 'inp01']
							]); 
							?>
						</td>
					</tr>
					<tr>
						<td class="td01">Процент обеспечения заявки</td><td class="td02">
							<?=
								Select2::widget([
									'name' => 'percentid',
									'value' => 0,
									'hideSearch' => true,
									'data' => ArrayHelper::map($percents, 'perid', 'percent_title'),
									'options' => ['multiple'=>false, 'class' => 'inp01']
								]); 
							?>
						</td>
					</tr>
					<tr>
						<td class="td01">Ценовое предложение АО KazTransCom, в тенге, без учета НДС</td><td class="td02">
							<input name="ktc_price" type="text" id="ELE_CSBIN" value="" class="inp01" autocomplete="off">
						</td>
					</tr>
					<tr>
						<td class="td01">Ценовое предложение АО KazTransCom, в тенге, с учетом НДС</td><td class="td02">
							<input name="ktc_price_vat" type="text" id="ELE_CSBIN" value="" class="inp01" autocomplete="off">
						</td>
					</tr>
					<tr>
						<td class="td01">Итоги закупки</td><td class="td02">
							<?=
								Select2::widget([
									'name' => 'lot_conclusion',
									'value' => 0,
									'hideSearch' => true,
									'data' => ArrayHelper::map($conclusions, 'concid', 'conclusion_title'),
									'options' => ['multiple'=>false, 'class' => 'inp01']
								]); 
							?>
						</td>
					</tr>
					<tr>
						<td class="td01">Перечень потенциальных поставщиков / участников закупки</td><td class="td02">
							<textarea name="potential_members" type="text" id="ELE_CSBIN" value="" class="inp01" autocomplete="off"></textarea>	
						</td>
					</tr>
					<tr>
						<td class="td01">Общество соответствует квалификационным требованиям и допущено на участие в закупке? (ДА/НЕТ)</td><td class="td02">
							<?=
								Select2::widget([
									'name' => 'ktc_qualification',
									'value' => 0,
									'hideSearch' => true,
									'data' => ['0'=>'Нет', '1'=>'Да'],
									'options' => ['multiple'=>false, 'class' => 'inp01']
								]); 
							?>
						</td>
					</tr>
					<tr>
						<td class="td01">Причина, послужившая основанием для отклонения заявки АО KazTransCom</td>
						<td class="td02">
							<textarea name="ktc_reason" type="text" id="ELE_CSBIN" value="" class="inp01" autocomplete="off"></textarea>
						</td>
					</tr>
					<tr>
						<td class="td01">Ценовое предложение победителя закупок, в тенге, без учета НДС</td><td class="td02">
							<input name="winner_price" type="text" id="ELE_CSBIN" value="" class="inp01" autocomplete="off">
						</td>
					</tr>
					<tr>
						<td class="td01">Ценовое предложение победителя закупок, в тенге, с учетом НДС</td><td class="td02">
							<input name="winner_price_vat" type="text" id="ELE_CSBIN" value="" class="inp01" autocomplete="off">
						</td>
					</tr>
					<tr>
						<td class="td01">Ссылка</td><td class="td02">
							<input name="link" type="text" id="ELE_CSBIN" value="" class="inp01" autocomplete="off">
						</td>
					</tr>
					<tr>
						<td class="td01">Примечания</td><td class="td02">
							<textarea name="notes" type="text" id="ELE_CSBIN" value="" class="inp01" autocomplete="off"></textarea>
						</td>
					</tr>
					<tr>
						<td class="td01">Партнер</td><td class="td02">
							<?=
								Select2::widget([
									'name' => 'partner',
									'value' => 0,
									'hideSearch' => true,
									'data' => ['0'=>'Нет', '1'=>'Да'],
									'options' => ['multiple'=>false, 'class' => 'inp01']
								]); 
							?>
						</td>
					</tr>
					<tr>
						<td class="td01">Номер договора в случае выигрыша закупа</td><td class="td02">
							<input name="dogovor_number" type="text" id="ELE_CSBIN" value="" class="inp01" autocomplete="off">
						</td>
					</tr>
					<tr>
						<td class="td01">Дата договора в случае выигрыша закупки</td><td class="td02">
							<?=
							DatePicker::widget([
								'name' => 'dogovor_date',
								'type' => DatePicker::TYPE_INPUT,
								'value' => date('d-m-Y'),
								'pluginOptions' => [
									'autoclose'=>true,
									'format' => 'dd-mm-yyyy',
								],
								'options' => ['class' => 'inp01']
							]);
							?>
						</td>
					</tr>
					<tr>
						<td class="td01">Файлы</td>
						<td class="td02" id="lot_files">
							<input name="tender_file" type="file" value="" id="lot_file" class="inp01" accept=
                            ".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" onchange="uploadFile();">
						</td>
					</tr>
				</tbody>
			</table>
			<button class="btn btn-default" type="submit">Сохранить</button>
		</form>
	</div>
	<div class="col-md-6">
		
	</div>
</div>
<script src="<?= Yii::$app->params['module_folder_name'] ?>js/lotCreate.js"></script>