<?php

use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\widgets\Pjax;

date_default_timezone_set('Asia/Almaty');

$user_tender_permission = Yii::$app->auth->tenderPermission()['tender'];
$user_tender_depid = Yii::$app->auth->tenderPermission()['depid'];
$lotid = Yii::$app->request->get('id');
$curManager = Yii::$app->LotComponent->getCurManager($lotid);
$curSpecialist = Yii::$app->LotComponent->getCurSpecialist($lotid);
$authUser = Yii::$app->auth->user()['userid'];


(($user_tender_permission == 1 || $user_tender_permission == 2) && ($curSpecialist == 0 || ($curSpecialist == $authUser))) ? $disabled = '' : $disabled = 'disabled';
?>

<script>
	console.log('currManager: ', <?= $curManager ?>);
</script>

<div class="row" style="margin-top: 10px;">
	<div class="col-md-12">
		<a href="<?= Url::toRoute(['tender/view', 'id' => $lot_info['l_tenderid']]) ?>" class="btn btn-default">Просмотреть тендер</a>
	</div>
</div>

<input type="hidden" id="lot_file_code" name="lot_file_code" value="<?= $lot_info['file_code'] ?>">
<input type="hidden" id="lot_tenderid" name="lot_file_code" value="<?= $lot_info['l_tenderid'] ?>">
<div class="row" style="margin-top: 10px;">
	<div class="col-md-6">
		<table class="table" style="width:100%;background:#ffffff;border:10px solid #ffffff;">
			<tbody>
				<tr>
					<td colspan="2">
						<div class="alert alert-info haction1">Информация о лоте</div><br><br>
					</td>
				</tr>
				<tr>
					<td class="td01">ID Лота</td>

					<td width="50%" class="td02">
						<?= $lot_info['lotid'] ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Участие аффилированных лиц</td>
					<td width="50%" class="td02">
						<?= $lot_info['is_affiliated'] ? 'Да' : 'Нет' ?>
					</td>
				</tr>
				<tr>
					<td class="td01">№ (номер) Лота</td>
					<td class="td02">
						<?= $lot_info['lot_number'] ?>
					</td>
				</tr>
				<tr>
					<td width="50%" class="td01">№ (номер), наименование Лота (ов) закупки</td>
					<td width="50%" class="td02">
						<?= $lot_info['lot_name'] ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Место поставки товара / выполнения работ / оказания услуг</td>
					<td class="td02">
						<?= $lot_info['place'] ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Дата проведения закупки (вскрытия)</td>
					<td class="td02">
						<?= date('d-m-Y', $lot_info['date_purchase']) ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Бюджет закупки, в тенге, без учета НДС</td>
					<td class="td02">
						<?= number_format($lot_info['budget'], 2, ',', ' ') . ' тг'; ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Бюджет закупки, в тенге, с учетом НДС</td>
					<td class="td02">
						<?= number_format($lot_info['budget_vat'], 2, ',', ' ') . ' тг'; ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Статус объявленной закупки</td>
					<td class="td02">
						<?= $lot_info['status_title'] ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Результаты ТВ/ТР</td>
					<td class="td02">
						<?php foreach ($lot_info['treqform'] as $key => $value) : ?>
							Заявка ID: <b><?= $value['trid'] ?></b> Дата: <b><?= date('d-m-y H:m', $value['fdate']) ?></b><br>
							<i><?= $value['tcomment'] ?></i> <br>
							<hr>
						<?php endforeach ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Признак закупки ("Существующий" / "Новый" объем услуг АО KazTransCom)</td>
					<td class="td02">
						<?= $lot_info['indication_title'] ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Процент обеспечения заявки</td>
					<td class="td02">
						<?= $lot_info['percent_title'] ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Ценовое предложение АО KazTransCom, в тенге, без учета НДС</td>
					<td class="td02">
						<?= number_format($lot_info['ktc_price'], 2, ',', ' ') . ' тг'; ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Ценовое предложение АО KazTransCom, в тенге, с учетом НДС</td>
					<td class="td02">
						<?= number_format($lot_info['ktc_price_vat'], 2, ',', ' ') . ' тг'; ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Итоги закупки</td>
					<td class="td02">
						<?= $lot_info['conclusion_title'] ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Описание итогов</td>
					<td class="td02">
						<?= $lot_info['conclusion_reason'] ?>
					</td>
				</tr>
				<!-- <tr>
					<td class="td01">Перечень потенциальных поставщиков / участников закупки</td><td class="td02">
						<?= $lot_info['potential_members'] ?>
					</td>
				</tr> 
				<tr>
					<td class="td01">Общество соответствует квалификационным требованиям и допущено на участие в закупке? (ДА/НЕТ)</td><td class="td02">
						<?= $lot_info['ktc_qualification'] ? 'Да' : 'Нет' ?>
					</td>
				</tr> -->
				<tr>
					<td class="td01">Тип причины отказа</td>
					<td class="td02">
						<?= array_key_exists($lot_info['ktc_cancel_reason'], $reasonsList) ? $reasonsList[$lot_info['ktc_cancel_reason']] : ''; ?>
					</td>
				</tr>

				<tr>
					<td class="td01">Детальная причина отказа</td>
					<td class="td02">
						<?= $lot_info['ktc_reason_cancel'] ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Тип причины отклонения</td>
					<td class="td02">
						<?= array_key_exists($lot_info['ktc_qf_reason'], $reasonsList) ? $reasonsList[$lot_info['ktc_qf_reason']] : ''; ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Детальная причина отклонения</td>
					<td class="td02">
						<?= $lot_info['ktc_reason'] ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Ценовое предложение победителя закупок, в тенге, без учета НДС</td>
					<td class="td02">
						<?= number_format($lot_info['winner_price'], 2, ',', ' ') . ' тг'; ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Ценовое предложение победителя закупок, в тенге, с учетом НДС</td>
					<td class="td02">
						<?= number_format($lot_info['winner_price_vat'], 2, ',', ' ') . ' тг'; ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Ссылка</td>
					<td class="td02">
						<?= $lot_info['link'] ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Примечания</td>
					<td class="td02">
						<?= $lot_info['notes'] ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Партнер</td>
					<td class="td02">
						<?= $lot_info['partner'] ? 'Да' : 'Нет' ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Номер договора в случае выигрыша закупа</td>
					<td class="td02">
						<?= $lot_info['dogovor_number'] ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Дата договора в случае выигрыша закупки</td>
					<td class="td02">
						<?= date('d-m-Y', $lot_info['dogovor_date']) ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Файлы</td>
					<td>
						<?php if ($lot_info['files']) : ?>
							<?php foreach ($lot_info['files'] as $key => $value) : ?>
								<div class="drive-item module">
									<div class="drive-item-inner module-inner">
										<div class="drive-item-title"><i class="fa fa-file-o" aria-hidden="true"></i><a target="_blank" href="<?= Url::toRoute(['lot/download-file', 'id' => $value['fileid']]) ?>"><?= Yii::$app->File->pcgbasename($value['f_path']) ?></a></div>
									</div>
								</div>
							<?php endforeach ?>
						<?php else : ?>
							<b style="color:red;">Файлов не найдено</b>
						<?php endif ?>
					</td>
				</tr>
			</tbody>
		</table>
		<?php if (($user_tender_permission == 1 || $user_tender_permission == 2) || ($curSpecialist == 0 || ($curSpecialist == $authUser))) : ?>
			<a class="btn btn-default" href="<?= Url::toRoute(['lot/edit', 'id' => $lot_info['lotid'], 'tenderid' => $lot_info['l_tenderid']]) ?>">Изменить</a>
		<?php endif ?>

		<?php if ($lot_info['l_dealid'] == 0) : ?>
			<a class="btn btn-success" onclick="return confirm('Вы уверены?')" href="<?= Url::toRoute(['lot/create-new-deal', 'id' => $lot_info['lotid']]) ?>">Создать новую сделку</a>
			<a class="btn btn-primary" data-toggle="modal" data-target="#deal">Выбрать существующую сделку</a>
			<div class="modal fade" id="deal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="deal">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel" style="color:red;"><b>Выберите сделку с контрагентом.</b></h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<?php foreach ($lot_deals as $key => $value) : ?>
										<div class="panel panel-default">
											<a class="devam pull-center cursor-disabled" onclick="return confirm('Вы уверены?')" href="<?= Url::toRoute(['lot/create-deal', 'id' => $lot_info['lotid'], 'dealid' => $value['dealid']]) ?>" style="cursor: pointer;">
												<div class="panel-heading"><?= $value['dealid'] ?>. <?= $value['ddescr'] ?>. <?= Yii::$app->UserComponent->getById($value['userid'])['fullname'] ?> <?= Yii::$app->UserComponent->getById($value['userid'])['dname'] ? '(' . Yii::$app->UserComponent->getById($value['userid'])['dname'] . ')' : '' ?>.</div>
											</a>
										</div>
									<?php endforeach ?>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
							<!-- <button type="button" class="btn btn-primary" onclick="createOwnDoc(<?= $lotid ?>)">Добавить</button> -->
						</div>
					</div>
				</div>
			</div>
		<?php else : ?>
			<a class="btn btn-success" disabled>Сделка уже создана</a>
		<?php endif ?>

		<!-- Риски -->

		<div class="col-md-12" style="background-color: white; padding-bottom: 25px; margin-top: 20px;">
			<div class="alert alert-info haction1" style="margin-top:15px;">Риски</div>

			<div class="col-md-12">
				<div id="risks">
					<?=
						$this->render('partials/_risk.php', ['risks' => $risks, 'user_tender_permission' => $user_tender_permission, 'userlist' => $userlist, 'lotid' => $lotid, 'lot_info' => $lot_info]);
					?>
				</div>
			</div>
			<?php if (($user_tender_permission == 1 || $user_tender_permission == 2) && ($curSpecialist == 0 || ($curSpecialist == $authUser))) : ?>
				<div class="col-md-12">
					<div class="col-md-3 col-md-offset-9">
						<div class="col-md-6">
							<div class="controls"><br>
								<a id="" class="btn btn-success btn-xs" href="<?= Url::toRoute(['lot/add-risk', 'lotid' => $lotid]) ?>">Добавить риск</a>
							</div>
						</div>
					</div>
				</div>
			<?php endif ?>
		</div>

		<!-- Конец Рисков	 -->

		<style type="text/css">
			h1 {
				margin: 0;
				padding: 0;
				font-size: 2em;
			}

			p.subtitle {
				margin: 0;
				padding: 0 0 0 0.125em;
				font-size: 0.77em;
				color: gray;
			}

			ul#messages {
				overflow: auto;
				height: 20em;
				padding: 0 3px;
				list-style: none;
				border: 1px solid #bce8f1;
			}

			ul#messages li {
				margin: 0.35em 0;
				padding: 0;
			}

			ul#messages li small {
				display: block;
				font-size: 0.79em;
				color: gray;
			}

			ul#messages li.pending {
				color: #aaa;
			}

			form {
				font-size: 1em;
				margin: 1em 0;
				padding: 0;
			}

			form p {
				position: relative;
				margin: 0.5em 0;
				padding: 0;
			}

			form p input {
				font-size: 1em;
			}

			form p input#name {
				width: 10em;
			}

			form p button {
				position: absolute;
				top: 0;
				right: -0.5em;
			}

			form p {
				width: 40em;
			}

			input#content {
				width: 92%;
				float: left;
			}

			pre {
				font-size: 0.77em;
			}
		</style>

		<script type="text/javascript" src="https://ajax.microsoft.com/ajax/jQuery/jquery-1.4.2.min.js"></script>
		<script type="text/javascript">
			// <![CDATA[
			$(document).ready(function() {
				// Remove the "loading…" list entry
				$('ul#messages > li').remove();

				$('form').submit(function() {

					var form = $(this);
					var name = '<?= Yii::$app->auth->user()['fullname'] ?>';
					//var content =  form.find("input[name='message']").val();
					var content = form.find("#content").val();
					var userId = form.find("input[name='userId']").val();
					var lotId = form.find("input[name='lotId']").val();

					// Only send a new message if it's not empty (also it's ok for the server we don't need to send senseless messages)
					if (name == '' || content == '')
						return false;

					// Append a "pending" message (not yet confirmed from the server) as soon as the POST request is finished. The
					// text() method automatically escapes HTML so no one can harm the client.

					console.log(form.attr('action'), {
						'name': name,
						'message': content,
						'userId': userId,
						'lotId': lotId
					});
					$.post(
						form.attr('action'), {
							'name': name,
							'message': content,
							'userId': userId,
							'lotId': lotId
						},
						function(data, status) {
							$('<li class="pending" />').text(content).prepend($('<small />').text(name)).appendTo('ul#messages');
							$('ul#messages').scrollTop($('ul#messages').get(0).scrollHeight);
							form.find("input[name='message']").val('').focus();
						});
					return false;
				});

				// Poll-function that looks for new messages
				var poll_for_new_messages = function() {
					$.ajax({
						url: '/extra_modules/index.php?r=chat%2Fmessages&lotId=<?= $lot_info['lotid'] ?>',
						dataType: 'json',
						ifModified: true,
						timeout: 2000,
						success: function(data, status) {

							// Skip all responses with unmodified data
							if (!data)
								return;

							// Remove the pending messages from the list (they are replaced by the ones from the server later)
							$('ul#messages > li.pending').remove();

							// Get the ID of the last inserted message or start with -1 (so the first message from the server with 0 will
							// automatically be shown).
							var last_message_id = $('ul#messages').data('last_message_id');
							if (last_message_id == null)
								last_message_id = -1;

							// Add a list entry for every incomming message, but only if we not already inserted it (hence the check for
							// the newer ID than the last inserted message).
							for (var i = 0; i < data.messages.length; i++) {
								var msg = data.messages[i];

								if (msg.id > last_message_id) {
									var date = new Date(msg.created_at * 1000);

									var dd = String(date.getDate()).padStart(2, '0');
									var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
									var yyyy = date.getFullYear();

									finalDate = dd + '/' + mm + '/' + yyyy;

									$('<li/>').text(msg.message).
									prepend($('<small />').text(finalDate + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ' ' + msg.fullname)).
									//prepend( $('<span />').text(msg.fullname)).
									appendTo('ul#messages');
									$('ul#messages').data('last_message_id', msg.id);
								}
							}

							// Remove all but the last 50 messages in the list to prevent browser slowdown with extremely large lists
							// and finally scroll down to the newes message.
							$('ul#messages > li').slice(0, -50).remove();
							$('ul#messages').scrollTop($('ul#messages').get(0).scrollHeight);
						}
					});
				};

				// Kick of the poll function and repeat it every two seconds
				poll_for_new_messages();
				//setInterval(poll_for_new_messages, 2000);
			});
			// ]]>
		</script>

		<div class="col-md-12" style="background-color: white; padding-bottom: 25px; margin-top: 20px;">
			<div class="alert alert-info haction1" style="margin-top:15px;">Чат
				<button id="notifications-bar" type="button" class="btn btn-primary btn-xs" data-whatever="@getbootstrap" style="float:right">Оповещения</button>
			</div>

			<div class="col-md-12" id="notifications" style="padding-left: 0; padding-right: 0; text-align:center">

				<?php if (!count($risks)) : ?>
					<p>Риски отсутствуют.</p>
				<?php endif ?>

				<?php foreach ($risks as $i => $risk) : ?>

					<div class="table-responsive">
						<table class="table table-striped table-condensed">
							<thead>
								<tr>
									<th>ФИО</th>
									<th>Подразделение</th>
									<th>Действие</th>
								</tr>
							</thead>
							<tbody>
								<?php if (isset($risk['signers'])) : ?>
									<?php foreach ($risk['signers'] as $j => $signer) : ?>
										<tr id="<?= 'signed_user_' . $signer['sid'] ?>">
											<td><?= $signer['s_username'] ?></td>
											<td><?= $signer['s_dname'] ?></td>
											<td>
												<input type="checkbox" onchange="toggleNotification(<?= $signer['s_userid'] ?>)">
											</td>
										</tr>
									<?php endforeach ?>
								<?php endif ?>
							</tbody>
						</table>
					</div>

				<?php endforeach ?>

				<button onclick="sendNotifications()" type="button" class="btn btn-primary btn-sm" data-whatever="@getbootstrap" style="margin: 15px auto;">Отправить</button>

				<div class="row">
					<div class="col-md-12" style="background-color: white"> <br>
						<div class="col-md-12">
							<span>Ответственное лицо</span>
							<div class="form-group">
								
								<?= 
									Select2::widget([
										'name' => 'user_id',
										'value' => '',
										'options' => ['placeholder' => 'Ответственное лицо', 'id'=>'t_user', 'type' => 'number'],
										'pluginOptions' => [
											'tags' => true,
											'allowClear' => true,
											'minimumInputLength' => 3,
											'maximumInputLength' => 12,
											'language' => [
												'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
												'inputTooShort' => new JsExpression("function () { return 'Введите минимум 3 символа'; }"),
											],
											'ajax' => [
												'url' => Url::to(['tender-ajax/search-user']),
												'dataType' => 'json',
												'data' => new JsExpression('function(params) { return {q:params.term}; }')
											],
											'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
											'templateResult' => new JsExpression('function(cs) { return [cs.name]; }'),
											'templateSelection' => new JsExpression('function (cs) { return cs.name; }'),
										],
									]);
								?>

								<button onclick="notifyUserForChat()" type="button" class="btn btn-primary btn-sm" data-whatever="@getbootstrap" style="margin: 15px auto;">Отправить приглашение</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- <input type="checkbox" title="Изменил: <?= $application_provision ? Yii::$app->UserComponent->getById($application_provision['ap_managerid'])['fullname'] : '' ?> " onchange="applicationIsExist(<?= $lot_info['lotid'] ?>, this.checked)" <?= $application_provision['ap_is_exist'] == 1 ? 'checked' : '' ?> <?= $disabled ?>> -->
			<!-- <div class="col-md-12" style="padding-left: 0; padding-right: 0;"> -->
			<div id="chat">
				<ul id="messages">
					<li>Загрузка сообщений...</li>
				</ul>

				<form action="/extra_modules/index.php?r=chat/message" method="post">
					<p>
						<!-- <input type="text" name="message" id="content" autocomplete="off" /> -->
						<textarea rows="4" cols="50" name="message" id="content" autocomplete="off" style="float:left; width: 91%; border: 1px solid #bce8f1;"></textarea>
						<input type="hidden" name="lotId" id="lotId" value="<?= $lot_info['lotid'] ?>" />
						<input type="hidden" name="userId" id="userId" value="<?= $authUser ?>" />
						<!-- <button type="submit">Send</button> -->
						<button type="submit" class="btn btn-primary btn-xs" data-whatever="@getbootstrap" style="float:right">Send</button>
					</p>
				</form>
			</div>
			<!-- </div> -->

		</div>

	</div>
	<div class="col-md-6">
		<?php if (Yii::$app->UserComponent->getById($curManager)) : ?>
			<div class="col-md-12" style="background-color: white; padding-bottom: 25px;">
				<div class="alert alert-info haction1" style="margin-top:15px;">
					Решение об участии
				</div>
				<div class="table-responsive">
					<table class="table table-striped table-condensed" id="table_participate">
						<thead>
							<tr>
								<th>Менеджер</th>
								<th>Решение</th>
								<th>Причина</th>
								<th>Дата</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($allSolutions as $key => $value) : ?>
								<tr>
									<td><?= Yii::$app->UserComponent->getById($value['participateManagerid'])['fullname'] ?></td>
									<td><?= $value['isParticipate'] ? '<span class="glyphicon glyphicon-ok" style="color:green">Принято</span>' : '<span class="glyphicon glyphicon-remove" style="color:red">Отказ</span>' ?></td>
									<td><?= $value['participateReason'] ?></td>
									<td><?= date('d-m-Y H:i:s', $value['participateDate']) ?></td>
									<th>
										<?php if (($value['participateManagerid'] == $authUser) && ($value['isActive'] == 1)) : ?>
											<button class="btn btn-warning btn-xs" id="<?= 'cancel-solution-button-' . $value['id'] ?>" onclick="cancelSolution(<?= $value['lotid'] ?>, <?= $value['id'] ?>)">Отозвать решение</button>
										<?php endif ?>
									</th>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
				<div id="conduct-solution">
					<!-- $authUser == $curManager && !$isNeedMakeSolution &&  -->
					<?php if ($authUser == $curManager && !$isNeedMakeSolution && ($application_provision['ap_is_need'] == 0 || $application_provision['ap_is_need'] == 1) && ($agreement_provision['lap_is_need'] == 0 || $agreement_provision['lap_is_need'] == 1)) : ?>
						<div class="form-group" id="participate-block">
							<div class="row">
								<div class="col-md-6">
									<textarea class="form-control" cols="2" type="text" id="participate-reason-textarea" placeholder="Причина, которая стала основанием для не участия в тендере"></textarea>
								</div>
								<div class="col-md-12" style="margin-top: 20px;">
									<button type="submit" id="btn-submit" name="is_sign" class="btn btn-success btn-xs" onclick="isParticipate(1, <?= $lot_info['lotid'] ?>)">Участвуем</button>
									<button type="submit" id="btn-submit" name="is_sign" class="btn btn-danger btn-xs" onclick="isParticipate(0, <?= $lot_info['lotid'] ?>)">Не участвуем</button>
								</div>
							</div>
						</div>
					<?php endif ?>
				</div>
				<?php if (((array_intersect($user_tender_depid, $lot_info['depids'])) != NULL && $user_tender_permission == 3 && $isNeedApproveSolution) || ($user_tender_depid[0] == 0 && $user_tender_permission == 3 && $isNeedApproveSolution)) : ?>
					<div class="form-group" id="participate-block">
						<div class="row">
							<div class="col-md-6">
								<textarea class="form-control" cols="2" type="text" id="participate-reason-textarea" placeholder="Причина, если вы не согласны с ответсвенным менеджером"></textarea>
							</div>
							<div class="col-md-12" style="margin-top: 20px;">
								<button type="submit" id="btn-submit" name="is_sign" class="btn btn-success btn-xs" onclick="isParticipate(1, <?= $lot_info['lotid'] ?>)">Участвуем</button>
								<button type="submit" id="btn-submit" name="is_sign" class="btn btn-danger btn-xs" onclick="isParticipate(0, <?= $lot_info['lotid'] ?>)">Не участвуем</button>
							</div>
						</div>
					</div>
				<?php endif ?>
			</div>
		<?php endif ?>



		<div class="col-md-12" style="background-color: white; padding-bottom: 25px; margin-top: 25px;">
			<div class="alert alert-info haction1" style="margin-top:15px;">Менеджер лота</div>
			<span style="border-bottom: 1px solid green;"><b>Действующий менеджер:</b> <?= Yii::$app->UserComponent->getById($curManager) ? Yii::$app->UserComponent->getById($curManager)['fullname'] . '(' . Yii::$app->UserComponent->getById($curManager)['dname'] . ')' : 'Не выбран' ?></span><br> <br> <br>
			<span><i>История всех менеджеров</i></span>
			<div class="table-responsive">
				<table class="table table-striped table-condensed" id="manager_history_table">
					<thead>
						<tr>
							<th>Инициатор</th>
							<th>Текущий менеджер</th>
							<th>Потенциальный менеджер</th>
							<th>Причина</th>
							<th>Дата инициации</th>
							<th>Утвердил</th>
							<th>Дата утверждения</th>
							<th>Утверждено</th>
						</tr>
					</thead>
					<tbody>
						<?php if ($manager_history) : ?>
							<?php foreach ($manager_history as $key => $value) : ?>
								<tr id="mh_id_<?= $value['id'] ?>">
									<td><?= Yii::$app->UserComponent->getById($value['mh_userid'])['fullname'] ?></td>
									<td><?= Yii::$app->UserComponent->getById($value['mh_user2id'])['fullname'] ?></td>
									<td><?= Yii::$app->UserComponent->getById($value['mh_user3id'])['fullname'] ?></td>
									<td><?= $value['mh_reason'] ?></td>
									<td><?= date('d-m-Y', $value['mh_created_at']) ?></td>
									<td><?= Yii::$app->UserComponent->getById($value['mh_user4id'])['fullname'] ?></td>
									<td><?= $value['mh_updated_at'] == 0 ? 'Не утвержден' : date('d-m-Y', $value['mh_updated_at']) ?></td>
									<?php if ($user_tender_permission == 3 && $user_tender_depid[0] == 0) : ?>
										<td><input type="checkbox" <?= $value['mh_status'] ? 'checked' : '' ?> onchange="approveChangeManager(<?= $value['id'] ?>, +this.checked, <?= $value['mh_lotid'] ?>, <?= $value['mh_user3id'] ?>, <?= $value['mh_user2id'] ?>)"></td>
									<?php else : ?>
										<td><input type="checkbox" disabled <?= $value['mh_status'] ? 'checked' : '' ?> /></td>
									<?php endif ?>
								</tr>
							<?php endforeach ?>
						<?php endif ?>
					</tbody>
				</table>
			</div>
			<?php if (($user_tender_permission == 3 && Yii::$app->params['can_delegate_manager'] == 0)) : ?>
				<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#new_manager_modal" data-whatever="@getbootstrap" style="margin-top: 25px;">Создать запрос на смену менеджера</button>
			<?php else : ?>
				<span>Вы можете самостоятельно переделегировать менеджера.</span>
			<?php endif ?>
			<div class="modal fade" id="new_manager_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel" style="color:red;"><b>Выберите менеджера, которому вы хотите делегировать данный лот, и укажите причину(обязательно)!</b></h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form>
								<div class="form-group">
									<label for="recipient-name" class="col-form-label">Менеджер:</label>
									<?= Select2::widget([
										'name' => 'userid',
										'value' => 0,
										'hideSearch' => true,
										'pluginOptions' => [
											'width' => '200px',
										],
										'data' => ArrayHelper::map($managers, 'userid', 'fullname'),
										'options' => ['multiple' => false, 'class' => 'inp01', 'id' => 'manager-modal'],
									]);
									?>
								</div>
								<div class="form-group">
									<label for="message-text" class="col-form-label">Причина</label>
									<textarea class="form-control required" id="new-manager-reason"></textarea>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
							<button type="button" class="btn btn-primary" onclick="newManager(<?= $lotid ?>,<?= $curManager ?>)">Создать</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12" style="background-color: white; padding-bottom: 25px; margin-top: 25px;">
			<div class="alert alert-info haction1" style="margin-top:15px;">Обеспечения</div>
			<div class="table-responsive">
				<table class="table table-striped table-condensed" id="provision">
					<thead>
						<tr>
							<th>Название</th>
							<th>Нужно ли обеспечение?</th>
							<th>Сумма</th>
							<th>Внесли ли обеспечение</th>
							<th>Возврат</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Обеспечения заявки</td>
							<td>
								<!-- <input type="checkbox" onchange="applicationIsNeed(<?= $lot_info['lotid'] ?>, this.checked)" title="Изменил: <?= $application_provision ? Yii::$app->UserComponent->getById($application_provision['ap_userid'])['fullname'] : '' ?> " <?= $application_provision['ap_is_need'] == 1 ? 'checked' : '' ?> <?= $disabled ?>> -->
								<!-- <?php echo $application_provision['ap_is_need'] ?> -->

								<?= Select2::widget([
									'name' => 'supply_app',
									'value' => $application_provision['ap_is_need'] == 0 || $application_provision['ap_is_need'] == 1 ? $application_provision['ap_is_need'] : 2,
									'hideSearch' => true,
									'data' => ArrayHelper::map([

										['id' => 0, 'title' => 'Не требуется'],
										['id' => 1, 'title' => 'Требуется'],
										['id' => 2, 'title' => 'Не указано']

									], 'id', 'title'),
									'options' => ['multiple' => false, 'class' => 'inp01', 'id' => 'supply_app'],
									'pluginOptions' => [],
									'pluginEvents' => [
										"select2:select" => "function(e) { 
											console.log('Selected:', e.params.data.id);
											applicationIsNeed({$lot_info['lotid']}, e.params.data.id);
											location.reload();
										}",
									]
								]);
								?>
							</td>
							<td>
								<p id="ap_amount"><?= $application_provision ? number_format($application_provision['ap_amount'], 2, ',', ' ') . ' тг' : '0,00 тг' ?></p>
								<input id="ap_amount_input" type="number" value="<?= $application_provision['ap_amount'] ?>" style="display: none;" <?= $disabled ?>>
							</td>
							<td><input type="checkbox" title="Изменил: <?= $application_provision ? Yii::$app->UserComponent->getById($application_provision['ap_managerid'])['fullname'] : '' ?> " onchange="applicationIsExist(<?= $lot_info['lotid'] ?>, this.checked)" <?= $application_provision['ap_is_exist'] == 1 ? 'checked' : '' ?> <?= $disabled ?>></td>
							<td><input type="checkbox" onchange="applicationIsReturn(<?= $lot_info['lotid'] ?>, this.checked)" <?= $application_provision['ap_is_return'] == 1 ? 'checked' : '' ?> <?= $disabled ?>></td>
						</tr>
						<tr>
							<td>Обеспечения исполнения договора</td>
							<td>
								<!-- <input type="checkbox" onchange="agreementIsNeed(<?= $lot_info['lotid'] ?>, this.checked)" title="Изменил: <?= $agreement_provision ? Yii::$app->UserComponent->getById($agreement_provision['lap_userid'])['fullname'] : '' ?> " <?= $agreement_provision['lap_is_need'] == 1 ? 'checked' : '' ?> <?= $disabled ?>> -->
								<!-- <?php echo $agreement_provision['lap_is_need'] ?> -->

								<?= Select2::widget([
									'name' => 'supply_agg',
									'value' => $agreement_provision['lap_is_need'] == 0 || $agreement_provision['lap_is_need'] == 1 ? $agreement_provision['lap_is_need'] : 2,
									'hideSearch' => true,
									'data' => ArrayHelper::map([

										['id' => 0, 'title' => 'Не требуется'],
										['id' => 1, 'title' => 'Требуется'],
										['id' => 2, 'title' => 'Не указано']

									], 'id', 'title'),
									'options' => ['multiple' => false, 'class' => 'inp01', 'id' => 'supply_agg'],
									'pluginOptions' => [],
									'pluginEvents' => [
										"select2:select" => "function(e) { 
											console.log('Selected:', e.params.data.id);
											agreementIsNeed({$lot_info['lotid']}, e.params.data.id);
											location.reload();
										}",
									],
								]);
								?>

							</td>
							<td>
								<p id="lap_amount"><?= $agreement_provision ? number_format($agreement_provision['lap_amount'], 2, ',', ' ') . ' тг' : '0,00 тг' ?></p>
								<input id="lap_amount_input" type="number" value="<?= $agreement_provision['lap_amount'] ?>" style="display: none;" <?= $disabled ?>>
							</td>
							<td><input type="checkbox" onchange="agreementIsExist(<?= $lot_info['lotid'] ?>, this.checked)" title="Изменил: <?= $agreement_provision ? Yii::$app->UserComponent->getById($agreement_provision['lap_managerid'])['fullname'] : '' ?> " <?= $agreement_provision['lap_is_exist'] == 1 ? 'checked' : '' ?> <?= $disabled ?>></td>
							<td><input type="checkbox" onchange="agreementIsReturn(<?= $lot_info['lotid'] ?>, this.checked)" <?= $agreement_provision['lap_is_return'] == 1 ? 'checked' : '' ?> <?= $disabled ?>></td>
						</tr>
					</tbody>
				</table>
				<?php if (($user_tender_permission == 1 || $user_tender_permission == 2)) : ?>
					<button class="btn btn-default btn-xs" id="change_amount_button" onclick="changeAmount()">Изменить суммы</button>
					<button class="btn btn-success btn-xs" id="save_amount_button" onclick="saveAmount(<?= $lot_info['lotid'] ?>)" style="display: none;">Сохранить</button>
				<?php endif ?>
			</div>
		</div>

		<div class="col-md-12" style="background-color: white; margin-top: 25px; padding-bottom:25px; ">
			<div class="alert alert-info haction1" style="margin-top:15px;">Чеклист</div>
			<div class="table-responsive">
				<table class="table table-striped table-condensed" id="table_checklist">
					<thead>
						<tr>
							<th>Наименование Отдела/Департамента</th>
							<th>Наименование/вид документа</th>
							<th>Документы</th>
							<th>Комментарии</th>
							<th>Действия</th>
						</tr>
					</thead>
					<tbody>
						<?php if (($user_tender_permission == 1 || $user_tender_permission == 2)) : ?>
							<?php foreach ($need_docs as $key => $value) : ?>
								<tr id="<?= 'checklist_' . $value['ld_id'] ?>">
									<td><?= $value['ld_dname'] ?></td>
									<td><?= $value['ld_doc_title'] ?></td>
									<td id="<?= 'checklist_file_' . $value['ld_id'] ?>" style="width: 300px;">
										<input name="doc_file" type="file" value="" id="<?= 'checklist_file_input_' . $value['ld_id'] ?>" class="inp01" accept=".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" onchange="uploadChecklistFile(<?= $value['ld_id'] ?>, <?= $lot_info['lotid'] ?>);">
										<?php foreach ($doc_files as $k => $v) : ?>
											<?php if ($v['df_ld_id'] == $value['ld_id']) : ?>
												<div class="<?= 'file_' . $v['df_id'] ?>">
													<div class="drive-item module">
														<div class="drive-item-inner module-inner">
															<div class="drive-item-title"><button type="button" id="remove" class="btn btn-outline-danger btn-xs" title="Удалить файл" onclick="removeFile(<?= $v['df_id'] ?>, <?= $lot_info['lotid'] ?>, <?= $lot_info['l_tenderid'] ?>)"><i class="fa fa-remove" aria-hidden="true" style="color:red;"></i></button><a target="_blank" href="<?= Url::toRoute(['check-list-ajax/download-file', 'id' => $v['df_id']]) ?>"><?= Yii::$app->File->pcgbasename($v['df_path']) ?></a></div>
														</div>
													</div>
												</div>
											<?php endif ?>
										<?php endforeach ?>
									</td>
									<td>
										<div class="<?= 'comment_' . $value['ld_id'] ?>">
											<i id="<?= 'comment_' . $value['ld_id'] ?>"><?= nl2br($value['ld_comment']) ?></i>
											<button class="btn btn-default btn-xs" style="float:right; margin-top: 20px;" onclick="comment(<?= $value['ld_id'] ?>)">Комментировать</button>
										</div>
										<div class="<?= 'comment_save_' . $value['ld_id'] ?>" style="display: none;">
											<textarea class="form-control" id="<?= 'comment_save_' . $value['ld_id'] ?>" rows="4" cols="50"><?= $value['ld_comment'] ?></textarea>
											<button class="btn btn-default btn-xs" style="float:right; margin-top: 20px;" onclick="saveComment(<?= $value['ld_id'] ?>)">Сохранить</button>
										</div>
									</td>
									<td><button class="btn btn-danger btn-xs" onclick="removeChecklist(<?= $value['ld_id'] ?>, <?= $lot_info['l_tenderid'] ?>)">Удалить</button></td>
								</tr>
							<?php endforeach ?>
						<?php else : ?>
							<?php foreach ($need_docs as $key => $value) : ?>
								<?php if ((in_array($value['ld_depid'], $user_tender_depid)) && $user_tender_permission == 4) : ?>
									<tr id="<?= 'checklist_' . $value['ld_id'] ?>">
										<td><?= $value['ld_dname'] ?></td>
										<td><?= $value['ld_doc_title'] ?></td>
										<td id="<?= 'checklist_file_' . $value['ld_id'] ?>" style="width: 200px;">
											<input name="doc_file" type="file" value="" id="<?= 'checklist_file_input_' . $value['ld_id'] ?>" class="inp01" accept=".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" onchange="uploadChecklistFile(<?= $value['ld_id'] ?>);">
											<?php foreach ($doc_files as $k => $v) : ?>
												<?php if ($v['df_ld_id'] == $value['ld_id']) : ?>
													<div class="<?= 'file_' . $v['df_id'] ?>">
														<div class="drive-item module">
															<div class="drive-item-inner module-inner">
																<div class="drive-item-title"><button type="button" id="remove" class="btn btn-outline-danger btn-xs" title="Удалить файл" onclick="removeFile(<?= $v['df_id'] ?>, <?= $lot_info['lotid'] ?>, <?= $lot_info['l_tenderid'] ?>)"><i class="fa fa-remove" aria-hidden="true" style="color:red;"></i></button><a target="_blank" href="<?= Url::toRoute(['check-list-ajax/download-file', 'id' => $v['df_id']]) ?>"><?= Yii::$app->File->pcgbasename($v['df_path']) ?></a></div>
															</div>
														</div>
													</div>
												<?php endif ?>
											<?php endforeach ?>
										</td>
										<td><?= $value['ld_comment'] ?></td>
									</tr>
								<?php elseif ($value['ld_depid'] == -1 && $authUser == $curManager) : ?>
									<tr id="<?= 'checklist_' . $value['ld_id'] ?>">
										<td><?= $value['ld_dname'] ?></td>
										<td><?= $value['ld_doc_title'] ?></td>
										<td id="<?= 'checklist_file_' . $value['ld_id'] ?>" style="width: 200px;">
											<input name="doc_file" type="file" value="" id="<?= 'checklist_file_input_' . $value['ld_id'] ?>" class="inp01" accept=".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" onchange="uploadChecklistFile(<?= $value['ld_id'] ?>, <?= $lot_info['lotid'] ?>);">
											<?php foreach ($doc_files as $k => $v) : ?>
												<?php if ($v['df_ld_id'] == $value['ld_id']) : ?>
													<div class="<?= 'file_' . $v['df_id'] ?>">
														<div class="drive-item module">
															<div class="drive-item-inner module-inner">
																<div class="drive-item-title"><button type="button" id="remove" class="btn btn-outline-danger btn-xs" title="Удалить файл" onclick="removeFile(<?= $v['df_id'] ?>, <?= $lot_info['lotid'] ?>, <?= $lot_info['l_tenderid'] ?>)"><i class="fa fa-remove" aria-hidden="true" style="color:red;"></i></button><a target="_blank" href="<?= Url::toRoute(['check-list-ajax/download-file', 'id' => $v['df_id']]) ?>"><?= Yii::$app->File->pcgbasename($v['df_path']) ?></a></div>
															</div>
														</div>
													</div>
												<?php endif ?>
											<?php endforeach ?>
										</td>
										<td><?= $value['ld_comment'] ?></td>
									</tr>
								<?php endif ?>
							<?php endforeach ?>
						<?php endif ?>
					</tbody>
				</table>
			</div>
			<?php if (($user_tender_permission == 1 || $user_tender_permission == 2) && ($curSpecialist == 0 || ($curSpecialist == $authUser))) : ?>
				<div class="col-md-12">
					<strong>Выберите подразделение и файлы, необходимые для загрузки выбранного подразделения.</strong>
					<hr>
					<div class="col-md-3">
						<?= Select2::widget([
								'name' => 'depid',
								'value' => 3,
								'data' => ArrayHelper::merge(['-1' => 'Менеджер'], ArrayHelper::map($deplist, 'depid', 'dname')),
								'options' => ['multiple' => false, 'class' => 'inp01', 'id' => 'depid'],
								'pluginEvents' => [
									'select2:select' => 'function() { getDocs(this.value) }'
								],
							]);
							?>
					</div>
					<div class="col-md-3">
						<?= Select2::widget([
								'name' => 'typeid',
								'value' => '0',
								'hideSearch' => true,
								'data' => ArrayHelper::map($documents, 'td_id', 'td_title'),
								'options' => ['multiple' => false, 'class' => 'inp01', 'id' => 'typeid'],
								'pluginOptions' => [],
							]);
							?>
					</div>
					<div class="col-md-3">
						<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#document_modal" data-whatever="@getbootstrap">Добавить свой документ</button>
					</div>
					<div class="col-md-9" style="margin-top: 20px;">
						<button class="btn btn-success btn-xs" onclick="addCheckList(<?= $lotid ?>);">Добавить</button>
					</div>
					<div class="modal fade" id="document_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel" style="color:red;"><b>Добавьте документ самостоятельно. При следующем добавлении, документ появится в общем списке.</b></h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form>
										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Подразделение:</label>
											<?= Select2::widget([
													'name' => 'depid',
													'value' => 3,
													'hideSearch' => true,
													'pluginOptions' => [
														'width' => '200px',
													],
													'data' => ArrayHelper::merge(['-1' => 'Менеджер'], ArrayHelper::map($deplist, 'depid', 'dname')),
													'options' => ['multiple' => false, 'class' => 'inp01', 'id' => 'depid-modal'],
												]);
												?>
										</div>
										<div class="form-group">
											<label for="message-text" class="col-form-label">Название/вид документа</label>
											<textarea class="form-control required" id="new-doc-modal"></textarea>
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
									<button type="button" class="btn btn-primary" onclick="createOwnDoc(<?= $lotid ?>)">Добавить</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endif ?>
		</div>


		<?php if (Yii::$app->UserComponent->getById($curManager)) : ?>
			<div class="col-md-12" style="background-color: white; padding-bottom: 25px; margin-top: 25px;">
				<div class="alert alert-info haction1" style="margin-top:15px;">Ценовое предложение</div>
				<div class="table-responsive">
					<table class="table table-striped table-condensed" id="table_checklist">
						<thead>
							<tr>
								<th>Ответсвенный менеджер</th>
								<th>Ценовое предложение</th>
								<th>Условная скидка</th>
								<th>Расчет</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<i><?= Yii::$app->UserComponent->getById($curManager)['fullname'] ?></i>
								</td>
								<td>
									<p id="po_price"><?= number_format($lot_info['po_price'], 2, ',', ' ') . ' тг'; ?></p>
									<input id="po_price_input" type="number" value="<?= $lot_info['po_price'] ?>" class="inp01" autocomplete="off" placeholder="Ценовое предложение" style="display: none;">
								</td>
								<td>
									<p id="po_discount"><?= $lot_info['po_discount'] . '%' ?></p>
									<input id="po_discount_input" type="number" value="<?= $lot_info['po_discount'] ?>" class="inp01" autocomplete="off" placeholder="Условная скидка(Укажите только цифру)" style="display: none;">
								</td>
								<td>
									<p id="po_total"><?= number_format($lot_info['po_total'], 2, ',', ' ') . ' тг' ?></p>
								</td>
								<td>
									<!-- <button class="btn btn-default btn-xs" id="po_change_button" style="float:right;" onclick="change_po(1)">Изменить</button>
										<button class="btn btn-success btn-xs" id="po_save_button" style="float:right; display: none;" onclick="save_po(<?= $lotid ?>)">Сохранить</button> -->
									<?php if ($authUser == $curManager) : ?>
										<button class="btn btn-default btn-xs" id="po_change_button" style="float:right;" onclick="change_po(1)">Изменить</button>
										<button class="btn btn-success btn-xs" id="po_save_button" style="float:right; display: none;" onclick="save_po(<?= $lotid ?>)">Сохранить</button>
									<?php elseif ($user_tender_permission == 1 || $user_tender_permission == 2 || $curSpecialist == 0 || ($curSpecialist == $authUser)) : ?>
										<button class="btn btn-default btn-xs" id="po_change_button" style="float:right;" onclick="change_po(2)">Изменить</button>
										<button class="btn btn-success btn-xs" id="po_save_button" style="float:right; display: none;" onclick="save_po(<?= $lotid ?>)">Сохранить</button>
									<?php endif ?>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

			</div>
		<?php endif ?>


		<div class="col-md-12" style="background-color: white; padding-bottom: 25px; margin-top: 25px;">
			<div class="alert alert-info haction1" style="margin-top:15px;">Протоколы</div>
			<div class="table-responsive">
				<table class="table table-striped table-condensed" id="table_protocols">
					<thead>
						<tr>
							<th>Название Протокола</th>
							<th>Документы</th>
							<th>Действия</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($lot_protocols as $key => $value) : ?>
							<tr id="<?= 'protocol_' . $value['id'] ?>">
								<td>
									<?= $value['prt_title'] ?>
								</td>
								<td id="<?= 'protocol_file_' . $value['id'] ?>">
									<input type="file" value="" id="<?= 'protocol_file_input_' . $value['id'] ?>" class="inp01" accept=".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" onchange="uploadProtocolFile(<?= $value['id'] ?>);">
									<?php foreach ($lot_protocol_files as $k => $v) : ?>
										<?php if ($v['pf_prt_id'] == $value['id']) : ?>
											<div class="<?= 'protocol_file_' . $v['pf_id'] ?>">
												<div class="drive-item module">
													<div class="drive-item-inner module-inner">
														<div class="drive-item-title"><button type="button" id="remove" class="btn btn-outline-danger btn-xs" title="Удалить файл" onclick="removeProtocolFile(<?= $v['pf_id'] ?>)"><i class="fa fa-remove" aria-hidden="true" style="color:red;"></i></button><a target="_blank" href="<?= Url::toRoute(['protocol-ajax/download-file', 'id' => $v['pf_id']]) ?>"><?= Yii::$app->File->pcgbasename($v['pf_path']) ?></a></div>
													</div>
												</div>
											</div>
										<?php endif ?>
									<?php endforeach ?>
								</td>
								<td>
									<button class="btn btn-danger btn-xs" onclick="removeProtocol(<?= $value['id'] ?>)">Удалить</button>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>

		<div class="col-md-12" style="background-color: white; padding-bottom: 25px; margin-top: 25px;">
			<div class="alert alert-info haction1" style="margin-top:15px;">Участники лота</div>
			<div class="table-responsive">
				<table class="table table-striped table-condensed" id="table_participant">
					<thead>
						<tr>
							<th>Название участника</th>
							<th>Ценовое предложение</th>
							<th>Условная скидка</th>
							<th>Рассчет</th>
							<th>Победитель</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($lot_participants as $key => $value) : ?>
							<tr id="<?= 'lot_participants_' . $value['lp_id'] ?>">
								<td><?= $value['lp_title'] ?></td>
								<td><?= number_format($value['lp_price'], 2, ',', ' ') . ' тг'; ?></td>
								<td><?= $value['lp_discount'] . '%' ?></td>
								<td><?= number_format($value['lp_total'], 2, ',', ' ') . ' тг';
										?></td>

								<?php if (($user_tender_permission == 1 || $user_tender_permission == 2) && ($curSpecialist == 0 || ($curSpecialist == $authUser))) : ?>
									<td><input type="checkbox" onchange="lotWinner(<?= $value['lp_id'] ?>, +this.checked)" <?= $value['lp_is_win'] == 1 ? 'checked' : '' ?> id="<?= 'lp_is_win_' . $value['lp_id'] ?>"></td>
									<td><button type="button" id="remove" class="btn btn-danger btn-xs" onclick="removeParticipant(<?= $value['lp_id'] ?>)" style="margin-left: 20px;">Удалить</button></td>
								<?php else : ?>
									<td><input type="checkbox" <?= $value['lp_is_win'] == 1 ? 'checked' : '' ?> id="<?= 'lp_is_win_' . $value['lp_id'] ?>"></td>
								<?php endif ?>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			<?php if (($user_tender_permission == 1 || $user_tender_permission == 2) && ($curSpecialist == 0 || ($curSpecialist == $authUser))) : ?>
				<div class="col-md-12">
					<strong>Добавьте всех участников лота, заполнив необходимые поля</strong>
					<hr>
					<div class="col-md-4">
						<input id="lp_title" type="hidden" value="" class="inp01" autocomplete="off" placeholder="Название участника">
						<?=
								Select2::widget([
									'name' => 'lp_competitor_id',
									'value' => '',
									'options' => [
										'class' => 'inp01', 'multiple' => false, 'id' => 'lp_competitor_id',
										'placeholder' => 'Конкурент', 'onchange' => 'setCompetitorTitle()'
									],
									'pluginOptions' => [
										'tags' => true,
										'allowClear' => true,
										'ajax' => [
											'url' => Url::to(['tender-ajax/search-competitor']),
											'dataType' => 'json',
											'data' => new JsExpression('function(params) { return {q:params.term}; }')
										],
										'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
										'templateResult' => new JsExpression('function(cs) { return [cs.cdid, cs.text]; }'),
										'templateSelection' => new JsExpression('function (cs) { return cs.text; }'),
									],
								]);
							?>
					</div>
					<div class="col-md-3">
						<input id="lp_price" type="number" value="" class="form-control" autocomplete="off" placeholder="Ценовое предложение">
					</div>
					<div class="col-md-2">
						<input id="lp_discount" type="number" value="" class="form-control" autocomplete="off" placeholder="Скидка" step="1">
					</div>
					<div class="col-md-9" style="margin-top: 20px;">
						<button class="btn btn-success btn-xs" onclick="addParticipant(<?= $lotid ?>);">Добавить</button>
					</div>
				</div>
			<?php endif ?>
		</div>

		<div class="col-md-12" style="background-color: white; padding-bottom: 25px; margin-top: 25px;">
			<div class="alert alert-info haction1" style="margin-top:15px;">Ссылки</div>
			<?php if ($lot_info['l_dealid'] != 0) : ?>
				<!-- <b>Ссылка на сделку в KCRM (ID сделки <?= $lot_info['l_dealid'] ?>):</b> <a target="_blank" href="<?= Yii::$app->params['main_site_domain'] . '/showdeal.php?q=1&q1=' . $lot_info['l_dealid'] . '&q2=' . $lot_info['link_to_deal'] ?>"><?= Yii::$app->params['main_site_domain'] . '/showdeal.php?q=1&q1=' . $lot_info['l_dealid'] . '&q2=' . $lot_info['link_to_deal'] ?></a><br><br> -->
				<b>Ссылка на сделку в KCRM (ID сделки <?= $lot_info['l_dealid'] ?>):</b> <a target="_blank" href="<?= Yii::$app->params['main_site_domain'] . 'view/?deals?show?' . $lot_info['link_to_deal'] . '?0?' . $lot_info['l_dealid'] ?>"><?= Yii::$app->params['main_site_domain'] . 'view/?deals?show?' . $lot_info['link_to_deal'] . '?0?' . $lot_info['l_dealid'] ?></a><br><br>
				<b>Ссылка на ТВ И ТР в KCRM:</b><a target="_blank" href="<?= Yii::$app->params['main_site_domain'] . '/showtreq.php?q=' . $lot_info['l_dealid'] . '&q1=' . $lot_info['link_to_deal'] . '' ?>"><?= Yii::$app->params['main_site_domain'] . '/showtreq.php?q=' . $lot_info['l_dealid'] . '&q1=' . $lot_info['link_to_deal'] . '' ?></a><br><br>
				<b>Ссылка на КомПред:</b><a target="_blank" href="<?= Yii::$app->params['main_site_domain'] . '/comoffer.php?df5e=' . base64_encode($lot_info['comoffer']['cmid']) . '&defa=' . $lot_info['link_to_deal'] . '&aace=' . base64_encode($lot_info['l_dealid']) . '' ?>"><?= Yii::$app->params['main_site_domain'] . '/comoffer.php?df5e=' . base64_encode($lot_info['comoffer']['cmid']) . '&defa=' . $lot_info['link_to_deal'] . '&aace=' . base64_encode($lot_info['l_dealid']) . '' ?></a>
			<?php endif ?>
		</div>
	</div>
</div>

<script>
	var risks = [];

	<?php

	foreach ($risks as $i => $risk)

		if (isset($risk['signers'])) {
			foreach ($risk['signers'] as $j => $signer) {
				echo 'console.log(' . json_encode($signer) . ');';
				echo 'risks.push({id: ' . $signer['s_userid'] . ' , name: "' . $signer['s_username'] . '" });';
			}
		}

	?>

	//console.log('Risks: ', risks);

	$(document).ready(function() {

		$("#notifications").slideToggle("fast");

		$("#notifications-bar").click(function() {

			$("#notifications").slideToggle("fast", function() {
				// Animation complete.
			});
		});
	});

	var toggleNotificationsIds = [];

	function toggleNotification(userId) {

		if (toggleNotificationsIds.indexOf(userId) === -1) {
			toggleNotificationsIds.push(userId)
		} else {
			toggleNotificationsIds = toggleNotificationsIds.filter(function(item) {
				return item != userId;
			});
		}

		console.log('toggleNotificationsIds: ', toggleNotificationsIds);
	}


	function sendNotifications() {

		console.log('sendNotifications');

		$.post({
			url: '/extra_modules/index.php?r=chat%2Fnotifications&lotId=<?= $lot_info['lotid'] ?>',
			data: {
				data: toggleNotificationsIds
			},
			ifModified: true,
			timeout: 2000,
			success: function(data, status) {
				console.log('sendNotifications success: ', data);

				$.toast({
					text: 'Оповещения успешно доставлены.',
					heading: 'Внимание!',
					showHideTransition: 'slide',
					loader: false,
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#1BA261',
					hideAfter: 3000,
				});
			},
			error: function(error, status) {
				console.log('sendNotifications error: ', error, status);

				$.toast({
					text: 'Не удалось отправить оповещения.',
					heading: 'Ошибка!',
					showHideTransition: 'slide',
					loader: false,
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#DD5145',
					hideAfter: 3000,
				});
			}
		});
	}

	function notifyUserForChat() {
		var selectedUser = $('#t_user').val();
		var lotId = '<?= $lot_info['lotid'] ?>';

		console.log('selectedUser: ', selectedUser, ' lotId: ', lotId);

		$.post({
			url: '/extra_modules/index.php?r=chat%2Fnotify-chat-user&userId=' + selectedUser + '&lotId=' + lotId,
			data: {
				data: [],
			},
			ifModified: true,
			timeout: 2000,
			success: function(data, status) {
				console.log('notifyUserForChat success: ', data);

				$.toast({
					text: 'Оповещения успешно доставлены.',
					heading: 'Внимание!',
					showHideTransition: 'slide',
					loader: false,
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#1BA261',
					hideAfter: 3000,
				});
			},
			error: function(error, status) {
				console.log('notifyUserForChat error: ', error, status);

				$.toast({
					text: 'Не удалось отправить оповещения.',
					heading: 'Ошибка!',
					showHideTransition: 'slide',
					loader: false,
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#DD5145',
					hideAfter: 3000,
				});
			}
		});
	}

</script>

<script src="<?= Yii::$app->params['module_folder_name'] ?>js/lot_info.js?v=5"></script>
<?php if (Yii::$app->session->hasFlash('sign_lot')) : ?>
	<script>
		function modal() {
			$.toast({
				text: '<?= Yii::$app->session->getFlash('sign_lot') ?>',
				heading: 'Внимание!',
				showHideTransition: 'slide',
				loader: false,
				loaderBg: '#9EC600',
				position: 'top-right',
				bgColor: '#EC971F',
				hideAfter: 20000,
			});
		}
	</script>
<?php endif; ?>
<?php if (Yii::$app->session->hasFlash('success')) : ?>
	<script>
		function modal() {
			$.toast({
				text: '<?= Yii::$app->session->getFlash('success') ?>',
				heading: 'Успешно',
				showHideTransition: 'slide',
				loader: false,
				loaderBg: '#9EC600',
				position: 'top-right',
				bgColor: '#1BA261'
			});
		}
	</script>
<?php endif; ?>
<?php if (Yii::$app->session->hasFlash('error')) : ?>
	<script>
		function modal() {
			$.toast({
				text: '<?= Yii::$app->session->getFlash('error') ?>',
				heading: 'Ошибка',
				showHideTransition: 'slide',
				loader: false,
				loaderBg: '#9EC600',
				position: 'top-right',
				bgColor: '#DD5145'
			});
		}
	</script>
<?php endif; ?>