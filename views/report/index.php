<?php
use yii\helpers\Url;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

$this->title = "Отчет";
$tender_permission = Yii::$app->auth->tenderPermission()['tender'];
if ($tender_permission != 1 || $tender_permission != 2) {
    $disabled = true;
} else {
    $disabled = false;
}

date_default_timezone_set('Asia/Almaty');
?>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h2>Выберите отчёт</h2>
            <div class="list-group">
                <a href="<?=Url::toRoute(['report/tender'])?>" class="list-group-item list-group-item-action" target="_blank">
                    Отчёт по тендерам
                </a>
                <a href="<?=Url::toRoute(['report/lot'])?>" class="list-group-item list-group-item-action" target="_blank">Отчёт по лотам</a>
                <a href="<?=Url::toRoute(['report/competitors'])?>" class="list-group-item list-group-item-action">Отчёт по конкурентам</a>
                <a href="<?=Url::toRoute(['report/full'])?>" class="list-group-item list-group-item-action">Полный отчет по лотам</a>
            </div>
        </div>
    </div>
</div>
