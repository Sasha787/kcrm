<?php
use kartik\select2\Select2;
use kartik\date\DatePicker;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = "Отчет";
$tender_permission = Yii::$app->auth->tenderPermission()['tender'];
if ($tender_permission != 1 || $tender_permission != 2) {
    $disabled = true;
} else {
    $disabled = false;
}

date_default_timezone_set('Asia/Almaty');
?>

<style>

.wrapper1, .wrapper2 {
  width: 100%;
}

.wrapper1 { height: 20px; }

.div1 {
  width: 1000px;
  height: 20px;
}

.table > thead > tr > th {
    background: #d8e8f7;
}

.sticky {
  position: fixed;
  top: 0;
  width: 200vw
}

/* Add some top padding to the page content to prevent sudden quick movement (as the header gets a new position at the top of the page (position:fixed and top:0) */
.sticky + .content {
  padding-top: 202px;
}

.inp01 {
    min-width: 200px;
}

</style>

<div class="row" style="width: 200vw;">
<div class="col-md-12" style="background-color: white"> <br>
        <div class="col-md-4">
            <div class="col-md-3">
                <span>Филиал</span>
                <div class="form-group">
                    <?=
                        Select2::widget([
                            'name' => 'depid',
                            'value' => 0,
                            'data' => ArrayHelper::map($deplist, 'depid', 'dname'),
                            'options' => ['multiple' => false, 'class' => 'inp01', 'placeholder' => 'Филиал', 'id' => 'depid'],
                            'pluginOptions' => [
                                'width' => '200px',
                            ],
                        ]);
                    ?>
                </div>
            </div>
            <div class="col-md-3">
                <span>Решение об участии</span>
                <div class="form-group">
                    <?=
                        Select2::widget([
                            'name' => 'isParticipate',
                            'value' => 0,
                            'data' => ArrayHelper::map($isParticipate, 'value', 'title'),
                            'options' => ['multiple' => false, 'class' => 'inp01', 'placeholder' => 'Решение об участии', 'id' => 'isParticipate'],
                            'pluginOptions' => [
                                'width' => '200px',
                            ],
                        ]);
                    ?>
                </div>
            </div>
            <div class="col-md-3">
                <span>Тип причины отказа</span>
                <div class="form-group">
                    <?=
                        Select2::widget([
                            'name' => 'cancelReason',
                            'value' => 0,
                            'data' => ArrayHelper::map($reasonsOptions, 'value', 'title'),
                            'options' => ['multiple' => false, 'class' => 'inp01', 'placeholder' => 'Тип причины отказа', 'id' => 'cancelReason'],
                            'pluginOptions' => [
                                'width' => '200px',
                            ],
                        ]);
                    ?>
                </div>
            </div>
            <div class="col-md-3">
                <span>Тип причины отклонения</span>
                <div class="form-group">
                    <?=
                        Select2::widget([
                            'name' => 'qfReason',
                            'value' => 0,
                            'data' => ArrayHelper::map($reasonsOptions, 'value', 'title'),
                            'options' => ['multiple' => false, 'class' => 'inp01', 'placeholder' => 'Тип причины отклонения', 'id' => 'qfReason'],
                            'pluginOptions' => [
                                'width' => '200px',
                            ],
                        ]);
                    ?>
                </div>
            </div>
        </div>
   </div>
   <div class="col-md-12" style="background-color: white"> <br>
        <div class="col-md-4">
            <div class="col-md-3">
                <span>Конкуренты</span>
                <div class="form-group">
                    <?=
                    Select2::widget([
                        'name' => 'contragent',
                        'value' => 0,
                        'data' => ArrayHelper::merge(['0' => 'Не выбрано'], ArrayHelper::map($competitors, 'competitor_id', 'competitor_title')),
                        'options' => ['multiple' => false, 'class' => 'inp01', 'placeholder' => 'Название', 'id' => 'competitor'],
                        'pluginOptions' => [
                            'width' => '200px',
                        ],
                    ]);
                    ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <span>Итоги закупки</span>
                                    <div class="form-group">
                                        <?=
                    Select2::widget([
                        'name' => 'conclusions',
                        'value' => 0,
                        'data' => ArrayHelper::map($conclusions, 'concid', 'conclusion_title'),
                        'options' => ['multiple' => false, 'class' => 'inp01', 'placeholder' => 'Итоги закупки', 'id' => 'conclusion'],
                        'pluginOptions' => [
                            'width' => '200px',
                        ],
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-md-3">
                <span>Период с</span>
                <div class="form-group">
                    <?=  DatePicker::widget([
                        'id' => 'period_from',
                        'name' => 'period_from',
                        'type' => DatePicker::TYPE_INPUT,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd-mm-yyyy',
                        ],
                        'options' => ['class' => 'inp01']
                    ]);
                    ?>
                </div>
            </div>

            <div class="col-md-3">
                <span>Период по</span>
                <div class="form-group">
                    <?=  DatePicker::widget([
                        'id' => 'period_to',
                        'name' => 'period_to',
                        'type' => DatePicker::TYPE_INPUT,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd-mm-yyyy',
                        ],
                        'options' => ['class' => 'inp01']
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
   <div class="col-md-12" style="background-color: white">
        <div class="col-md-4 col-md-offset-2">
            <div class="form-group">
                <button type="submit" class="btn btn-primary" onclick="report();">Применить фильтр</button>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="background-color: white">
        <table class="table table-striped table-bordered">
            <tr>
                <td><a id="column_id">ID</a></td>
                <td><a id="column_purchase_number" class="column_hide">№ (номер) закупки</a></td>
                <!-- <td><a id="column_t_csname" class="column_hide">Наименование Организатора/Заказчика закупок</a></td> -->
                <td><a id="column_purchase_title" class="column_hide">Вид закупки</a></td>
                <td><a id="column_purchase_name" class="column_hide">Наименование закупок</a></td>
                <td><a id="column_place" class="column_hide">Место поставки товара / выполнения работ / оказания услуг</a></td>
                <td><a id="column_budget" class="column_hide">Бюджет закупки, в тенге, без учета НДС</a></td>
                <td><a id="column_depnames" class="column_hide">Ответственное подразделение  Филиала / ГО АО KazTransCom</a></td>
                <td><a id="column_m_created_at" class="column_hide">Дата отправки на рассмотрение</a></td>
                <td><a id="column_participateDate" class="column_hide">Дата принятия решения</a></td>
                <td><a id="column_date_purchase" class="column_hide">Дата вскрытия</a></td>
                <td><a id="column_isParticipate" class="column_hide">Решение об участии</a></td>
                <td><a id="column_participants" class="column_hide">Перечень потенциальных поставщиков / участников закупки</a></td>
                <td><a id="column_ktc_cancel_reason" class="column_hide">Тип причины отказа</a></td>
                <td><a id="column_tcomment" class="column_hide">Комментарий ТВ и ТР</a></td>
                <td><a id="column_reason" class="column_hide">Детальная причина отказа</a></td>
                <td><a id="column_ktc_price" class="column_hide">Ценовое предложение АО KazTransCom, в тенге, без учета НДС</a></td>
                <td><a id="column_po_discount" class="column_hide">Условные скидки</a></td>
                <td><a id="column_participants" class="column_hide">Конкуренты</a></td>
                <td><a id="column_participants_prices" class="column_hide">Ценовое предложение конкурентов, в тенге, без учета НДС</a></td>
                <td><a id="column_conclusion_title" class="column_hide">Итоги закупки</a></td>
                <td><a id="column_winner_participants" class="column_hide">Победитель закупок</a></td>
                <td><a id="column_ktc_qf_reason" class="column_hide">Тип причины отклонения</a></td>
                <td><a id="column_ktc_reason" class="column_hide">Детальная причина отклонения</a></td>
            </tr>
        </table>
    </div>

    <div class="col-md-12" style="background-color: white;">
        <div class="wrapper2">
            <div class="table-responsive table-container" style="margin-top: 30px">
                <div></div>
                <?php Pjax::begin(['id' => 'report'])?>
                <?php

$provider->setSort(false);

echo GridView::widget([
    'dataProvider' => $provider,
    'tableOptions' => [
        'id' => 'main_table',
        'class' => 'table table-striped table-bordered',
        'style' => 'background: #fff;',
    ],
    'summary' => false,
    'rowOptions' => function ($lots) {
        return ['id' => 'tender_' . $lots['lotid']];
    },
    'options' => [
        'style' => 'overflow: auto; word-wrap: break-word;',
        'class' => 'sticky-header'
    ],
    'showOnEmpty' => false,
    'columns' => [
        [
            'label' => 'id',
            'encodeLabel' => false,
            'contentOptions' => ['class' => 'column_id'],
            'headerOptions' => ['class' => 'column_id'],
            'value' => function () {
                static $i = 1;
                return (string) $i++;
            },
        ],

        [
            'attribute' => 'purchase_number',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_purchase_number'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_purchase_number'],
            'label' => '№ (номер) закупки',
            'options' => ['id' => 'serial-column'],
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['purchase_number'];
            },
        ],

        [
            'attribute' => 't_csname',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_t_csname'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_t_csname'],
            'label' => 'Наименование Организатора/Заказчика закупок',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['t_csname'];
            },
            'contentOptions' => function ($lots) {
                return ['onclick' => 'window.open("' . Url::toRoute(['lot/view', 'id' => $lots['lotid']]) . '")'];
            },
        ],

        [
            'attribute' => 'purchase_title',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_purchase_title'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_purchase_title'],
            'label' => 'Вид закупки',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['purchase_title'];
            },
        ],

        [
            'attribute' => 'purchase_name',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_purchase_name'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_purchase_name'],
            'label' => 'Наименование закупок',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['purchase_name'];
            },
        ],

        [
            'attribute' => 'place',
            'headerOptions' => ['style' => 'max-width: 156px', 'class' => 'column_place'],
            'contentOptions' => ['style' => 'max-width: 156px;', 'class' => 'column_place'],
            'label' => 'Место поставки товара / выполнения работ / оказания услуг',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['place'];
            },
        ],

        [
            'attribute' => 'budget',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_budget'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_budget'],
            'label' => 'Бюджет закупки, в тенге, без учета НДС',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['budget'];
            },
        ],

        [
            'attribute' => 'depnames',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_depnames'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_depnames'],
            'label' => 'Ответственное подразделение  Филиала / ГО АО KazTransCom',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['depnames'];
            },
        ],

        [
            'attribute' => 'm_created_at',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_m_created_at'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_m_created_at'],
            'label' => 'Дата отправки на рассмотрение',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['m_created_at'];
            },
        ],

        [
            'attribute' => 'participateDate',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_participateDate'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_participateDate'],
            'label' => 'Дата принятия решения',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['participateDate'];
            },
        ],

        [
            'attribute' => 'date_purchase',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_date_purchase'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_date_purchase'],
            'label' => 'Дата вскрытия',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['date_purchase'];
            },
        ],

        [
            'attribute' => 'isParticipate',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_isParticipate'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_isParticipate'],
            'label' => 'Решение об участии',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['isParticipate'] ? 'Учавствуем' : 'Отказ';
            },
        ],

        [
            'attribute' => 'participants',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_participants'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_participants'],
            'label' => 'Перечень потенциальных поставщиков / участников закупки',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['participants'];
            },
        ],

        [
            'attribute' => 'ktc_qf_reason',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_ktc_cancel_reason'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_ktc_cancel_reason'],
            'label' => 'Тип причины отказа',
            'encodeLabel' => false,
            'value' => function ($lots) {
                $reasonsList = [0 => 'N/A', 1 => 'По квалификационной части', 2 => 'По технической части', 3 => 'По ценовой'];

                return $reasonsList[((int) $lots['ktc_cancel_reason'])];
            },
        ],

        [
            'attribute' => 'tcomment',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_tcomment'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_tcomment'],
            'label' => 'Комментарий ТВ и ТР',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['tcomment'];
            },
        ],

        [
            'attribute' => 'reason',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_reason'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_reason'],
            'label' => 'Детальная причина отказа',
            'encodeLabel' => false,
            'format' => 'raw',
            'value' => function ($lots) {

                $reason = '';

                $lots['ktc_reason_cancel'] ? $reason .= $lots['ktc_reason_cancel'] . "<br><br>" : '';
                $lots['reason'] ? $reason .= $lots['reason'] . "<br><br>" : '';
                $lots['participateReason'] ? $reason .= $lots['participateReason'] : '';

                return $reason;
            },
        ],

        [
            'attribute' => 'ktc_price',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_ktc_price'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_ktc_price'],
            'label' => 'Ценовое предложение АО KazTransCom, в тенге, без учета НДС',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['ktc_price'];
            },
        ],

        [
            'attribute' => 'po_discount',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_po_discount'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_po_discount'],
            'label' => 'Условные скидки',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['po_discount'];
            },
        ],

        [
            'attribute' => 'participants',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_participants'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_participants'],
            'label' => 'Конкуренты',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['participants'];
            },
        ],

        [
            'attribute' => 'participants_prices',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_participants_prices'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_participants_prices'],
            'label' => 'Ценовое предложение конкурентов, в тенге, без учета НДС',
            'encodeLabel' => false,
            'format' => 'raw',
            'value' => function ($lots) {
                return $lots['participants_prices'];
            },
        ],

        [
            'attribute' => 'conclusion_title',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_conclusion_title'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_conclusion_title'],
            'label' => 'Итоги закупки',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['conclusion_title'];
            },
        ],

        [
            'attribute' => 'winner_participants',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_winner_participants'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_winner_participants'],
            'label' => 'Победитель закупок',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['winner_participants'];
            },
        ],

        [
            'attribute' => 'ktc_qf_reason',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_ktc_qf_reason'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_ktc_qf_reason'],
            'label' => 'Тип причины отклонения',
            'encodeLabel' => false,
            'value' => function ($lots) {
                $reasonsList = [0 => 'N/A', 1 => 'По квалификационной части', 2 => 'По технической части', 3 => 'По ценовой'];
                return $reasonsList[(int) $lots['ktc_qf_reason']];
            },
        ],

        [
            'attribute' => 'ktc_reason',
            'headerOptions' => ['style' => 'max-width: 196px', 'class' => 'column_ktc_reason'],
            'contentOptions' => ['style' => 'max-width: 196px;', 'class' => 'column_ktc_reason'],
            'label' => 'Детальная причина отклонения',
            'encodeLabel' => false,
            'value' => function ($lots) {
                return $lots['ktc_reason'];
            },
        ],
    ],
]);
?>
<?php Pjax::end()?>
            </div>
        </div>
    </div>
</div>

<script>

    function download(){
        var today = new Date();
        var dd = today.getDate();

        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();
        if(dd<10)
        {
            dd='0'+dd;
        }

        if(mm<10)
        {
            mm='0'+mm;
        }
        today = mm+'-'+dd+'-'+yyyy;

        $("#main_table").tableExport({
          headers: true,
          footers: true,
          formats: ["xlsx"],
          fileName: today + "Отчёт по тендерам",
          bootstrap: false,
          exportButtons: true,
          position: "bottom",
          ignoreRows: null,
          ignoreCols: null,
          trimWhitespace: false
      });
    }

    function report()
    {
        var conclusion = btoa($('#conclusion').val());
        var contragent = btoa($('#contragent').val());

        var depid = btoa($('#depid').val());
        var isParticipate = btoa($('#isParticipate').val());
        var cancelReason = btoa($('#cancelReason').val());
        var qfReason = btoa($('#qfReason').val());
        var competitor = btoa($('#competitor').val());

        var periodFrom = btoa($('#period_from').val());
        console.log('periodFrom: ', periodFrom);

        var periodTo = btoa($('#period_to').val());
        console.log('periodTo: ', periodTo);

        $.pjax.reload({
            type: 'GET',
            container: "#report",
            url: "index.php?r=report/full",
            data: {
                conclusion: conclusion,
                contragent: contragent,

                depid: depid,
                isParticipate: isParticipate,
                cancelReason: cancelReason,
                qfReason: qfReason,
                competitor: competitor,

                periodFrom: periodFrom,
                periodTo: periodTo,
            }
        });
    }

</script>

<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>

<script>
    $(document).ready(function() {
        $('.column_hide').on('click', function(e) {
            var myClass = e.target.id;
            $('#main_table th.' + myClass + ', #main_table td.' + myClass + '').toggle();
        });

        window.onscroll = function() {myFunction()};

        // Get the header
        var header = $('.sticky-header table thead')[0]; //document.getElementById("myHeader");
        console.log('Header: ', header);

        var mainTable = $('#main_table')[0]; //document.getElementById("myHeader");
        console.log('mainTable: ', mainTable);

        // Get the offset position of the navbar
        var sticky = header.offsetTop;
        console.log('Sticky: ', sticky);

        var tableSticky = mainTable.offsetTop;
        console.log('tableSticky: ', tableSticky);

        // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
        /* function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
        } */
    });
</script>