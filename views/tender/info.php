<?php
use yii\helpers\Url;
use yii\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

$tender_permission = Yii::$app->auth->tenderPermission()['tender'];
$user_tender_depid = Yii::$app->auth->tenderPermission()['depid'];
$current_user = Yii::$app->auth->user();


$this->title = $tender['tenderid'];

?>

<!-- Изменить -->

<div class="row" style="margin-top: 10px;">
	<div class="col-md-6" style="background:#ffffff;border:10px solid #ffffff;">
		<table class="table">
			<tbody>
				<tr>
					<td colspan="2" class="haction1">Информация о Тендере<br><br>
					</td>
				</tr>
				<tr>
					<td class="td01">ID тендера</td>
					<td width="50%" class="td02">
						<?= $tender['tenderid'] ?>
					</td>
				</tr>
                <tr>
                    <td class="td01">Cпециалист</td>
                    <td width="50%" class="td02">
                        <?= $tender['tender_userid'] ? $tender['tender_username'] : 'Неизвестно' ?>
                    </td>
                </tr>
				<tr>
					<td class="td01">Дата направления объявления на рассмотрение</td><td class="td02">
						<?= date('d-m-Y', $tender['senddate']) ?>
					</td>
				</tr>
                <tr>
                    <td class="td01">Дата окончания обсуждения</td><td class="td02">
                        <?= date('d-m-Y', $tender['date_end_discuss']) ?>
                    </td>
                </tr>
				<tr>
                    <td class="td01">Установленная дата принятия решения об участии в закупке</td><td class="td02">
                        <?= date('d-m-Y', $tender['accept_solution']) ?>
                    </td>
                </tr>
                <tr>
                    <td class="td01">Дата вскрытия</td><td class="td02">
                        <?= date('d-m-Y', $tender['tender_date_purchase']) ?>
                    </td>
                </tr>
                
				<tr>
					<td width="50%" class="td01">Источник получения объявления</td><td width="50%" class="td02">
						<?= $tender['source_title'] ?>
					</td>
				</tr>
				<!--LEGAL_ONLY_STOP-->

				<tr>
					<td class="td01">ID Контрагента</td><td class="td02">
						<?= $tender['t_csid'] == 0 ? 'Неизвестно' : $tender['t_csid'] ?>
					</td>
				</tr>
				<tr>
					<td class="td01">БИН Организатора / Заказчика закупок</td><td class="td02">
						<?= $tender['t_csbin'] ?>
					</td>
				</tr>
                <tr>
                    <td class="td01">Тип предприятия</td><td class="td02">
                        <?= $tender['origname'] ?>
                    </td>
                </tr>
                 <tr>
                    <td class="td01">Форма собственности предприятия</td><td class="td02">
                        <?= $tender['ognname'] ?>
                    </td>
                </tr>
				<!--LEGAL_ONLY_START-->
				<tr>
					<td class="td01">Наименование Организатора/Заказчика закупок</td><td class="td02">
						<?= $tender['t_csname'] ?>
					</td>
				</tr>
                <tr>
                    <td class="td01">Сектор Организатора / Заказчика закупок</td><td class="td02">
                        <?= $tender['sector_title'] ?>
                    </td>
                </tr>
				<tr>
					<td class="td01">Вид закупки</td><td class="td02">
						<?= $tender['purchase_title'] ?>
					</td>
				</tr>
                <tr>
                    <td class="td01">Измененый вид закупки</td><td class="td02">
                        <?= $tender['tender_purchase_changed']['purchase_title'] ?>
                    </td>
                </tr>
                <tr>
                    <td class="td01">Консорциум</td><td class="td02">
                        <?= nl2br($tender['consortium']) ?>
                    </td>
                </tr>
				<tr>
					<td class="td01">№ (номер) закупки</td><td class="td02">
						<?= $tender['purchase_number'] ?>
					</td>
				</tr>
				<tr>
					<td class="td01">Наименование закупок</td><td class="td02">
						<?= $tender['purchase_name'] ?>
					</td>
				</tr>  
                 <tr>
                     <td class="td01">Файлы</td>
                     <td>
                        <?php foreach ($tender['files'] as $key => $value): ?>
                            <div class="drive-item module">
                                <div class="drive-item-inner module-inner">
                                    <div class="drive-item-title"><i class="fa fa-file-text-o text-primary"></i><a target="_blank" href="<?= Url::toRoute(['tender/download-file', 'id'=>$value['fileid']])?>"><?= Yii::$app->File->pcgbasename($value['path']) ?></a></div>
                                </div>
                            </div>
                        <?php endforeach ?>
                     </td>
                 </tr>          
			</tbody>
		</table>
        <a href="<?= Url::toRoute(['tender/edit', 'tenderid' => Yii::$app->request->get('id')])?>" class="btn btn-default" target="_blank">Изменить тендер</a>
	</div>
    <div class="col-md-6">
      <div class="col-md-12" style="background-color: white; padding-bottom: 25px;">
        <input type="hidden" id="tender_file_code" value="<?=$tender['file_code']?>">
            <div class="haction1" style="margin-top:15px;">Протоколы</div> <br><br><br><br>
            <div class="table-responsive">
                    <table class="table table-striped table-condensed" id="table_protocols">
                        <thead>
                            <tr>
                                <th>Название Протокола</th>
                                <th>Документы</th>
                                <th>Действия</th>                                     
                            </tr>
                        </thead>   
                        <tbody>
                        <?php foreach ($tender_protocols as $key => $value): ?>
                            <tr id="<?='protocol_'.$value['protocol_id']?>">
                                <td>
                                    <?=$value['protocol_title']?>
                                </td>
                                <td id="<?='protocol_file_'.$value['protocol_id']?>">
                                    <input type="file" value="" id="<?='protocol_file_input_'.$value['protocol_id']?>" class="inp01" accept=".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" onchange="uploadProtocolFile(<?=$value['protocol_id']?>, <?= $tender['file_code'] ?>, <?= $tender['tenderid'] ?>);">
                                    <?php foreach ($tender_protocol_files as $k => $v): ?>
                                        <?php if ($v['protocol_id'] == $value['protocol_id']): ?>
                                            <div class="<?= 'protocol_file_'.$v['protocol_file_id']?>">
                                                        <div class="drive-item module">
                                                            <div class="drive-item-inner module-inner">
                                                                <div class="drive-item-title"><button type="button" id="remove" class="btn btn-outline-danger btn-xs" title="Удалить файл" onclick="removeProtocolFile(<?=$v['protocol_file_id']?>, <?= $tender['tenderid'] ?>)"><i class="fa fa-remove" aria-hidden="true" style="color:red;"></i></button><a target="_blank" href="<?= Url::toRoute(['protocol-ajax/download-file', 'id'=>$v['protocol_file_id']])?>"><?= Yii::$app->File->pcgbasename($v['protocol_file_path'])?></a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </td>
                                <td>
                                    <button class="btn btn-danger btn-xs" onclick="removeProtocol(<?=$value['protocol_id']?>, <?= $tender['tenderid'] ?>)">Удалить</button>
                                </td>
                            </tr>
                        <?php endforeach ?>                                    
                        </tbody>
                    </table>
            </div>
            <?php if ($tender_permission == 1 || $tender_permission == 2): ?>
                    <div class="col-md-12" >
                        <strong>Выберите тип протокола.</strong><hr>
                        <div class="col-md-3">
                            <?= Select2::widget([
                                'name' => 'depid',
                                'value' => 0,
                                'data' => ArrayHelper::map($protocols, 'pd_id', 'pd_title'),
                                'options' => ['multiple'=>false, 'class' => 'inp01', 'id' => 'pd_id'],
                            ]);
                            ?>
                        </div><br><br><br>
                        <div class="col-md-3" style="margin-top: 20px;">
                            <button class="btn btn-success btn-xs" onclick="addProtocol(<?= $tender['tenderid'] ?>);">Добавить</button>
                        </div>
                    </div>
            <?php endif ?>
        </div>
    </div>
</div><br>
<div class="row" style="width:100%;background:#ffffff;border:10px solid #ffffff;">
	<div class="alert alert-secondary" role="alert">
		<span class="haction">Лоты</span>
        <a href="<?= Url::toRoute(['lot/create', 'tenderid' => Yii::$app->request->get('id')])?>" class="new_tender" title="Новый лот" >&nbsp;&nbsp;&nbsp;+&nbsp;&nbsp;&nbsp;</a>
	</div>
	<div class="col-md-12">
		<div class="table-responsive">
    	<?php 
    	echo GridView::widget([
    		'dataProvider' => $provider,
            'tableOptions' => [
                'id' => 'main_table',
                'class' => 'table table-striped table-bordered'
            ],
    		'columns' => [
                [ 
                    'attribute' => 'lotid',                                               
                    'header' => 'ID',
                    'value' => function ($lots) {
                        return $lots['lotid'];
                    },
                    'contentOptions' => function($lots)
                    {
                      return ['onclick' => 'window.open("' . Url::toRoute(['lot/view', 'id'=>$lots['lotid']]). '")'];
                    },
                ],	
    			[ 
    				'attribute' => 'lot_number',                                               
    				'header' => '№ (номер) Лота',
    				'value' => function ($lots) {
    					return $lots['lot_number'];
    				},
                    'contentOptions' => function($lots)
                    {
                      return ['onclick' => 'window.open("' . Url::toRoute(['lot/view', 'id'=>$lots['lotid']]). '")'];
                    },
    			],
    			[ 
    				'attribute' => 'lot_name',                                               
    				'header' => 'Наименование лота закупки',
    				'value' => function ($lots) {
    					return $lots['lot_name'];
    				},
                    'contentOptions' => function($lots)
                    {
                      return ['onclick' => 'window.open("' . Url::toRoute(['lot/view', 'id'=>$lots['lotid']]). '")'];
                    },

    			],
    			[ 
    				'attribute' => 'place',                                               
    				'header' => 'Место поставки товара',
    				'value' => function ($lots) {
    					return $lots['place'];
    				},
                    'contentOptions' => function($lots)
                    {
                      return ['onclick' => 'window.open("' . Url::toRoute(['lot/view', 'id'=>$lots['lotid']]). '")'];
                    },
    			],
    			[ 
    				'attribute' => 'date_purchase',                                               
    				'header' => 'Дата вскрытия',
    				'value' => function ($lots) {
    					return $lots['date_purchase'];
    				}, 
                    'contentOptions' => function($lots)
                    {
                      return ['onclick' => 'window.open("' . Url::toRoute(['lot/view', 'id'=>$lots['lotid']]). '")'];
                    },         
    				'format' => ['date', 'php:d.m.Y'],
    			],
    			[ 
    				'attribute' => 'isParticipate',                                               
    				'header' => 'Решение',
    				'value' => function ($lots) {
                        if ($lots['isParticipate'] == NULL) {
                            return 'Решение не принято';
                        } elseif ($lots['isParticipate'] == 1) {
                            return 'Участвуем';
                        } elseif ($lots['isParticipate'] == 0) {
                            return 'Не участвуем';
                        }
    				},
                    'contentOptions' => function($lots)
                    {
                        if ($lots['isParticipate'] == NULL) {
                           return [
                                'onclick' => 'window.open("' . Url::toRoute(['lot/view', 'id'=>$lots['lotid']]). '")',
                            ];
                        } elseif ($lots['isParticipate'] == 1) {
                            return [
                                'onclick' => 'window.open("' . Url::toRoute(['lot/view', 'id'=>$lots['lotid']]). '")',
                                'style' => 'color:white; background-color: '.Yii::$app->params['green']
                            ];
                        } elseif ($lots['isParticipate'] == 0) {
                            return [
                                'onclick' => 'window.open("' . Url::toRoute(['lot/view', 'id'=>$lots['lotid']]). '")',
                                'style' => 'color:white; background-color: '.Yii::$app->params['red']
                            ];
                        }
                      
                    },
    			],
    			[ 
    				'attribute' => 'lot_conclusion',                                               
    				'header' => 'Итоги закупки',
    				'value' => function ($lots) {
    					return $lots['conclusion_title'];
    				},
                    'contentOptions' => function($lots)
                    {
                      return ['onclick' => 'window.open("' . Url::toRoute(['lot/view', 'id'=>$lots['lotid']]). '")'];
                    },
    			],
                [ 
                // 'attribute' => 'blocked',                                               
                    'header' => 'Делегирование на подразделение',
                    'format' => 'raw',
                    'value' => function ($lots) use ($deplist, $tender_permission, $current_user, $user_tender_depid) {
                        if (((($tender_permission == 1 || $tender_permission == 2) && $lots['is_delegated'] == 0)) || ($tender_permission == 1)) {
                            return $lots['userids'] != NULL ?
                            Select2::widget([
                                'name' => 'is_affiliated',
                                'value' => explode(',', $lots['depids']),
                                'data' =>  ArrayHelper::map($deplist, 'depid', 'dname'),
                                'options' => ['multiple'=>true, 'class' => 'inp01', 'placeholder'=>'Выберите подразделения'],
                                'pluginOptions' => [
                                    'width' => '200px',
                                ],
                                'pluginEvents' => [
                                    'select2:select' => 'function(e) {
                                        delegateToDep('.$lots['lotid'].', e.params.data.id,'.$current_user['userid'].', "'.$current_user['fullname'].'","'.$lots['lot_number'].'", '.$lots['l_tenderid'].')
                                    }',
                                    'select2:unselect' => 'function(e) {
                                        delegateFromDep('.$lots['lotid'].', e.params.data.id, "'.$lots['lot_number'].'")
                                    }'
                                ],
                            ]).'<b>Делегировал(-и):</b><br>'.$lots['usernames'] :
                            Select2::widget([
                                'name' => 'is_affiliated',
                                'value' => explode(',', $lots['depids']),
                                'data' =>  ArrayHelper::map($deplist, 'depid', 'dname'),
                                'options' => ['multiple'=>true, 'class' => 'inp01', 'placeholder'=>'Выберите подразделения'],
                                'pluginOptions' => [
                                    'width' => '200px',
                                ],
                                'pluginEvents' => [
                                    'select2:select' => 'function(e) {
                                        delegateToDep('.$lots['lotid'].', e.params.data.id,'.$current_user['userid'].', "'.$current_user['fullname'].'","'.$lots['lot_number'].'", '.$lots['l_tenderid'].')
                                    }',
                                    'select2:unselect' => 'function(e) {
                                        delegateFromDep('.$lots['lotid'].', e.params.data.id, "'.$lots['lot_number'].'")
                                    }'
                                ],
                            ]) ;
                        } elseif ($tender_permission == 3 && $user_tender_depid[0] == 0) {
                            return $lots['userids'] != NULL ?
                            Select2::widget([
                                'name' => 'is_affiliated',
                                'value' => explode(',', $lots['depids']),
                                'data' =>  ArrayHelper::map($deplist, 'depid', 'dname'),
                                'options' => ['multiple'=>true, 'class' => 'inp01', 'placeholder'=>'Выберите подразделения'],
                                'pluginOptions' => [
                                    'width' => '200px',
                                ],
                                'pluginEvents' => [
                                    'select2:select' => 'function(e) {
                                        assignDepartment('.$lots['lotid'].', e.params.data.id, $(this).val(),'.$current_user['userid'].', "'.$current_user['fullname'].'")
                                    }',
                                    'select2:unselect' => 'function(e) {
                                        reAssignDepartment('.$lots['lotid'].', e.params.data.id, $(this).val())
                                    }'
                                ],
                            ]).'<b>Делегировал(-и):</b><br>'.$lots['usernames'] :
                            Select2::widget([
                                'name' => 'is_affiliated',
                                'value' => explode(',', $lots['depids']),
                                'data' =>  ArrayHelper::map($deplist, 'depid', 'dname'),
                                'options' => ['multiple'=>true, 'class' => 'inp01', 'placeholder'=>'Выберите подразделения'],
                                'pluginOptions' => [
                                    'width' => '200px',
                                ],
                                'pluginEvents' => [
                                    'select2:select' => 'function(e) {
                                        assignDepartment('.$lots['lotid'].', e.params.data.id, $(this).val(),'.$current_user['userid'].', "'.$current_user['fullname'].'")
                                    }',
                                    'select2:unselect' => 'function(e) {
                                        reAssignDepartment('.$lots['lotid'].', e.params.data.id, $(this).val())
                                    }'
                                ],
                            ]) ;
                        } else {
                            return $lots['userids'] != NULL ?
                            Select2::widget([
                                'name' => 'is_affiliated',
                                'value' => explode(',', $lots['depids']),
                                'hideSearch' => true,
                                'data' =>  ArrayHelper::map($deplist, 'depid', 'dname'),
                                'options' => ['multiple'=>true, 'class' => 'inp01', 'disabled'=>true, 'placeholder'=>'Выберите подразделения'],
                                'pluginOptions' => [
                                    'width' => '200px',
                                    'allowClear' => true
                                ],
                            ]). '<b>Делегировал(-и):</b><br>'.$lots['usernames'] :
                            Select2::widget([
                                'name' => 'is_affiliated',
                                'value' => explode(',', $lots['depids']),
                                'hideSearch' => true,
                                'data' =>  ArrayHelper::map($deplist, 'depid', 'dname'),
                                'options' => ['multiple'=>true, 'class' => 'inp01', 'disabled'=>true, 'placeholder'=>'Выберите подразделения'],
                                'pluginOptions' => [
                                    'width' => '200px',
                                    'allowClear' => true
                                ],
                            ]);
                        }
                    },            
                ],
    			[ 
                	'attribute' => 'blocked',                                               
    				'header' => 'Делегирование менеджеру',
    				'format' => 'raw',
    				'value' => function ($lots) use ($managers, $tender_permission, $current_user, $user_tender_depid) {
                        if(($tender_permission == 3 && $user_tender_depid[0] == 0) || ($tender_permission == 3 && Yii::$app->params['can_delegate_manager'] == 1)){
                            if ($lots['depids'] != NULL) {
                                foreach ($managers as $key => $value) {
                                    if (!in_array($value['depid'], explode(',', $lots['depids']))) {
                                        unset($managers[$key]);
                                    }
                                }
                            }
                            return Select2::widget([
                                    'name' => 'is_affiliated',
                                    'value' => $lots['m_managerid'],
                                    'data' => ArrayHelper::merge(['0'=>'Не выбран'], ArrayHelper::map($managers, 'userid', 'fullname')),
                                    'options' => ['multiple'=>false, 'class' => 'inp01', 'id' => 'manager_'.$lots['lotid']],
                                    'pluginOptions' => [
                                        'width' => '200px',
                                    ],
                                    'pluginEvents' => [
                                        'select2:select' => 'function(e) {
                                            assignManager('.$lots['lotid'].', e.params.data.id)
                                        }',
                                        'select2:unselect' => 'function(e) {
                                            reAssignManager('.$lots['lotid'].')
                                        }'
                                    ],
                                ]).'Менеджер из: <b>'.Yii::$app->UserComponent->getById($lots['m_managerid'])['dname'].'</b> <br>'. 'Когда делегировали: '.date('d-m-y', $lots['m_created_at']);
                            
                        } else {
                            return Select2::widget([
                                    'name' => 'is_affiliated',
                                    'value' => $lots['m_managerid'],
                                    'data' => ArrayHelper::merge(['0'=>'Не выбран'], ArrayHelper::map($managers, 'userid', 'fullname')),
                                    'options' => ['multiple'=>false, 'class' => 'inp01', 'disabled' => true, 'id' => 'manager_'.$lots['lotid']],
                                    'pluginOptions' => [
                                        'width' => '200px',
                                    ],
                                ]).'Менеджер из: <b>'.Yii::$app->UserComponent->getById($lots['m_managerid'])['dname'].'</b> <br>'. 'Когда делегировали: '.date('d-m-y', $lots['m_created_at']);
                        }
            		},            
    			],
                [
                    'attribute' => 'blocked',                                               
                    'header' => 'Статус сделки',
                    'format' => 'raw',
                    'contentOptions' => function($lots){
                        if (isset($lots['actdescr'])) {
                           return [
                                'style' => 'background-color:'.$lots['color'].';color:#FFFFFF;'
                            ];
                        } else {
                            return [
                                'style' => 'background-color:'.Yii::$app->params['red'].';color:#FFFFFF;',  
                            ];
                        }
                    },
                    'value' => function($lots){
                        if (isset($lots['actdescr'])) {
                          return '<b style="font-size:18px;">'.$lots['actdescr'].'</b><br>'.$lots['act2descr'];  
                        } else {
                            return '<b style="font-size:14px;">Сделка еще не начата</b>';
                        }             
                    }
                ],
                [
                    'attribute' => 'blocked',                                               
                    'header' => 'ТВ И ТР',
                    'format' => 'raw',
                    'visible' => true,
                    'contentOptions' => function($lots){
                        if (isset($lots['ishastv'])) {
                           return [
                                'style' => 'background-color:'.Yii::$app->params['green'].';color:#FFFFFF;'
                            ];
                        } else {
                            return [
                                'style' => 'background-color:'.Yii::$app->params['red'].';color:#FFFFFF;',  
                            ];
                        }
                    },
                    'value' => function($lots){
                        if (isset($lots['ishastv'])) {
                          return '<b style="font-size:18px;">Есть ТВ И ТР</b><br>';  
                        } else {
                            return '<b style="font-size:14px;">ТВ И ТР отсутствует</b>';
                        }             
                    }
                ],
    		],
    	]);
    	?>
        </div><br>
	</div>
</div>

<script>
    function isParticipate(lotid, decision)
    {
        $.ajax({
            url: 'index.php?r=lot-ajax%2Fis-participate',
            type: 'POST',
            data : { 
                lotid: lotid,
                decision: +decision,
            },
            success: function(data){
                console.log(data);
                $.toast({
                    text : 'Решение принято!',
                    heading: 'Успешно',
                    showHideTransition: 'slide',
                    loader: false,
                    loaderBg: '#9EC600',
                    position: 'top-right',
                    bgColor: '#1BA261'
                });
        }});
    }

    function assignDepartment(lotid, curDep, deps, userid, fullname)
    {
        $.ajax({
            url: 'index.php?r=lot-ajax%2Fassign-dep',
            type: 'POST',
            data : { 
                lotid: lotid,
                curDep: curDep,
                deps: deps,
                userid: userid,
                fullname: fullname,
            },
            success: function(data){
                // console.log(data);
                var newOptions = data.managers;
                var $el = $("#manager_"+lotid);
                $el.empty(); 
                $el.append($("<option></option>").attr("value", 0).text("Не выбран"))
                $.each(newOptions, function(key,value) {
                    $el.append($("<option></option>")
                        .attr("value", value.userid).text(value.fullname));
                });

                $.toast({
                    text : 'Лот делегирован на подразделение!',
                    heading: 'Успешно',
                    showHideTransition: 'slide',
                    loader: false,
                    loaderBg: '#9EC600',
                    position: 'top-right',
                    bgColor: '#1BA261'
                });
        }});
    }

    function reAssignDepartment(lotid, curDep, deps)
    {
        $.ajax({
            url: 'index.php?r=lot-ajax%2Fre-assign-dep',
            type: 'POST',
            data : { 
                lotid: lotid,
                curDep: curDep,
                deps: deps,
            },
            success: function(data){
                console.log(data);
                var newOptions = data.managers;
                var $el = $("#manager_"+lotid);
                $el.empty(); 
                $el.append($("<option></option>").attr("value", 0).text("Не выбран"))
                $.each(newOptions, function(key,value) {
       
                $el.append($("<option></option>")
                        .attr("value", value.userid).text(value.fullname));
                });
                $.toast({
                    text : 'Лот отозван от подразделения!',
                    heading: 'Успешно',
                    showHideTransition: 'slide',
                    loader: false,
                    loaderBg: '#9EC600',
                    position: 'top-right',
                    bgColor: '#1BA261'
                });
        }});
    }

    function assignManager(lotid, userid)
    {
         $.ajax({
            url: 'index.php?r=lot-ajax%2Fassign-manager',
            type: 'POST',
            data : { 
                lotid: lotid,
                managerid: userid,
            },
            success: function(data){
                console.log(data);
                $.toast({
                    text : 'Менеджер назначен на данный лот!',
                    heading: 'Успешно',
                    showHideTransition: 'slide',
                    loader: false,
                    loaderBg: '#9EC600',
                    position: 'top-right',
                    bgColor: '#1BA261'
                });
            }});
    }

    function reAssignManager(lotid)
    {
         $.ajax({
            url: 'index.php?r=lot-ajax%2Fre-assign-manager&lotid=' + lotid,
            type: 'POST',
            success: function(data){
                console.log(data);
                $.toast({
                    text : 'Менеджер отозван из данного лота!.',
                    heading: 'Успешно',
                    showHideTransition: 'slide',
                    loader: false,
                    loaderBg: '#9EC600',
                    position: 'top-right',
                    bgColor: '#1BA261'
                });
            }});
    }

    function delegateFromDep(lotid, depid, lotnum)
    {
       $.ajax({
            url: 'index.php?r=lot-ajax%2Fdelegate-from-dep',
            type: 'POST',
            data: { 
                lotid: lotid,
                depid: depid,
            },
            success: function(data){
                console.log(data);
                $.toast({
                    text : 'Лот № '+lotnum+' успешно отозван.',
                    heading: 'Успешно',
                    showHideTransition: 'slide',
                    loader: false,
                    loaderBg: '#9EC600',
                    position: 'top-right',
                    bgColor: '#1BA261'
                });
            },

            error: function(data){
                $.toast({
                    text : data,
                    heading: 'Не удалось отозвать лот',
                    showHideTransition: 'slide',
                    loader: false,
                    loaderBg: '#9EC600',
                    position: 'top-right',
                    bgColor: '#DD5145'
                });
            }
        });
    }
	function delegateToDep(lotid, depid, userid, fullname,lotnum, tenderid)
	{      
		$.ajax({
			url: 'index.php?r=lot-ajax%2Fdelegate-to-dep&tenderid='+tenderid,
			type: 'POST',
			data: { 
				lotid: lotid,
				depid: depid,
				userid: userid,
                fullname: fullname,
			},
			success: function(data){
				console.log(data);
                if (data.delegateStatus == 100) {
                    $('#manager_'+lotid).val(data.userid).change();
                    $.toast({
                        text : 'Лот № '+lotnum+' успешно делегирован менеждеру, который уже имеет доступ к контрагенту.',
                        heading: 'Успешно',
                        showHideTransition: 'slide',
                        loader: false,
                        loaderBg: '#9EC600',
                        position: 'top-right',
                        bgColor: '#1BA261'
                    });
                } else if(data.delegateStatus == 200){
                    $('#manager_'+lotid).val(data.userid).change();
                    $.toast({
                        text : 'Лот № '+lotnum+' успешно делегирован рандомному менеджеру из данного подразделения',
                        heading: 'Успешно',
                        showHideTransition: 'slide',
                        loader: false,
                        loaderBg: '#9EC600',
                        position: 'top-right',
                        bgColor: '#1BA261'
                    });
                } else if (data.delegateStatus == 300) {
                    $.toast({
                        text : 'Лот № '+lotnum+' успешно делегирован на подраздление!',
                        heading: 'Успешно',
                        showHideTransition: 'slide',
                        loader: false,
                        loaderBg: '#9EC600',
                        position: 'top-right',
                        bgColor: '#1BA261'
                    });
                } else if (data.delegateStatus == 400) {

                    $.toast({
                        text : 'Выберите сектор организатора/ Заказчика закупок',
                        heading: 'Лот не делегирован!',
                        showHideTransition: 'slide',
                        loader: false,
                        loaderBg: '#9EC600',
                        position: 'top-right',
                        bgColor: '#DD5145'
                    });

                } else {
                    $.toast({
                        text : 'Что-то пошло не так :(',
                        heading: 'Не удалось делегировать лот',
                        showHideTransition: 'slide',
                        loader: false,
                        loaderBg: '#9EC600',
                        position: 'top-right',
                        bgColor: '#DD5145'
                    });
                }
			},

			error: function(data){
				$.toast({
					text : data,
					heading: 'Не удалось делегировать лот',
					showHideTransition: 'slide',
					loader: false,
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#DD5145'
				});
			}
		});
	}
</script>

<script>

function addProtocol(tenderid)
{
    var pd_id = $('#pd_id').val();
    var protocol_title =$('#pd_id').select2('data')[0].text;

    var data = {
        pd_id: pd_id,
        protocol_title: protocol_title,
    };

    $.ajax({
        url: 'index.php?r=protocol-ajax%2Fcreate&tenderid=' + tenderid,
        type: 'POST',
        datatype:'json',
        data : data,
        success: function(result){

            if (result == false) {
                $.toast({
                    text : 'Что-то пошло не так:(',
                    heading: 'Ошибка',
                    showHideTransition: 'slide',
                    loader: false,
                    loaderBg: '#9EC600',
                    position: 'top-right',
                    bgColor: '#DD5145'
                });
            } else {
                var row = $('<tr id="protocol_'+result.protocol_id+'" />')
                $('#table_protocols').append(row);
                row.append($('<td>'+result.protocol_title+'</td>')); 
                row.append($('<td id="protocol_file_'+result.protocol_id+'"><input name="doc_file" type="file" value="" id="protocol_file_input_'+result.protocol_id+'" class="inp01" accept=".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" onchange="uploadProtocolFile('+result.protocol_id+', '+tenderid+');"></td>'));
                row.append($('<td><button class="btn btn-danger btn-xs" onclick="removeProtocol('+result.protocol_id+', '+tenderid+')">Удалить</button></td>')); 
                    $.toast({
                        text : 'Протокол добавлен',
                        heading: 'Успешно',
                        showHideTransition: 'slide',
                        loader: false,
                        loaderBg: '#9EC600',
                        position: 'top-right',
                        bgColor: '#1BA261'
                    });
                }

        },
        error: function(result){
            $.toast({
                    text : 'Что-то пошло не так:(',
                    heading: 'Ошибка',
                    showHideTransition: 'slide',
                    loader: false,
                    loaderBg: '#9EC600',
                    position: 'top-right',
                    bgColor: '#DD5145'
                });
        }
        });
}

function uploadProtocolFile(id, file_code, tenderid)
{
    var formData = new FormData();
    formData.append('file', $('#protocol_file_input_'+id)[0].files[0]);
    formData.append('protocol_id', id);
    formData.append('file_code', $('#tender_file_code').val());

    $.ajax({
        url: 'index.php?r=protocol-ajax%2Fupload-protocol-file&tenderid='+tenderid,
        type: 'POST',
        mimeType: 'multipart/form-data',
        data: formData,   
        datatype:'json',
        cache: false,
        contentType: false,
        processData: false,

        success: function (data) {
            var result = JSON.parse(data);

            $('#protocol_file_input_'+id).val("");

            var row = $('<div class="protocol_file_'+result.protocol_file_id+'"/>')
            $('#protocol_file_'+id).append(row);
            row.append($('<div class="file_'+result.protocol_file_id+'"><div class="drive-item module"><div class="drive-item-inner module-inner"><div class="drive-item-title"><button type="button" id="remove" class="btn btn-outline-danger btn-xs" title="Удалить файл" onclick="removeProtocolFile('+result.protocol_file_id+', '+tenderid+')"><i class="fa fa-remove" aria-hidden="true" style="color:red;"></i></button><a target="_blank" href="index.php?r=protocol-ajax%2Fdownload-file&id='+result.protocol_file_id+'">'+result.protocol_file_name+'</a></div></div></div></div>'));
            $.toast({
                text : 'Файл успешно загружен',
                heading: 'Добавлен',
                showHideTransition: 'slide',
                loader: false,
                loaderBg: '#9EC600',
                position: 'top-right',
                bgColor: '#1BA261',
                hideAfter : 3000,
            });    
        },

        error: function(err) {
            $.toast({
                text : 'Не удалось загрузить файл',
                heading: 'Ошибка',
                showHideTransition: 'slide',
                loader: false,
                loaderBg: '#9EC600',
                position: 'top-right',
                bgColor: '#DD5145',
                hideAfter : 7000,
            });
        }
    });
}


function removeProtocolFile(id, tenderid)
{
    $(".protocol_file_"+id).remove();

    $.ajax({
        url: 'index.php?r=protocol-ajax%2Fremove-file&id=' + id + '&tenderid=' + tenderid,
        type: 'POST',
        success: function(data){
            console.log(data);
            $.toast({
                text : 'Файл успешно удален.',
                heading: 'Удален',
                showHideTransition: 'slide',
                loader: false,
                loaderBg: '#9EC600',
                position: 'top-right',
                bgColor: '#1BA261',
                hideAfter : 3000,
            }); 
        }
    });
    
}

function removeProtocol(id, tenderid)
{
    $("#protocol_"+id).remove();

    $.ajax({
        url: 'index.php?r=protocol-ajax%2Fremove-protocol&id=' + id + '&tenderid=' + tenderid,
        type: 'POST',
        success: function(data){
            console.log(data);
            $.toast({
                text : 'Файл успешно удален.',
                heading: 'Удален',
                showHideTransition: 'slide',
                loader: false,
                loaderBg: '#9EC600',
                position: 'top-right',
                bgColor: '#1BA261',
                hideAfter : 3000,
            }); 
        }
    });

}
</script>
<?php if (Yii::$app->session->hasFlash('success')): ?>
    <script>
        function modal(){
            $.toast({
                text : '<?= Yii::$app->session->getFlash('success') ?>',
                heading: 'Успешно',
                showHideTransition: 'slide',
                loader: false,
                loaderBg: '#9EC600',
                position: 'top-right',
                bgColor: '#1BA261'
            });
        }
    </script>
<?php endif; ?>
<?php if (Yii::$app->session->hasFlash('error')): ?>
    <script>
        function modal(){
            $.toast({
                text : '<?= Yii::$app->session->getFlash('error') ?>',
                heading: 'Ошибка',
                showHideTransition: 'slide',
                loader: false,
                loaderBg: '#9EC600',
                position: 'top-right',
                bgColor: '#DD5145'
            });
        }
    </script>
<?php endif; ?>