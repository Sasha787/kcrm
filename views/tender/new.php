<?php
use yii\helpers\Url;
use kartik\date\DatePicker;
// use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

?>
<!-- 
<div class="row">
	<div class="col-md-6">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Общая информация</a></li>
				</ul>
			</div>
		</nav>
	</div>
</div> -->

<div class="row" style="margin-top: 20px;">
	<div class="col-md-6">
		<form action="<?= Url::toRoute(['tender/store'])?>" method="POST" onkeypress="return event.keyCode!=13">
			<input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
			<input id="tmp_code" name="tmp_code" type="hidden" value="<?= Yii::$app->params['tmp_code']?>">
			<table class="table" style="width:100%;background:#ffffff;border:10px solid #ffffff;">
				<tbody>
					<tr>
						<td colspan="2" class="haction1">Информация о Тендере<br><br>
						</td>
					</tr>
					<tr>
						<td class="td01">Дата направления объявления на рассмотрение</td>
						<td class="td02">
							<?=  DatePicker::widget([
								'name' => 'senddate',
								'type' => DatePicker::TYPE_INPUT,
								'value' => date('d-m-Y'),
								'pluginOptions' => [
									'autoclose'=>true,
									'format' => 'dd-mm-yyyy',
								],
								'options' => ['class' => 'inp01']
							]);
							?>
						</td>
					</tr>
					<tr>
						<td class="td01">Дата окончания обсуждения</td><td class="td02">
							<?=  DatePicker::widget([
								'name' => 'date_end_discuss',
								'type' => DatePicker::TYPE_INPUT,
								'value' => date('d-m-Y'),
								'pluginOptions' => [
									'autoclose'=>true,
									'format' => 'dd-mm-yyyy',
								],
								'options' => ['class' => 'inp01']
							]);
							?>
						</td>
					</tr>
					<tr>
              <td class="td01">Установленная дата принятия решения об участии в закупке</td><td class="td02">
                  <?=  DatePicker::widget([
                      'name' => 'accept_solution',
                      'type' => DatePicker::TYPE_INPUT,
                      'value' => date('d-m-Y'),
                      'pluginOptions' => [
                          'autoclose'=>true,
                          'format' => 'dd-mm-yyyy',
                      ],
                      'options' => ['class' => 'inp01']
                  ]);
                  ?>
              </td>
          </tr>
           <tr>
              <td class="td01">Дата вскрытия</td><td class="td02">
                  <?=  DatePicker::widget([
                      'name' => 'date_purchase',
                      'type' => DatePicker::TYPE_INPUT,
                      'value' => date('d-m-Y'),
                      'pluginOptions' => [
                          'autoclose'=>true,
                          'format' => 'dd-mm-yyyy',
                      ],
                      'options' => ['class' => 'inp01']
                  ]);
                  ?>
              </td>
          </tr>
					<tr>
						<td width="50%" class="td01">Источник получения объявления</td><td width="50%" class="td02">
							<?= 
								Select2::widget([
									'name' => 'source_get',
								    'value' => 0,
								    'data' => ArrayHelper::map($sources, 'sourceid', 'source_title'),
								    'maintainOrder' => true,
								    'options' => ['class' => 'inp01', 'multiple' => false],
								    'pluginOptions' => [
								    	'tags' => true,
								    ],
								]);
							?>
						</td>
					</tr>
					<tr>
						<td class="td01">БИН Организатора / Заказчика закупок <span style="color:red">*</span></td><td class="td02">
						<?= 
							Select2::widget([
								'name' => 't_csbin',
							    'value' => '',
							    'options' => ['placeholder' => 'Введите БИН', 'onchange'=>'setCsIdAndName(this);', 'id'=>'t_csbin', 'type' => 'number'],
							    'pluginOptions' => [
							    	'tags' => true,
							    	'allowClear' => true,
							    	'minimumInputLength' => 5,
							    	'maximumInputLength' => 12,
							    	'language' => [
							    		'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
							    		'inputTooShort' => new JsExpression("function () { return 'Введите минимум 5 символов'; }"),
							    	],
							    	'ajax' => [
							    		'url' => Url::to(['tender-ajax/search-bin']),
							    		'dataType' => 'json',
							    		'data' => new JsExpression('function(params) { return {q:params.term}; }')
							    	],
							    	'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
							    	'templateResult' => new JsExpression('function(cs) { return [cs.cdid, cs.text]; }'),
							    	'templateSelection' => new JsExpression('function (cs) { return cs.text; }'),
							    ],
							]);
						?>
						</td>
					</tr>
					<input id="t_csid" type = "hidden" name = "t_csid" value = "0" />
					<input id="t_csbin_text" type = "hidden" name = "t_csbin_text" value = "" />
					<input id="t_csname_text" type = "hidden" name = "t_csname_text" value = "" />
					<tr>
						<td class="td01">Тип предприятия</td>
						<td class="td02"><?= Select2::widget([
									'name' => 'origid',
								    'value' => 1,
								    'data' => ArrayHelper::map($origlist, 'origid', 'origname'),
								    'maintainOrder' => true,
								    'options' => ['class' => 'inp01', 'multiple' => false],
								    'pluginOptions' => [
								    	'tags' => true,
								    ],
								]); ?></td>
					</tr>
					<tr>
						<td class="td01">Форма собственности предприятия</td>
						<td class="td02"><?= Select2::widget([
									'name' => 'ognid',
								    'value' => 0,
								    'data' => ArrayHelper::map($ognlist, 'ognid', 'ognname'),
								    'maintainOrder' => true,
								    'options' => ['class' => 'inp01', 'multiple' => false],
								    'pluginOptions' => [
								    	'tags' => true,
								    ],
								]); ?></td>
					</tr>
					<tr>
						<td class="td01">Наименование Организатора/Заказчика закупок <span style="color:red">*</span></td><td class="td02"><?=

							Select2::widget([
								'name' => 't_csname',
							    'value' => '',
							    'options' => ['class' => 'inp01', 'multiple' => false, 'id'=>'t_csname',
							    'placeholder'=>'Введите название', 'onchange' => 'setCsName(this)'],
							    'pluginOptions' => [
							    	'tags' => true,
							    	'allowClear' => true,
							    	// 'minimumInputLength' => 5,
							    	'language' => [
							    		'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
							    		'inputTooShort' => new JsExpression("function () { return 'Введите минимум 5 символов'; }"),
							    	],
							    	'ajax' => [
							    		'url' => Url::to(['tender-ajax/search-cs']),
							    		'dataType' => 'json',
							    		'data' => new JsExpression('function(params) { return {q:params.term}; }')
							    	],
							    	'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
							    	'templateResult' => new JsExpression('function(cs) { return [cs.cdid, cs.text]; }'),
							    	'templateSelection' => new JsExpression('function (cs) { return cs.text; }'),
							    ],
							]);
							?>		
						</td>
					</tr>
					<tr>
						<td class="td01">Сектор Организатора / Заказчика закупок</td><td class="td02">
							<?= 
							Select2::widget([
								'name' => 'sector',
							    'value' => '',
							    'data' => ArrayHelper::map($sectors, 'sectorid', 'sector_title'),
							    'maintainOrder' => true,
							    'hideSearch' => true,
							    'options' => ['class' => 'inp01', 'multiple' => false, ],
							    'pluginOptions' => [
							    	'tags' => true,

							    ],
							]);
							?>
						</td>
					</tr>
					<tr>
						<td class="td01">Вид закупки</td><td class="td02">
							<?= 
							Select2::widget([
								'name' => 't_purchase',
							    'value' => '',
							    'data' => ArrayHelper::map($purchases, 'purchaseid', 'purchase_title'),
							    'maintainOrder' => true,
							    'hideSearch' => true,
							    'options' => ['class' => 'inp01', 'multiple' => false, 'id'=>'t_purchase'],
							    'pluginOptions' => [
							    	'tags' => true,
							    	'maximumInputLength' => 20
							    ],
							]);
							?>
						</td>
					</tr>
					<tr>
						<td class="td01">Измененный вид закупки</td><td class="td02">
							<?= 
							Select2::widget([
								'name' => 'purchase_type_changed',
							    'value' => '',
							    'data' => ArrayHelper::map($purchases, 'purchaseid', 'purchase_title'),
							    'maintainOrder' => true,
							    'hideSearch' => true,
							    'options' => ['class' => 'inp01', 'multiple' => false, 'id'=>'t_purchase'],
							    'pluginOptions' => [
							    	'tags' => true,
							    	'maximumInputLength' => 20
							    ],
							]);
							?>
						</td>
					</tr>
					<tr>
                        <td class="td01">Консорциум</td><td class="td02">
                            <textarea name="consortium" type="text" id="consortium" class="inp01" autocomplete="off" cols="30" rows="3"></textarea>
                        </td>
                    </tr>
					<tr>
						<td class="td01">№ (номер) закупки</td><td class="td02">
							<input name="purchase_number" type="text" value="" id="purchase_number" class="inp01" autocomplete="off">
						</td>
					</tr>
					<tr>
						<td class="td01">Наименование закупок</td>
						<td class="td02">
							<input name="purchase_name" type="text" value="" id="purchase_name" class="inp01" autocomplete="off">
						</td>
					</tr>
					<tr>
						<td class="td01">Файлы</td>
						<td class="td02" id="tender_files">
							<input name="tender_file" type="file" value="" id="tender_file" class="inp01" accept=
							".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" onchange="uploadFile();">
						</td>
					</tr>
				</tbody>
			</table>
			<button class="btn btn-default" type="submit">Сохранить</button>
		</form>
	</div>
</div>

<script>
	function uploadFile()
	{
		var formData = new FormData();

		formData.append('file', $('#tender_file')[0].files[0]);
    formData.append('tmp_code',$('#tmp_code').val())

        $.ajax({
            url: 'index.php?r=tender-ajax%2Fupload-file',
            type: 'POST',
            mimeType: 'multipart/form-data',
            data: formData,   
            datatype:'json',
            cache: false,
            contentType: false,
            processData: false,

            success: function (data) {
                var result = JSON.parse(data);
                console.log(result);
                $('#tender_file').val("");

                var row = $("<div class='file_"+result.fileid+"' />")
                $('#tender_files').append(row);
                row.append($('<div class="drive-item module"><div class="drive-item-module-inner"><div class="drive-item-title"><button type="button" id="remove" class="btn btn-outline-danger btn-xs" title="Удалить файл" onclick="rmFile('+result.fileid+')"><i class="fa fa-remove" aria-hidden="true" style="color:red;"></i></button><a target="_blank" href="index.php?r=tender%2Fdownload-file&id='+result.fileid+'">'+result.fileName+'</a></div></div></div>'));

                $.toast({
                	text : 'Файл успешно загружен',
                	heading: 'Добавлен',
                	showHideTransition: 'slide',
                	loader: false,
                	loaderBg: '#9EC600',
                	position: 'top-right',
                	bgColor: '#1BA261',
                	hideAfter : 3000,
                });    
            },

            error: function(err) {
            	$.toast({
                text : 'Не удалось загрузить файл',
                heading: 'Ошибка',
                showHideTransition: 'slide',
                loader: false,
                loaderBg: '#9EC600',
                position: 'top-right',
                bgColor: '#DD5145',
                hideAfter : 7000,
            });
            }
        });
	}

	function rmFile(id)
	{
		$(".file_"+id).remove();
        $.ajax({
        	url: 'index.php?r=tender-ajax%2Fremove-file&id=' + id,
        	type: 'POST',
        	success: function(data){
        		console.log(data);
        	}
    	});
        $(this).closest('tr').remove();
        $.toast({
        	text : 'Файл успешно удален.',
        	heading: 'Удален',
        	showHideTransition: 'slide',
        	loader: false,
        	loaderBg: '#9EC600',
        	position: 'top-right',
        	bgColor: '#1BA261',
        	hideAfter : 3000,
        });  

	}	
</script>

<?php if (Yii::$app->session->hasFlash('error')): ?>
    <script>
        function modal(){
            $.toast({
                text : '<?= Yii::$app->session->getFlash('error') ?>',
                heading: 'Ошибка',
                showHideTransition: 'slide',
                loader: false,
                loaderBg: '#9EC600',
                position: 'top-right',
                bgColor: '#DD5145',
                hideAfter : 7000,
            });
        }
    </script>
<?php endif; ?>