<?php
use yii\helpers\Url;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

$this->title = "Тендеры";
$tender_permission = Yii::$app->auth->tenderPermission()['tender'];
if ($tender_permission != 1 || $tender_permission != 2) {
    $disabled = true;
} else {
    $disabled = false;
}

date_default_timezone_set('Asia/Almaty');
?>
<div class="row">
    <div class="col-md-12" style="background-color: white;">
        <div class="row">
            <br>
            <div class="col-md-12">
                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-4">
                        <div id="imaginary_container"> 
                            <div class="input-group stylish-input-group">
                                <input type="text" id="search" class="form-control"  placeholder="Поиск" >
                                <span class="input-group-addon">
                                    <button type="submit" id="submit_button" onclick="search()">
                                      <span class="glyphicon glyphicon-search"></span>
                                  </button>  
                              </span>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-12">
                <div class="col-md-7">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link" href="<?=Url::toRoute(['parse/index'])?>">Объявления</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=Url::toRoute(['tender/index'])?>">Тендеры</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=Url::toRoute(['tender/filed'])?>">Отработанные(подана)</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=Url::toRoute(['tender/worked'])?>">Отработанные(отказ от участия)</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Не отработанные</a>
                        </li>
                        <li class="nav-item">
                            <a class="btn" href="<?=Url::toRoute(['tender/create'])?>" title="Новый тендер" style="" target="_blank">&nbsp;&nbsp;&nbsp;+&nbsp;&nbsp;&nbsp;</a>
                        </li>
                    </ul>
                </div>
                <?php if ($tender_permission == 1 || $tender_permission == 2): ?>
                    <div class="col-md-offset-11">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link" href="<?=Url::toRoute(['report/index'])?>" target="_blank">Отчёты</a>
                            </li>
                        </ul>
                    </div>
                <?php endif ?>
            </div>
        </div>
        </div>
        <div>
            <div class="table-responsive">
                <?php Pjax::begin(['id' => 'tenders']) ?>
                <?php 
                echo GridView::widget([
                    'dataProvider' => $provider,
                    'tableOptions' => [
                        'id' => 'main_table',
                        'class' => 'table table-striped table-bordered',
                    ],
                    'rowOptions' => function($tenders)
                    {
                        return ['id' => 'tender_'.$tenders['tenderid']];
                    },
                    'summary' => false,
                    'columns' => [
                    [ 
                        'attribute' => 'tenderid',                                               
                        'label' => 'ID',
                        'value' => function ($tenders) {
                            return $tenders['tenderid'];
                        },
                        'contentOptions' => function($tenders)
                        {
                          return ['onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'];
                        },
                    ],
                    [
                        'attribute' => 'lot_counts_tv',
                        'label' => 'ТВ/ТР',
                        'format' => 'raw',
                        'contentOptions' => function($tenders){
                            if ($tenders['lot_counts_tv'] > 0) {
                               return [
                                    'style' => 'background-color: '.Yii::$app->params['green'].'; color:#FFFFFF;',
                                    'onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'
                                ];
                            } else {
                                return [
                                    'onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'
                                ];
                            }
                        },
                        'value' => function ($tenders) {
                            if ($tenders['lot_counts_tv'] > 0) {
                                return 'ТВ/ТР есть';
                            } else {
                                return 'ТВ/ТР нет';
                            }
                         }, 

                    ],
                [ 
                    'attribute' => 't_csbin',                                               
                    'label' => 'БИН',
                    'encodeLabel' => false,
                    'value' => function ($tenders) {
                        return $tenders['t_csbin'];
                    },
                    'contentOptions' => function($tenders)
                    {
                      return ['onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'];
                    },
                ],
            [ 
                'attribute' => 't_csname',                                               
                'label' => 'Название',
                'encodeLabel' => false,
                'headerOptions' => ['style' => 'width:20px;'],
                'value' => function ($tenders) {
                    return $tenders['t_csname'];
                },  
                'contentOptions' => function($tenders)
                {
                  return ['onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'];
              },        
            ],
            [ 
                'attribute' => 'lot_participate_count_true',                                               
                'label' => 'Решение',
                'headerOptions' => ['style' => 'width: 100px;'],
                'value' => function ($tenders) {
                    if ($tenders['lot_participate_count_true'] > 0) {
                        return 'Участвуем';
                    } elseif ($tenders['lot_participate_count_false'] > 0) {
                        return 'Не участвуем';
                    } else {
                        return 'Решение не принято';
                    }
                },
                'contentOptions' => function($tenders)
                {
                    if ($tenders['lot_participate_count_true'] > 0) {
                        return [
                            'onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")',
                            'style' => 'background-color: '.Yii::$app->params['green'].'; color:#FFFFFF;',
                        ];
                    } elseif ($tenders['lot_participate_count_false'] > 0) {
                        return [ 
                            'onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")',
                            'style' => 'background-color: '.Yii::$app->params['red'].'; color:#FFFFFF;',
                        ];
                    } else {
                        return [
                            'onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")',
                        ];
                    }
                  
              },
            ],
            [ 
                'attribute' => 'purchase_number',                                               
                'label' => '№ Закупки',
                'value' => function ($tenders) {
                    return $tenders['purchase_number'];
                },
                'contentOptions' => function($tenders)
                {
                  return ['onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'];
                },
            ],
           [ 
                'attribute' => 'accept_solution',                                              
                'label' => 'Решение об участии',
                'headerOptions' => ['style' => 'width:20px;'],
                'encodeLabel' => false,
                'value' => function ($tenders) {
                    return date('d-m-Y', $tenders['accept_solution']);
                },  
                'contentOptions' => function($tenders)
                {  
                     return [
                        'onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'
                    ];
                },        
            ],
            [ 
                'attribute' => 'date_purchase',                                              
                'label' => 'Дата вскрытия',
                'encodeLabel' => false,
                'value' => function ($tenders) {
                    return date('d-m-Y', $tenders['tender_date_purchase']);
                },  
                'contentOptions' => function($tenders)
                {  
                     return [
                        'onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'
                    ];
                },        
            ],
    [ 
                    // 'attribute' => 'blocked',                                               
        'header' => 'Ответственный специалист',
        'format' => 'raw',
        'value' => function ($tenders) use ($userlist, $tender_permission) {
            if ($tender_permission == 1) {
                return 
                $tenders['tm_created_at'] ?
                    Select2::widget([
                        'name' => 'is_affiliated',
                        'value' => $tenders['tm_managerid'],
                        'data' => ArrayHelper::merge(['0'=>'Не выбран'], ArrayHelper::map($userlist, 'userid', 'fullname')),
                        'options' => ['multiple'=>false, 'class' => 'inp01'],
                        'pluginOptions' => [
                            'width' => '140px',
                        ],
                        'pluginEvents' => [
                            'select2:select' => 'function() { 
                                toSpecialist('.$tenders['tenderid'].', $(this).val(), '.Yii::$app->auth->user()['userid'].', "'.Yii::$app->auth->user()['fullname'].'") 
                            }',
                        ],
                    ]).'От: <b>'.$tenders['tm_userfullname'].'</b> <br>'. 'Когда: '.date('d-m-y H:m:s', $tenders['tm_created_at']) :
                    Select2::widget([
                        'name' => 'is_affiliated',
                        'value' => $tenders['tm_managerid'],
                        'data' => ArrayHelper::merge(['0'=>'Не выбран'], ArrayHelper::map($userlist, 'userid', 'fullname')),
                        'options' => ['multiple'=>false, 'class' => 'inp01'],
                        'pluginOptions' => [
                            'width' => '140px',
                        ],
                        'pluginEvents' => [
                            'select2:select' => 'function() { 
                                toSpecialist('.$tenders['tenderid'].', $(this).val(), '.Yii::$app->auth->user()['userid'].', "'.Yii::$app->auth->user()['fullname'].'") 
                            }',
                        ],
                    ]) ;

            } else {
                return 'Получили: <br>'.Select2::widget([
                    'name' => 'is_affiliated',
                    'value' => $tenders['tm_managerid'],
                    'data' => ArrayHelper::merge(['0'=>'Не выбран'], ArrayHelper::map($userlist, 'userid', 'fullname')),
                    'options' => ['multiple'=>false, 'class' => 'inp01', 'disabled' =>true],
                    'pluginOptions' => [
                        'width' => '140px',
                    ],
                ]).'От: <b>'.$tenders['tm_userfullname'].'</b> <br>'. 'Когда: '.date('d-m-y', $tenders['tm_created_at']);
            }       
        },            
    ],
    [
        'attribute' => 'application_status',                                               
        'header' => 'Статус заявки',
        'format' => 'raw',
        'visible' => true,
        'value' => function($tenders) use ($tender_permission){
         return $tenders['application_status_date'] ?
                 Select2::widget([
                    'name' => 'application_status',
                    'value' => $tenders['application_status'],
                    'hideSearch' => true,
                    'data' => [ 
                        '0'=>'Не подана',
                        '1'=>'Подана',
                        '2'=>'Отказ от участия',
                        '3'=>'Не отработана'
                    ],
                    'options' => ['multiple'=>false, 'class' => 'inp01', 'disabled' => ($tender_permission == 1 || $tender_permission == 2  ? false : true)],
                    'pluginOptions' => [
                        'width' => '120px',
                     ],
                    'pluginEvents' => [
                        'select2:select' => 'function() { 
                            changeTenderStatus('.$tenders['tenderid'].', $(this).val()) 
                        }',
                    ],
                ]).'Изменил статус: <br><b>'.Yii::$app->UserComponent->getById($tenders['application_status_userid'])['fullname'].'</b> <br>'. 'Когда: '.date('d-m-y', $tenders['application_status_date']) :
                    Select2::widget([
                        'name' => 'application_status',
                        'value' => $tenders['application_status'],
                        'hideSearch' => true,
                        'data' => [
                            '0'=>'Не подана',
                            '1'=>'Подана',
                            '2'=>'Отказ от участия',
                            '3'=>'Не отработана' 
                        ],
                        'options' => ['multiple'=>false, 'class' => 'inp01', 'disabled' => ($tender_permission == 1 || $tender_permission == 2  ? false : true)],
                        'pluginOptions' => [
                            'width' => '120px',
                        ],
                        'pluginEvents' => [
                            'select2:select' => 'function() { 
                                changeTenderStatus('.$tenders['tenderid'].', $(this).val()) 
                            }',
                        ],
                    ]);
        }
    ],
    [
        'attribute' => 'aid',
        'label' => 'Обеспечения <br> исполнения договора',
        'format' => 'raw',
        'encodeLabel' => false,
        'contentOptions' => function($tenders){
            if ($tenders['lot_counts_false'] > 0) {
               return [
                    'class' => 'blink',
                    'style' => 'background-color: '.Yii::$app->params['red'].';color:#FFFFFF;',
                    'onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'
                ];
            } elseif ($tenders['lot_counts_true'] > 0) {
                return [
                    'style' => 'background-color: '.Yii::$app->params['green'].';color:#FFFFFF;',
                    'onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'
                ];
            } elseif ($tenders['lot_counts_neutral'] > 0) {
               return [
                    'onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")',
                ];
             } else {
                    return [
                        'onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")' 
                    ];
            }  
        },
        'value' => function ($tenders) {
            if ($tenders['lot_counts_false'] > 0) {
                return 'Внесите обеспечения.';
            } elseif($tenders['lot_counts_true'] > 0) {
                return 'Обеспечения внесены.';
            } elseif ($tenders['lot_counts_neutral'] > 0) {
                return 'Договоров нет.';
            } else {
                return 'Сделок не найдено.';
            }
        }, 

    ],
    [
        'attribute' => 'lot_counts_signed_agreements',
        'label' => 'Стадия договора',
        'format' => 'raw',
        'contentOptions' => function($tenders){
            if ($tenders['lot_counts_signed_agreements'] > 0) {
               return [
                    'style' => 'background-color: '.Yii::$app->params['green'].';color:#FFFFFF;',
                    'onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'
                ];
            } else {
                return [
                    'onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'
                ];
            } 
        },
        'value' => function ($tenders) {
            if ($tenders['lot_counts_signed_agreements'] > 0) {
                return 'Договор подписан';
            } else {
                return 'Договоры не подписаны';
            } 
        }, 
    ],
    [ 
        'attribute' => 'senddate',                                               
        'label' => 'Дата <br> направления',
        'encodeLabel' => false,
        'value' => function ($tenders) {
            return $tenders['senddate'];
        },
        'format' => ['date', 'php:d.m.Y'],
        'contentOptions' => function($tenders)
        {
          return ['onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'];
        },
    ],
    [ 
        'attribute' => 'source_title',                                               
        'label' => 'Источник получения',
        'value' => function ($tenders) {
            return $tenders['source_title'];
        },
        'contentOptions' => function($tenders)
        {
          return ['onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'];
        },
    ],
    [ 
        'attribute' => 'tender_fullname',                                               
        'label' => 'Специалист',
        'value' => function ($tenders) {
            return $tenders['tender_userid'] ? 
            $tenders['tender_username'] :
            'Неизвестно';
        },
        'contentOptions' => function($tenders)
        {
          return ['onclick' => 'window.open("' . Url::toRoute(['tender/view', 'id'=>$tenders['tenderid']]). '")'];
      },
    ],
],
]);
?>
<?php Pjax::end() ?>
</div>
        </div>
    	
    </div>
</div>
<?php 
    $this->registerJs(' 
    setInterval(function(){  
         $.pjax.reload({container:"#tenders"});
    }, 360000);', \yii\web\VIEW::POS_HEAD); 
?>

<script>
    function changeTenderStatus(tenderid, status)
    {
        $.ajax({
            url: 'index.php?r=tender-ajax%2Ftoggle-tender-status',
            type: 'POST',
            data : { 
                tenderid: tenderid,
                status: +status,
            },
            success: function(data){
                console.log(data);
                
                $('#tender_'+tenderid).remove();
                
                $.toast({
                    text : 'Статус заявки тендера изменён! :)',
                    heading: 'Успешно',
                    showHideTransition: 'slide',
                    loader: false,
                    loaderBg: '#9EC600',
                    position: 'top-right',
                    bgColor: '#1BA261'
                });
            }});
    }

    function toSpecialist(tenderid, manager_id, userid, fullname){
        $.ajax({
            url: 'index.php?r=tender-ajax%2Fto-specialist',
            type: 'POST',
            data : { 
                tenderid: tenderid,
                manager_id: manager_id,
                userid: userid,
                fullname: fullname
            },
            success: function(data){
                console.log(data);
                $.toast({
                    text : 'Лот №  успешно делегирован менеджеру',
                    heading: 'Успешно',
                    showHideTransition: 'slide',
                    loader: false,
                    loaderBg: '#9EC600',
                    position: 'top-right',
                    bgColor: '#1BA261'
                });
            }});
    }
    function search()
    {
        $.pjax.reload({container: "#tenders", url: "index.php?r=tender-ajax/search&q=" + $('#search').val()});
    }

    document.getElementById("search").onkeypress = function(event){
        if (event.keyCode == 13 || event.which == 13){
            search();
        }
    };
</script>