<?php
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

?>
<!-- 
<div class="row">
    <div class="col-md-6">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Общая информация</a></li>
                </ul>
            </div>
        </nav>
    </div>
</div> -->

<div class="row" style="margin-top: 20px;">
    <div class="col-md-6">
        <form action="<?= Url::toRoute(['tender/update', 'id'=>Yii::$app->request->get('tenderid')])?>" method="POST" onkeypress="return event.keyCode!=13">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
            <input id="userid" name="userid" type="hidden" value="<?= Yii::$app->auth->user()['userid']?>">
            <table class="table" style="width:100%;background:#ffffff;border:10px solid #ffffff;">
                <tbody>
                    <tr>
                        <td colspan="2" class="haction1">Редактирование тендера<br><br>
                        </td>
                    </tr>
                    <tr>
                        <td class="td01">Дата направления объявления на рассмотрение</td><td class="td02">
                            <?=  DatePicker::widget([
                                'name' => 'senddate',
                                'type' => DatePicker::TYPE_INPUT,
                                'value' => date('d-m-Y', $tender['senddate']),
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'dd-mm-yyyy',
                                ],
                                'options' => ['class' => 'inp01']
                            ]);
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="td01">Дата окончания обсуждения</td><td class="td02">
                            <?=  DatePicker::widget([
                                'name' => 'date_end_discuss',
                                'type' => DatePicker::TYPE_INPUT,
                                'value' => date('d-m-Y', $tender['date_end_discuss']),
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'dd-mm-yyyy',
                                ],
                                'options' => ['class' => 'inp01']
                            ]);
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="td01">Установленная дата принятия решения об участии в закупке</td><td class="td02">
                            <?=  DatePicker::widget([
                                'name' => 'accept_solution',
                                'type' => DatePicker::TYPE_INPUT,
                                'value' => date('d-m-Y', $tender['accept_solution']),
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'dd-mm-yyyy',
                                ],
                                'options' => ['class' => 'inp01']
                            ]);
                            ?>
                        </td>
                    </tr>
                    <tr>
                      <td class="td01">Дата вскрытия</td><td class="td02">
                          <?=  DatePicker::widget([
                              'name' => 'date_purchase',
                              'type' => DatePicker::TYPE_INPUT,
                              'value' => date('d-m-Y', $tender['tender_date_purchase']),
                              'pluginOptions' => [
                                  'autoclose'=>true,
                                  'format' => 'dd-mm-yyyy',
                              ],
                              'options' => ['class' => 'inp01']
                          ]);
                          ?>
                      </td>
                  </tr>
                    <tr>
                        <td width="50%" class="td01">Источник получения объявления</td><td width="50%" class="td02">
                            <?= 
                                Select2::widget([
                                    'name' => 'source_get',
                                    'value' => $tender['source_get'],
                                    'data' => ArrayHelper::map($sources, 'sourceid', 'source_title'),
                                    'maintainOrder' => true,
                                    'options' => ['class' => 'inp01', 'multiple' => false],
                                    'pluginOptions' => [
                                        'tags' => true,
                                    ],
                                ]);
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="td01">БИН Организатора / Заказчика закупок <span style="color:red">*</span></td><td class="td02">
                            <?= 
                            // $tender['t_csid'] ?
                            Select2::widget([
                                'name' => 't_csbin',
                                'value' => $tender['t_csbin'],
                                'options' => ['placeholder' => 'Введите БИН', 'onchange'=>'setCsIdAndName(this);', 'id'=>'t_csbin'],
                                'pluginOptions' => [
                                    'tags' => true,
                                    'allowClear' => true,
                                    'minimumInputLength' => 5,
                                    'maximumInputLength' => 12,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                        'inputTooShort' => new JsExpression("function () { return 'Введите минимум 5 символов'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => Url::to(['tender-ajax/search-bin']),
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(cs) { return [cs.cdid, cs.text]; }'),
                                    'templateSelection' => new JsExpression('function (cs) { return cs.text; }'),
                                ],
                            ]) ;
                            ?>
                        </td>
                    </tr>
                    <input id="t_csid" type = "hidden" name = "t_csid" value = "<?=$tender['t_csid']?>" />
                    <input id="t_csbin_text" type = "hidden" name = "t_csbin_text" value = "<?=$tender['t_csbin']?>" />
                    <input id="t_csname_text" type = "hidden" name = "t_csname_text" value = "<?=htmlentities($tender['t_csname'])?>" />
                    <tr>
                        <td class="td01">Тип предприятия</td>
                        <td class="td02"><?= Select2::widget([
                                    'name' => 'origid',
                                    'value' => $tender['t_csorigid'],
                                    'data' => ArrayHelper::map($origlist, 'origid', 'origname'),
                                    'maintainOrder' => true,
                                    'options' => ['class' => 'inp01', 'multiple' => false],
                                    'pluginOptions' => [
                                        'tags' => true,
                                    ],
                                ]); ?></td>
                    </tr>
                    <tr>
                        <td class="td01">Тип предприятия</td>
                        <td class="td02"><?= Select2::widget([
                                    'name' => 'ognid',
                                    'value' => $tender['t_csognid'],
                                    'data' => ArrayHelper::map($ognlist, 'ognid', 'ognname'),
                                    'maintainOrder' => true,
                                    'options' => ['class' => 'inp01', 'multiple' => false],
                                    'pluginOptions' => [
                                        'tags' => true,
                                    ],
                                ]); ?></td>
                    </tr>
                    <tr>
                        <td class="td01">Наименование Организатора/Заказчика закупок <span style="color:red">*</span></td><td class="td02"><?= 
                            Select2::widget([
                                'name' => 't_csname',
                                'value' => $tender['t_csname'],
                                'options' => ['class' => 'inp01', 'multiple' => false, 'id'=>'t_csname',
                                'placeholder'=>'Введите название', 'onchange' => 'setCsName(this)'],
                                'pluginOptions' => [
                                    'tags' => true,
                                    'allowClear' => true,
                                    'minimumInputLength' => 5,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                        'inputTooShort' => new JsExpression("function () { return 'Введите минимум 5 символов'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => Url::to(['tender-ajax/search-cs']),
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(cs) { return [cs.cdid, cs.text]; }'),
                                    'templateSelection' => new JsExpression('function (cs) { return cs.text; }'),
                                ],
                            ]);
                            ?>      
                        </td>
                    </tr>
                    <tr>
                        <td class="td01">Сектор Организатора / Заказчика закупок</td><td class="td02">
                            <?= 
                            Select2::widget([
                                'name' => 'sector',
                                'value' => $tender['sector'],
                                'data' => ArrayHelper::map($sectors, 'sectorid', 'sector_title'),
                                'maintainOrder' => true,
                                'options' => ['class' => 'inp01', 'multiple' => false],
                                'pluginOptions' => [
                                    'tags' => true,
                                ],
                            ]);
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="td01">Вид закупки</td><td class="td02">
                            <?= 
                            Select2::widget([
                                'name' => 't_purchase',
                                'value' => $tender['purchase_type'],
                                'data' => ArrayHelper::map($purchases, 'purchaseid', 'purchase_title'),
                                'maintainOrder' => true,
                                'options' => ['class' => 'inp01', 'multiple' => false, 'id'=>'t_purchase'],
                                'pluginOptions' => [
                                    'tags' => true,
                                    'maximumInputLength' => 20
                                ],
                            ]);
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="td01">Измененный вид закупки</td><td class="td02">
                            <?= 
                            Select2::widget([
                                'name' => 'purchase_type_changed',
                                'value' => $tender['purchase_type_changed'],
                                'data' => ArrayHelper::map($purchases, 'purchaseid', 'purchase_title'),
                                'maintainOrder' => true,
                                'options' => ['class' => 'inp01', 'multiple' => false, 'id'=>'purchase_type_changed'],
                                'pluginOptions' => [
                                    'tags' => true,
                                    'maximumInputLength' => 20
                                ],
                            ]);
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="td01">Консорциум</td><td class="td02">
                            <textarea name="consortium" type="text" id="consortium" class="inp01" autocomplete="off" cols="30" rows="3"><?=$tender['consortium']?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="td01">№ (номер) закупки</td><td class="td02">
                            <input name="purchase_number" type="text" value="<?=$tender['purchase_number']?>" id="purchase_number" class="inp01" autocomplete="off">
                        </td>
                    </tr>
                    <tr>
                        <td class="td01">Наименование закупок</td><td class="td02">
                            <input name="purchase_name" type="text" value="<?=$tender['purchase_name']?>" id="purchase_name" class="inp01" autocomplete="off">
                        </td>
                    </tr>
                    <tr>
                        <td class="td01">Файлы</td>
                        <td class="td02" id="tender_files">
                            <input name="tender_file" type="file" value="" id="tender_file" class="inp01" accept=
                            ".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" onchange="updateFile(<?= Yii::$app->request->get('tenderid')?>);">

                            <?php foreach ($tender['files'] as $key => $value): ?>
                                <div class="<?= 'file_'.$value['fileid']?>">
                                    <div class="drive-item module">
                                        <div class="drive-item-inner module-inner">
                                            <div class="drive-item-title"><button type="button" id="remove" class="btn btn-outline-danger btn-xs" title="Удалить файл" onclick="rmFile(<?=$value['fileid']?>)"><i class="fa fa-remove" aria-hidden="true" style="color:red;"></i></button><a target="_blank" href="<?= Url::toRoute(['tender/download-file', 'id'=>$value['fileid']])?>"><?= pathinfo($value['path'])['filename'].'.'.pathinfo($value['path'])['extension'] ?></a></div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <button class="btn btn-default" type="submit">Сохранить</button>
        </form>
    </div>
</div>
<script src="<?= Yii::$app->params['module_folder_name'] ?>js/tenderEdit.js"></script>
<?php if (Yii::$app->session->hasFlash('error')): ?>
<script>
    function modal(){
        $.toast({
            text : '<?= Yii::$app->session->getFlash('error') ?>',
            heading: 'Ошибка',
            showHideTransition: 'slide',
            loader: false,
            loaderBg: '#9EC600',
            position: 'top-right',
            bgColor: '#DD5145'
        });
    }
</script>
<?php endif; ?>