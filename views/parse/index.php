<?php
use yii\helpers\Url;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = "Объявления";
$tender_permission = Yii::$app->auth->tenderPermission()['tender'];
if ($tender_permission != 1 || $tender_permission != 2) {
    $disabled = true;
} else {
    $disabled = false;
}

date_default_timezone_set('Asia/Almaty');
?>

<div class="row">
	<div class="col-md-12" style="background-color: white;">
        <div class="row">
            <br>
            <div class="col-md-12">
                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-4">
                        <div id="imaginary_container"> 
                            <div class="input-group stylish-input-group">
                                <input type="text" id="search" class="form-control"  placeholder="Поиск" >
                                <span class="input-group-addon">
                                    <button type="submit" id="submit_button" onclick="search()">
                                      <span class="glyphicon glyphicon-search"></span>
                                  </button>  
                              </span>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-12">
                <div class="col-md-9">
                    <ul class="nav nav-tabs">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Объявления</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=Url::toRoute(['tender/index'])?>">Тендеры</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=Url::toRoute(['tender/filed'])?>">Отработанные(подана)</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=Url::toRoute(['tender/worked'])?>">Отработанные(отказ от участия)</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=Url::toRoute(['tender/un-worked'])?>">Не отработанные</a>
                        </li>
                        <li class="nav-item">
                            <a class="btn" href="<?=Url::toRoute(['tender/create'])?>" title="Новый тендер" style="" target="_blank">&nbsp;&nbsp;&nbsp;+&nbsp;&nbsp;&nbsp;</a>
                        </li>
                    </ul>
                </div>
                <?php if ($tender_permission == 1 || $tender_permission == 2): ?>
                    <div class="col-md-offset-11">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link" href="<?=Url::toRoute(['report/index'])?>" target="_blank">Отчёты</a>
                            </li>
                        </ul>
                    </div>
                <?php endif ?>
            </div>
        </div>
        </div>
		<div>
			<?php 
    	echo GridView::widget([
    		'dataProvider' => $provider,
    		'summary' => false,
    		'tableOptions' => [
    			'id' => 'main_table',
    			'class' => 'table table-striped table-bordered',
    		],
    		'columns' => [
    			[ 
    				'attribute' => 'is_completed',                                               
    				'label' => 'Отработан',
    				'format' => 'raw',
    				'value' => function ($lots) {
    					return $lots['is_completed'] ? 
    					'<span class="glyphicon glyphicon-ok" style="color:green"></span> <br>'.Yii::$app->UserComponent->getById($lots['userid'])['fullname'] : 
    					'<input type="checkbox" id="complete_'.$lots['id'].'" onchange="completeLot('.$lots['id'].')"/>';
    				},
    			],
    			[ 
    				'attribute' => 'parsed_from',                                               
    				'label' => 'Ресурс',
    				'value' => function ($lots) {
    					return $lots['parsed_from'];
    				},
    			],
    			[ 
    				'attribute' => 'lot_title',                                               
    				'label' => 'Название лота',
    				'value' => function ($lots) {
    					return $lots['lot_title'];
    				},
    			],
    			[ 
    				'attribute' => 'add_info',                                               
    				'label' => 'Дополнительная информация',
    				'value' => function ($lots) {
    					return $lots['add_info'];
    				},
    			],
                [ 
                    'attribute' => 'cs_title',                                               
                    'label' => 'Заказчик',
                    'value' => function ($lots) {
                        return $lots['cs_title'];
                    },
                ],
    			[ 
    				'attribute' => 'total_amount',                                               
    				'label' => 'Общая сумма, без НДС',
    				'value' => function ($lots) {
    					return $lots['total_amount'];
    				},
    			],
    			[ 
    				'attribute' => 'lot_status',                                               
    				'label' => 'Статус заявки',
    				'value' => function ($lots) {
    					return $lots['lot_status'];
    				},
    			],
    			[ 
    				'attribute' => 'lot_link',                                               
    				'label' => 'Ссылка на лот',
    				'format' => 'raw',
    				'value' => function ($lots) {
    					return '<a href="'.$lots['lot_link'].'" target="_blank">Ссылка тут</a>';
    				},
    			],
    			[ 
    				'attribute' => 'cs_bin',                                               
    				'label' => 'БИН заказчика',
    				'value' => function ($lots) {
    					return $lots['cs_bin'];
    				},
    			],
    			[ 
    				'attribute' => 'cs_link',                                               
    				'label' => 'Ссылка на заказчика',
    				'format' => 'raw',
    				'value' => function ($lots) {
    					return '<a href="'.$lots['cs_link'].'" target="_blank">Ссылка тут</a>';
    				},
    			],
    		],
    	]);
    	?>
		</div>
		
	</div>
</div>

<script>
	function completeLot(id)
	{
		if ($("#complete_"+id).prop('checked') == false) {
			return false;
		} else {
			var box= confirm("Вы отработали данный лот?");


			if (box==true)
				$.ajax({
					url: 'index.php?r=parse%2Fcomplete-lot',
					type: 'POST',
					data: {
						id: id
					},
					success: function (data) {
						console.log(data);
						$( "#complete_"+id ).replaceWith( '<span class="glyphicon glyphicon-ok" style="color:green"></span> <br>' + data.username );
					},
					error: function(){
						alert('Ошибка');
					}
				}); 
			else
				$("#complete_"+id).prop('checked', false);
				$("#complete_"+id).removeAttr("checked")
		}
	}
</script>
<?php if (Yii::$app->session->hasFlash('success')): ?>
    <script>
        function modal(){
            $.toast({
                text : '<?= Yii::$app->session->getFlash('success') ?>',
                heading: 'Ошибка',
                showHideTransition: 'slide',
                loader: false,
                loaderBg: '#9EC600',
                position: 'top-right',
                bgColor: '#DD5145'
            });
        }
    </script>
<?php endif; ?>