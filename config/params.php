<?php

include($_SERVER['DOCUMENT_ROOT'].'/config.php');
include($_SERVER['DOCUMENT_ROOT'].'/config_mail.php');
return [
    'adminEmail'       => 'admin@example.com',
    'tmp_code'         => round(mt_rand(1,9)).round(mt_rand(0,9)).round(mt_rand(0,9)).round(mt_rand(0,9)).round(mt_rand(0,9)).round(mt_rand(0,9)).round(mt_rand(0,9)).round(mt_rand(0,9)).round(mt_rand(0,9)).round(mt_rand(0,9)).round(mt_rand(0,9)).round(mt_rand(0,9)).round(mt_rand(0,9)).round(mt_rand(0,9)).round(mt_rand(0,9)).round(mt_rand(0,9)).round(mt_rand(0,9)).round(mt_rand(0,9)).round(mt_rand(0,9)),

    'currentTime'      => strtotime(date('d-m-Y H:i:s')),

    'cfg_nouser'       => $cfg_nouser,


    'cfg_treq_stage'   => $cfg_treq_stage,
    'cfg_co_stage'     => $cfg_co_stage,
    'cfg_aggr_stage'   => $cfg_aggr_stage,
    'cfg_conn_stage'   => $cfg_conn_stage,
    'cfg_last_stage'   => $cfg_last_stage,
    'cfg_cancel_stage' => $cfg_cancel_stage,
    'cfg_contr_act2id' => $cfg_contr_act2id,

    
    'main_site_domain' => 'https://kcrm.kaztranscom.kz/',
    'module_folder_name' => '/extra_modules/',
    'cfg_mail_domain' => $cfg_mail_domain,

    /* 
    *  Configuration of mail
    */

    'cfg_smtpServer' => $cfg_smtpServer,
    'cfg_fromAddress' => $cfg_fromAddress,




    


    /*
    *    COLORS
    */
    
    'green' => '#449D44',
    'red' => '#C9302C',
    'blue' => '#286090',


    /*
    *   Key words for parsing
    */

    'key_words' => ['интернет', 'связь', 'технологии'],


    /*
    *    sandbox
    *   
    */

    'cfg_sandbox_db' => $cfg_sandbox_db,
	
	
    /*
    *    Настройка переделегирования менеджеров
    *       1 - привилегированный пользователь филиала может переделегировать менеджера самостоятельно
    *       0 - переделегирование идет через согласование ГО
    */

    'can_delegate_manager' => 1

];
