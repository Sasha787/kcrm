<?php

$params = require __DIR__ . '/params.php';
$kcrm_db = require __DIR__ . '/kcrm_db.php';
$contracts_db = require __DIR__ . '/contracts_db.php';

$config = [
	'id' => 'module',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'aliases' => [
		'@bower' => '@vendor/bower-asset',
		'@npm' => '@vendor/npm-asset',
		'@config' => '/config.php',
	],
	'defaultRoute' => 'tender/index',
	'components' => [
		'request' => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'epKWADqTAxtzLXQ5zWZRHV84mpohsYuR',
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'auth' => [
			'class' => 'app\models\Auth',
		],
		'user' => [
			'identityClass' => 'app\models\User',
			'enableAutoLogin' => true,
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			// send all mails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'useFileTransport' => true,
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				// [
				//     'class' => 'yii\log\FileTarget',
				//     'levels' => ['error', 'warning'],
				// ],
				[
					'class' => 'yii\log\FileTarget',
					'categories' => ['delegation'],
					'levels' => ['error', 'warning', 'info'],
					'logFile' => '@app/runtime/logs/delegation.log',
				],
				[
					'class' => 'yii\log\FileTarget',
					'categories' => ['mailsToSignersOfLot'],
					'levels' => ['info'],
					'logVars' => [],
					'logFile' => '@app/runtime/logs/mailsToSignersOfLot.log',
				],

				[
					'class' => 'yii\log\FileTarget',
					'categories' => ['mailsToCheckList'],
					'levels' => ['info'],
					'logVars' => [],
					'logFile' => '@app/runtime/logs/mailsToCheckList.log',
				],
				[
					'class' => 'yii\log\FileTarget',
					'categories' => ['notifySpecialist'],
					'levels' => ['info'],
					'logVars' => [],
					'logFile' => '@app/runtime/logs/notifySpecialist.log',
				],
			],
		],
		'db' => $kcrm_db,
		'db2' => $contracts_db,
		'UserComponent' => [
			'class' => 'app\components\UserComponent',
		],
		'Directory' => [
			'class' => 'app\components\DirectoryComponent',
		],
		'File' => [
			'class' => 'app\components\FileComponent',
		],
		'MailComponent' => [
			'class' => 'app\components\MailComponent',
		],
		'MitworkParseComponent' => [
			'class' => 'app\components\parsings\MitworkParseComponent',
		],
		'LotComponent' => [
			'class' => 'app\components\LotComponent',
		],
		'CacheComponent' => [
			'class' => 'app\components\CacheComponent',
		],
		'RiskComponent' => [
			'class' => 'app\components\RiskComponent',
		],
		'ChatComponent' => [
			'class' => 'app\components\ChatComponent',
		],
	],
	'params' => $params,
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = [
		'class' => 'yii\debug\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		//'allowedIPs' => ['127.0.0.1', '::1'],
	];

	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		//'allowedIPs' => ['127.0.0.1', '::1'],
	];
}

return $config;
