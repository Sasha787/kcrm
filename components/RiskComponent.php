<?php

namespace app\components;

use yii\base\Component;
use Yii;

use yii\helpers\ArrayHelper;


/**
 * 
 */
class RiskComponent extends Component
{		

		private $auth;

		public function get($risks = [], $signers = [], $files = [])
		{		

				$this->auth = Yii::$app->auth->user()['userid'];

				for( $q = 0; $q < count( $risks ); $q++ )
				{
					$risk = $risks[$q];

					for ($i = 0; $i < count( $signers ); $i++ ) {
						
						$signer = $signers[$i];

						if ($signer['s_risk_id'] == $risk['lr_id']) {
								$risks[$q]['signers'][$i] = $signer;
						}

						if ($signer['s_userid'] == $this->auth && $signer['s_risk_id'] == $risk['lr_id'] && $signer['s_date_signed'] == 0) {
								$risks[$q]['is_need_sign'] = 1;
						}


					}
					for ($k = 0; $k < count( $files ); $k++ ) {
						
						$file = $files[$k];

						if ($file['rf_risk_id'] == $risk['lr_id']) {
								$risks[$q]['files'][$k] = $file;
						}

					}
			
				}

				return $risks;
		}
}