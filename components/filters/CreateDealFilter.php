<?php 
namespace app\components\filters;

use Yii;
use yii\base\ActionFilter;

class CreateDealFilter extends ActionFilter
{ 
    protected $userid;
    protected $user_depid = array();
    protected $lotid;
    protected $permission;
    protected $fullaccess;
    protected $ids = array();


    public function beforeAction($action)
    {	
        $this->userid = Yii::$app->auth->user()['userid'];
        $this->lotid = Yii::$app->request->get('id');
        $this->permission = Yii::$app->auth->tenderPermission()['tender'];
        $this->fullaccess = Yii::$app->auth->tenderPermission()['fullacc'];
        $this->user_depid = Yii::$app->auth->tenderPermission()['depid'];

        $check1 = (new \yii\db\Query())
                    ->select(['id'])
                    ->from('tender_lot_managers')
                    ->where( 
                        ['tender_lot_managers.m_lotid'=>$this->lotid,
                        'tender_lot_managers.m_managerid'=>$this->userid]
                    )
                    ->one();

        $check2 = (new \yii\db\Query())
                    ->select(['depid'])
                    ->from('tender_lot_dep')
                    ->where( 
                        ['tender_lot_dep.dep_lotid'=>$this->lotid]
                      )
                    ->andWhere(
                        ['in', 'tender_lot_dep.depid', $this->user_depid]
                      )
                    ->one();
                    
        if ($check1) {
            return parent::beforeAction($action);
        }

        if (($this->permission == 3 && $check2 && $this->fullaccess == 1) || ($this->permission == 3 && $this->user_depid[0] == 0 && $this->fullaccess == 1)) {
            return parent::beforeAction($action);
        }

        echo "Access Denied [#1386].";
        die();  
        
    }

}