<?php 
namespace app\components\filters;

use Yii;
use yii\base\ActionFilter;
use yii\helpers\Url;

use app\models\Auth;

include_once($_SERVER['DOCUMENT_ROOT'].'/apps/SSOConnection.php');

class AuthFilter extends ActionFilter
{

    public function beforeAction($action)
    {	
        if (isset($_COOKIE['userid'])) {

            $token = $_COOKIE['userid'];

            if (Auth::isGuest($token)) {

              setcookie("userid", "", time()-3600);
              Yii::$app->response->redirect(Url::to(['/']));
              return false;
            
            } else {

              return parent::beforeAction($action);

            }

        } elseif (isset($_COOKIE['token'])) {

          $SSO_conn = new \SSOConnection();

          $userid = $SSO_conn->getToken();  

          if ($userid > 0) {

            if (Auth::login($userid)) {

              Yii::$app->controller->refresh();
              return false;

            };
            
          }

           Yii::$app->response->redirect('/');
           return false;
           
        } else {

          Yii::$app->response->redirect('/');
          return false;

        }
    }   
}