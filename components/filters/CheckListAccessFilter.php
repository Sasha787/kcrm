<?php 
namespace app\components\filters;


use Yii;
use yii\base\ActionFilter;
use yii\helpers\Url;


class CheckListAccessFilter extends ActionFilter
{ 
    protected $userid;
    protected $user_depid = array();
    protected $lotid;
    protected $permission;
    protected $ids = array();


    public function beforeAction($action)
    {	
        $this->userid = Yii::$app->auth->user()['userid'];
        $this->user_depid = Yii::$app->auth->tenderPermission()['depid'];
        $this->permission = Yii::$app->auth->tenderPermission()['tender'];
        $this->lotid = Yii::$app->request->get('lotid');

        $curManager = Yii::$app->LotComponent->getCurManager($this->lotid);
        $this->userid = Yii::$app->auth->user()['userid'];

        if ($this->permission == 4 || $this->permission == 2 || $this->permission == 1 || ($this->userid == $curManager)) {
        	return parent::beforeAction($action);
        } else {
        	echo "Access Denied [#1385].";
        	die();  
        }  
    }   
}