<?php 
namespace app\components\filters;

use Yii;
use yii\base\ActionFilter;
use yii\helpers\Url;

use app\models\Auth;

class DelegateLotFilter extends ActionFilter
{

	protected $userid;
	protected $user_depid = array();
	protected $lotid;
	protected $permission;
	protected $ids = array();
	protected $curManager;

	public function beforeAction($action)
	{	
				$this->userid = Yii::$app->auth->user()['userid'];
				$this->user_depid = Yii::$app->auth->tenderPermission()['depid'];
				$this->permission = Yii::$app->auth->tenderPermission()['tender'];

				if (($this->permission == 1 || $this->permission == 2) || ($this->permission == 3 && $this->user_depid[0] == 0)) {
					return parent::beforeAction($action);
				}

				echo "Access Denied [#1387].";
				die();  

			}
}
