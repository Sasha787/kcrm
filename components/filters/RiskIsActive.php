<?php 
namespace app\components\filters;

use Yii;
use yii\base\ActionFilter;
use yii\helpers\Url;

use app\models\Auth;

class RiskIsActive extends ActionFilter
{ 
    public $riskId;


    public function beforeAction($action)
    {   
     
        $this->riskId = Yii::$app->request->get('risk_id');

        $check = (new \yii\db\Query())
                    ->select(['tender_lot_risks.lr_isActive'])
                    ->from('tender_lot_risks')
                    ->where( ['tender_lot_risks.lr_lotid' => $this->riskId])
                    ->one();

        if ($check['lr_isActive'] == 1) {
            return parent::beforeAction($action);
        }

        echo json_encode(['err' => 'Риск находится на стадии подоготовки. Действие запрещено!']);
        return false;       
    }
}