<?php 
namespace app\components\filters;

use Yii;
use yii\base\ActionFilter;
use yii\helpers\Url;

class SpecialistFilter extends ActionFilter
{ 
    private $tenderid;
    private $auth;


   	public function beforeAction($action)
    {	
     
        $this->tenderid = Yii::$app->request->get('tenderid');
        $this->auth = Yii::$app->auth->user()['userid'];

        $check = (new \yii\db\Query())
                    ->select('tender')
                    ->from('privusers')
                    ->where(['privusers.userid' => $this->auth])
                    ->one();

        if (!$check) {
            return parent::beforeAction($action);
        }

        if ($check['tender'] == 1 || $check['tender'] == 2) {
            return parent::beforeAction($action);
         }

        echo "Access Denied [#1389].";
        return false;   

        /* $check = (new \yii\db\Query())
                    ->select('tm_managerid')
                    ->from('tender_managers')
                    ->where(['tender_managers.tm_tenderid' => $this->tenderid])
                    ->one();

        if (!$check) {
            return parent::beforeAction($action);
        }

        if ($check['tm_managerid'] == $this->auth) {
           return parent::beforeAction($action);
        }

        if ($check['tm_managerid'] == 0) {
           return parent::beforeAction($action);
        } 

        echo 'Access Denied [SP01]';
        return false;
        */
    }
}