<?php 
namespace app\components\filters;


use Yii;
use yii\base\ActionFilter;
use yii\helpers\Url;


class TenderAccessFilter extends ActionFilter
{ 
    protected $userid;
    protected $user_depid = array();
    protected $lotid;
    protected $permission;
    protected $ids = array();


    public function beforeAction($action)
    {	
        $this->userid = Yii::$app->auth->user()['userid'];
        $this->user_depid = Yii::$app->auth->tenderPermission()['depid'];
        $this->permission = Yii::$app->auth->tenderPermission()['tender'];
        

        if ($this->permission == 1 || $this->permission == 2) {
        	return parent::beforeAction($action);
        } else {
        	echo "Access Denied [#1390].";
        	die();  
        }
    }   
}