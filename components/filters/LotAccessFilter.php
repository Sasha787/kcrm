<?php 
namespace app\components\filters;

use Yii;
use yii\base\ActionFilter;
use yii\helpers\Url;

use app\models\Auth;

class LotAccessFilter extends ActionFilter
{ 
    protected $userid;
    protected $user_depid = array();
    protected $lotid;
    protected $permission;
    protected $ids = array();
    protected $curManager;


    public function beforeAction($action)
    {	
        $this->userid = Yii::$app->auth->user()['userid'];
        $this->user_depid = Yii::$app->auth->tenderPermission()['depid'];
        $this->lotid = Yii::$app->request->get('id');
        $this->permission = Yii::$app->auth->tenderPermission()['tender'];
        $this->curManager = Yii::$app->LotComponent->getCurManager($this->lotid);

        //return var_dump($this->permission);

        if ($this->permission == 1 || $this->permission == 2) {
            return parent::beforeAction($action);
        }

        if ($this->user_depid[0] == 0 && $this->permission == 3) {
            return parent::beforeAction($action);
        }

        if ($this->permission == 3) {
            $check = (new \yii\db\Query())
                      ->select(['tender_lots.lotid'])
                      ->from('tender_lots')
                      ->join('left join', 'tender_lot_dep', 'tender_lot_dep.dep_lotid = tender_lots.lotid')
                      ->where( 
                        ['tender_lots.lotid'=>$this->lotid]
                      )
                      ->andWhere(
                        ['in', 'tender_lot_dep.depid', $this->user_depid]
                      )
                      ->one();

            if ($check) {
              return parent::beforeAction($action);
            }
         } 

        $check = (new \yii\db\Query())
                    ->select(['tender_lots.lotid'])
                    ->from('tender_lots')
                    ->join('left join', 'tender_lot_signed', 'tender_lot_signed.s_lotid = tender_lots.lotid')
                    ->join('left join', 'tender_lot_documents', 'tender_lot_documents.ld_lotid = tender_lots.lotid')
                    ->where( 
                        ['tender_lots.lotid'=>$this->lotid]
                    )
                    ->andWhere(['or',
                        ['tender_lot_signed.s_userid' => $this->userid],
                        ['in', 'tender_lot_documents.ld_depid', $this->user_depid],
                    ])
                    ->one();
                    
        if ($check) {
            return parent::beforeAction($action);
        }

        if ($this->curManager == $this->userid) {
            return parent::beforeAction($action);
        }

        echo "Access Denied [#1388].";
        die();  
        
    }

}