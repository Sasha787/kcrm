<?php 
namespace app\components\parsings;

use Yii;
use yii\base\Component;

include($_SERVER['DOCUMENT_ROOT'].'/'.Yii::$app->params['module_folder_name'].'/vendor/simple-html-dom/simple_html_dom.php');
/**
 * 
 */
class MitworkParseComponent extends Component
{


	protected $data = array();
	protected $temp_data = array();
	protected $key_words = array();
	protected $next_link;
	protected $html;
	protected $link;
	protected $domain = 'https://eep.mitwork.kz';

	public function parse()
	{

		$this->key_words = Yii::$app->params['key_words'];
		foreach ($this->key_words as $key => $word) {

			$this->link = 'https://eep.mitwork.kz/ru/publics/lots?filter%5Bsubmit%5D=&filter%5Bsearch%5D='.$word.'&filter%5Bstatus%5D=5';
			$this->next_link = $this->link;
			$page = 2;
			while ($this->next_link) {

				$this->parsePage($this->next_link);
				if ($this->checkNextLink($this->next_link)) {
					
					$this->next_link = 'https://eep.mitwork.kz/ru/publics/lots?filter%5Bsubmit%5D=&filter%5Bsearch%5D='.$word.'&filter%5Bstatus%5D=5&page='.$page.'&per-page=50';
					$page++;
				} else {
					$this->next_link = NULL;
					$page = 2;
				}
				
			};
		}
		unset($this->next_link);
		unset($this->link);
		unset($page);
		return $this->data;
	}

	public function checkNextLink($link)
	{
		if (null == (file_get_html($link)->find('li[class=next]')) || null !== (file_get_html($link)->find('li[class=next disabled]'))) {
			return false; 
		}
		unset($link);
		return true;
	}

	public function getNextLink($link)
	{
		// returns without domain
		$html = file_get_html($link);
		unset($link);
		return $html->find('li[class=next]')[0]->children[0]->attr['href'];
		
		
	}

	public function parsePage($link)
	{
		$html = file_get_html($link);
		foreach ($html->find('tr[class=item]') as $row) {

			$id = $row->find('td',0)->plaintext;
			$title = $row->find('td',1)->children(0)->plaintext;
			$add_info = $row->find('td',2)->plaintext;
			$price = $row->find('td',3)->plaintext;
			$bin = $row->find('td',4)->plaintext;
			$status = $row->find('td',5)->plaintext;
			$link_to_lot = $row->find('td', 1)->children(0)->href;
			$link_to_cs = $row->find('td',4)->children(0)->href; 

			$temp['id'] = $id;
			$temp['title'] = $title;
			$temp['add_info'] = $add_info;
			$temp['price'] = $price;
			$temp['bin'] = $bin;
			$temp['status'] = $status;
			$temp['link_to_lot'] = $link_to_lot;
			$temp['link_to_cs'] = $link_to_cs;

			array_push($this->data, $temp);
			unset($temp);
			unset($html);
			unset($link);
		}
	}

}
