<?php

namespace app\components;

use yii\base\Component;
use Yii;

use yii\helpers\ArrayHelper;

class LotComponent extends Component
{

		protected $userid;
		protected $user_depid = array();
		protected $lotid;
		protected $permission;
		protected $ids = array();
		protected $curManager;
		protected $curSpecialist;

		public function getCurManager($lotid)
		{
				$this->lotid = $lotid;

        $curManager = (new \yii\db\Query())
        					->select('m_managerid')
        					->from('tender_lot_managers')
        					->join('inner join', 'tender_lots', 'tender_lots.lotid = tender_lot_managers.m_lotid')
        					->where(['lotid' => $this->lotid])
        					->one();

        $this->curManager = $curManager['m_managerid'];

       	return $this->curManager;
		}

		public function getCurSpecialist($lotid)
		{
				$this->lotid = $lotid;

				$curSpecialist = (new \yii\db\Query())
        					->select('tm_managerid')
        					->from('tender_managers')
        					->join('inner join', 'tender_lots', 'tender_lots.l_tenderid = tender_managers.tm_tenderid')
        					->where(['tender_lots.lotid' => $this->lotid])
        					->one();

        $curSpecialist['tm_managerid'] == NULL ? $this->curSpecialist = 0 : $this->curSpecialist = $curSpecialist['tm_managerid'];
        return $this->curSpecialist;
		}
}