<?php 
namespace app\components;
use yii\base\Component;


/**
 * 
 */
class DirectoryComponent extends Component
{
	public function delete($path){
		if (is_file($path)) return unlink($path);
		if (is_dir($path)) {
			foreach(scandir($path) as $p) if (($p!='.') && ($p!='..'))
				$this->delete($path.DIRECTORY_SEPARATOR.$p);
			return rmdir($path); 
		}
		return false;
	}

	public function get($path, $count=1){
		if ($count > 1){
			return dirname(getDirectory($path, --$count));
		}else{
			return dirname($path);
		}
	}
}

?>