<?php

namespace app\components;

use yii\base\Component;
use Yii;

use app\models\Tender;
use app\models\TenderSource;
use app\models\TenderSector;
use app\models\TenderOrig;
use app\models\TenderOgn;
use app\models\DepList;
use app\models\ProtocolDictionary;
use app\models\User;
use app\models\LotStatus;
use app\models\LotIndication;
use app\models\LotPercent;
use app\models\LotConclusion;

use yii\helpers\ArrayHelper;


/**
 * 
 */
class CacheComponent extends Component
{
		public function getPurchases()
		{	
			$purchases = Yii::$app->cache->get('purchases');

			if(!$purchases){
				$purchases = Tender::getPurchases();
				Yii::$app->cache->set('purchases', $purchases, 86400);
			}

			return $purchases;
		}

		public function getSources()
		{

			$sources = Yii::$app->cache->get('sources');

			if(!$sources){
				$sources = TenderSource::getAll();
				Yii::$app->cache->set('sources', $sources, 86400);
			}

			return $sources;
		}

		public function getSectors()
		{
			$sectors = Yii::$app->cache->get('sectors');

			if(!$sectors){
				$sectors = TenderSector::getAll();
				Yii::$app->cache->set('sectors', $sectors, 86400);
			}

			return $sectors;
		}

		public function getOrigList()
		{
			$origlist = Yii::$app->cache->get('origlist');

			if(!$origlist){
				$origlist = TenderOrig::getAll();
				Yii::$app->cache->set('origlist', $origlist, 86400);
			}

			return $origlist;
		}


		public function getOgnList()
		{
			$ognlist = Yii::$app->cache->get('ognlist');

			if(!$ognlist){
				$ognlist = TenderOgn::getAll();
				Yii::$app->cache->set('ognlist', $ognlist, 86400);
			}

			return $ognlist;
		}

		public function getDepList()
		{
			$deplist = Yii::$app->cache->get('deplist');

			if(!$deplist){
				$deplist = DepList::all();
				Yii::$app->cache->set('deplist', $deplist, 86400);
			}

			return $deplist;
		}

		public function getProtocols()
		{
			$protocols = Yii::$app->cache->get('protocols');

			if(!$protocols){
				$protocols = ProtocolDictionary::getAll();
				Yii::$app->cache->set('protocols', $protocols, 86400);
			}

			return $protocols;
			
		}

		public function getUsers()
		{
			$users = Yii::$app->cache->get('users');

			if(!$users){
				$users = User::all();
				Yii::$app->cache->set('users', $users, 14400);
			}

			return $users;
		}

		public function getOnlyManagers($depid = 0)
		{
			$managers = Yii::$app->cache->get('managers');

			// if (!$managers && $depid) {
			// 	# code...
			// }

			if ($depid != 0) {
				$managers = User::getOnlyManagers();
				return $managers;
			}

			if(!$managers){
				$managers = User::getOnlyManagers();
				Yii::$app->cache->set('managers', $managers, 14400);
			}

			return $managers;
		}

		public function getDrtpSpecialists()
		{
			$drtp_specialists = Yii::$app->cache->get('drtp_specialists');

			if(!$drtp_specialists){
				$drtp_specialists = User::getDrtpSpecialists();
				Yii::$app->cache->set('drtp_specialists', $drtp_specialists, 14400);
			}

			return $drtp_specialists;
		}

		public function getLotStatuses()
		{
			$lot_statuses = Yii::$app->cache->get('lot_statuses');

			if(!$lot_statuses){
				$lot_statuses = LotStatus::getAll();
				Yii::$app->cache->set('lot_statuses', $lot_statuses, 86400);
			}

			return $lot_statuses;
		}

		public function getLotIndications()
		{	
			$lot_indications = Yii::$app->cache->get('lot_indications');

			if(!$lot_indications){
				$lot_indications = LotIndication::getAll();
				Yii::$app->cache->set('lot_indications', $lot_indications, 86400);
			}

			return $lot_indications;
		}

		public function getLotPercents()
		{
			$lot_percents = Yii::$app->cache->get('lot_percents');

			if(!$lot_percents){
				$lot_percents =  LotPercent::getAll();
				Yii::$app->cache->set('lot_percents', $lot_percents, 86400);
			}

			return $lot_percents;
		}

		public function getLotConclusions()
		{
			$lot_cncls = Yii::$app->cache->get('lot_cncls');

			if(!$lot_cncls){
				$lot_cncls = LotConclusion::getAll();
				Yii::$app->cache->set('lot_cncls', $lot_cncls, 86400);
			}

			return $lot_cncls;
		}
}