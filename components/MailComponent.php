<?php 
namespace app\components;


use yii\base\Component;
use Yii;

use yii\helpers\ArrayHelper;
use app\models\User;


class MailComponent extends Component
{

	public function sentRiskSigners($users, $lotInfo){

        ini_set('SMTP',Yii::$app->params['cfg_smtpServer']);
        ini_set('sendmail_from',Yii::$app->params['cfg_fromAddress']);
        $headers = "From:docs@kaztranscom.kz\r\n";
        $headers.= "MIME-Version: 1.0\r\n";
        $headers.= "Content-type: text/html; charset=UTF-8\r\n";

        $theme='kCRM. Тендеры. Согласование рисков.';

        $txt='<html>
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <title>kCRM KazTransCom</title>
                </head>
                <body style="background:#ffffff;">
                    <table style="width:600px;" cellpadding="10" cellspacing="3" border="0">
                    <tr>
                    <td style="width:30px;vertical-align:top;border-right:1px solid #cccccc;background:#87aade;">&nbsp;</td>
                    <td>
                        <a style="font:28px Arial;color#999999;">Вас добавили в список подписантов лота. Перейдите по ссылке для подписания <a style="font:14px Arial;color#555555;" href="'.Yii::$app->params['main_site_domain'].Yii::$app->params['module_folder_name'].'index.php?r=lot%2Fview&id='.$lotInfo['lotid'].'" target="_blank" title="Открыть лот">Лот. Номер лота '.$lotInfo['lot_number'].'</a><br /><br />
                        <a style="font:14px Arial;color#555555;">С Уважением,<br />
                        KTC One<br />
                        </a>
                    </td>
                    </tr>
                    </table>
                </body>
        </html>';


        foreach ($users as $key => $value) {

            if (Yii::$app->UserComponent->getById($value['s_userid'])['email'] == '') {
                $recipient = Yii::$app->UserComponent->getById($value['s_userid'])['login'].'@'.Yii::$app->params['cfg_mail_domain'];
            } else {
                $recipient = Yii::$app->UserComponent->getById($value['s_userid'])['email'];
            } 

            if (mail($recipient,$theme,$txt,$headers, '-f'.Yii::$app->params['cfg_fromAddress'])) {
                Yii::info('Сообщение было отправлено пользователю '.$recipient.' по лоту '.$lotInfo['lotid'], 'mailsToSignersOfLot');
            } else {
                Yii::info('Сообщение не было отправлено пользователю '.Yii::$app->UserComponent->getById($value['s_userid'])['email'].' по лоту '.$lotInfo['lotid'], 'mailsToSignersOfLot');
            }
        }

        return true;
    }

     public function sendDepApproveSolution($depid, $lotid, $lot_number, $lot_theme)
    {
        if ($depid == 0) {
           return true; 
        } else {
            ini_set('SMTP',Yii::$app->params['cfg_smtpServer']);
            ini_set('sendmail_from',Yii::$app->params['cfg_fromAddress']);

            $headers = "From:docs@kaztranscom.kz\r\n";
            $headers.= "MIME-Version: 1.0\r\n";
            $headers.= "Content-type: text/html; charset=UTF-8\r\n";

            $theme='kCRM. Тендеры. Отказ от участия.';

            $txt='<html>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <title>kCRM KazTransCom</title>
            </head>
            <body style="background:#ffffff;">
            <table style="width:600px;" cellpadding="10" cellspacing="3" border="0">
            <tr>
            <td style="width:30px;vertical-align:top;border-right:1px solid #cccccc;background:#87aade;">&nbsp;</td>
            <td>
            <a style="font:28px Arial;color#999999;">Менеджер отказался от участия в тендере по лоту: <b>'.$lot_theme.'</b> .Перейдите по ссылке, и подтвердите решение. <br> <a style="font:14px Arial;color#555555;" href="'.Yii::$app->params['main_site_domain'].Yii::$app->params['module_folder_name'].'index.php?r=lot%2Fview&id='.$lotid.'" target="_blank" title="Открыть тендер">Номер лота '.$lot_number.'</a><br /><br />
            <a style="font:14px Arial;color#555555;">С Уважением,<br />
            KTC One<br />
            </a>
            </td>
            </tr>
            </table>
            </body>
            </html>';

            $priv_users = (new \yii\db\Query())
                        ->select(['userid'])
                        ->from('privusers')
                        ->where(['depid' => $depid, 'tender'=>3, 'isactive'=>1])
                        ->all();

            foreach ($priv_users as $key => $value) {
                if (Yii::$app->UserComponent->getById($value['userid'])['email'] == '') {
                    $recipient = Yii::$app->UserComponent->getById($value['userid'])['login'].'@'.Yii::$app->params['cfg_mail_domain'];
                } else {
                    $recipient = Yii::$app->UserComponent->getById($value['userid'])['email'];
                }
                mail($recipient,$theme,$txt,$headers, '-f'.Yii::$app->params['cfg_fromAddress']);
            }
            return true;
        }
    }

    public function sendDepMail($depid, $tenderid, $lot_number, $lot_theme)
    {
        if ($depid == 0) {
           return true; 
        } else {
            ini_set('SMTP',Yii::$app->params['cfg_smtpServer']);
            ini_set('sendmail_from',Yii::$app->params['cfg_fromAddress']);

            $headers = "From:docs@kaztranscom.kz\r\n";
            $headers.= "MIME-Version: 1.0\r\n";
            $headers.= "Content-type: text/html; charset=UTF-8\r\n";

            $theme='kCRM. Тендеры. Делегирование лота на подразделение.';

            $txt='<html>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <title>kCRM KazTransCom</title>
            </head>
            <body style="background:#ffffff;">
            <table style="width:600px;" cellpadding="10" cellspacing="3" border="0">
            <tr>
            <td style="width:30px;vertical-align:top;border-right:1px solid #cccccc;background:#87aade;">&nbsp;</td>
            <td>
            <a style="font:28px Arial;color#999999;">В ваше подразделение был делегирован лот. Наименование лота закупки: <b>'.$lot_theme.'</b> .Перейдите по ссылке, чтобы см. больше информации. <br> <a style="font:14px Arial;color#555555;" href="'.Yii::$app->params['main_site_domain'].Yii::$app->params['module_folder_name'].'index.php?r=tender%2Fview&id='.$tenderid.'" target="_blank" title="Открыть тендер">Номер лота '.$lot_number.'</a><br /><br />
            <a style="font:14px Arial;color#555555;">С Уважением,<br />
            KTC One<br />
            </a>
            </td>
            </tr>
            </table>
            </body>
            </html>';

            $priv_users = (new \yii\db\Query())
                        ->select(['userid'])
                        ->from('privusers')
                        ->where(['depid' => $depid, 'tender'=>3, 'isactive'=>1])
                        ->all();

            foreach ($priv_users as $key => $value) {
                if (Yii::$app->UserComponent->getById($value['userid'])['email'] == '') {
                    $recipient = Yii::$app->UserComponent->getById($value['userid'])['login'].'@'.Yii::$app->params['cfg_mail_domain'];
                } else {
                    $recipient = Yii::$app->UserComponent->getById($value['userid'])['email'];
                }
                mail($recipient,$theme,$txt,$headers, '-f'.Yii::$app->params['cfg_fromAddress']);
            }
            return true;
        }
    }

    public function sendChatMail($userId, $lotId)
    {
        if ($lotId == 0) {

           return false; 

        } else {

            ini_set('SMTP',Yii::$app->params['cfg_smtpServer']);
            ini_set('sendmail_from',Yii::$app->params['cfg_fromAddress']);

            $headers = "From:docs@kaztranscom.kz\r\n";
            $headers.= "MIME-Version: 1.0\r\n";
            $headers.= "Content-type: text/html; charset=UTF-8\r\n";

            $theme='kCRM. Тендеры. Приглашение в чат для обсуджение лота.';

            $txt='<html>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <title>kCRM KazTransCom</title>
            </head>
            <body style="background:#ffffff;">
            <table style="width:600px;" cellpadding="10" cellspacing="3" border="0">
            <tr>
            <td style="width:30px;vertical-align:top;border-right:1px solid #cccccc;background:#87aade;">&nbsp;</td>
            <td>
            <a style="font:28px Arial;color#999999;">Вас пригласили в чат лота для обсуждения. Наименование лота закупки: <b>'.$lotId.'</b> .Перейдите по ссылке, чтобы см. больше информации. <br> <a style="font:14px Arial;color#555555;" href="'.Yii::$app->params['main_site_domain'].Yii::$app->params['module_folder_name'].'index.php?r=lot%2Fview&id='.$lotId.'" target="_blank" title="Открыть лот">Номер лота '.$lotId.'</a><br /><br />
            <a style="font:14px Arial;color#555555;">С Уважением,<br />
            KTC One<br />
            </a>
            </td>
            </tr>
            </table>
            </body>
            </html>';

            if (Yii::$app->UserComponent->getById($userId)) {

                $recipient = Yii::$app->UserComponent->getById($userId);
                
                if($recipient['email'] == '') {
                    $recipient['email'] = $recipient['login'].'@'.Yii::$app->params['cfg_mail_domain'];
                }

                mail($recipient['email'], $theme, $txt, $headers, '-f'.Yii::$app->params['cfg_fromAddress']);

            } else {

                return false;
            }

            return true;
        }
    }

    public function sendReqToChangeManager($lotid)
    {
        ini_set('SMTP',Yii::$app->params['cfg_smtpServer']);
        ini_set('sendmail_from',Yii::$app->params['cfg_fromAddress']);

        $headers = "From:docs@kaztranscom.kz\r\n";
        $headers.= "MIME-Version: 1.0\r\n";
        $headers.= "Content-type: text/html; charset=UTF-8\r\n";


        $theme='kCRM. Тендеры. Заявка на переделегирование менеджера.';


        $txt='<html>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <title>kCRM KazTransCom</title>
            </head>
            <body style="background:#ffffff;">
            <table style="width:600px;" cellpadding="10" cellspacing="3" border="0">
            <tr>
            <td style="width:30px;vertical-align:top;border-right:1px solid #cccccc;background:#87aade;">&nbsp;</td>
            <td>
            <a style="font:28px Arial;color#999999;">Вам поступила заявка на смену менеджера.<br><a style="font:14px Arial;color#555555;" href="'.Yii::$app->params['main_site_domain'].Yii::$app->params['module_folder_name'].'index.php?r=lot%2Fview&id='.$lotid.'" target="_blank" title="Открыть лот">Ссылка</a><br /><br />
            <a style="font:14px Arial;color#555555;">С Уважением,<br />
            KTC One<br />
            </a>
            </td>
            </tr>
            </table>
            </body>
            </html>';

        $priv_users = (new \yii\db\Query())
                        ->select(['userid'])
                        ->from('privusers')
                        ->where(['depid' => 0, 'tender'=>3, 'isactive'=>1])
                        ->all();


        foreach ($priv_users as $key => $value) {
            if (Yii::$app->UserComponent->getById($value['userid'])['email'] == '') {
                $recipient = Yii::$app->UserComponent->getById($value['userid'])['login'].'@'.Yii::$app->params['cfg_mail_domain'];
            } else {
                $recipient = Yii::$app->UserComponent->getById($value['userid'])['email'];
            }
            mail($recipient,$theme,$txt,$headers, '-f'.Yii::$app->params['cfg_fromAddress']);
        }
    }

    public function sendManagerMail($userid, $tenderid, $lot_number, $lot_theme)
    {
        if ($userid == 0) {
            return true;
        } else {
            ini_set('SMTP',Yii::$app->params['cfg_smtpServer']);
            ini_set('sendmail_from',Yii::$app->params['cfg_fromAddress']);

            $headers = "From:docs@kaztranscom.kz\r\n";
            $headers.= "MIME-Version: 1.0\r\n";
            $headers.= "Content-type: text/html; charset=UTF-8\r\n";

            $theme='kCRM. Тендеры. Делегирование лота на подразделение.';

            $txt='<html>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <title>kCRM KazTransCom</title>
            </head>
            <body style="background:#ffffff;">
            <table style="width:600px;" cellpadding="10" cellspacing="3" border="0">
            <tr>
            <td style="width:30px;vertical-align:top;border-right:1px solid #cccccc;background:#87aade;">&nbsp;</td>
            <td>
            <a style="font:28px Arial;color#999999;">Вам делегировали лот. Наименование лота закупки: <b>'.$lot_theme.'</b> Перейдите по ссылку для большей информации. <br><a style="font:14px Arial;color#555555;" href="'.Yii::$app->params['main_site_domain'].Yii::$app->params['module_folder_name'].'index.php?r=tender%2Fview&id='.$tenderid.'" target="_blank" title="Открыть тендер">Тендер. Номер лота '.$lot_number.'</a><br /><br />
            <a style="font:14px Arial;color#555555;">С Уважением,<br />
            KTC One<br />
            </a>
            </td>
            </tr>
            </table>
            </body>
            </html>';


            if (Yii::$app->UserComponent->getById($userid)['email'] == '') {
                $recipient = Yii::$app->UserComponent->getById($userid)['login'].'@'.Yii::$app->params['cfg_mail_domain'];
            } else {
                $recipient = Yii::$app->UserComponent->getById($userid)['email'];
            }

            return mail($recipient,$theme,$txt,$headers, '-f'.Yii::$app->params['cfg_fromAddress']);
        }
    }

    public function sendCheckListMail($lotid, $users)
    {
        ini_set('SMTP',Yii::$app->params['cfg_smtpServer']);
        ini_set('sendmail_from',Yii::$app->params['cfg_fromAddress']);
        $headers = "From:docs@kaztranscom.kz\r\n";
        $headers.= "MIME-Version: 1.0\r\n";
        $headers.= "Content-type: text/html; charset=UTF-8\r\n";

        $theme='kCRM. Тендеры. Чеклист.';

        $txt='<html>
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <title>kCRM KazTransCom</title>
                </head>
                <body style="background:#ffffff;">
                    <table style="width:600px;" cellpadding="10" cellspacing="3" border="0">
                    <tr>
                    <td style="width:30px;vertical-align:top;border-right:1px solid #cccccc;background:#87aade;">&nbsp;</td>
                    <td>
                        <a style="font:28px Arial;color#999999;">Вас добавили в список чеклист лота. Перейдите по ссылке для подписания <a style="font:14px Arial;color#555555;" href="'.Yii::$app->params['main_site_domain'].Yii::$app->params['module_folder_name'].'index.php?r=lot%2Fview&id='.$lotid.'" target="_blank" title="Открыть лот">Лот.</a><br /><br />
                        <a style="font:14px Arial;color#555555;">С Уважением,<br />
                        KTC One<br />
                        </a>
                    </td>
                    </tr>
                    </table>
                </body>
        </html>';


        foreach ($users as $key => $value) {

            if (Yii::$app->UserComponent->getById($value['userid'])['email'] == '') {
                $recipient = Yii::$app->UserComponent->getById($value['userid'])['login'].'@'.Yii::$app->params['cfg_mail_domain'];
            } else {
                $recipient = Yii::$app->UserComponent->getById($value['userid'])['email'];
            } 

            if (mail($recipient,$theme,$txt,$headers, '-f'.Yii::$app->params['cfg_fromAddress'])) {
                Yii::info('Сообщение было отправлено пользователю '.$recipient.' по лоту '.$lotid, 'mailsToCheckList');
            } else {
                Yii::info('Сообщение не было отправлено пользователю '.$recipient.' по лоту '.$lotid, 'mailsToCheckList');
            }
        }

        return true;
    }

    public function sendToSpecialist($tenderid, $specialistId)
    {
        ini_set('SMTP',Yii::$app->params['cfg_smtpServer']);
        ini_set('sendmail_from',Yii::$app->params['cfg_fromAddress']);

        $headers = "From:docs@kaztranscom.kz\r\n";
        $headers.= "MIME-Version: 1.0\r\n";
        $headers.= "Content-type: text/html; charset=UTF-8\r\n";

        $theme='kCRM. Тендеры.';

            $txt='<html>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                        <title>kCRM KazTransCom</title>
                    </head>
                    <body style="background:#ffffff;">
                        <table style="width:600px;" cellpadding="10" cellspacing="3" border="0">
                        <tr>
                        <td style="width:30px;vertical-align:top;border-right:1px solid #cccccc;background:#87aade;">&nbsp;</td>
                        <td>
                        <a style="font:28px Arial;color#999999;">Вам делегировали тендер.<br><a style="font:14px Arial;color#555555;" href="'.Yii::$app->params['main_site_domain'].Yii::$app->params['module_folder_name'].'index.php?r=tender%2Fview&id='.$tenderid.'" target="_blank" title="Открыть тендер">Ссылка</a><br /><br />
                        <a style="font:14px Arial;color#555555;">С Уважением,<br />
                        KTC One<br />
                        </a>
                        </td>
                        </tr>
                        </table>
                    </body>
                </html>';

        if (Yii::$app->UserComponent->getById($specialistId)['email'] == '') {
            $recipient = Yii::$app->UserComponent->getById($specialistId)['login'].'@'.Yii::$app->params['cfg_mail_domain'];
        } else {
            $recipient = Yii::$app->UserComponent->getById($specialistId)['email'];
        }

        return mail($recipient,$theme,$txt,$headers, '-f'.Yii::$app->params['cfg_fromAddress']);
    }

    public function sendNotifyToSpecialist($lotid, $purchase_number, $purchase_name, $lot_name, $csname)
    {
        ini_set('SMTP',Yii::$app->params['cfg_smtpServer']);
        ini_set('sendmail_from',Yii::$app->params['cfg_fromAddress']);

        $headers = "From:docs@kaztranscom.kz\r\n";
        $headers.= "MIME-Version: 1.0\r\n";
        $headers.= "Content-type: text/html; charset=UTF-8\r\n";

        $theme='kCRM. Тендеры.';

        $txt='<html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                    <title>kCRM KazTransCom</title>
                </head>
                <body style="background:#ffffff;">
                    <table style="width:600px;" cellpadding="10" cellspacing="3" border="0">
                    <tr>
                    <td style="width:30px;vertical-align:top;border-right:1px solid #cccccc;background:#87aade;">&nbsp;</td>
                    <td>
                    <a style="font:17px Arial;color#999999;">Менеджер принял решение участвовать в лоте.<br>
                        Заказчик:<b>'.$csname.'</b>.<br>
                        Номер закупки: <b>'.$purchase_number.'</b>.<br>
                        Наименование закупки: <b>'.$purchase_name.'</b> <br>
                        Наименование лота: <b>'.$lot_name.'</b>.<br></a>
                        <a style="font:14px Arial;color#555555;" href="'.Yii::$app->params['main_site_domain'].Yii::$app->params['module_folder_name'].'index.php?r=lot%2Fview&id='.$lotid.'" target="_blank" title="Открыть лот">Ссылка</a><br /><br />
                    <a style="font:14px Arial;color#555555;">С Уважением,<br />
                    KTC One<br />
                    </a>
                    </td>
                    </tr>
                    </table>
                </body>
            </html>';

        $specialists = User::getDrtpSpecialists();

        foreach ($specialists as $key => $value) {

            if (Yii::$app->UserComponent->getById($value['userid'])['email'] == '') {
                $recipient = Yii::$app->UserComponent->getById($value['userid'])['login'].'@'.Yii::$app->params['cfg_mail_domain'];
            } else {
                $recipient = Yii::$app->UserComponent->getById($value['userid'])['email'];
            } 
            if (mail($recipient,$theme,$txt,$headers, '-f'.Yii::$app->params['cfg_fromAddress'])) {
                Yii::info('Сообщение было отправлено пользователю '.$recipient.' по лоту '.$lotid, 'notifySpecialist');
            } else {
                Yii::info('Сообщение не было отправлено пользователю '.$recipient.' по лоту '.$lotid, 'notifySpecialist');
            }
        }

    }

    public function sendToUser($userId, $lotId) {
        ini_set('SMTP',Yii::$app->params['cfg_smtpServer']);
        ini_set('sendmail_from',Yii::$app->params['cfg_fromAddress']);

        $headers = "From:docs@kaztranscom.kz\r\n";
        $headers.= "MIME-Version: 1.0\r\n";
        $headers.= "Content-type: text/html; charset=UTF-8\r\n";


        $theme='kCRM. Тендеры.';

            $txt='<html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <title>kCRM KazTransCom</title>
            </head>
            <body style="background:#ffffff;">
                <table style="width:600px;" cellpadding="10" cellspacing="3" border="0">
                <tr>
                <td style="width:30px;vertical-align:top;border-right:1px solid #cccccc;background:#87aade;">&nbsp;</td>
                <td>
                <a style="font:17px Arial;color#999999;">Уведомление! Необходимо внести обеспечения в лот.<br>
                <a style="font:14px Arial;color#555555;"  href="'.Yii::$app->params['main_site_domain'].Yii::$app->params['module_folder_name'].'index.php?r=lot%2Fview&id='.$lotId.'" target="_blank" title="Открыть лот">Ссылка</a><br /><br />
                
                <a style="font:14px Arial;color#555555;">С Уважением,<br />
                KTC One<br />
                </a>
                </td>
                </tr>
                </table>
            </body>
        </html>';

        if (Yii::$app->UserComponent->getById($userId)['email'] == '') {
            $recipient = Yii::$app->UserComponent->getById($userId)['login'].'@'.Yii::$app->params['cfg_mail_domain'];
        } else {
            $recipient = Yii::$app->UserComponent->getById($userId)['email'];
        }

        return mail($recipient,$theme,$txt,$headers, '-f'.Yii::$app->params['cfg_fromAddress']);
    }

    public function notifyLotManagers($tenderId) {
        ini_set('SMTP',Yii::$app->params['cfg_smtpServer']);
        ini_set('sendmail_from',Yii::$app->params['cfg_fromAddress']);

        $headers = "From:docs@kaztranscom.kz\r\n";
        $headers.= "MIME-Version: 1.0\r\n";
        $headers.= "Content-type: text/html; charset=UTF-8\r\n";


        $theme='kCRM. Тендеры.';

        $txt='<html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <title>kCRM KazTransCom</title>
            </head>
            <body style="background:#ffffff;">
                <table style="width:600px;" cellpadding="10" cellspacing="3" border="0">
                <tr>
                <td style="width:30px;vertical-align:top;border-right:1px solid #cccccc;background:#87aade;">&nbsp;</td>
                <td>
                <a style="font:17px Arial;color#999999;">Уведомление! Протокол для тендера '. $tenderId.' загружен в систему.<br>
                <a style="font:14px Arial;color#555555;"  href="'.Yii::$app->params['main_site_domain'].'extra_modules/index.php?r=tender%2Fview&id='.$tenderId.'" target="_blank" title="Открыть лот">Ссылка</a><br /><br />
                
                <a style="font:14px Arial;color#555555;">С Уважением,<br />
                KTC One<br />
                </a>
                </td>
                </tr>
                </table>
            </body>
        </html>';

        $managers = Yii::$app->UserComponent->getTenderLotManagers($tenderId);

        if($managers) {

            foreach($managers as $manager) {
                
                if (Yii::$app->UserComponent->getById($manager)['email'] == '') {
                    $recipient = Yii::$app->UserComponent->getById($manager)['login'].'@'.Yii::$app->params['cfg_mail_domain'];
                } else {
                    $recipient = Yii::$app->UserComponent->getById($manager)['email'];
                }
        
                $result = mail($recipient,$theme,$txt,$headers, '-f'.Yii::$app->params['cfg_fromAddress']);

                // Debug

                /* if (Yii::$app->UserComponent->getById(1810)['email'] == '') {
                    $recipient = Yii::$app->UserComponent->getById(1810)['login'].'@'.Yii::$app->params['cfg_mail_domain'];
                } else {
                    $recipient = Yii::$app->UserComponent->getById(1810)['email'];
                }

                $result = mail($recipient,$theme,$txt,$headers, '-f'.Yii::$app->params['cfg_fromAddress']); */
            }

            return true;
        }

        return false;
    }

    public function notifyRiskManagers($lotId, $managers) {
        ini_set('SMTP',Yii::$app->params['cfg_smtpServer']);
        ini_set('sendmail_from',Yii::$app->params['cfg_fromAddress']);

        $headers = "From:docs@kaztranscom.kz\r\n";
        $headers.= "MIME-Version: 1.0\r\n";
        $headers.= "Content-type: text/html; charset=UTF-8\r\n";


        $theme='kCRM. Тендеры.';

        $txt='<html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <title>kCRM KazTransCom</title>
            </head>
            <body style="background:#ffffff;">
                <table style="width:600px;" cellpadding="10" cellspacing="3" border="0">
                <tr>
                <td style="width:30px;vertical-align:top;border-right:1px solid #cccccc;background:#87aade;">&nbsp;</td>
                <td>
                <a style="font:17px Arial;color#999999;">Уведомление! Доступно новое сообщение в лоте '. $lotId.'.<br>
                <a style="font:14px Arial;color#555555;"  href="'.Yii::$app->params['main_site_domain'].'extra_modules/index.php?r=lot%2Fview&id='.$lotId.'" target="_blank" title="Открыть лот">Ссылка</a><br /><br />
                
                <a style="font:14px Arial;color#555555;">С Уважением,<br />
                KTC One<br />
                </a>
                </td>
                </tr>
                </table>
            </body>
        </html>';

        if($managers) {

            foreach($managers as $manager) {
                
                if (Yii::$app->UserComponent->getById($manager)['email'] == '') {
                    $recipient = Yii::$app->UserComponent->getById($manager)['login'].'@'.Yii::$app->params['cfg_mail_domain'];
                } else {
                    $recipient = Yii::$app->UserComponent->getById($manager)['email'];
                }
        
                mail($recipient,$theme,$txt,$headers, '-f'.Yii::$app->params['cfg_fromAddress']);
            }

            return true;
        }

        return false;
    }
}

