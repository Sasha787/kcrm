<?php 
namespace app\components;


use yii\base\Component;
use Yii;

use yii\helpers\ArrayHelper;

class UserComponent extends Component
{
	public function getById($id)
	{
		return Yii::$app->db2->createCommand("SELECT users.userid,users.login,users.fullname,users.position,users.depid,users.email, deplist.dname FROM contracts.users JOIN contracts.deplist ON `contracts`.`users`.`depid` = `contracts`.`deplist`.`depid`  WHERE userid= $id AND blocked=0 LIMIT 1")->queryOne();
       	
	}

	public function getTenderPermissionById($id)
	{
		$role = (new \yii\db\Query())
            ->select(['depid', 'tender', 'tender_hidden', 'fullacc'])
            ->from('privusers')
            ->where(['privusers.userid' => $id, 'privusers.isactive'=>1])
            ->andWhere(['in', 'privusers.tender', [1,2,3,4]])
            ->all();

        if ($role) {
        	return ['fullacc'=>$role[0]['fullacc'], 'tender'=>$role[0]['tender'], 'depid'=> ArrayHelper::getColumn($role, 'depid'), 'tender_hidden' => $role[0]['tender_hidden']];
        } else {
        	return ['fullacc'=>0,'tender'=>0, 'depid'=>[0], 'tender_hidden'=>0];
        }

	}
}

