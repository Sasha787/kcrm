<?php 
namespace app\components;

use yii\base\Component;
use Yii;

class FileComponent extends Component
{

	public function pcgbasename($param, $suffix=null) { 
		if ( $suffix ) { 
			$tmpstr = ltrim(substr($param, strrpos($param, DIRECTORY_SEPARATOR) ), DIRECTORY_SEPARATOR); 
			if ( (strpos($param, $suffix)+strlen($suffix) )  ==  strlen($param) ) { 
				return str_ireplace( $suffix, '', $tmpstr); 
			} else { 
				return ltrim(substr($param, strrpos($param, DIRECTORY_SEPARATOR) ), DIRECTORY_SEPARATOR); 
			} 
		} else { 
			return ltrim(substr($param, strrpos($param, DIRECTORY_SEPARATOR) ), DIRECTORY_SEPARATOR); 
		} 
	}

	public function get($file){

		if(file_exists($file)){
			$ctype='octet-stream';
			$cdisp='attachment';

			if(strtolower(substr($file,-3,3))=='pdf') {
				$ctype='pdf';
				$cdisp='inline';
			}                              

			header ("Content-Type: application/".$ctype);
			header ("Accept-Ranges: bytes");
			header ("Content-Length: ".filesize($file));
			header ("Content-Disposition: ".$cdisp."; filename=".$this->pcgbasename($file));  
			@readfile($file);
			
		} else {
			return "File not exist";
		}

	}
}

?>