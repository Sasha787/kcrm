/**
*
*		функции для блока Менеджер Лота
*
*/

function approveChangeManager(rowId, checked, lotid, user3id, user2id)
{
	var check = confirm("Вы уверены?");

	if (check) {
		$.ajax({
			url: 'index.php?r=lot-ajax%2Fapprove-change-manager',
			type: 'POST',
			datatype:'json',
			data : { 
				rowId: rowId,
				checked: +checked,
				lotid: lotid,
				user3id: user3id,
				user2id: user2id,
			},
			success: function(data){
				console.log(data);
			}
		});
	}
}

function newManager(lotid, curManager)
{
		var lotid = lotid;
		var curManager = curManager;
		var reason = $('#new-manager-reason').val();
		var nextManager = $('#manager-modal').val();

		if (!reason) {
			alert('Укажите причину смены менеджера');
			return 0;
		}
		$.ajax({
			url: 'index.php?r=lot-ajax%2Fchange-manager',
			type: 'POST',
			datatype:'json',
			data : {
				lotid: lotid,
				curManager: curManager,
				reason: reason,
				nextManager: nextManager
			},
			success: function(result){
			
				$('#manager-modal').val(0).change();
				$('#new-manager-reason').val('').change();

				
				$('#new_manager_modal').modal('hide');

				if (result == false) {
					$.toast({
						text : 'Что-то пошло не так:(',
						heading: 'Ошибка',
						showHideTransition: 'slide',
						loader: false,
						loaderBg: '#9EC600',
						position: 'top-right',
						bgColor: '#DD5145'
					});
				} else {
					console.log(result)
					var row = $('<tr id="mh_id_'+result.id+'" />')
					$('#manager_history_table').prepend(row);
					row.append($('<td>'+result.userid_fullname+'</td>')); 
					row.append($('<td>'+result.user2id_fullname+'</td>'));
					row.append($('<td>'+result.user3id_fullname+'</td>'));
					row.append($('<td>'+result.mh_reason+'</td>'));
					row.append($('<td>'+result.created_at+'</td>'));
					row.append($('<td></td>'));
					row.append($('<td>Не увтержден</td>'));
						$.toast({
							text : 'Заявка подана. Ожидайте ответа.',
							heading: 'Успешно',
							showHideTransition: 'slide',
							loader: false,
							loaderBg: '#9EC600',
							position: 'top-right',
							bgColor: '#1BA261'
						});
					}

				}});
}


/**
*
*		функции для блока ОБЕСПЕЧЕНИЯ
*
*/

function applicationIsNeed(lotid, checked)
{
	var tenderid = $('#lot_tenderid').val();
	$.ajax({
		url: 'index.php?r=lot-ajax%2Fapp-is-need&tenderid='+tenderid,
		type: 'POST',
		datatype:'json',
		data : { 
			lotid: lotid,
			checked: +checked
		},
		success: function(data){
			console.log(data);
			
			$('#ap_amount').text(data.ap_amount.toLocaleString('us', {minimumFractionDigits: 2, maximumFractionDigits: 2}) + ' тг');
		}
	});
}


function applicationIsExist(lotid, checked)
{
	var tenderid = $('#lot_tenderid').val();
	$.ajax({
		url: 'index.php?r=lot-ajax%2Fapp-is-exist&tenderid='+tenderid,
		type: 'POST',
		datatype:'json',
		data : { 
			lotid: lotid,
			checked: +checked
		},
		success: function(data){
			console.log(data);
		}
	});
}


function applicationIsReturn(lotid, checked)
{
	var tenderid = $('#lot_tenderid').val();
	$.ajax({
		url: 'index.php?r=lot-ajax%2Fapp-is-return&tenderid='+tenderid,
		type: 'POST',
		datatype:'json',
		data : { 
			lotid: lotid,
			checked: +checked
		},
		success: function(data){
			console.log(data);
		}
	});
}

function agreementIsNeed(lotid, checked)
{
	var tenderid = $('#lot_tenderid').val();
	$.ajax({
		url: 'index.php?r=lot-ajax%2Fagree-is-need&tenderid='+tenderid,
		type: 'POST',
		datatype:'json',
		data : { 
			lotid: lotid,
			checked: +checked
		},
		success: function(data){
			console.log(data);
		}
	});
}


function agreementIsExist(lotid, checked)
{
	var tenderid = $('#lot_tenderid').val();
	$.ajax({
		url: 'index.php?r=lot-ajax%2Fagree-is-exist&tenderid='+tenderid,
		type: 'POST',
		datatype:'json',
		data : { 
			lotid: lotid,
			checked: +checked
		},
		success: function(data){
			console.log(data);
		}
	});
}


function agreementIsReturn(lotid, checked)
{
	var tenderid = $('#lot_tenderid').val();
	$.ajax({
		url: 'index.php?r=lot-ajax%2Fagree-is-return&tenderid='+tenderid,
		type: 'POST',
		datatype:'json',
		data : { 
			lotid: lotid,
			checked: +checked
		},
		success: function(data){
			console.log(data);
		}
	});
}


function changeAmount()
{
	$('#ap_amount').css('display', 'none');
	$('#ap_amount_input').css('display', 'block');
	
	$('#lap_amount').css('display', 'none');
	$('#lap_amount_input').css('display', 'block');

	$('#change_amount_button').css('display', 'none');
	$('#save_amount_button').css('display', 'block');

}

function saveAmount(lotid)
{
	var ap_amount_input = $('#ap_amount_input').val();
	var lap_amount_input = $('#lap_amount_input').val();
	var tenderid = $('#lot_tenderid').val();
	$.ajax({
		url: 'index.php?r=lot-ajax%2Fchange-provision-amount&tenderid='+tenderid,
		type: 'POST',
		datatype:'json',
		data : { 
			ap_amount_input: ap_amount_input,
			lap_amount_input: lap_amount_input,
			lotid: lotid
		},
		success: function(data){

			$('#ap_amount').text(data.ap_amount.toLocaleString('us', {minimumFractionDigits: 2, maximumFractionDigits: 2}) + ' тг').change();
			$('#lap_amount').text(data.lap_amount.toLocaleString('us', {minimumFractionDigits: 2, maximumFractionDigits: 2}) + ' тг').change();

			$('#ap_amount').css('display', 'block');
			$('#ap_amount_input').css('display', 'none');

			$('#lap_amount').css('display', 'block');
			$('#lap_amount_input').css('display', 'none');

			$('#change_amount_button').css('display', 'block');
			$('#save_amount_button').css('display', 'none');
			console.log(data);
		}
	});


}
/**
*
*		Скрипты для блока ПОДПИСАНТЫ
*
*/
function addUser(userid, riskId, lotId)
{
	var tenderid = $('#lot_tenderid').val();
	$.ajax({
		url: 'index.php?r=lot-sign-ajax%2Fadd-sign-user&risk_id=' + riskId + '&tenderid='+tenderid,
		type: 'POST',
		datatype:'json',
		data : { 
			userid: userid,
			lotid: lotId,
			riskId: riskId,
		},
		success: function(data){

			if (data.sid) {
				var result = data;
				if (result == false) {
					$.toast({
						text : 'Данный подписант уже добавлен',
						heading: 'Ошибка',
						showHideTransition: 'slide',
						loader: false,
						loaderBg: '#9EC600',
						position: 'top-right',
						bgColor: '#DD5145'
					});
				} else {
					console.log(result)
					var row = $('<tr id="signed_user_'+result.sid+'" />')
					$('#table_signed_'+result.s_risk_id).append(row);
					row.append($('<td>'+result.s_username+'</td>')); 
					row.append($('<td>'+result.s_dname+'</td>'));
					row.append($('<td>'+result.s_date_received+'</td>')); 
					row.append($('<td><span class="glyphicon glyphicon-remove" style="color:red;"></span></td>')); 
					row.append($('<td>'+result.s_date_signed+'</td>'));
					row.append($('<td>'+result.s_comment+'</td>'));  
					row.append($('<td><button type="button" id="remove" class="btn btn-danger btn-xs" onclick="rmUser('+result.sid+', '+result.s_risk_id+')" style="margin-left: 20px;">Удалить</button></td>'));
					$.toast({
						text : 'Подписант успешно добавлен',
						heading: 'Успешно',
						showHideTransition: 'slide',
						loader: false,
						loaderBg: '#9EC600',
						position: 'top-right',
						bgColor: '#1BA261'
					});
				}
			} else {
				var result = JSON.parse(data);
				if (result.err) {

					$.toast({
						text : result.err,
						heading: 'Внимание!',
						showHideTransition: 'slide',
						textColor: 'black',
						loader: false,
						loaderBg: 'black',
						position: 'top-right',
						bgColor: '#ffff00',
						hideAfter : 7000,
					});
					return 0;
				}
			}
		}});
		$('#lot_signed').val(0).change();
}
function rmUser(sid, riskId)
{
	var tenderid = $('#lot_tenderid').val();
	$.ajax({
		url: 'index.php?r=lot-sign-ajax%2Fremove-user&sid=' + sid + '&risk_id=' + riskId + '&tenderid=' + tenderid,
		type: 'POST',
		success: function(data){
			var result = JSON.parse(data);
			console.log(result);
			if (!result.err) {
				$("#signed_user_"+sid+"").remove();
				$.toast({
					text : 'Подписант успешно удалён.',
					heading: 'Успешно',
					showHideTransition: 'slide',
					loader: false,
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#1BA261',
					hideAfter : 5000,
				}); 
			} else {
				$.toast({
					text : result.err,
					heading: 'Внимание!',
					showHideTransition: 'slide',
					textColor: 'black',
					loader: false,
					loaderBg: 'black',
					position: 'top-right',
					bgColor: '#ffff00',
					hideAfter : 7000,
				});
			}
		},
		error: function(result){
			$.toast({
					text : 'Что-то пошло не так:(',
					heading: 'Ошибка',
					showHideTransition: 'slide',
					loader: false,
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#DD5145'
				});
		}
	});
}



/**
*
*		Скрипты для блока Принятия решения об участии
*
*/


/*
*	 decision - решение. 1 - да. 0 - нет
*	 lotid - id лота
*	 idButton - id кнопки "отменить решение".
*/
function isParticipate(decision, lotid, idButton = 0)
{
	var reason = $('#participate-reason-textarea').val();
	var check = confirm('Вы уверены в принятии решения?');

	if (check) {
		$.ajax({
			url: 'index.php?r=participate-ajax%2Fis-participate',
			type: 'POST',
			datatype:'json',
			data : { 
				decision: decision,
				lotid: lotid,
				reason: reason
			},
			success: function(result){
				console.log(result);
				if (result.status == 404) {
					$.toast({
						text : 'Ошибка.',
						heading: 'Не удалось принять решение.',
						showHideTransition: 'slide',
						loader: false,
						loaderBg: '#9EC600',
						position: 'top-right',
						bgColor: '#DD5145',
						hideAfter : 5000,
					}); 
					return 0;
				}

				if (result.status == 200) {
					$.toast({
						text : 'Успешно.',
						heading: 'Решение принято.',
						showHideTransition: 'slide',
						loader: false,
						loaderBg: '#9EC600',
						position: 'top-right',
						bgColor: '#1BA261',
						hideAfter : 5000,
					}); 

					$('#participate-block').remove();
					$('#cancel-solution-button-'+idButton).remove();

					if (result.res.isParticipate == 1) {
						var solution = '<span class="glyphicon glyphicon-ok" style="color:green">Принятно</span>';
					}

					if (result.res.isParticipate == 0) {
						var solution = '<span class="glyphicon glyphicon-remove" style="color:red">Отказано</span>';
					}

					var row = $('<tr/>')
					$('#table_participate').prepend(row);
					row.append($('<td>'+result.res.manager+'</td>')); 
					row.append($('<td>'+solution+'</td>'));
					row.append($('<td>'+result.res.reason+'</td>')); 
					row.append($('<td>'+result.res.date+'</td>'));
					row.append($('<td>\
						<button class="btn btn-warning btn-xs" onclick="cancelSolution('+lotid+')">Отозвать решения</button>\
						</td>'));  
					
				}

			}
		});
	}
}
/*
*	 lotid - id лота
*	 id -  id записи истории принятии решении.
*/
function cancelSolution(lotid, id)
{
	$.ajax({
		url: 'index.php?r=participate-ajax%2Fcancel-solution&id='+lotid+'&idButton='+id,
		type: 'POST',
		datatype:'json',
		data : { 
			lotid: lotid,
		},
		success: function(data){
			$('#conduct-solution').html(data);
		}
	});
}
function allowParticipate(lotid, checked, managerId)
{
	$.ajax({
		url: 'index.php?r=participate-ajax%2Fallow-participate',
		type: 'POST',
		datatype:'json',
		data : { 
			lotid: lotid,
			checked: +checked, 
			managerId: managerId,
		},
		success: function(data){
			console.log(data);
		}
	});
}

/**
*
*		Скрипты для блока Участников лота
*
*/

function lotWinner(lp_id, is_checked)
{
	$.ajax({
		url: 'index.php?r=lot-ajax%2Flot-winner&id=' + lp_id,
		type: 'POST',
		datatype:'json',
		data : { 
			is_checked: is_checked,
		},
		success: function(data){
			console.log(data);
		}
	});

}

function removeParticipant(id)
{
	$("#lot_participants_"+id).remove();

	$.ajax({
		url: 'index.php?r=lot-ajax%2Fremove-participant&id=' + id,
		type: 'POST',
		success: function(data){
			console.log(data);
			$.toast({
				text : 'Файл успешно удален.',
				heading: 'Удален',
				showHideTransition: 'slide',
				loader: false,
				loaderBg: '#9EC600',
				position: 'top-right',
				bgColor: '#1BA261',
				hideAfter : 3000,
			}); 
		}
	});

}

function addParticipant(lotid)
{
	var lp_title = $('#lp_title').val();
	var lp_competitor_id = $('#lp_competitor_id').val();
	var lp_price = $('#lp_price').val();
	var lp_discount = $('#lp_discount').val();

	$.ajax({
		url: 'index.php?r=lot-ajax%2Fadd-participant',
		type: 'POST',
		datatype:'json',
		data : { 
			lp_title: lp_title,
			lp_competitor_id: lp_competitor_id,
			lp_lotid: lotid,
			lp_price: lp_price,
			lp_discount: lp_discount,
		},
		success: function(result){
		
			if (result == false) {
				$.toast({
					text : 'Ошибка валидации. Введите данные корректно!',
					heading: 'Ошибка',
					showHideTransition: 'slide',
					loader: false,
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#DD5145'
				});
			} else {
				console.log(result)
				var row = $('<tr id="lot_participants_'+result.lp_id+'" />')
				$('#table_participant').append(row);
				row.append($('<td>'+result.lp_title+'</td>')); 
				row.append($('<td>'+new Intl.NumberFormat('ru-RU').format(result.lp_price)+' тг</td>'));
				row.append($('<td>'+result.lp_discount+'%</td>')); 
				row.append($('<td>'+new Intl.NumberFormat('ru-RU').format(result.lp_total)+' тг</td>'));  
				row.append($('<td><input type="checkbox" onchange="lotWinner('+result.lp_id+', +this.checked)" id="lp_is_win_'+result.lp_id+'"></td>'));
				row.append($('<td><button type="button" id="remove" class="btn btn-danger btn-xs" onclick="removeParticipant('+result.lp_id+')" style="margin-left: 20px;">Удалить</button></td>'));
				$.toast({
					text : 'Подписант успешно добавлен',
					heading: 'Успешно',
					showHideTransition: 'slide',
					loader: false,
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#1BA261'
				});
			}

		}});
	$('#lp_title').val('').change();
	$('#lp_price').val('').change();
	$('#lp_discount').val('').change();
}


/**
*
*		Скрипты для блока Рисков
*/

function riskIsActive(isActive, riskId, lotId, tenderid)
{
	$.ajax({
		url: 'index.php?r=risk-ajax%2Frisk-is-active&tenderid='+tenderid,
		type: 'POST',
		datatype:'json',
		data : {
			isActive: +isActive,
			lotid: lotId,
			riskId: riskId,
		},
		success: function(res){

			if (res.status == 200) {

				if (isActive == 1) {
					$('#risk-is-active-1-'+riskId).css('display', 'none');
					$('#risk-is-active-0-'+riskId).css('display', 'block');
				} else {
					$('#risk-is-active-1-'+riskId).css('display', 'block');
					$('#risk-is-active-0-'+riskId).css('display', 'none');
				}

				$.toast({
					text : res.res,
					heading: 'Успешно',
					showHideTransition: 'slide',
					loader: false,
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#1BA261',
					hideAfter : 3000,
				});    

			} else if(res.status == 'warning'){
				$.toast({
					text : res.res,
					heading: 'Внимание!',
					showHideTransition: 'slide',
					loader: false,
					textColor: 'black',
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#ffff00',
					hideAfter : 3000,
				});    
			} else {
				$.toast({
					text : 'Что-то пошло не так.',
					heading: 'Внимание!',
					showHideTransition: 'slide',
					loader: false,
					position: 'top-right',
					bgColor: 'red',
					hideAfter : 3000,
				});
			}

		},
		error: function(err){
			console.log(err);
		}

	});
}

function uploadRiskFile(riskId, lotId, tenderid)
{
	var formData = new FormData();
	formData.append('file', $('#risk-file-input-'+riskId)[0].files[0]);
	formData.append('lotid', lotId);
	formData.append('riskId', riskId);
	formData.append('code', $('#lot_file_code').val());

	$.ajax({
		url: 'index.php?r=risk-ajax%2Fupload-risk-file&risk_id='+riskId+'&tenderid='+tenderid,
		type: 'POST',
		mimeType: 'multipart/form-data',
		data: formData,   
		datatype:'json',
		cache: false,
		contentType: false,
		processData: false,

		success: function (data) {
			var result = JSON.parse(data);
			console.log(result);
			if (result.err) {
				$.toast({
					text : result.err,
					heading: 'Внимание!',
					showHideTransition: 'slide',
					textColor: 'black',
					loader: false,
					loaderBg: 'black',
					position: 'top-right',
					bgColor: '#ffff00',
					hideAfter : 7000,
				}); 
			} else {
				$('#risk-file-input-'+riskId).val("");

				var row = $('<span id="risk_file_'+result.id+'" />')
				row.append($('<a href="index.php?r=risk-ajax%2Fdownload-file&id='+result.id+'">'+result.fileName+'</a><button type="button"  class="btn btn-outline-danger btn-xs" title="Удалить файл" onclick="rmRiskFile('+result.id+', '+riskId+')"><i class="fa fa-remove" aria-hidden="true" style="color:red;"></i></button><br>'));
				$('#risk_files_'+result.risk_id).append(row);
				$.toast({
					text : 'Файл успешно загружен',
					heading: 'Добавлен',
					showHideTransition: 'slide',
					loader: false,
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#1BA261',
					hideAfter : 3000,
				});    
			}
		},

		error: function(err) {
			$.toast({
				text : 'Не удалось загрузить файл',
				heading: 'Ошибка',
				showHideTransition: 'slide',
				loader: false,
				loaderBg: '#9EC600',
				position: 'top-right',
				bgColor: '#DD5145',
				hideAfter : 7000,
			});
		}
	});

	$('#risk-file-input').val('');
}

function rmRiskFile(id, riskId)
{
	$.ajax({
		url: 'index.php?r=risk-ajax%2Fremove-file&fileid=' + id + '&risk_id=' + riskId,
		type: 'POST',
		success: function(data){
			var result = JSON.parse(data);
			if (result.err) {
				$.toast({
					text : result.err,
					heading: 'Внимание!',
					showHideTransition: 'slide',
					textColor: 'black',
					loader: false,
					loaderBg: 'black',
					position: 'top-right',
					bgColor: '#ffff00',
					hideAfter : 7000,
				}); 
			} else {
				$('#risk_file_'+id).remove();
				$.toast({
					text : 'Файл удалён.',
					heading: 'Успешно',
					showHideTransition: 'slide',
					loader: false,
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#1BA261'
				});
			}			
		}
	});
}

function createRisk(riskId, tenderid)
{
	
		$('#risk_create_button_'+riskId).css('display', 'none');
		$('#risk_save_button_'+riskId).css('display', 'block');
	// $('#no_risks').css('display', 'none');

	$('#risk_textarea_'+riskId).css('display', 'block');
	$('#risk_title_'+riskId).css('display', 'none');
	
}


function saveRisk(riskId, lotId, tenderid)
{
	var risk = $('#risk_textarea_'+riskId).val();

	$.ajax({
		url: 'index.php?r=risk-ajax%2Fcreate-risk&risk_id='+riskId+'&tenderid=' + tenderid,
		type: 'POST',
		datatype:'json',
		data : {
			risk: risk,
			lotId: lotId,
		},
		success: function(data){
			
			if (data.lr_id) {
				$('#risk_title_'+riskId).html(data.lr_title.replace(/\n/ig, '<br>'));
				$('#risk_textarea_'+riskId).val(data.lr_title).change();

				$('#author_of_risk_'+riskId).text(data.lr_fullname).change();
				$('#date_of_risk_'+riskId).text(data.lr_updated_at).change();

				$('#risk_create_button_'+riskId).css('display', 'block');
				$('#risk_save_button_'+riskId).css('display', 'none');

				$('#risk_textarea_'+riskId).css('display', 'none');
				$('#risk_title_'+riskId).css('display', 'block');

				$('#risk_info_'+riskId).css('display', 'block');

				if (data == false) {

					$.toast({
						text : 'Ошибка валидации. Введите данные корректно!',
						heading: 'Ошибка',
						showHideTransition: 'slide',
						loader: false,
						loaderBg: '#9EC600',
						position: 'top-right',
						bgColor: '#DD5145'
					});

				} else {

					$.toast({
						text : 'Риски описаны.',
						heading: 'Успешно',
						showHideTransition: 'slide',
						loader: false,
						loaderBg: '#9EC600',
						position: 'top-right',
						bgColor: '#1BA261'
					});
				}
			} else {

				var result = JSON.parse(data);
				console.log(result);

					$.toast({
						text : result.err,
						heading: 'Внимание!',
						showHideTransition: 'slide',
						textColor: 'black',
						loader: false,
						loaderBg: 'black',
						position: 'top-right',
						bgColor: '#ffff00',
						hideAfter : 7000,
					});
			}
		}
	});	
	$('#risk_create_button_'+riskId).css('display', 'block');
	$('#risk_save_button_'+riskId).css('display', 'none');

	$('#risk_textarea_'+riskId).css('display', 'none');
	$('#risk_title_'+riskId).css('display', 'block');
	$('#risk_title_'+riskId).css('color', '#555555');
}


/**
*
*		Скрипты для блока Ценового предложения
*
*/

function change_po(who)
{	
	if (who == 1) {
		$('#po_price').css('display', 'none');
		$('#po_price_input').css('display', 'block');
	}

	if (who == 2) {
		$('#po_discount').css('display', 'none');
		$('#po_discount_input').css('display', 'block');
	}

	/* $('#po_price').css('display', 'none');
	$('#po_discount').css('display', 'none');
	$('#po_price_input').css('display', 'block');
	$('#po_discount_input').css('display', 'block'); */

	$('#po_change_button').css('display', 'none');
	$('#po_save_button').css('display', 'block');
}

function save_po(lotid)
{
	var po_price = $('#po_price_input').val();
	var po_discount = $('#po_discount_input').val();

	console.log(po_price);
	$.ajax({
		url: 'index.php?r=lot-ajax%2Fchange-price-offer&id='+lotid,
		type: 'POST',
		datatype:'json',
		data : {
			po_price: po_price,
			po_discount: po_discount,
		},
		success: function(data){
			console.log(data);

			$('#po_price').text(data.po_price);
			$('#po_price_input').val(data.po_price_number);

			$('#po_discount').text(data.po_discount);
			$('#po_discount_input').val(data.po_price_discount_number);

			$('#po_total').text(data.po_total);

			$('#po_price').css('display', 'block');
			$('#po_price_input').css('display', 'none');

			$('#po_discount').css('display', 'block');
			$('#po_discount_input').css('display', 'none');

			$('#po_change_button').css('display', 'block');
			$('#po_save_button').css('display', 'none');

			if (data == false) {

				$.toast({
					text : 'Ошибка валидации. Введите данные корректно!',
					heading: 'Ошибка',
					showHideTransition: 'slide',
					loader: false,
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#DD5145'
				});

			} else {

				$.toast({
					text : 'Ценовое предложение внесено.',
					heading: 'Успешно',
					showHideTransition: 'slide',
					loader: false,
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#1BA261'
				});
			}
		}
	});	
}



/**
*
*		Скрипты для блока ЧЕКЛИСТ
*
*/

function removeChecklist(id, tenderid)
{
	$("#checklist_"+id).remove();

	$.ajax({
		url: 'index.php?r=check-list-ajax%2Fremove-checklist&id=' + id + '&tenderid='+tenderid,
		type: 'POST',
		success: function(data){
			console.log(data);
			$.toast({
				text : 'Файл успешно удален.',
				heading: 'Удален',
				showHideTransition: 'slide',
				loader: false,
				loaderBg: '#9EC600',
				position: 'top-right',
				bgColor: '#1BA261',
				hideAfter : 3000,
			}); 
		}
	});

}

function comment(id)
{
	$('.comment_'+id).css('display', 'none');
	$('.comment_save_'+id).css('display', 'block');
}
function saveComment(id)
{
	var old_comment = $('#comment_'+id).text();
	var new_comment = $('#comment_save_'+id).val();
	var tenderid = $('#lot_tenderid').val();
	
	$.ajax({
		url: 'index.php?r=check-list-ajax%2Fcomment&tenderid='+tenderid,
		type: 'POST',
		datatype:'json',
		data : {
			new_comment: new_comment,
			id: id,
		},
		success: function(data){
			console.log(data);

			$('#comment_'+id).html(data.ld_comment.replace(/\n/ig, '<br>'));
			$('#comment_save_'+id).val(data.ld_comment);

			$('.comment_'+id).css('display', 'block');
			$('.comment_save_'+id).css('display', 'none');

			$.toast({
				text : 'Комментарий успешно был оставлен!',
				heading: 'Успешно',
				showHideTransition: 'slide',
				loader: false,
				loaderBg: '#9EC600',
				position: 'top-right',
				bgColor: '#1BA261',
				hideAfter : 3000,
			});
		}
	});
}
function removeFile(id, lotid, tenderid)
{
	$(".file_"+id).remove();

	$.ajax({
		url: 'index.php?r=check-list-ajax%2Fremove-file&id=' + id + '&lotid=' + lotid + '&tenderid='+tenderid,
		type: 'POST',
		success: function(data){
			console.log(data);
			$.toast({
				text : 'Файл успешно удален.',
				heading: 'Удален',
				showHideTransition: 'slide',
				loader: false,
				loaderBg: '#9EC600',
				position: 'top-right',
				bgColor: '#1BA261',
				hideAfter : 3000,
			}); 
		}
	});
	
}
function uploadChecklistFile(id, lotid)
{
	var formData = new FormData();
	var tenderid = $('#lot_tenderid').val();
	formData.append('file', $('#checklist_file_input_'+id)[0].files[0]);
	formData.append('ld_id', id);
	formData.append('file_code', $('#lot_file_code').val())

	$.ajax({
		url: 'index.php?r=check-list-ajax%2Fupload-checklist-file&lotid='+lotid+'&tenderid='+tenderid,
		type: 'POST',
		mimeType: 'multipart/form-data',
		data: formData,   
		datatype:'json',
		cache: false,
		contentType: false,
		processData: false,

		success: function (data) {
			var result = JSON.parse(data);
			console.log(result);

			$('#checklist_file_input_'+id).val("");

			var row = $('<div class="file_'+result.df_id+'"/>')
			$('#checklist_file_'+id).append(row);
			row.append($('<div class="file_'+result.df_id+'"><div class="drive-item module"><div class="drive-item-inner module-inner"><div class="drive-item-title"><button type="button" id="remove" class="btn btn-outline-danger btn-xs" title="Удалить файл" onclick="removeFile('+result.df_id+')"><i class="fa fa-remove" aria-hidden="true" style="color:red;"></i></button><a target="_blank" href="index.php?r=check-list-ajax%2Fdownload-file&id='+result.df_id+'">'+result.df_file_title+'</a></div></div></div></div>'));
			$.toast({
				text : 'Файл успешно загружен',
				heading: 'Добавлен',
				showHideTransition: 'slide',
				loader: false,
				loaderBg: '#9EC600',
				position: 'top-right',
				bgColor: '#1BA261',
				hideAfter : 3000,
			});    
		},

		error: function(err) {
			$.toast({
				text : 'Не удалось загрузить файл',
				heading: 'Ошибка',
				showHideTransition: 'slide',
				loader: false,
				loaderBg: '#9EC600',
				position: 'top-right',
				bgColor: '#DD5145',
				hideAfter : 7000,
			});
		}
	});
}
function createOwnDoc(lotid)
{
	var depidModal = $('#depid-modal').val();
	var newDocModal = $('#new-doc-modal').val();
	var dnameModal = $('#depid-modal').select2('data')[0].text;

	if (!newDocModal) {
		alert('Напишите название документа!');
		return 0;
	}
	$.ajax({
		url: 'index.php?r=check-list-ajax%2Fcreate-own-doc&lotid=' + lotid,
		type: 'POST',
		datatype:'json',
		data : {
			depid: depidModal,
			doc_title: newDocModal,
			dname: dnameModal,
		},
		success: function(result){
			$('#depid-modal').val(0).change();
			$('#new-doc-modal').val('').change();

			console.log(result);
			$('#document_modal').modal('hide');

			if (result == false) {
				$.toast({
					text : 'Что-то пошло не так:(',
					heading: 'Ошибка',
					showHideTransition: 'slide',
					loader: false,
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#DD5145'
				});
			} else {
				console.log(result)
				var row = $('<tr id="checklist_'+result.ld_id+'" />')
				$('#table_checklist').append(row);
				row.append($('<td>'+result.ld_dname+'</td>')); 
				row.append($('<td>'+result.ld_doc_title+'</td>'));
				row.append($('<td id="checklist_file_'+result.ld_id+'"><input name="doc_file" type="file" value="" id="checklist_file_input_'+result.ld_id+'" class="inp01" accept=".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" onchange="uploadChecklistFile('+result.ld_id+');"></td>'));
				row.append($('<td><div class="comment_'+result.ld_id+'"><i id="comment_'+result.ld_id+'"></i><button class="btn btn-default btn-xs" style="float:right; margin-top: 20px;" onclick="comment('+result.ld_id+')">Комментировать</button></div><div class="comment_save_'+result.ld_id+'" style="display: none;"><textarea class="form-control" id="comment_save_'+result.ld_id+'" rows="4" cols="50"></textarea><button class="btn btn-default btn-xs" style="float:right; margin-top: 20px;" onclick="saveComment('+result.ld_id+')">Сохранить</button></div></td>')); 
				row.append($('<td><button class="btn btn-danger btn-xs" onclick="removeChecklist('+result.ld_id+')">Удалить</button></td>')); 
					// row.append($('<td>'+result.s_date_signed+'</td>'));
					// row.append($('<td>'+result.s_comment+'</td>'));  
					// row.append($('<td><button type="button" id="remove" class="btn btn-danger btn-xs" onclick="rmUser('+result.sid+')" style="margin-left: 20px;">Удалить<span class="glyphicon glyphicon-remove-sign"></span></button></td>'));
					$.toast({
						text : 'Подписант успешно добавлен',
						heading: 'Успешно',
						showHideTransition: 'slide',
						loader: false,
						loaderBg: '#9EC600',
						position: 'top-right',
						bgColor: '#1BA261'
					});
				}

			}});
}
function addCheckList(id)
{
	var dname = $('#depid').select2('data')[0].text;
	var depid = $('#depid').select2('data')[0].id;
	var doc_title = $('#typeid').select2('data')[0].text;
	var tenderid = $('#lot_tenderid').val();

	var data = {
		dname: dname,
		depid: depid, 
		doc_title: doc_title,
	};

	$.ajax({
		url: 'index.php?r=check-list-ajax%2Fcreate&lotid=' + id + '&tenderid='+tenderid,
		type: 'POST',
		datatype:'json',
		data : data,
		success: function(result){

			if (result == false) {
				$.toast({
					text : 'Что-то пошло не так:(',
					heading: 'Ошибка',
					showHideTransition: 'slide',
					loader: false,
					loaderBg: '#9EC600',
					position: 'top-right',
					bgColor: '#DD5145'
				});
			} else {
				console.log(result)
				var row = $('<tr id="checklist_'+result.ld_id+'" />')
				$('#table_checklist').append(row);
				row.append($('<td>'+result.ld_dname+'</td>')); 
				row.append($('<td>'+result.ld_doc_title+'</td>'));
				row.append($('<td id="checklist_file_'+result.ld_id+'"><input name="doc_file" type="file" value="" id="checklist_file_input_'+result.ld_id+'" class="inp01" accept=".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf" onchange="uploadChecklistFile('+result.ld_id+');"></td>'));
				row.append($('<td><div class="comment_'+result.ld_id+'"><i id="comment_'+result.ld_id+'"></i><button class="btn btn-default btn-xs" style="float:right; margin-top: 20px;" onclick="comment('+result.ld_id+')">Комментировать</button></div><div class="comment_save_'+result.ld_id+'" style="display: none;"><textarea class="form-control" id="comment_save_'+result.ld_id+'" rows="4" cols="50"></textarea><button class="btn btn-default btn-xs" style="float:right; margin-top: 20px;" onclick="saveComment('+result.ld_id+')">Сохранить</button></div></td>')); 
				row.append($('<td><button class="btn btn-danger btn-xs" onclick="removeChecklist('+result.ld_id+')">Удалить</button></td>')); 
					$.toast({
						text : 'Подписант успешно добавлен',
						heading: 'Успешно',
						showHideTransition: 'slide',
						loader: false,
						loaderBg: '#9EC600',
						position: 'top-right',
						bgColor: '#1BA261'
					});
				}

			}});
}
function getDocs(depid)
{	
	$('#depid-modal').val(depid).change();
	$.ajax({
		url: 'index.php?r=check-list-ajax%2Fget-docs',
		type: 'POST',
		datatype:'json',
		data : {
			depid: depid
		},
		success: function(data){
			var newOptions = data;

			var $el = $("#typeid");
			$el.empty(); 
			$.each(newOptions, function(key,value) {
				$el.append($("<option></option>")
					.attr("value", key).text(value));
			});

		}});
}








