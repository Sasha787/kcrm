function countVat()
	{
		const cur_budget = $('#budget').val();
		$('#budget_vat').val((cur_budget*1.12).toFixed(2));
	}

	function countKtcVat()
	{
		const cur_budget = $('#ktc_price').val();
		$('#ktc_price_vat').val((cur_budget*1.12).toFixed(2));
	}

	function countWinVat()
	{
		const cur_budget = $('#winner_price').val();
		$('#winner_price_vat').val((cur_budget*1.12).toFixed(2));
	}
	function updateFile(lotid)
    {
        var formData = new FormData();
        formData.append('file', $('#lot_file')[0].files[0]);
        formData.append('lotid', lotid);

        $.ajax({
            url: 'index.php?r=lot-ajax%2Fupdate-file',
            type: 'POST',
            mimeType: 'multipart/form-data',
            data: formData,   
            datatype:'json',
            cache: false,
            contentType: false,
            processData: false,

            success: function (data) {
                
                var result = JSON.parse(data);
                console.log(result);
        
                $('#lot_file').val("");
                var row = $('<div class="file_'+result.fileid+'"/>')
                $('#lot_files').append(row);
                row.append($('<div class="file_'+result.fileid+' drive-item module"><div class="drive-item-module-inner"><div class="drive-item-title"><button type="button" id="remove" class="btn btn-outline-danger btn-xs" title="Удалить файл" onclick="rmFile('+result.fileid+')"><i class="fa fa-remove" aria-hidden="true" style="color:red;"></i></button><a target="_blank" href="index.php?r=lot%2Fdownload-file&id='+result.fileid+'">'+result.fileName+'</a></div></div></div>'));
                $.toast({
                    text : 'Файл успешно загружен',
                    heading: 'Добавлен',
                    showHideTransition: 'slide',
                    loader: false,
                    loaderBg: '#9EC600',
                    position: 'top-right',
                    bgColor: '#1BA261',
                    hideAfter : 3000,
                });    
            },

            error: function(err) {
                $.toast({
                text : 'Не удалось загрузить файл',
                heading: 'Ошибка',
                showHideTransition: 'slide',
                loader: false,
                loaderBg: '#9EC600',
                position: 'top-right',
                bgColor: '#DD5145',
                hideAfter : 7000,
            });
            }
        });
    }

    function rmFile(id)
    {
        $(".file_"+id).remove();

        $.ajax({
            url: 'index.php?r=lot-ajax%2Fremove-file&id=' + id,
            type: 'POST',
            success: function(data){
                console.log(data);
                
            }
        });
        $.toast({
            text : 'Файл успешно удален.',
            heading: 'Удален',
            showHideTransition: 'slide',
            loader: false,
            loaderBg: '#9EC600',
            position: 'top-right',
            bgColor: '#1BA261',
            hideAfter : 3000,
        });  

    }  