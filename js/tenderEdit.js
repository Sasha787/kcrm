 function updateFile(tenderid)
 {
     var formData = new FormData();
     formData.append('file', $('#tender_file')[0].files[0]);
     formData.append('tenderid', tenderid);

     $.ajax({
         url: 'index.php?r=tender-ajax%2Fupdate-file',
         type: 'POST',
         mimeType: 'multipart/form-data',
         data: formData,   
         datatype:'json',
         cache: false,
         contentType: false,
         processData: false,

         success: function (data) {

             var result = JSON.parse(data);
             console.log(result);

             $('#tender_file').val("");
             var row = $('<div class="file_'+result.fileid+'"/>')
             $('#tender_files').append(row);
             row.append($('<div class="drive-item module"><div class="drive-item-module-inner"><div class="drive-item-title"><button type="button" id="remove" class="btn btn-outline-danger btn-xs" title="Удалить файл" onclick="rmFile('+result.fileid+')"><i class="fa fa-remove" aria-hidden="true" style="color:red;"></i></button><a target="_blank" href="index.php?r=tender%2Fdownload-file&id='+result.fileid+'">'+result.fileName+'</a></div></div></div>'));

             $('#tender_files').append(row);
             $.toast({
                 text : 'Файл успешно загружен',
                 heading: 'Добавлен',
                 showHideTransition: 'slide',
                 loader: false,
                 loaderBg: '#9EC600',
                 position: 'top-right',
                 bgColor: '#1BA261',
                 hideAfter : 3000,
             });    
         },

         error: function(err) {
             $.toast({
                 text : 'Не удалось загрузить файл',
                 heading: 'Ошибка',
                 showHideTransition: 'slide',
                 loader: false,
                 loaderBg: '#9EC600',
                 position: 'top-right',
                 bgColor: '#DD5145',
                 hideAfter : 7000,
             });
         }
     });
 }

 function rmFile(id)
 {
     $(".file_"+id).remove();

     $.ajax({
         url: 'index.php?r=tender-ajax%2Fremove-file&id=' + id,
         type: 'POST',
         success: function(data){
             console.log(data);

         }
     });
     $.toast({
         text : 'Файл успешно удален.',
         heading: 'Удален',
         showHideTransition: 'slide',
         loader: false,
         loaderBg: '#9EC600',
         position: 'top-right',
         bgColor: '#1BA261',
         hideAfter : 3000,
     });  

 }  