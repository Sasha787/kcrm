var IE = document.all?true:false;
var editor;
var siteurl = 'https://copycrm.kaztranscom.kz/';
var curmenu = 0;	// ------ номер текущего меню, при запуске должен=0
var si = 0;	// ------- таймер
var searchstr = '';	// ------ строка поиска
var reloadParent = 0;	// ------ признак для doLoad
var reloadLocation = 0;	// ------ признак для doLoad
var gopener = 0;	// ------ признак для doLoad (загрузка в окно-родитель)
var iscdchanged = 0;	// ------ признак внесения изменений в заметку календаря
var bgsize = 100;	// ----- изначальный размер фона
var frwd = 0;	// ----- увеличение/уменьшение фона
var bgpause = 0;	// ----- пауза в обратном ходе фона
var abg;	// таймаут для фона
var iscontdef = 0;	// ----- признак того, что введен тип контакта
var doctopA = 0;   // положение окна выбора из списка
var docleftA = 0;   // положение окна выбора из списка
var pricesum1 = 0;	// сумма отображаемая в прейскуранте при выборе позиций
var pricesum2 = 0;	// сумма отображаемая в прейскуранте при выборе позиций
var pricesum3 = 0;	// сумма отображаемая в прейскуранте при выборе позиций
var comoffer;	// окно с конструктором КП
var a2id=0;	// используется в сделке
var curpage = 0;	// текущая страница
var ttr = 0;
var ttw = 0;
var advt = 0;
var updt = 0;	// ----- время последнего изменения в документах
var curstage = 0;	// текущий подэтап в сделке
var skpstage = 0;
var cncstage = 0;
var ccmenu = 2;	// меню в информации по клиенту
var divedit = 0;	// 1-при добавлении действия в сделке не обрезается html
var frmcode = 0;	// код для ассоциации формы при создании действия в сделке
var datele;		// элемент для ввода даты в форме при создании действия в сделке
var f_aid = 0;	// запоминаем существующую запись для даты окончания встречи
var test1 = 0;
var scont_enable = 0;	// кнопка сохранения контакта отключена
var cfg_empty = '';		// пустое значение
var refreshEnable = 1;	// автоматическое обновление разрешено по умолчанию
var ssmt = 0;	// счетчик для плавной прокрутки
var func;
var funcParam1 = '';
var funcParam2 = 0;
var tempval1 = 0;	// переменная для временных значений

function winReload() {
	setTimeout("window.location.reload()",1000);
}

function replacehtml(text) {
  return text
      .replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#039;");
}

function replacehtml2(text) {
  return text
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#039;");
}

function splitNums(delimiter,str){
	str = str.toString();
	str = str.replace(/(\d+)(\.\d+)?/g,
	function(c,b,a){return b.replace(/(\d)(?=(\d{3})+$)/g, '$1'+delimiter) + (a ? a : '')} );
	return str;
}

function getOffset(elem) {
    if (elem.getBoundingClientRect) {
        return getOffsetRect(elem)
    } else {
        return getOffsetSum(elem)
    }
}
// ---------------------------------------------------
function getOffsetSum(elem) {
    var top=0, left=0
    while(elem) {
        top = top + parseInt(elem.offsetTop)
        left = left + parseInt(elem.offsetLeft)
        elem = elem.offsetParent
    }

    return {top: top, left: left}
}

function getOffsetRect(elem) {
    var box = elem.getBoundingClientRect()
    var body = document.body
    var docElem = document.documentElement
    var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop
    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft
    var clientTop = docElem.clientTop || body.clientTop || 0
    var clientLeft = docElem.clientLeft || body.clientLeft || 0
    var top  = box.top +  scrollTop - clientTop
    var left = box.left + scrollLeft - clientLeft
    return { top: Math.round(top), left: Math.round(left) }
}

function showfunnel(a,csid,dealid) {
if(document.getElementById('funnel'+a))
	{
	var elem=document.getElementById('funnel'+a);
	var epos=getOffset(elem);
	if(a==2 && dealid==0) dealid=document.getElementById('fdealid').value;
	document.getElementById('funnelA'+a).style.top=epos.top+'px';
	document.getElementById('funnelA'+a).style.left=epos.left+5+'px';
    var req = new JsHttpRequest();
    req.onreadystatechange = function()
		{
        if (req.readyState == 4)
			{
			document.getElementById('funnelA'+a).innerHTML = req.responseJS.q;
			document.getElementById('funnelex'+a).innerHTML = req.responseJS.q1;
			document.getElementById('funnelbot'+a).innerHTML = req.responseJS.q2;
			document.getElementById('funnelA'+a).style.display='block';
			}
		}
    if(a==2) req.open(null, 'funnel.php', true);
    if(a>2 && a<6) req.open(null, 'wallfunnel.php', true);
    req.send( { q: a, q1: csid, q2: dealid } );
	}
}
// ---------------------------------------------------
function openEditor(){
	if(document.getElementById('editor1') && document.getElementById('editor2'))
	{
	var edtext1 = document.getElementById('editor1').innerHTML;
	var edtext2 = document.getElementById('editor2').innerHTML;
	document.getElementById('editor1').innerHTML='';
	document.getElementById('editor2').innerHTML='';
	CKEDITOR.config.height = 200;
	editor1 = CKEDITOR.appendTo( 'editor1',{
		toolbar :
		[
['PasteFromWord'],['Image','Table','HorizontalRule'],['Bold','Italic','Underline','-','Subscript','Superscript'],['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],['Outdent','Indent'],['Font'],['FontSize'],['TextColor','BGColor']
		]
		});
	editor2 = CKEDITOR.appendTo( 'editor2',{
		toolbar :
		[
['PasteFromWord'],['Image','Table','HorizontalRule'],['Bold','Italic','Underline','-','Subscript','Superscript'],['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],['Outdent','Indent'],['Font'],['FontSize'],['TextColor','BGColor']
		]
		});
	editor1.setData(edtext1);
	editor2.setData(edtext2);
	document.getElementById('editor1').style.display='block';
	document.getElementById('editor2').style.display='block';
	}
}

function getCookieVal (offset) 
{
var endstr = document.cookie.indexOf (";", offset);
if (endstr == -1)
endstr = document.cookie.length;
return unescape(document.cookie.substring(offset, endstr));
}

function getCookie (name) 
{
var arg = name + "=";
var alen = arg.length;
var clen = document.cookie.length;
var i = 0;
while (i < clen) 
{
var j = i + alen;
if (document.cookie.substring(i, j) == arg)
return getCookieVal (j);
i = document.cookie.indexOf(" ", i) + 1;
if (i == 0) break; 
}
return null;
}

function $(v) { return(document.getElementById(v)); }
function agent(v) { return(Math.max(navigator.userAgent.toLowerCase().indexOf(v),0)); }
function xy(e,v) { return(v?(agent('msie')?event.clientY+document.body.scrollTop:e.pageY):(agent('msie')?event.clientX+document.body.scrollTop:e.pageX)); }
 function dragOBJ(d,e) {
  if (d==1) { d = document.getElementById('viewb'); }
   function drag(e) { if(!stop) { d.style.top=(tX=xy(e,1)+oY-eY+'px'); d.style.left=(tY=xy(e)+oX-eX+'px'); } }
	var oX=parseInt(d.style.left),oY=parseInt(d.style.top),eX=xy(e),eY=xy(e,1),tX,tY,stop;
	document.onmousemove=drag;
	document.onmouseup=function(){ stop=1;
	document.onmousemove='';
	document.onmouseup=''; };
}

function logout () 
{
document.getElementById('topmenu2').innerHTML='';
document.cookie ='userid=;';
document.cookie ='login=;';
setTimeout("window.location.reload()",1000);
}

function accenter() {
	var usrn=document.getElementById('usrn').value;
	var usrp=document.getElementById('usrp').value;

	doLoad('login',siteurl+'/accenter.php',1,usrn,usrp);
	setTimeout("start(0)",2000);
}

function setCsName(ddl)
{
	document.getElementById('t_csname_text').value = $("#t_csname option:selected").text();
}
function setCompetitorTitle()
{
	var selected_text = $("#lp_competitor_id option:selected").text();
	var selected_val = $("#lp_competitor_id option:selected").val();

	document.getElementById('lp_title').value = selected_text;
	
	selected_text === selected_val ? document.getElementById('lp_competitor_id').value = 0 : document.getElementById('lp_competitor_id').value = selected_val;
}
function setCsIdAndName(id)
{
	var csid = $("#t_csbin").val();
	console.log(csid);
	$('#t_csbin_text').val($("#t_csbin option:selected").text());
	$('#t_csid').val(csid);
	$.ajax({
            url: 'index.php?r=tender-ajax%2Ffind-cs-byid&id='+csid,
            type: 'POST',  
            datatype:'json',
            cache: false,
            contentType: false,
            processData: false,

            success: function (data) {
                $("#t_csname").append('<option value='+data.csid+'>'+data.csname+'</option>');
   				$('#t_csname').val(data.csid).change();

            },

            error: function(err) {
            	$.toast({
                text : 'Произошла ошибка, перезагрузите страницу и попробуйте снова!',
                heading: 'Ошибка',
                showHideTransition: 'slide',
                loader: false,
                loaderBg: '#9EC600',
                position: 'top-right',
                bgColor: '#DD5145',
                hideAfter : 7000,
            });
            }
        });
}

function setUserIdForChatNotification(id)
{
	var csid = $("#t_csbin").val();
	console.log(csid);
	$('#t_csbin_text').val($("#t_csbin option:selected").text());
	$('#t_csid').val(csid);
	$.ajax({
            url: 'index.php?r=tender-ajax%2Ffind-cs-byid&id='+csid,
            type: 'POST',  
            datatype:'json',
            cache: false,
            contentType: false,
            processData: false,

            success: function (data) {
                $("#t_csname").append('<option value='+data.csid+'>'+data.csname+'</option>');
   				$('#t_csname').val(data.csid).change();

            },

            error: function(err) {
            	$.toast({
                text : 'Произошла ошибка, перезагрузите страницу и попробуйте снова!',
                heading: 'Ошибка',
                showHideTransition: 'slide',
                loader: false,
                loaderBg: '#9EC600',
                position: 'top-right',
                bgColor: '#DD5145',
                hideAfter : 7000,
            });
            }
        });
}

function start(n)
{
if(getCookie('userid')>0)
	{
	setTimeout("refresh()",180000);
	if(n==0)
		{
		clearTimeout(abg);
		doLoad('topmenu',siteurl+'topmenu.php');
		doLoad('topmenu2',siteurl+'topmenu2.php');
		doLoad('fullname',siteurl+'checkusername.php');
		setbg(1);
		animatebg();
		checkwallmsg();
		reminder();
		if(curmenu==0) setTimeout("startSelector()",1000);
		document.getElementById('login').style.display='none';
		}
	else menuact(curmenu);
	}
	else
	{
	clearTimeout(ttr);
	document.getElementById('login').style.display='block';
	doLoad('login','accenter.php',0);
	setTimeout("document.getElementById('usrn').focus()",1000);
	}
}

function startSelector(){
	if(document.getElementById('mkey1')) { menuact(1); return; }
	if(document.getElementById('mkey7')) { menuact(7); return; }
	if(document.getElementById('mkey8')) { menuact(8); return; }
	if(document.getElementById('mkey11')) { menuact(11); return; }
	if(document.getElementById('mkey12')) { menuact(12); return; }
}

function refresh(){
	clearTimeout(ttr);
	reminder(); 
	if(curmenu==1 
		|| curmenu==2
		|| curmenu==3
		|| curmenu==7
		|| curmenu==8
		|| curmenu==10
		|| curmenu==11
		|| curmenu==12)
	{
	var req = new JsHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if(req.responseJS.q>0 && refreshEnable==1) 
				if(updt<req.responseJS.q) { updt=req.responseJS.q; menuact(curmenu); }
				if(req.responseJS.q==999)
					{
					//alert('Время сессии истекло.');
					setTimeout("document.cookie ='userid=;'",5000);
					setTimeout("window.location.reload()",7000);
					}
        }
    }
    req.open(null, 'checkupd.php', true);
    req.send( { q: curmenu } );
	}
	ttr=setTimeout("refresh()",120000);
}

function checkwallmsg() {
	clearTimeout(ttw);
	if(curmenu==10)	doLoad('walldiv','checkwallmsg.php');
	ttw=setTimeout("checkwallmsg()",10000);
}

function animatebg() {
	if(bgpause<1)
		{
		if(bgsize<100) { frwd=1; bgpause=10; }
		if(bgsize>150) { frwd=0; bgpause=10; }
		k=0.1;
		if(bgsize>140 || bgsize<105) k=0.08;
		if(bgsize>148 || bgsize<101) k=0.05;
		if(frwd==0) bgsize-=k;
		if(frwd==1) bgsize+=k;
		document.getElementById('mainbg').style.backgroundSize=bgsize+'%';
		}
	else bgpause--;
	abg=setTimeout("animatebg()",50);
}

function setbg(n) {
    var req = new JsHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
			if(req.responseJS.q!='')
				{
				if(document.getElementById('bgimg2')) document.getElementById('bgimg2').src = req.responseJS.q;
				document.getElementById('mainbg').style.background='URL('+req.responseJS.q1+')';
				}
        }
    }
	req.open(null, 'profileimg.php', true);
	req.send( { q: 10, q1: 0 } );

	if(document.getElementById('bgph1') && document.getElementById('bgph2'))
		{
		var epos=getOffset(document.getElementById('mainbg'));
		document.getElementById('bgph1').style.top=epos.top+120+'px';
		document.getElementById('bgph1').style.left=epos.left+210+'px';
		document.getElementById('bgph1').style.display='block';
		epos=getOffset(document.getElementById('bgimg2'));
		document.getElementById('bgph2').style.top=epos.top+5+'px';
		document.getElementById('bgph2').style.left=epos.left+5+'px';
		document.getElementById('bgph2').style.display='block';
		}
}

function doLoad(divId, phps, v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16) {
    var req = new JsHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4 && document.getElementById(divId)) {
			if(gopener==0 && divId!='emptydiv') document.getElementById(divId).innerHTML = req.responseJS.q;
			if(gopener==1)
				{
				window.opener.document.getElementById(divId).innerHTML = req.responseJS.q;
				gopener=0;
				}
			if(reloadParent==1) { reloadParent=0; menuactM(1); }
			if(reloadLocation==1) { reloadLocation=0; window.location.reload(); }
			if(funcParam1!='') {
				var fp=funcParam1;
				funcParam1='';
				if(fp=='self') func(req.responseJS.q);
				else func(fp);
				}
        }
    }
    req.open(null, phps, true);
    req.send( { q: v0, q1: v1, q2: v2, q3: v3, q4: v4, q5: v5, q6: v6, q7: v7, q8: v8, q9: v9, q10: v10, q11: v11, q12: v12, q13: v13, q14: v14, q15: v15, q16: v16 } );
}

function doLoadOpener(divId, phps, v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16) {
    var req = new JsHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
			window.opener.document.getElementById(divId).innerHTML = req.responseJS.q;
        }
    }
    req.open(null, phps, true);
    req.send( { q: v0, q1: v1, q2: v2, q3: v3, q4: v4, q5: v5, q6: v6, q7: v7, q8: v8, q9: v9, q10: v10, q11: v11, q12: v12, q13: v13, q14: v14, q15: v15, q16: v16 } );
}

function menuact(n){
	if(getCookie('userid')>0)
	{
	//document.getElementById('div2').innerHTML='<br />&nbsp;&nbsp;&nbsp;<img src="'+siteurl+'img/sp.gif" border="0">';
	var cyear=0;
	if(document.getElementById('spyear')) cyear=document.getElementById('spyear').value;
	if(document.getElementById('uniwin')) document.getElementById('uniwin').style.display='none';
	extrawinclose();
	if(n!=curmenu)
		for(i=1;i<=12;i++) if(document.getElementById('mkey'+i)) document.getElementById('mkey'+i).style.borderBottom='0px';
	//if(curmenu!=10)
		//document.getElementById('div1').innerHTML='<br />&nbsp;&nbsp;&nbsp;<img src="'+siteurl+'img/sp.gif" border="0">';
	curmenu=n;
    var req = new JsHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4 && document.getElementById('mkey'+n)) {
			curpage=0;
			document.getElementById('mkey'+n).style.borderBottom='3px solid #ffffff';
			document.getElementById('div1').innerHTML = req.responseJS.q;
			document.getElementById('div2').innerHTML = req.responseJS.q1;
			//doLoad('topmenu2',siteurl+'topmenu2.php',n);
			setTimeout("document.getElementById('items').innerHTML = "+req.responseJS.q2,500);
			if(n==10) { /* showfunnel(3,0,0); */ setTimeout("showfunnel(4,0,0)",500); setTimeout("showfunnel(5,0,0)",500); }
			setTimeout('checkNewLead()',1000);
        }
    }
    req.open(null, 'doaction'+n+'.php', true);
    req.send( { q: n, q1: searchstr, q2: cyear, q3: curpage } );
	}
	else window.location.reload();
}
function selpage(pg){
	curpage=pg;
	menuact(curmenu);
	window.scrollBy(0,-1000);
}
// ------ обновление главного окна после добавления контрагента или атрибутов
function menuactM(n){
    var req = new JsHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
			if(n!=curmenu) window.opener.document.getElementById('div1').innerHTML = req.responseJS.q;
			window.opener.document.getElementById('div2').innerHTML = req.responseJS.q1;
			setTimeout("window.opener.document.getElementById('items').innerHTML = "+req.responseJS.q2,500);
        }
    }
    req.open(null, 'doaction'+n+'.php', true);
    req.send( { q: n, q1: '', q2: '' } );
}

// ------ выбор в выпадающем меню
function showmenu(ecsid,trid,ispriv,e,v1,v2){
	for(i=1;i<=50;i++)
		if(document.getElementById('tr'+i)) if(i%2) document.getElementById('tr'+i).className='lotr1'; else document.getElementById('tr'+i).className='lotr2';
	if(document.getElementById('tr'+trid)) document.getElementById('tr'+trid).className='hitr';
	document.getElementById('menu01').style.display='none';
	if (IE)
		{
		var docleft = window.event.clientX + document.body.scrollLeft;
		var doctop  = window.event.clientY + document.body.scrollTop;
		}
	else
		{
		var docleft = e.pageX;
		var doctop  = e.pageY;
		}
	if (docleft<100) docleft = 100;
	if (docleft>document.body.clientWidth-250) docleft=document.body.clientWidth-250;
	if (doctop>=document.body.clientHeight+document.body.scrollTop-70) doctop=document.body.clientHeight+document.body.scrollTop-70;		
	if (doctop<20) doctop = 20;
	document.getElementById('menu01').innerHTML='';
	document.getElementById('menu01').style.left=docleft-20+'px'; 
	document.getElementById('menu01').style.top=doctop-15+'px';
	document.getElementById('menu01').innerHTML='<img src="img/sp.gif">';
	setTimeout("document.getElementById('menu01').style.display='block'",100);
	doLoad('menu01','showmenu.php',ecsid,ispriv,curmenu,v1,v2);
}

function closemenu(){
	clearTimeout(si);
	si=setTimeout("document.getElementById('menu01').style.display='none'",1500);
 }
 
function openmenu(m,d,v1,v2,v3,v4,v5,v6){
	if(document.getElementById('menu01')) document.getElementById('menu01').style.display='none';
	//if(m==1) window.open(siteurl+'showcust.php?q='+v1);
	if(m==1) window.open(siteurl+'view.php?profile?generate?1?'+v1);
	if(m==2) window.open(siteurl+'showdeal.php?q=1&q1='+d+'&q2='+v1);
	if(m==3) location.href=siteurl+'showdeal.php?q=1&q1='+d+'&q2='+v1;
	//if(m==5) window.open(siteurl+'addtreq.php?q='+d+'&q1='+v1);
	if(m==5) window.open(siteurl+'showtreq.php?q='+d+'&q1='+v1+'&q2='+v2+'&q3='+v3+'&q4='+v4);
	if(m==6) location.href=siteurl+'pricelist.php?c8a5d0='+d+'&e4f2b7='+v1+'&a2de36='+v2+'&e8c0fa='+v3+'&bc5fe4='+v4+'&d8abcc='+v5;
	//if(m==7) window.open(siteurl+'pricelist.php?c8a5d0='+d+'&e4f2b7='+v1+'&a2de36='+v2+'&e8c0fa='+v3+'&bc5fe4='+v4+'&d8abcc='+v5);
	if(m==7) window.open(siteurl+'pcat.php?c8a5d0='+d+'&e4f2b7='+v1+'&a2de36='+v2+'&e8c0fa='+v3+'&bc5fe4='+v4+'&d8abcc='+v5+'&s3g67='+v6);
	if(m==8) location.href=siteurl+'comoffer.php?c5a0='+d+'&defa='+v1+'&aace='+v2+'&d2er='+v3;
	if(m==9) location.href=siteurl+'comoffer.php?c5a0='+d+'&defa='+v1+'&ef80='+v2+'&aace='+v3+'&d2er='+v4;
	if(m==10) doLoad('exfun','selaction.php',4,d,v1);
	if(m==11) window.open(siteurl+'showdeal.php?q=1&q1='+d+'&q2='+v1);
	if(m==12) window.open(siteurl+'pladmin.php');
	// if(m==13) location.href=siteurl+'pladmin.php?c8a5d0='+d+'&e4f2b7='+v1+'&a2de36='+v2+'&e8c0fa='+v3+'&bc5fe4='+v4+'&d8abcc='+v5;
	// if(m==14) window.open(siteurl+'contract.php?defa='+v1+'&aace='+v2+'&d2er='+v3);
	if(m==15) window.open(siteurl+'printtreq.php?q='+d+'&q1='+v1);
	if(m==16) window.open(siteurl+'showcreq.php?q='+d+'&q1='+v1);
	if(m==17) window.open(siteurl+'showtreq.php?q='+d+'&q1='+v1+'&isprint=1');
	if(m==18) window.open(siteurl+'reports.php');
	if(m==19) window.open(siteurl+'report/?r300?show');
}
 
function openMenuTest(m,d,v1,v2,v3,v4,v5){
	document.getElementById('menu01').style.display='none';
	
	if(m==6) location.href=siteurl+'pricelist001.php?c8a5d0='+d+'&e4f2b7='+v1+'&a2de36='+v2+'&e8c0fa='+v3+'&bc5fe4='+v4+'&d8abcc='+v5;
	if(m==7) window.open(siteurl+'pricelist001.php?c8a5d0='+d+'&e4f2b7='+v1+'&a2de36='+v2+'&e8c0fa='+v3+'&bc5fe4='+v4+'&d8abcc='+v5);
	
}
function selectMenu(test)
{
	test1 = test;
}

 // открываем редактирование/добавление контакта
 function addcont(a,ecsid){
	if(a==1) location.href=siteurl+'showcust.php?q='+ecsid+'&q1=1';
	if(a==2 && document.getElementById('act03').innerHTML=='')
		{
		menuactM(1);
		setTimeout("location.href='"+siteurl+"showcust.php?q="+ecsid+"'",1000);
		}
 }

// сохранение полей при редактировании
 function chcont(a,ecsid,csinfoid,val,seq,seq2){
	if(val.length>3)
	{
	if(a>100 && a<110)
		{
		val = val.replace(/'/g, "&quot;");
		val = val.replace(/"/g, "&quot;");
		}
	else
		{
		val = val.replace(/'/g, "");
		val = val.replace(/"/g, "");
		}
	val = val.replace(/\\/g,"/");
	}
	if(a==26) { reloadParent=1; reloadLocation=1; }	// скрытие/открытие клиента
	if((a==10 || a==21) && document.getElementById('act03').innerHTML!='') return;
	if(a==110 || a==380) if(!confirm('Удалить?')) return;
	doLoad('emptydiv','chcont.php',a,ecsid,csinfoid,val,seq,seq2);
	if((a==18 || a==38 || a==74 || a==110 || a==380) && val==0) setTimeout("window.location.reload()",2000); // удаление контакта
 }
 
// сохранение полей при создании
 function savecont(a,userid,csinfoid,val){
	if(val.length>3)
	{
 	val = val.replace(/'/g, "&quot;");
	val = val.replace(/"/g, "&quot;");
	val = val.replace(/\\/g,"/");
	}
	if(a==21 && document.getElementById('act03').innerHTML!='') { alert('Такое название уже используется'); return; }
	if(a==22 || a==21)	// введено название ЮЛ, блокируем поля ФЛ
		{
		if(document.getElementById('inp'+a).value!='')
			{
			for(i=11;i<=14;i++) { document.getElementById('inp'+i).value=''; document.getElementById('inp'+i).disabled=true; }
			for(i=23;i<=25;i++) { document.getElementById('inp'+i).value=''; document.getElementById('inp'+i).disabled=true; }
			}
		else
			{
			for(i=11;i<=14;i++) document.getElementById('inp'+i).disabled=false;
			for(i=23;i<=25;i++) document.getElementById('inp'+i).disabled=false;
			}
		}
	if(a==23)	// введена фамилия ФЛ, блокируем поля ЮЛ
		{
		if(document.getElementById('inp'+a).value!='')
			{
			document.getElementById('inp22').value=''; document.getElementById('inp22').disabled=true; document.getElementById('inp22').style.borderBottom="1px solid #dddddd";
			document.getElementById('inp21').value=''; document.getElementById('inp21').disabled=true;
			}
		else
			{
			document.getElementById('inp22').disabled=false; document.getElementById('inp22').style.borderBottom="1px solid #ff0000";
			document.getElementById('inp21').disabled=false;
			}
		}
	// отображаем кнопку "Сохранить", если все заполнено
	if(document.getElementById('inp1').value!=''
		&& ((document.getElementById('inp22').value!='' && document.getElementById('inp21').value!='') || (document.getElementById('inp23').value!='' && document.getElementById('inp24').value!=''))
		&& document.getElementById('inp331').value!=''
		&& document.getElementById('inp332').value!=''
		&& document.getElementById('inp333').value!=''
		&& document.getElementById('inp3').value!=''
		&& document.getElementById('inp4').value!=''
		&& document.getElementById('inp5').value!=''
		&& document.getElementById('inp6').value!=''
		&& document.getElementById('inp7').value!=''
		&& document.getElementById('act03').innerHTML==''
		&& document.getElementById('inp9').value!='') document.getElementById('scont').style.display='block';
	else document.getElementById('scont').style.display='none';
	if(a!=100) doLoad('emptydiv','savecont.php',a,userid,csinfoid,val);
	if(a==100) // записываем и проверяем произведена ли новая запись и присвоился ли csid
		{
		document.getElementById('scont').style.display='none';
		var statid = parseInt(document.getElementById('inp7').value);	// передаем текущий статус
		var req = new JsHttpRequest();
		req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if(parseInt(req.responseJS.q)>0)
				{
				menuactM(1);
				si=setTimeout("window.close()",1000);
				}
			else
				{
				document.getElementById('scont').style.display='block';
				alert('Ошибка записи. Попробуйте еще раз или обратитесь к администратору.');
				}
			}
		}
		req.open(null, 'savecont.php', true);
		req.send( { q: a, q1: userid, q2: csinfoid, q3: statid } );
		}
 }

 
// отображение/закрытие календаря
function calendar(a,e,mod,csid){
if(document.getElementById('menu01')) document.getElementById('menu01').style.display='none';
if(a==2)
	{
	document.getElementById('calendar').style.display='none';
	if(document.getElementById('escr')) document.getElementById('escr').style.display='none';
	}
if(a==1)
	{
	if(e)
	{
	if (IE)
		{
		var docleft = window.event.clientX + document.body.scrollLeft;
		var doctop  = window.event.clientY + document.body.scrollTop;
		}
	else
		{
		var docleft = e.pageX;
		var doctop  = e.pageY;
		}
	if (docleft<100) docleft = 100;
	if (docleft>document.body.clientWidth-250) docleft=document.body.clientWidth-250;
	if (doctop>=document.body.clientHeight+document.body.scrollTop-350) doctop=document.body.clientHeight+document.body.scrollTop-350;		
	if (doctop<20) doctop = 20;
	document.getElementById('calendar').innerHTML='***';
	document.getElementById('calendar').style.left=docleft-50+'px'; 
	document.getElementById('calendar').style.top=doctop-20+'px';
	document.getElementById('calendar').innerHTML='<img src="img/sp.gif">';
	setTimeout("document.getElementById('calendar').style.display='block'",100);
	}
	//if(mod==0) showempty();
	doLoad('calendar','calendar.php',a,0,0,mod,csid);
	}
if(a==3)
	{
	var m=document.getElementById('selmon').value;
	var y=document.getElementById('selyea').value;
	doLoad('calendar','calendar.php',a,m,y,mod,csid);
	}
}

// отображение/закрытие упрощенного календаря
function calendar_light(a,e,mod,ele){
if(a==2)
	{
	document.getElementById('uniwin').style.display='none';
	//document.getElementById('escr').style.display='none';
	}
if(a==1 || a==6)
	{
	f_aid=mod;
	datele=ele;
	if(ele!=0) ele.value='';
	if(e)
	{
	if (IE)
		{
		var docleft = window.event.clientX + document.body.scrollLeft;
		var doctop  = window.event.clientY + document.body.scrollTop;
		}
	else
		{
		var docleft = e.pageX;
		var doctop  = e.pageY;
		}
	if (docleft<100) docleft = 100;
	if (docleft>document.body.clientWidth-250) docleft=document.body.clientWidth-250;
	if (doctop>=document.body.clientHeight+document.body.scrollTop-350) doctop=document.body.clientHeight+document.body.scrollTop-350;		
	if (doctop<20) doctop = 20;
	document.getElementById('uniwin').innerHTML='';
	document.getElementById('uniwin').style.left=docleft-100+'px'; 
	document.getElementById('uniwin').style.top=doctop-50+'px';
	document.getElementById('uniwin').innerHTML='<img src="img/sp.gif">';
	setTimeout("document.getElementById('uniwin').style.display='block'",100);
	}
	//if(mod==0) showempty();
	doLoad('uniwin','calendar_light.php',a,0,0,mod);
	}
if(a==3)
	{
	var tmho=document.getElementById('tmho').value;
	var tmmi=document.getElementById('tmmi').value;
	var m=document.getElementById('selmon').value;
	var y=document.getElementById('selyea').value;
	doLoad('uniwin','calendar_light.php',a,m,y,mod,tmho,tmmi);
	}
if(a==4 || a==7)
	{
	var tmho=document.getElementById('tmho').value;
	var tmmi=document.getElementById('tmmi').value;
	tmho=tmho*3600;
	tmmi=tmmi*60;
	e=e+tmho+tmmi;
	if(a==4) setformval(5,1,e);
	if(a==7) { frmcode=f_aid; setformval(6,2,e); setTimeout("window.location.reload()",2000); }
	if(datele!=0) datele.value=mod;
	calendar_light(2);
	}
if(a==5) 
	{
	var tmho=document.getElementById('tmho').value;
	var tmmi=document.getElementById('tmmi').value;
	var m=document.getElementById('selmon').value;
	var y=document.getElementById('selyea').value;
	doLoad('uniwin','calendar_light.php',3,m,y,0,tmho,tmmi);
	}
if(a==8) 
	{
	var tmho=document.getElementById('tmho').value;
	var tmmi=document.getElementById('tmmi').value;
	var m=document.getElementById('selmon').value;
	var y=document.getElementById('selyea').value;
	doLoad('uniwin','calendar_light.php',6,m,y,0,tmho,tmmi);
	}
}
// ------ новый тендер
function createTender(){
	if(!confirm('Создать новый?')) return;
	window.open(siteurl+'m/index.php?r=tender%2Fcreate');
}

// ------ новый контрагент
function newcust(){
	if(!confirm('Создать нового контрагента?')) return;
	window.open(siteurl+'view.php?profile?create');
	//window.open(siteurl+'newcust.php');
}

// ------ установка фильтра
function setfilter(n,v){
	document.cookie ='filter'+n+'='+v+';';
//	for(i=1;i<=10;i++) if(document.getElementById('flt'+i) && getCookie('filter'+i)==0) alert(i); //document.getElementById('flt'+i).style.background='#ffffff';
//	if(v>0) document.getElementById('flt'+n).style.background='#ffbbbb';
	menuact(curmenu);
}

// ------ установка фильтра без обновления страницы
function toggleact(n){
	var v=0;
	var src=document.getElementById('tact').src.substr(-5,1);
	if(src=='f') { document.getElementById('tact').src='img/swon.png'; v=1; }
	if(src=='n') { document.getElementById('tact').src='img/swoff.png'; v=0; }
	document.cookie ='filter'+n+'='+v+';';
	if(v==1)
		{
		document.getElementById('actlist').style.display='none';
		document.getElementById('exfun').style.display='none';
		document.getElementById('reqlist').style.display='block';
		document.getElementById('rtd01').style.background='#ffeeee';
		document.getElementById('rtd02').style.background='#ffeeee';
		document.getElementById('rtd03').style.background='#ffeeee';
		}
	else
		{
		document.getElementById('actlist').style.display='block';
		document.getElementById('exfun').style.display='block';
		document.getElementById('reqlist').style.display='none';
		document.getElementById('rtd01').style.background='#eeeeee';
		document.getElementById('rtd02').style.background='#eeeeee';
		document.getElementById('rtd03').style.background='#eeeeee';
		}
}

// ------ установка сортировки
function setsort(n){
	for(i=1;i<=3;i++) if(i!=n) document.cookie ='sort'+i+'=0;';
	if(getCookie('sort'+n)==null) { document.cookie ='sort'+n+'=1;'; menuact(curmenu); return; }
	if(getCookie('sort'+n)==0) { document.cookie ='sort'+n+'=1;'; menuact(curmenu); return; }
	if(getCookie('sort'+n)==1) { document.cookie ='sort'+n+'=2;'; menuact(curmenu); return; }
	if(getCookie('sort'+n)==2) { document.cookie ='sort'+n+'=0;'; menuact(curmenu); return; }
 }

// ------ установка отображения всех событий за год в календаре
function setchfye(){
	if(getCookie('chfye')!=1) { document.cookie ='chfye=1;'; return; }
	if(getCookie('chfye')==1) { document.cookie ='chfye=0;'; return; }
}

// ------ поиск
function doSearch(v){
	if(v==0)
		{
		document.getElementById('sstr').value='';
		document.cookie ='filter220=;';	// снимаем фильтр по первой букве
		document.cookie ='filter701=;';	// снимаем фильтр по инициатору заявки на ТВ и ТР
		searchstr='';
		}
	else searchstr=v;
	menuact(curmenu);
}

// ------ добавляем/удаляем пользователей из acclist
function acclist(act,csid,userid){
	if(act==2) if(!confirm('Удалить права пользователя на доступ к данному контрагенту?')) return;
	doLoad('emptydiv','acclist.php',act,csid,userid);
	if(act<3 && curmenu!=1) setTimeout("window.location.reload()",2000);
	if(curmenu==1) menuact(curmenu);
}

// ------ добавляем/редактируем план продаж
function addsplan(act,manid,e){
if(act==1)
	{
	//showempty();
	if(e)
	{
	if (IE)
		{
		var docleft = window.event.clientX + document.body.scrollLeft;
		var doctop  = window.event.clientY + document.body.scrollTop;
		}
	else
		{
		var docleft = e.pageX;
		var doctop  = e.pageY;
		}
	//if (docleft>document.body.clientWidth-300) docleft=document.body.clientWidth-300;
	if (doctop>=document.body.clientHeight+document.body.scrollTop-700) doctop=document.body.clientHeight+document.body.scrollTop-700;		
	if (doctop<20) doctop = 20;
	//if (docleft<30) docleft = 30;
	//if (docleft>400) docleft -= 400;
	docleft=70;
	document.getElementById('uniwin').innerHTML='';
	document.getElementById('uniwin').style.left=docleft+'px'; 
	document.getElementById('uniwin').style.top=doctop+'px';
	document.getElementById('uniwin').innerHTML='<img src="img/sp.gif">';
	setTimeout("document.getElementById('uniwin').style.display='block'",100);
	}
	var spyear=document.getElementById('spyear').value;
	doLoad('uniwin','addsplan.php',act,manid,spyear);
	}
}

// ------ закрытие uniwin
function uniclose(){
	//document.getElementById('escr').style.display='none';
	document.getElementById('uniwin').style.display='none';
	if(curmenu==6) menuact(curmenu);	// перезагружаем после правки плана продаж
}

// ------ закрытие uniwin с перезагрузкой
function unicloseM(){
	document.getElementById('escr').style.display='none';
	if(iscdchanged==1 && curmenu!=10) { iscdchanged=0; setTimeout("window.location.reload()",500); }
	else document.getElementById('uniwin').style.display='none';
	if(curmenu==10) menuact(curmenu);
}

// ------ закрытие дополнительных окон
function extrawinclose(){
	if(document.getElementById('winact01')) document.getElementById('winact01').style.display='none';
}

// ------ выбор цвета заметки календаря
function togglecolr(n){
	document.getElementById('colron'+n).style.display='none';
	document.getElementById('colset'+n).style.display='block';
}

// ------ сохраняем план продаж
function selsp(a,v){
	if(document.getElementById('spusr01')) var spusr=document.getElementById('spusr01').value;
	var spyear=document.getElementById('spyear').value;
//	if(a==1) for(i=1;i<=24;i++) document.getElementById('spin'+i).value='0';
//	if(a==1 && spusr==0) for(i=1;i<=24;i++) document.getElementById('spin'+i).disabled=true;
//	if(a==1 && spusr>0) for(i=1;i<=24;i++) document.getElementById('spin'+i).disabled=false;
	if(a==1) doLoad('uniwin','addsplan.php',a,v,spyear);
	if(a==2 && spusr>0)
		{
		var str=document.getElementById('spin'+v).value;
		str=str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		document.getElementById('spin'+v).value=str;
		}
	if(a==2 && spusr==0) { alert('Сначала выберите менеджера'); document.getElementById('spin'+v).value='0'; return; }
	if(a==2)
		{
		var vval=document.getElementById('spin'+v).value;
		doLoad('emptydiv','addsplan.php',a,spusr,spyear,v,vval);
		}
}

// ------ отображаем пустой фон под всплывающим окном
function showempty(){
			var docleft = document.body.clientWidth;
			var doctop  = document.body.clientHeight;
			document.getElementById('escr').style.width=parseInt(docleft)+'px'; 
			document.getElementById('escr').style.height=parseInt(doctop)+'px';
			document.getElementById('escr').style.left='0px'; 
			document.getElementById('escr').style.top='0px';
			document.getElementById('escr').style.display='block';
}

// ------ добавляем/редактируем запись календаря
function setevent(dat,csid,act,e){
if(act==0)
	{
	if(e)
	{
	if (IE)
		{
		var docleft = window.event.clientX + document.body.scrollLeft;
		var doctop  = window.event.clientY + document.body.scrollTop;
		}
	else
		{
		var docleft = e.pageX;
		var doctop  = e.pageY;
		}
	if (docleft<360) docleft = 360;
	if (docleft>document.body.clientWidth-250) docleft=document.body.clientWidth-250;
	if (doctop>=document.body.clientHeight+document.body.scrollTop-200) doctop=document.body.clientHeight+document.body.scrollTop-200;		
	if (doctop<20) doctop = 20;
	document.getElementById('uniwin').innerHTML='';
	document.getElementById('uniwin').style.left=docleft-350+'px'; 
	document.getElementById('uniwin').style.top=doctop-50+'px';
	document.getElementById('uniwin').innerHTML='<img src="img/sp.gif">';
	setTimeout("document.getElementById('uniwin').style.display='block'",100);
	}
	doLoad('uniwin','setevent.php',dat,csid,act);
	return;
	}
if(act==10)
	{
	doLoad('emptydiv','setevent.php',dat,csid,10);
	setTimeout("doLoad('uniwin','setevent.php',"+dat+","+csid+",0)",1000);
	return;
	}
if(act==11)
	if(confirm('Удалить заметку?')) {
	doLoad('emptydiv','setevent.php',dat,csid,act);
	iscdchanged=1;
	unicloseM();
	return;
	}
if(act==15)
	{
	doLoad('emptydiv','setevent.php',dat,csid,act);
	if(document.getElementById('steb'+csid)) document.getElementById('steb'+csid).style.display='none';
	iscdchanged=1;
	unicloseM();
	return;
	}
if(act>1 && act<7)
	{
	var dat1=document.getElementById('aday'+dat).value+'-'+document.getElementById('amon'+dat).value+'-'+document.getElementById('ayea'+dat).value+'-'+document.getElementById('ahou'+dat).value+'-'+document.getElementById('amin'+dat).value;
	dat=dat1;
	}
	if(document.getElementById('steb'+csid)) document.getElementById('steb'+csid).style.display='block';
	doLoad('emptydiv','setevent.php',dat,csid,act);
	iscdchanged=1;
}

// управление новым действием в сделках
function newact(n,ecsid,deal,code){
//alert(n+' - '+ecsid+' - '+deal+' - '+code);
	if(n==0) document.getElementById('naction').style.display='block';
	if(n==1) document.getElementById('naction').style.display='none';
	if(n==2)	// запись нового действия
		{
		//if(document.getElementById('iscom'))
			//if(document.getElementById('iscom').value=='0') { alert('Нет ни одного активного КП'); return; }
		var isreq=0; if(document.getElementById('reqlist').style.display=='block') isreq=1;
		var actid=document.getElementById('actidM').value;
		var act2id=document.getElementById('act2list').value;
		var sl01=replacehtml(document.getElementById('sl01').value);	// имя контакта
		var sl02=replacehtml(document.getElementById('sl02').value); // контакт
		//if(isreq==1) actid=document.getElementById('reqlist').value;
		//var txt=document.getElementById('tdescr').innerHTML;
		var txt='';
		if(divedit==0)
			{
			//txt=replacehtml(document.getElementById('tdescr').innerHTML);
			txt=document.getElementById('tdescr').innerText;
			// doLoad('emptydiv','setevent.php',txt,ecsid,13,act2id,deal);
			}
		if(actid==10)
			{
			if(document.getElementById('cncid'))
				{
				s101=0;
				if(document.getElementById('cncid').value=='0')
					{ alert('Укажите причину аннулирования сделки'); return; }
				else
					if(!confirm('Аннулировать сделку?')) return;
				sl01 = document.getElementById('cncid').value
				}
			}
		if(txt.length<10 && divedit==0 && actid<10) { alert('Требуется описание действия'); return; }
//alert(n+' ecsid='+ecsid+' deal='+deal+' code='+code+' isreq='+isreq+' actid='+actid+' act2id='+act2id+' txt='+txt+' code='+code+' s101='+sl01+' s102='+sl02);
		doLoad('emptydiv','newact.php',n,ecsid,deal,isreq,actid,act2id,txt,code,sl01,sl02);
		if(divedit==1) setTimeout("doLoad('emptydiv','setevent.php',0,'"+ecsid+"',14,"+act2id+","+deal+")",1500);
		document.getElementById('nact01').innerHTML='<img src="img/sp.gif">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		setTimeout("window.location.reload()",2000);
		curstage=0;
		}
}

function cancelstage(actid,dealid,ecsid,act2id){
	if(!confirm('Это действие предназначено для отмены сделки. Вызвать форму для комментария по отмене сделки?')) return;
	document.getElementById('naction').style.display='block';
	document.getElementById('nstage').style.display='none';
	document.getElementById('cstage').style.display='none';
	document.getElementById('actidM').value=actid;
	document.getElementById('tdescr').innerHTML='';
	var req = new JsHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4)
			{
			document.getElementById('tdescr').contentEditable=true;
			document.getElementById('exfun').innerHTML=req.responseJS.q;
			document.getElementById('tdescr').innerHTML=req.responseJS.q1;
			}
		}
	req.open(null, 'selaction.php', true);
	req.send( { q: actid, q1: dealid, q2: ecsid, q3: act2id } );
	//doLoad('exfun','selaction.php',actid,dealid,ecsid,act2id);
	frmcode=0;
}

function nextstage(actid,act2id,ecsid,dealid){
		document.getElementById('naction').style.display='block';
		var ischild=0;
		if(document.getElementById('ischild')) ischild=document.getElementById('ischild').value;
		//document.getElementById('tdescr').style.height='125px';
		divedit=0;
		curstage=act2id;
		a2id=act2id;
		var req = new JsHttpRequest();
		req.onreadystatechange = function() {
			if (req.readyState == 4)
				{
				document.getElementById('tdescr').contentEditable=true;
				document.getElementById('tdescr').innerHTML=req.responseJS.q;
				if(req.responseJS.q4>0)
					{
					//document.getElementById('tdescr').style.height='300px';
					document.getElementById('tdescr').contentEditable=false;
					}
				skpstage=req.responseJS.q1;
				cncstage=req.responseJS.q2;
				if(ischild>0)
					{
					skpstage=1;
					document.getElementById('nstage').style.display='block';
					document.getElementById('vstage').style.display='none';
					}
				if(skpstage>0)
					{
					document.getElementById('nstage').style.display='block';
					if(req.responseJS.q3==1)
						{
						divedit=1;
						//document.getElementById('nstage').style.display='none';
						//document.getElementById('vstage').style.display='block';
						document.getElementById('nstage').style.display='block';
						document.getElementById('vstage').style.display='none';
						}
					}
				if(cncstage>0) document.getElementById('cstage').style.display='block';
				}
			}
		req.open(null, 'stagetempl.php', true);
		req.send( { q: act2id, q1: dealid, q2: ecsid, q3: 0, q4: 0 } );
		//doLoad('tdescr','stagetempl.php',act2id);
		setTimeout("selaction("+actid+","+dealid+",'"+ecsid+"')",100);
}

function skipstage(ecsid,dealid){
		//document.getElementById('tdescr').style.height='125px';
		divedit=0;
		var ischild=0;
		if(document.getElementById('ischild')) ischild=document.getElementById('ischild').value;
		var req = new JsHttpRequest();
		req.onreadystatechange = function() {
			if (req.readyState == 4)
				{
				advt=setTimeout("autoAdvice(1000)",1000);
				if(req.responseJS.q>0)
				{
				curstage=req.responseJS.q;
				skpstage=req.responseJS.q2;
				document.getElementById('tdescr').contentEditable=true;
				document.getElementById('tdescr').innerHTML=req.responseJS.q1;
				if(req.responseJS.q5>0) 
					{
					divedit=1;
					document.getElementById('tdescr').contentEditable=false;
					//document.getElementById('tdescr').style.height='300px';
					}
				if(req.responseJS.q2==0) document.getElementById('nstage').style.display='none';
					else document.getElementById('nstage').style.display='block';
				if(req.responseJS.q4==0) document.getElementById('cstage').style.display='none';
					else document.getElementById('cstage').style.display='block';
				document.getElementById('vstage').style.display='none';
				a2id=curstage;
				var actid=req.responseJS.q3;
				selaction(actid,dealid,ecsid);
				} else document.getElementById('exfun').innerHTML='Отсутствует конфигурация этапа.';
				if(ischild>0 && actid<9) document.getElementById('nstage').style.display='block';
				}
			}
		req.open(null, siteurl+'skipstage.php', true);
		req.send( { q: ecsid, q1: dealid, q2: curstage, q3: 0, q4: 0, q5: 0 } );
}

function setcode(code){
	frmcode=code;
}

function setformval(typ,n,v){
	doLoad('emptydiv',siteurl+'setformval.php',typ,n,v,frmcode);
	//alert(typ+' '+n+' '+v+' '+frmcode);
	n++;
	if(typ==4) if(document.getElementById('f_sele'+n)) document.getElementById('f_sele'+n).style.display='block';
}

function addfile(act, f, code, ecsid, dealid) {
	if (f.value!='')
		{
		if(act==1) doLoad('attfile',siteurl+'addfile.php', act, f, code, ecsid, dealid);
		}
	else alert('Выберите файл для загрузки');
}

function addfileact(act, f, code, ecsid, dealid, act2id) {
	if(act==2 && !confirm('Удалить файл?')) return;
	doLoad('attfile'+act2id,siteurl+'addfileact.php', act, f, code, ecsid, dealid, act2id);
}

function addreqfile(act, f, trid, ecsid, dealid) {
	if (f.value!='') doLoad('reqfile'+trid,siteurl+'addreqfile.php', act, f, trid, ecsid, dealid);
	else alert('Выберите файл для загрузки');
}

function delreqfile(trid,fid) {
	if (confirm('Удалить вложение?')) doLoad('reqfile'+trid,siteurl+'delreqfile.php', fid);
	else alert('Выберите файл для загрузки');
}

function addcomment(aid,dealid,ele,ecsid){
	var comm=replacehtml(ele.value);
	if(aid>0 && comm.length>2) doLoad('cmt'+aid,siteurl+'addcomment.php',aid,dealid,comm,ecsid,0,0);
	ele.value='';
}

function addcomment_cs(ele,ecsid,n){
	var comm=replacehtml(ele.value);
	var recipid=0; if(document.getElementById('commAddr'+n)) recipid=document.getElementById('commAddr'+n).value;
	if(ecsid!='') doLoad('cmt11'+n,siteurl+'addcomment.php',0,0,comm,ecsid,n,recipid);
	ele.value='';
}

// действие при выборе типа действия в сделке
function selaction(actid,dealid,ecsid){
	if(actid==0) document.getElementById('exfun').innerHTML='';
	else
		{
		//for(i=1;i<=10;i++) document.getElementById('tda'+i).style.borderBottom='2px solid #ffffff';
		//document.getElementById('tda'+actid).style.borderBottom='2px solid #ff0000';
		document.getElementById('actidM').value=actid;
		//document.getElementById('exfun').innerHTML='<br />&nbsp;&nbsp;&nbsp;<img src="'+siteurl+'img/sp.gif" border="0">';
		doLoad('exfun','selaction.php',actid,dealid,ecsid,a2id);
		}
}

// открытие новых блоков для доп.контактов и т.п. в showcust
function newblk(a){
 document.getElementById('nbl'+a).style.display='block';
}

// создание окна для выбора из списка
function sele(e) {
	if (IE)
		{
		docleftA = window.event.clientX + document.body.scrollLeft;
		doctopA  = window.event.clientY + document.body.scrollTop;
		}
	else
		{
		docleftA = e.pageX;
		doctopA  = e.pageY;
		}
}

function shlist(a,v,e,v1,v2){
	if(a==99) document.getElementById('act03').style.display='none';
	if((a==1 || a==3 || a==4 || a==5) && docleftA>0)
		{
		//scont_enable=1;
		v=v.replace(/"/g, '');
		v=v.replace(/'/g, '');
		var req = new JsHttpRequest();
		req.onreadystatechange = function() {
			if (req.readyState == 4)
				{
				//if(req.responseJS.q!='') scont_enable=0;
				document.getElementById('act03').innerHTML=req.responseJS.q;
				document.getElementById('act03').style.left=docleftA-120+'px'; 
				document.getElementById('act03').style.top=doctopA+15+'px';
				document.getElementById('act03').style.display='block';
				document.getElementById('uniwin').style.display='none';
				}
			}
		req.open(null, 'shlist.php', true);
		req.send( { q: a, q1: v, q2: v1, q3: v2 } );
		}
	if(a==2)
		{
		document.getElementById('uniwin').style.left=110+'px'; 
		document.getElementById('uniwin').style.top=doctopA+15+'px';
		if(v.length>1)
			{
			doLoad('uniwin','shlist.php',a,v,v1,v2);
			document.getElementById('uniwin').style.display='block';
			}
		else document.getElementById('uniwin').style.display='none';
		}
}

function saveplan(act,v0,v1,v2,v3){
	if(act==1) // сохраняем значение и отображаем его в поле
	{
	var req = new JsHttpRequest();
	req.onreadystatechange = function() {
		if (req.readyState == 4)
			{
			document.getElementById('inp'+v2).value=req.responseJS.q;
			document.getElementById('dv'+v2).innerHTML=req.responseJS.q1;
			document.getElementById('uniwin').style.display='none';
			}
		}
	req.open(null, 'saveplan.php', true);
	req.send( { q: act, q1: v0, q2: v1, q3: v2, q4: v3 } );
	}
	if(act==2 || act==3 || act==4)
	{
	var vvv1=v1.value;
	v1.disabled=true;
	if(act==4) //комментарии
		{
		vvv1 = vvv1.replace(/'/g, "&quot;");
		vvv1 = vvv1.replace(/"/g, "&quot;");
		vvv1 = vvv1.replace(/\\/g,"/");
		}
	doLoad('emptydiv','saveplan.php',act,v0,vvv1,v2);
	setTimeout("menuact("+curmenu+")",1500);
	}
	if(act==5)
	{
	doLoad('emptydiv','saveplan.php',act,v0,v1,v2);
	setTimeout("menuact("+curmenu+")",1500);
	}
}

function operdeal(a,ecsid,dealid,v){
		if(a==20) if(!confirm('К сделке получит доступ выбранный менеджер. Продолжить?')) return;
		if(a==1) document.getElementById('newdeal').style.display='none'; // новая сделка
		if(a==3) if(!confirm('Сделка будет скрыта. Продолжить?')) return;
		//if(a==4) if(!confirm('Заявка будет скрыта и дальнейшие действия по ней будут невозможны. Продолжить?')) return;
		setTimeout("location.href='"+siteurl+"showdeal.php?q=1&q1=0&q2="+ecsid+"'",1500);
		if(v==0 && document.getElementById('dealname')) v=document.getElementById('dealname').value;
		if(a==5) { alert(dealid+' '+v); return; }
		doLoad('emptydiv','operdeal.php',a,ecsid,dealid,v);
}

function opercdeal(a,ecsid,dealid,v){
		if(dealid==0) return;
		if(!confirm('Создать новое доп.соглашение?')) return;
		setTimeout("location.href='"+siteurl+"showdeal.php?q=1&q1="+dealid+"&q2="+ecsid+"'",1500);
		doLoad('emptydiv','operdeal.php',a,ecsid,dealid,v);
}

function putdata(n,v,v1,v2,v3){
	var ddiv='pdata'+n;
	if(n==4) ddiv='pdata3'; // удаление услуги из treqformchild
	if(v1) ddiv+=v1; if(v2) ddiv+=v2;
	doLoad(ddiv,siteurl+'putdata.php',n,v,v1,v3);
}

function putdata_new(n,v,v1,v2,v3){
	var ddiv='pdata'+n;
	if(n==4) ddiv='pdata3'; // удаление услуги из treqformchild
	if(v1) ddiv+=v1; if(v2) ddiv+=v2;
	doLoad(ddiv,siteurl+'putdata.php',n,v,v1,v3,tempval1);
	tempval1=0;
}

// из putdata.php
function savetreq(n,trid,servid,tsid,tvid){
	//if(n==7 && document.getElementById('tvsele'+tsid)) document.getElementById('tvsele'+tsid).selectedIndex="0";
	if(document.getElementById('tvcust'+servid))
		{
		document.getElementById('tvcust'+servid).style.display='block';
		//if(n!=7) document.getElementById('tvcust'+servid).value='';
		}
	doLoad('emptydiv',siteurl+'savetreq.php',n,trid,servid,tsid,tvid);
}

// из putdata.php
function savetreq_new(n,trid,servid,tsid,tvid){
	//if(n==7 && document.getElementById('tvsele'+tsid)) document.getElementById('tvsele'+tsid).selectedIndex="0";
	if(document.getElementById('tvcust'+servid))
		{
		document.getElementById('tvcust'+servid).style.display='block';
		//if(n!=7) document.getElementById('tvcust'+servid).value='';
		}
	doLoad('emptydiv',siteurl+'savetreq.php',n,trid,servid,tsid,tvid,tempval1);
	tempval1=0;
}

function saveCustTreq(trid,servid,tsid,val,tvid){
	doLoad('emptydiv',siteurl+'savetreq.php',7,trid,servid,tsid,val,tvid,tempval1);
	//document.getElementById('tvsele'+servid).disabled=true;
	tempval1=0;
}

function sendtreqform(trid,ecsid,dealid,act2id,trtypeid){
	if(trid>0)
	{
	var sval=1; var cval=0;
	var inp1 = document.getElementById('inp1').value;
	var inp2 = document.getElementById('s101').value;
	var mpoint = 0;
	if(inp1>0 && inp2>0)
		{
		// проверка все ли элементы заполнены
		var tableElem = document.getElementById('tdat');
		var slelem = tableElem.getElementsByTagName('select');
		var chelem = tableElem.getElementsByTagName('input');
		for (var i = 0; i < slelem.length; i++) {
		if(slelem[i].value==0) sval=0;
		}
		for (var i = 0; i < chelem.length; i++) {
		if(chelem[i].checked==true) cval=1;
		}
		if(cval==0) { alert('Услуга не выбрана'); return; }
		if(sval==0) { alert('Заполните все поля'); return; }
		//document.getElementById('sb01'+trid).style.display='none';
		func=sendtreqform_after; funcParam1='self'; funcParam2=trid; // проверка на запись заявки
		doLoad('emptydiv','savetreq.php',100,trid,trtypeid);
		//window.opener.document.getElementById('naction').style.display='block';
		// загрузка в окне-родителе
		setTimeout("doLoadOpener('exfun','selaction.php',4,"+dealid+",'"+ecsid+"',"+act2id+")",1200);
		//setTimeout("window.location.reload()",3000);
		}
	else alert('Укажите местоположение и контактное лицо контрагента');
	}
}

function sendtreqform_after(param) {
	if(param=='<ERROR>') { alert('Заполните обязательные поля!'); return; }
	if(funcParam2>0) document.getElementById('sb01'+funcParam2).style.display='none';
	window.opener.document.getElementById('naction').style.display='block';
	setTimeout("window.location.reload()",3000);
}

function treqaction(a,trid,ecsid,dealid,v,v1){
	if((a==1 || a==5) && confirm('Сохранить результаты ТВ и ТР для этого подключения?'))
		{
		//doLoad('emptydiv','treqaction.php',a,trid,ecsid,dealid,v);
		var req = new JsHttpRequest();
		req.onreadystatechange = function() {
			if (req.readyState == 4)
				{
				if(req.responseJS.q=='')
					{
					document.getElementById('sb01'+trid).style.display='none';
					setTimeout("menuactM(7)",300);
					setTimeout("window.location.reload()",700);
					}
				else alert('Не приложен файл с ТР');
				}
			}
		req.open(null, siteurl+'treqaction.php', true);
		req.send( { q: a, q1: trid, q2: ecsid, q3: dealid, q4: v } );
		return;
		}
	if(a==10)
		{
		if(confirm('Удалить подключение?'))
			{
			doLoad('emptydiv',siteurl+'treqaction.php',a,trid,ecsid,dealid,v);
			setTimeout("window.opener.location.reload()",2000);
			setTimeout("window.location.reload()",2000);
			document.getElementById('sb01'+trid).style.display='none';
			}
		return;
		}
	if(a==43)
		{ 
		if(confirm('Заявка будет отозвана. Продолжить?'))
			{
			doLoad('emptydiv',siteurl+'treqaction.php',a,trid,ecsid,dealid,v);
			setTimeout("window.opener.location.reload()",2000);
			setTimeout("window.location.reload()",2000);
			document.getElementById('sb01'+trid).style.display='none';
			}
		return;
		}
	if(a==20 || a==21)
		{ 
		if(confirm('Отклонить заявку?'))
			{
			doLoad('emptydiv',siteurl+'treqaction.php',a,trid,ecsid,dealid,v);
			setTimeout("window.opener.location.reload()",2000);
			setTimeout("window.location.reload()",2000);
			document.getElementById('sb01'+trid).style.display='none';
			}
		return;
		}
	if(a==11 || a==51)
		{
		document.getElementById('stwb'+trid).style.display='none';
		setTimeout("window.location.reload()",2000);
		}
	if(a==30)
		{
		if(confirm('Централизовать заявку для этого подключения?'))
			{
			doLoad('emptydiv',siteurl+'treqaction.php',a,trid,ecsid,dealid,v);
			setTimeout("window.location.reload()",2000);
			document.getElementById('gcent'+trid).style.display='none';
			}
		return;
		}
	if(a==40)
		{
		doLoad('trequser'+trid,siteurl+'treqaction.php',a,trid,ecsid,dealid,v);
		return;
		}
	if(a==41)
		{
		setTimeout("window.location.reload()",2000);
		}
	if(a==42)
		{
		if(!confirm('Вы уверены?')) return;
		setTimeout("window.location.reload()",2000);
		}

	doLoad('emptydiv',siteurl+'treqaction.php',a,trid,ecsid,dealid,v,v1);
}

function showaddtreq(trid){
	document.getElementById('sb02'+trid).style.display='none';
	document.getElementById('sb03'+trid).style.display='block';
}

function selprice(a,n,ctid,plsid,code,ecsid,edealid,eactid,v1){
//	if(comoffer) if(comoffer.opener) comoffer.location.reload();
//	if(comoffer) if(comoffer.document.write('')) comoffer.location.reload();
	if(a==4)
		{
		var nchk=document.getElementById('chkq'+v1).value;
		var sd=document.getElementById('sdq'+v1).value;
		var cls='lotr1';
		if(sd=='') cls='lotr2';
		if(nchk=='')
			{
			document.getElementById('chkq'+v1).value='1';
			document.getElementById('tridq'+v1).className='hitr';
			doLoad('emptydiv','selprice.php',4,3,n,ctid,plsid,code,ecsid,edealid,eactid,v1); // добавляем
			}
		else
			{
			document.getElementById('chkq'+v1).value='';
			document.getElementById('tridq'+v1).className=cls;
			//doLoad('emptydiv','selprice.php',5,3,n,ctid,plsid,code,ecsid,edealid,eactid,v1); // добавляем
			doLoad('emptydiv','selprice.php',2,3,n,ctid,plsid,code,ecsid,edealid,eactid,v1); // удаляем
			}
		return;
		}
	var nsum=parseFloat(document.getElementById('prc'+n).value);
	var nchk=document.getElementById('chk'+n).value;
	var sd=document.getElementById('sd'+n).value;
	var cls='lotr1';
	var isedit = ecsid.length;
	if(sd=='') cls='lotr2';
	if(nchk=='')
		{
		if(a==1) pricesum1+=nsum;
		if(a==2) pricesum2+=nsum;
		if(a==3) pricesum3+=nsum;
		document.getElementById('chk'+n).value='1';
		document.getElementById('trid'+n).className='hitr';
		if(isedit>7) doLoad('emptydiv','selprice.php',1,a,n,ctid,plsid,code,ecsid,edealid,eactid); // добавляем
		}
	else
		{
		if(a==1) pricesum1-=nsum;
		if(a==2) pricesum2-=nsum;
		if(a==3) pricesum3-=nsum;
		document.getElementById('chk'+n).value='';
		document.getElementById('trid'+n).className=cls;
		if(isedit>7) doLoad('emptydiv','selprice.php',2,a,n,ctid,plsid,code,ecsid,edealid,eactid); // удаляем
		}

	if(a==1) document.getElementById('pay'+a).innerHTML=splitNums(' ',pricesum1)+' тг';
	if(a==2) document.getElementById('pay'+a).innerHTML=splitNums(' ',pricesum2)+' тг';
	if(a==3) document.getElementById('pay'+a).innerHTML=splitNums(' ',pricesum3)+' тг';
	document.getElementById('sb01').style.display='none';
	if(pricesum1>0) document.getElementById('sb01').style.display='block';
	if(pricesum2>0) document.getElementById('sb01').style.display='block';
	if(pricesum3>0) document.getElementById('sb01').style.display='block';
	priceDiv(pricesum1,pricesum2,pricesum3);
	//if(a==1) doLoad('mpadiv','plchilddiv.php',1,a,n,ctid,plsid,code,ecsid,edealid,eactid);
	//if(a==1) alert('AAA');
}

function priceDiv(p1,p2,p3){
	var yt=parseInt(document.body.scrollTop);
	var xt=parseInt(document.body.clientWidth);
	document.getElementById('uniwin').style.top=yt+20+'px';
	document.getElementById('uniwin').style.left=xt-220+'px';
	document.getElementById('uniwin').innerHTML='<table style="width:200px;background:#ffffff;border:10px solid #ffffff;" border="0"><tr><td style="border-bottom:1px solid #cccccc;">Единовременные платежи</td></tr><tr><td class="haction" style="text-align:right;">'+splitNums(' ',p1)+'</td></tr><tr><td style="border-bottom:1px solid #cccccc;">Ежемесячные платежи</td></tr><tr><td class="haction" style="text-align:right;">'+splitNums(' ',p2)+'</td></tr><tr><td style="border-bottom:1px solid #cccccc;">Платежи за трафик</td></tr><tr><td class="haction" style="text-align:right;">'+splitNums(' ',p3)+'</td></tr></table>';
	document.getElementById('uniwin').style.display='block';
	setTimeout("uniclose()",2000);
}
// пересчет цен в КП
function corecnt(a,n){
	var price=parseFloat(document.getElementById('a0'+a+n).value);
	var eprice=parseFloat(document.getElementById('in0'+a+n).value);
	var eoprice=parseFloat(document.getElementById('in1'+a+n).value);
	var qty=parseInt(document.getElementById('qt'+a+n).value);
	var oqty=parseInt(document.getElementById('qt1'+a+n).value);
	var tsum=parseFloat(document.getElementById('tsum'+a).value);
	var t2sum=parseFloat(document.getElementById('t2sum'+a).value);
	var eosum=price*oqty;
	var eo2sum=eoprice*oqty;
	var esum=price*qty;
	var e2sum=eprice*qty;
	document.getElementById('in0'+a+n).style.background='#ffffff';
	if(eprice<price) document.getElementById('in0'+a+n).style.background='#ffcccc';
	tsum=(tsum-eosum)*1+esum*1;
	t2sum=(t2sum-eo2sum)*1+e2sum*1;
	document.getElementById('a1'+a+n).innerHTML=splitNums(' ',esum);
	document.getElementById('a2'+a+n).innerHTML=splitNums(' ',e2sum);
	document.getElementById('atsum'+a).innerHTML=splitNums(' ',tsum);
	document.getElementById('at2sum'+a).innerHTML=splitNums(' ',t2sum);
	document.getElementById('tsum'+a).value=tsum;
	document.getElementById('t2sum'+a).value=t2sum;
	document.getElementById('in1'+a+n).value=eprice;
	document.getElementById('qt1'+a+n).value=qty;
    var req = new JsHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
				eprice=req.responseJS.q;
				e2sum=eprice*qty;
				document.getElementById('in0'+a+n).value=eprice;
				document.getElementById('a2'+a+n).innerHTML=splitNums(' ',e2sum);
				if(req.responseJS.q1=='1') reasonBar(1,a,n,eprice);
        }
    }
    req.open(null, 'coaction.php', true);
    req.send( { q: 1, q1: n, q2: price, q3: eprice, q4: qty } );
}

function reasonBar(act,a,n,eprice){
if(act==1)
	{
    var req = new JsHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
			var ts=req.responseJS.q;
			if (ts!='')
			{
			var docleft = document.body.clientWidth;
			var doctop  = document.body.clientHeight;
			document.getElementById('escr').style.width=parseInt(docleft)+'px'; 
			document.getElementById('escr').style.height=parseInt(doctop)+'px';
			document.getElementById('escr').style.left='0px'; 
			document.getElementById('escr').style.top='0px';
            document.getElementById('uniwin').innerHTML = ts;
			document.getElementById('uniwin').style.left=parseInt(docleft/2-215)+'px'; 
			document.getElementById('uniwin').style.top=parseInt(doctop/2-200)+'px';
			document.getElementById('escr').style.display='block';
			document.getElementById('uniwin').style.display='block';
			setTimeout("document.getElementById('rb01').focus()",1000);
			}
        }
    }
    req.open(null, 'reasonbar.php', true);
    req.send( { q: act, q1: a, q2: n, q3: eprice, q4: 0 } );
	}
if(act==2)
	{
//	var qty=parseInt(document.getElementById('qt'+a+n).value);
//	var e2sum=eprice*qty;
//	document.getElementById('in0'+a+n).value=eprice;
//	document.getElementById('a2'+a+n).innerHTML=splitNums(' ',e2sum);
//	document.getElementById('in0'+a+n).style.background='#ffffff';
	document.getElementById('escr').style.display='none';
	document.getElementById('uniwin').style.display='none';
	}
if(act==3)
	{
	var descr=document.getElementById('rb01').value;
	doLoad('emptydiv','reasonbar.php',act,a,n,eprice,descr);
	document.getElementById('escr').style.display='none';
	document.getElementById('uniwin').style.display='none';
	}
}

function delLoad(dealid,ecsid,act2id){
	if(window.opener)
	{
	if(window.opener.document.getElementById('naction'))
		{
		if(window.opener.document.getElementById('naction').style.display=='block')
			{
			// загрузка в окне-родителе
			doLoadOpener('exfun','selaction.php',5,dealid,ecsid,act2id);
			}
		else window.opener.location.reload();
		}
	}
}

function reloadPrice(dealid,ecsid){
	if(window.opener)
	if(window.opener.document.getElementById('exprice'))
		{
		var req = new JsHttpRequest();
		req.onreadystatechange = function() {
			if (req.readyState == 4)
				window.opener.document.getElementById('exprice').innerHTML=req.responseJS.q;
			}
	    req.open(null, 'reloadprice.php', true);
		req.send( { q: dealid, q1: ecsid } );
		}
}

function coaction(a,lid,v1,v2,v3,v4,v5){
	if(a==2) // удаление/восстановление позиции
		{
		if(document.getElementById('bd'+lid)) document.getElementById('bd'+lid).innerHTML='...';
		doLoad('emptydiv','coaction.php',a,lid,v1,0,0);
		setTimeout("window.location.reload()",1500);
		}
	if(a==10) // измененеие физ.объемов
		{
		doLoad('emptydiv','coaction.php',a,lid,v1,v2,v3,v4,v5);
		setTimeout("window.location.reload()",1500);
		}
	if(a==3) // сохранение КП
		{
		// lid выполняет роль cmid
		// v2=tid, id шаблона
		document.getElementById('sb01').style.display='none';
		var ed1 = editor1.getData();
		var ed2 = editor2.getData();
		document.getElementById('editor1').innerHTML = ed1;
		document.getElementById('editor2').innerHTML = ed2;
		editor1.destroy();
		editor2.destroy();
		doLoad('emptydiv','coaction.php',a,lid,v2,ed1,ed2);
		setTimeout("reloadPrice("+v3+",'"+v1+"')",2000); // перезагружаем показатели сделки
		setTimeout("delLoad("+v3+",'"+v1+"',"+v5+")",2000);
		setTimeout("location.href='comoffer.php?defa="+v1+"&df5e="+lid+"&ef80="+v2+"&aace="+v4+"'",3000);
		//setTimeout("window.close()",3000);
		}
	if(a==4) // редактирование КП и смена шаблона
		{
		doLoad('emptydiv','coaction.php',a,lid,v2,0,0);
		setTimeout("location.href='comoffer.php?defa="+v1+"&df5e="+lid+"&ef80="+v2+"&aa9e=85468&aace="+v3+"'",200);
		}
	if(a==5 && confirm('Удалить КП?')) // удаление КП
		{
		document.getElementById('sb01').style.display='none';
		//document.getElementById('sb01').innerHTML='<img src="'+siteurl+'img/sp.gif" border="0">';
		doLoad('emptydiv','coaction.php',a,lid,0,0,0);
		setTimeout("reloadPrice("+v3+",'"+v1+"')",2000); // перезагружаем показатели сделки
		setTimeout("delLoad("+v3+",'"+v1+"')",2000);
		setTimeout("window.close()",3000);
		}
	if(a==6) // смена валюты
		{
		doLoad('emptydiv','coaction.php',a,lid,v1,v2,0);
		setTimeout("window.location.reload()",2000);
		}
	if(a==7) // PDF
		{
		window.open('comoffer_pdf.php?defa='+v1+'&df5e='+lid+'&ef80='+v2);
		}
	if(a==8) // печать
		{
		location.href='comoffer.php?defa='+v1+'&df5e='+lid+'&ef80='+v2+'&fcuv=1';
		}
	if(a==20) // сохранение позиции от руки
		{
		var val1=''; var val2=0; var val3=0; var val4=0; var isok=1;
		if(document.getElementById('smartXInput_'+lid)) { val1=document.getElementById('smartXInput_'+lid).value; if(val1.length==0) isok=0; }
		if(document.getElementById('msrXinp01_'+lid)) { val2=document.getElementById('msrXinp01_'+lid).value; if(val2==0) isok=0; }
		if(document.getElementById('msrXinp02_'+lid)) val3=document.getElementById('msrXinp02_'+lid).value;
		if(document.getElementById('msrXinp03_'+lid)) val4=document.getElementById('msrXinp03_'+lid).value;
		if(isok==1) {
			doLoad('emptydiv','shlist.php',8,v1,v2,v3,val1,val2,val3,val4);
			}
		setTimeout("window.location.reload()",2000);
		}
	if(a==21) // расчет суммы от руки
		{
		var val3=0; var val4=0;
		if(document.getElementById('msrXinp02_'+lid)) val3=document.getElementById('msrXinp02_'+lid).value;
		if(document.getElementById('msrXinp03_'+lid)) val4=document.getElementById('msrXinp03_'+lid).value;
		if(document.getElementById('msrXinp04_'+lid)) document.getElementById('msrXinp04_'+lid).value=splitNums(' ',parseFloat(val3*val4));
		if(v1>0) doLoad('emptydiv','shlist.php',9,v1,v2,v3);
		}
}

function cntaction(a,v1,v2,v3,v4){
	if(a==3) // сохранение договора
		{
		document.getElementById('sb01').style.display='none';
		var ed1 = editor1.getData();
		var ed2 = editor2.getData();
		var sg = 0; var csg = 0; var acnum=''; var acntdate='';
		var lic = ''; var cslic = ''; var cslicdate = '';
		var ccphone = '';
		var text_01 = ''; var text_02 = ''; var text_03 = '';
		if(document.getElementById('acnum_kz')) acnum = document.getElementById('acnum_kz').value;
		if(document.getElementById('acnum_ru')) acnum = document.getElementById('acnum_ru').value;
		if(document.getElementById('acnum_en')) acnum = document.getElementById('acnum_en').value;
		if(document.getElementById('acntdate_kz')) acntdate = document.getElementById('acntdate_kz').value;
		if(document.getElementById('acntdate_ru')) acntdate = document.getElementById('acntdate_ru').value;
		if(document.getElementById('acntdate_en')) acntdate = document.getElementById('acntdate_en').value;
		if(document.getElementById('signer_kz')) sg = document.getElementById('signer_kz').value;
		if(document.getElementById('signer_ru')) sg = document.getElementById('signer_ru').value;
		if(document.getElementById('signer_en')) sg = document.getElementById('signer_en').value;
		if(document.getElementById('cssigner_kz')) csg = document.getElementById('cssigner_kz').value;
		if(document.getElementById('cssigner_ru')) csg = document.getElementById('cssigner_ru').value;
		if(document.getElementById('cssigner_en')) csg = document.getElementById('cssigner_en').value;
		if(document.getElementById('alic_kz')) lic = document.getElementById('alic_kz').value;
		if(document.getElementById('alic_ru')) lic = document.getElementById('alic_ru').value;
		if(document.getElementById('alic_en')) lic = document.getElementById('alic_en').value;
		if(document.getElementById('cslic_kz')) cslic = document.getElementById('cslic_kz').value;
		if(document.getElementById('cslic_ru')) cslic = document.getElementById('cslic_ru').value;
		if(document.getElementById('cslic_en')) cslic = document.getElementById('cslic_en').value;
		if(document.getElementById('cslicdate_kz')) cslicdate = document.getElementById('cslicdate_kz').value;
		if(document.getElementById('cslicdate_ru')) cslicdate = document.getElementById('cslicdate_ru').value;
		if(document.getElementById('cslicdate_en')) cslicdate = document.getElementById('cslicdate_en').value;
		if(document.getElementById('ccphone_ru')) ccphone = document.getElementById('ccphone_ru').value;
		if(document.getElementById('ccphone_kz')) ccphone = document.getElementById('ccphone_kz').value;
		if(document.getElementById('ccphone_en')) ccphone = document.getElementById('ccphone_en').value;
		if(document.getElementById('text_01')) text_01 = document.getElementById('text_01').value;
		if(document.getElementById('text_02')) text_02 = document.getElementById('text_02').value;
		if(document.getElementById('text_03')) text_03 = document.getElementById('text_03').value;
		document.getElementById('editor1').innerHTML = ed1;
		document.getElementById('editor2').innerHTML = ed2;
		editor1.destroy();
		editor2.destroy();
		doLoad('emptydiv','cntaction.php',a,v1,v2,v3,ed1,ed2,sg,csg,lic,cslic,cslicdate,acnum,acntdate,ccphone,text_01,text_02,text_03);
		setTimeout("location.href='contract.php?defa="+v1+"&aace="+v2+"'",3000);
		}
	if(a==4) // редактирование договора и смена шаблона
		{
		doLoad('emptydiv','cntaction.php',a,v1,v2,v3);
		setTimeout("location.href='contract.php?defa="+v1+"&aace="+v2+"&aa9e=1&ef80="+v3+"'",200);
		}
	if(a==7) // просмотр
		{
		location.href='contract.php?defa='+v1+'&aace='+v2+'&ef80='+v3;
		}
	if(a==8) // PDF
		{
		window.open('contract_pdf.php?defa='+v1+'&aace='+v2+'&ef80='+v3+'&fcuv=1');
		}
}

function actaction(a,v1,v2,v3,v4){
	if(a==3) // сохранение
		{
		var sg = 0; var csg = 0; var exactdate = 0; var csactdate = 0;
		var acttype = 0;
		document.getElementById('sb01').style.display='none';
		document.getElementById('sb01a').style.display='none';
		if(document.getElementById('signer_kz')) sg = document.getElementById('signer_kz').value;
		if(document.getElementById('signer_ru')) sg = document.getElementById('signer_ru').value;
		if(document.getElementById('signer_en')) sg = document.getElementById('signer_en').value;
		if(document.getElementById('cssigner_kz')) csg = document.getElementById('cssigner_kz').value;
		if(document.getElementById('cssigner_ru')) csg = document.getElementById('cssigner_ru').value;
		if(document.getElementById('cssigner_en')) csg = document.getElementById('cssigner_en').value;
		if(document.getElementById('exactdate')) exactdate = document.getElementById('exactdate').value;
		if(document.getElementById('csactdate')) csactdate = document.getElementById('csactdate').value;
		if(document.getElementById('acttype')) acttype = document.getElementById('acttype').value;
		if(acttype==0) doLoad('emptydiv','chcont.php',403,v1,v4,sg,csg);
		if(acttype==1) doLoad('emptydiv','chcont.php',406,v1,v4,sg,csg);
		if(acttype==0 && (exactdate!='' || csactdate!='')) doLoad('emptydiv','chcont.php',404,v1,v4,exactdate,csactdate);
		if(acttype==1 && (exactdate!='' || csactdate!='')) doLoad('emptydiv','chcont.php',407,v1,v4,exactdate,csactdate);
		setTimeout("location.href='showcact.php?defa="+v1+"&actt="+acttype+"&aace="+v2+"'",2000);
		}
	if(a==4) // редактирование и смена шаблона
		{
		if(v4==0) doLoad('emptydiv','chcont.php',405,v1,v2,v3,0);
		setTimeout("location.href='showcact.php?defa="+v1+"&aace="+v2+"&aa9e=1&ef80="+v3+"'",200);
		}
	if(a==10) // редактирование и смена шаблона
		{
		if(v4==0) doLoad('emptydiv','chcont.php',405,v1,v2,v3,1);
		setTimeout("location.href='showcact.php?defa="+v1+"&aace="+v2+"&actt=1&aa9e=1&ef80="+v3+"'",200);
		}
	if(a==7) // просмотр
		{
		location.href='showcact.php?defa='+v1+'&aace='+v2+'&ef80='+v3;
		}
	if(a==8) // PDF
		{
		window.open('showcact.php?defa='+v1+'&aace='+v2+'&ef80='+v3+'&fcuv=1&pdf=1');
		}
	if(a==11) // просмотр
		{
		location.href='showcact.php?defa='+v1+'&actt=1&aace='+v2+'&ef80='+v3;
		}
	if(a==12) // PDF
		{
		window.open('showcact.php?defa='+v1+'&actt=1&aace='+v2+'&ef80='+v3+'&fcuv=1&pdf=1');
		}
}

function showplchild(act,e,pid,n,ctid,plsid,code,ecsid,edealid,eactide){
if(act==1)
	{
	if(e)
	{
	if (IE)
		{
		var docleft = window.event.clientX + document.body.scrollLeft;
		var doctop  = window.event.clientY + document.body.scrollTop;
		}
	else
		{
		var docleft = e.pageX;
		var doctop  = e.pageY;
		}
	if (doctop<20) doctop = 20;
	document.getElementById('uniwin').innerHTML='';
	docleft=400;
	document.getElementById('uniwin').style.left=docleft+'px'; 
	document.getElementById('uniwin').style.top=doctop-150+'px';
	document.getElementById('uniwin').innerHTML='<img src="img/sp.gif">';
	setTimeout("document.getElementById('uniwin').style.display='block'",100);
	}
	doLoad('uniwin','pricelistchild.php',pid,n,ctid,plsid,code,ecsid,edealid,eactide);
	}
}

// confirm discount
function cnfdisc(lid,ecsid){
	//if(!confirm('Подтвердить цену?')) return;
	var prsel=0; var prcom="";
	if(document.getElementById('prsel')) prsel=document.getElementById('prsel').value;
	if(document.getElementById('prcom')) prcom=document.getElementById('prcom').value;
	doLoad('emptydiv','coaction.php',7,lid,ecsid,prsel,prcom);
	setTimeout("menuactM(8)",1500);
	setTimeout("window.location.reload()",2000);
}

// reject discount
function cancdisc(lid,ecsid){
	//if(!confirm('Отказать?')) return;
	var prsel=0; var prcom="";
	if(document.getElementById('prsel')) prsel=document.getElementById('prsel').value;
	if(document.getElementById('prcom')) prcom=document.getElementById('prcom').value;
	doLoad('emptydiv','coaction.php',72,lid,ecsid,prsel,prcom);
	setTimeout("menuactM(8)",1500);
	setTimeout("window.location.reload()",2000);
}


function discmenu(act,lid,ecsid,eprice,e){
if(act==1)
	{
	if(e)
		{
		if (IE)
			{
			var docleft = window.event.clientX + document.body.scrollLeft;
			var doctop  = window.event.clientY + document.body.scrollTop;
			}
		else
			{
			var docleft = e.pageX;
			var doctop  = e.pageY;
			}

		document.getElementById('uniwin').style.left=docleft-150+'px'; 
		document.getElementById('uniwin').style.top=doctop+'px';
		document.getElementById('uniwin').innerHTML='<img src="img/sp.gif">';
		doLoad('uniwin','coaction.php',71,lid,ecsid,eprice,0);
		setTimeout("document.getElementById('uniwin').style.display='block'",100);
		setTimeout("if(document.getElementById('prsel')) document.getElementById('prsel').select()",500);
		}
	}
}

function addwallcom(act,imgwall){
	var comm=document.getElementById('wallcomm').value;
	document.getElementById('wallcomm').value='';
	if(act==2 || comm.length>2) doLoad('emptydiv','addwallcom.php',act,comm,imgwall);
	setTimeout("menuact(10)",2000);
}

function openpicld(){
	document.getElementById('addpic').style.display='block';
}

function wallaction(act,aid){
	doLoad('like'+aid,'wallaction.php',act,aid);
	if(document.getElementById('like2'+aid))
		setTimeout("document.getElementById('like2"+aid+"').innerHTML=document.getElementById('like"+aid+"').innerHTML",2000);
}

function savecprice(act,ecsid,dealid,ele,aid){
	var prc=ele.value;
	if(act==1 || act==2)
		if(!confirm("Сохранить стоимость договора?")) return;
		else
		{
		act=1;
		prc=prc.replace(' ','');
		prc=prc.replace(',','.');
		prc=parseFloat(prc.replace(' ',''));
		ele.value=splitNums(' ',prc);
		}
	if(act==3) if(!confirm("Сохранить валюту договора?")) return;
	if(act==4) if(!confirm("Сохранить новый номер договора?")) return;
	if(act==5) if(!confirm("Сохранить новую дату договора?")) return;
	if(act>0) setTimeout("window.location.reload()",1500);
	doLoad('emptydiv','savecprice.php',act,ecsid,dealid,prc,aid);
}

function addcmpos(pid,cpid,cmid,ctype,pr){
	document.getElementById('newsrv'+ctype).innerHTML='&nbsp;<img style="width:15px;" src="'+siteurl+'img/sp.gif" border="0">';
	doLoad('emptydiv','addcmpos.php',pid,cpid,cmid,ctype,pr);
	setTimeout("window.location.reload()",2000);
}

function closedelay(){
	setTimeout("document.getElementById('act03').style.display='none'",500);
}

function undoact(dealid,aid,actid){
	if(confirm("Данное действие будет отменено. Продолжить?"))
		{
		doLoad('emptydiv','undoact.php',dealid,aid,actid);
		setTimeout("window.location.reload()",1500);
		}
}

// ------ просмотр истории
function showhist(ecsid){
	window.open(siteurl+'showhist.php?q='+ecsid);
}

function cc_setmanager(a,cid,trid,e){
	for(i=1;i<=50;i++)
		if(document.getElementById('tr'+i)) if(i%2) document.getElementById('tr'+i).className='lotr1'; else document.getElementById('tr'+i).className='lotr2';
	if(document.getElementById('tr'+trid)) document.getElementById('tr'+trid).className='hitr';
	document.getElementById('uniwin').style.display='none';
	if (IE)
		{
		var docleft = window.event.clientX + document.body.scrollLeft;
		var doctop  = window.event.clientY + document.body.scrollTop;
		}
	else
		{
		var docleft = e.pageX;
		var doctop  = e.pageY;
		}
	doctop=doctop-50;
	docleft=docleft-50;
	if (docleft<100) docleft = 100;
	if (docleft>document.body.clientWidth-250) docleft=document.body.clientWidth-250;
	if (doctop>=document.body.clientHeight+document.body.scrollTop-70) doctop=document.body.clientHeight+document.body.scrollTop-70;		
	if (doctop<20) doctop = 20;
	document.getElementById('uniwin').innerHTML='';
	document.getElementById('uniwin').style.left=docleft-20+'px'; 
	document.getElementById('uniwin').style.top=doctop-15+'px';
	document.getElementById('uniwin').innerHTML='<img src="img/sp.gif">';
	setTimeout("document.getElementById('uniwin').style.display='block'",100);
	doLoad('uniwin','setmanager.php',a,cid);
}

function cc_chdepman(v,cid){
	doLoad('sman01','setmanager.php',2,v,cid);
}

function cc_makeclient(v,cid){
	document.getElementById('sman01').innerHTML='&nbsp;<img style="height:15px;" src="'+siteurl+'img/sp.gif" border="0">';
	doLoad('emptydiv','setmanager.php',3,v,cid);
//	setTimeout("uniclose()",2200);
	setTimeout("menuact(11)",2000);
}

function cc_delegate(v,cid){
	document.getElementById('sman02').innerHTML='&nbsp;<img style="height:15px;" src="'+siteurl+'img/sp.gif" border="0">';
	doLoad('emptydiv','setmanager.php',4,v,cid);
	setTimeout("menuact(11)",2000);
}

function profileimg(a,f,e){
	if(a==1 || a==3)
	{
	if (IE)
		{
		var docleft = window.event.clientX + document.body.scrollLeft;
		var doctop  = window.event.clientY + document.body.scrollTop;
		}
	else
		{
		var docleft = e.pageX;
		var doctop  = e.pageY;
		}
	if (docleft<100) docleft = 100;
	if (docleft>document.body.clientWidth-250) docleft=document.body.clientWidth-250;
	if (doctop>=document.body.clientHeight+document.body.scrollTop-70) doctop=document.body.clientHeight+document.body.scrollTop-70;		
	if (doctop<20) doctop = 20;
	document.getElementById('uniwin').innerHTML='';
	document.getElementById('uniwin').style.left=docleft-20+'px'; 
	document.getElementById('uniwin').style.top=doctop-15+'px';
	document.getElementById('uniwin').innerHTML='<img src="img/sp.gif">';
	setTimeout("document.getElementById('uniwin').style.display='block'",100);
	doLoad('uniwin','profileimg.php',a,f);
	}
	if(a==2 || a==4)
	{
	var req = new JsHttpRequest();
	req.onreadystatechange = function() {
	//alert(req.readyState);
		if (req.readyState == 4)
			{
			if(a==2) document.getElementById('bgimg2').src=req.responseJS.q;
			if(a==4) document.getElementById('mainbg').style.background='URL('+req.responseJS.q+')';
			}
		}
	req.open(null, 'profileimg.php', true);
	req.send( { q: a, q1: f } );
	document.getElementById('uniwin').innerHTML='';
	document.getElementById('uniwin').style.display='none';
	}
}

function accrequest(a,ecsid,param){
if(!confirm('Запросить полный доступ к этому клиенту?')) return;
	doLoad('emptydiv','accrequest.php',a,ecsid,param);
	setTimeout("menuact(curmenu)",1500);
}

function accrequestM(a,ecsid,param){
if(!confirm('Запросить полный доступ к этому клиенту?')) return;
	doLoad('emptydiv','accrequest.php',a,ecsid,param);
	setTimeout("menuactM(1)",1500);
}

function seltreqman(trid,ecsid,dealid,val){
	document.getElementById('rse'+trid).style.display='none';
	doLoad('emptydiv','seltreqman.php',ecsid,dealid,val,trid);
	setTimeout("menuact(curmenu)",1500);
}

function cumenu(n){
	for(i=1;i<=7;i++)
		{
		if(document.getElementById('cmenu'+i)) document.getElementById('cmenu'+i).className='cumenu';
		if(document.getElementById('dcu'+i)) document.getElementById('dcu'+i).style.display='none';
		}
	document.getElementById('cmenu'+n).className='cumenu2';
	document.getElementById('dcu'+n).style.display='block';
	document.cookie ='filterSC='+n+';';
	if(n==7) initMap();
}

function trumenu(n){
	for(i=1;i<=7;i++)
		if(document.getElementById('trmenu'+i)) document.getElementById('trmenu'+i).className='cumenu';
	document.getElementById('trmenu'+n).className='cumenu2';
	document.cookie ='filter702='+n+';';
	menuact(7);
}

function abonmenu(n){
	for(i=1;i<=5;i++)
		if(document.getElementById('abmenu'+i)) document.getElementById('abmenu'+i).className='cumenu';
	document.getElementById('abmenu'+n).className='cumenu2';
	document.cookie ='filter1202='+n+';';
	menuact(12);
}

function comenu(n){
	for(i=1;i<=5;i++)
		if(document.getElementById('comenu'+i)) document.getElementById('comenu'+i).className='cumenu';
	document.getElementById('comenu'+n).className='cumenu2';
	document.cookie ='filter802='+n+';';
	menuact(8);
}

function cscontsel(v){
	var objSel=document.getElementById('sl02');
	var req = new JsHttpRequest();
	req.onreadystatechange = function()
		{
		if (req.readyState == 4)
			{
			var arrval=req.responseJS.q;
			var arrtxt=req.responseJS.q1;
			objSel.options.length = 0;
			for(i=0;i<=arrval.length;i++)
				objSel.options[i] = new Option(arrtxt[i],arrval[i]);
			}
		}
	req.open(null, 'cscontsel.php', true);
	req.send( { q: v, q1: 0 } );
}

function addcscont(act,ecsid,csinfoid,e){
	if(act==1 || act==10)	// 1-контакт, 10-местоположение
		{
		if (IE)
			{
			var docleft = window.event.clientX + document.body.scrollLeft;
			var doctop  = window.event.clientY + document.body.scrollTop;
			}
		else
			{
			var docleft = e.pageX;
			var doctop  = e.pageY;
			}
		if (docleft<100) docleft = 100;
		if (docleft>document.body.clientWidth-250) docleft=document.body.clientWidth-250;
		if (doctop>=document.body.clientHeight+document.body.scrollTop-70) doctop=document.body.clientHeight+document.body.scrollTop-70;		
		if (doctop<20) doctop = 20;
		document.getElementById('uniwin').innerHTML='';
		document.getElementById('uniwin').style.left=docleft-100+'px'; 
		document.getElementById('uniwin').style.top=doctop-15+'px';
		document.getElementById('uniwin').innerHTML='<img src="img/sp.gif">';
		setTimeout("document.getElementById('uniwin').style.display='block'",100);
		doLoad('uniwin',siteurl+'addcscont.php',act,ecsid,csinfoid);
		}
	if(act==2)
		{
		var req = new JsHttpRequest();
		req.onreadystatechange = function()
			{
			if (req.readyState == 4)
				{
				var objSel=document.getElementById('s101');
				var arrval=req.responseJS.q;
				var arrtxt=req.responseJS.q1;
				objSel.options.length = 0;
				for(i=0;i<=arrval.length-1;i++)
					objSel.options[i] = new Option(arrtxt[i],arrval[i]);
				}
			}
		req.open(null, siteurl+'addcscont.php', true);
		req.send( { q: act, q1: ecsid, q2: csinfoid } );
		}
	if(act==12)
		{
		var req = new JsHttpRequest();
		req.onreadystatechange = function()
			{
			if (req.readyState == 4)
				{
				var objSel=document.getElementById('inp1');
				var arrval=req.responseJS.q;
				var arrtxt=req.responseJS.q1;
				objSel.options.length = 0;
				for(i=0;i<=arrval.length-1;i++)
					objSel.options[i] = new Option(arrtxt[i],arrval[i]);
				}
			}
		req.open(null, siteurl+'addcscont.php', true);
		req.send( { q: act, q1: ecsid, q2: csinfoid } );
		}
}

function addcscont_t(ecsid,csinfoid){
	if(document.getElementById('contid1'))
		if(document.getElementById('contid1').value==0)
			{ alert('Заполните поле "тип контакта"'); return; }
	if(document.getElementById('cscont1'))
		if(document.getElementById('cscont1').value=='')
			{ alert('Заполните поле "контакт"'); return; }
	setTimeout("addcscont(2,'"+ecsid+"',"+csinfoid+",0)",1000);
	setTimeout("uniclose()",1500);
}

function addcscont_l(ecsid,csinfoid){
	setTimeout("addcscont(12,'"+ecsid+"',"+csinfoid+",0)",1000);
	setTimeout("uniclose()",1500);
}

function checkaddcscont() {
	if(document.getElementById('acc_cslname'))
		if(document.getElementById('acc_cslname').value=='') {
			alert('Заполните название места');
			return false;
			}
	if(document.getElementById('acc_cntname'))
		if(document.getElementById('acc_cntname').value==0) {
			alert('Заполните страну');
			return false;
			}
	if(document.getElementById('acc_cityname') && document.getElementById('acc_altcityname'))
		if(document.getElementById('acc_cityname').value==0)
			if(document.getElementById('acc_altcityname').value=='') {
				alert('Заполните город или другой город/нас.пункт');
				return false;
				}
	document.getElementById('acc_submit').style.display='none';
	return true;
}

function reportvalstore(a,aid,v){
	frmcode=aid;
	setformval(7,a,v);
}

function savereport(ecsid){
		document.getElementById('repbu').style.display='none';
		setTimeout("window.opener.location.href='"+siteurl+"showdeal.php?q=1&q1=0&q2="+ecsid+"'",1500);
		setTimeout("window.close()",2000);
}

function toggle_cnt_type(n,v1,v2){
	document.getElementById('cursrc').innerHTML=document.getElementById('src_cnt'+n).innerHTML;
	doLoad('emptydiv','cntaction.php',7,v1,v2,n);
}

function reminder(){
    var req = new JsHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
			var ts=req.responseJS.q;
			if (ts!='')
			{
			document.getElementById('reminder').innerHTML = ts;
			var docleft = document.body.clientWidth;
			var doctop  = document.body.clientHeight;
			docleft=70; doctop=70;
			//document.getElementById('reminder').style.left=parseInt(docleft/2-215)+'px'; 
			//document.getElementById('reminder').style.top=parseInt(doctop/2+70)+'px';
			document.getElementById('reminder').style.left=docleft+'px';
			document.getElementById('reminder').style.top=doctop+'px';
			document.getElementById('reminder').style.display='block';
			}
        }
    }
    req.open(null, 'reminder.php', true);
	req.send( { q: 0 } );
}

function markAsRead(t,n) {
	doLoad('emptydiv','reminder.php',1,t);
	if(n>1) setTimeout("reminder()",1000);
		else
		{
		document.getElementById('reminder').style.display='none';
		document.getElementById('reminder').innerHTML='';
		}
}

function toggleDiv(divId) {
	if(!document.getElementById(divId)) return;
	if(document.getElementById(divId).style.display=='none') { document.getElementById(divId).style.display='block'; return; }
	document.getElementById(divId).style.display='none';
}

function showAdvice(n,elem) {
	if(document.getElementById('advice_l')) {
		var epos=getOffset(elem);
		document.getElementById('advice_l').innerHTML='<div style="width:100%;text-align:center;vertical-align:middle;"><br /><br /><br /><br /><img src="'+siteurl+'img/sp.gif"></div>'; 
		document.getElementById('advice_l').style.left=epos.left+10+'px'; 
		document.getElementById('advice_l').style.top=epos.top-188+'px';
		doLoad('advice_l','showadvice.php',n);
		setTimeout("document.getElementById('advice_l').style.display='block'",300);
	}
}

function closeAdvice() {
	if(document.getElementById('advice_l')) document.getElementById('advice_l').style.display='none';
}

function autoAdvice(n){
	clearTimeout(advt);
	closeAdvice();
	var sti = 1+n;
	var eni = 10+n;
	for(i=sti;i<=eni;i++)
		if(document.getElementById('advmark'+i))
			{
			var elem = document.getElementById('advmark'+i);
			showAdvice(i,elem);
			setTimeout("closeAdvice()",5000);
			return;
			}
}

function movedeal(a,ecsid,dealid,v){
	if(a==100)
		{
		document.getElementById('movedeal2').style.display='none';
		document.getElementById('movedeal2').innerHTML='';
		document.getElementById('movedeal1').style.display='block';
		return;
		}
	if(a==7)
		{
		v='';
		if(document.getElementById('ndate')) v=document.getElementById('ndate').value;
		setTimeout("window.location.reload()",2000);
		}
	document.getElementById('movedeal1').style.display='none';
	document.getElementById('movedeal2').style.display='block';
	document.getElementById('movedeal2').innerHTML='<img src="'+siteurl+'img/sp.gif">';
	doLoad('movedeal2','operdeal.php',a,ecsid,dealid,v);
}

function rep_sel(a,v) {
	if(a==1) document.cookie ='rdepid01='+v+';';
	if(a==2) document.cookie ='rmanid01='+v+';';
	if(a==3) document.cookie ='servname01='+v+';';
	document.getElementById('rdepid').disabled=true;
	document.getElementById('rmanid').disabled=true;
	document.getElementById('servname').disabled=true;
	document.getElementById('rep_sp').style.display='block';
	setTimeout("window.location.reload()",200);
}

function requiredAlert(ele) {
	var bcg=ele.style.background;
	var eid=ele.id;
	setTimeout("document.getElementById('"+eid+"').style.background='"+bcg+"'",1000);
	ele.style.background='#ffcccc';
}

function delayRefresh() {
	refreshEnable=0;
	setTimeout("refreshEnable=1",120000);
}

function blockAndReload(blockEleId) {
	setTimeout("window.location.reload()",2000);
	if(blockEleId>0) if(document.getElementById('blockEle'+blockEleId)) document.getElementById('blockEle'+blockEleId).innerHTML='<img src="'+siteurl+'img/sp.gif">';
}

function showExpn(trid,ecsid,dealid) {
	window.open(siteurl+'showexpn.php?q='+dealid+'&q1='+ecsid+'&q2='+trid);
}

function findCsidByText(a,v,v1) {
	if(a==1) {
		v=v.replace(/"/g, '');
		v=v.replace(/'/g, '');
		var req = new JsHttpRequest();
		req.onreadystatechange = function() {
			if (req.readyState == 4)
				{
				document.getElementById('winact01').innerHTML=req.responseJS.q;
				document.getElementById('winact01').style.left=docleftA-50+'px'; 
				document.getElementById('winact01').style.top=doctopA+15+'px';
				document.getElementById('winact01').style.display='block';
				}
			}
		req.open(null, 'findcsidbytext.php', true);
		req.send( { q: a, q1: v, q2: v1 } );
		}
	if(a==2 && confirm('Создать сделку у существующего контрагента?')) {
		doLoad('emptydiv','findcsidbytext.php',a,v,v1);
		setTimeout("menuact(curmenu)",2000);
	}
}

function leadaction(a,v1,v2,v3) {
	if(a==1 && document.getElementById('tr00')) {
		if(document.getElementById('tr00').style.display=='block') document.getElementById('tr00').style.display='none';
		else document.getElementById('tr00').style.display='block';
		if(document.getElementById('trx00')) document.getElementById('trx00').style.display='none';
		delayRefresh();
		return;
		}
	if(a==3) setTimeout("menuact(curmenu)",1500);
	if(a==4) setTimeout("menuact(curmenu)",1500);
	if(a==5) if(document.getElementById('trx00')) {
		delayRefresh();
		document.getElementById('trx00').innerHTML='';
		doLoad('trx00','leadaction.php',a,v1,v2,v3);
		document.getElementById('trx00').style.display='block';
		if(document.getElementById('tr00')) document.getElementById('tr00').style.display='none';
		smoothScroll();
		return;
		}
	doLoad('emptydiv','leadaction.php',a,v1,v2,v3);	
}

function smoothScroll() {
	ssmt++;
	if(ssmt<100) setTimeout("smoothScroll()",10);
	else ssmt=0;
	window.scrollBy(0,-100);
}

function checkNewLead() {
	if(document.getElementById('ccNum')) {
	var elem=document.getElementById('ccNum');
	var epos=getOffset(elem);
	if(document.getElementById('newLeadInfo')) {
		var req = new JsHttpRequest();
		req.onreadystatechange = function() {
			if (req.readyState == 4 && req.responseJS.q.length>3)
				{
				document.getElementById('newLeadInfo').innerHTML=req.responseJS.q;
				document.getElementById('newLeadInfo').style.top=epos.top-40+'px';
				document.getElementById('newLeadInfo').style.left=epos.left-65+'px';
				document.getElementById('newLeadInfo').style.display='block';
				}
			else document.getElementById('newLeadInfo').style.display='none';
			}
		req.open(null, 'checknewlead.php', true);
		req.send( { q: 0 } );
		}
	}
}

/*
function changeCont(n) {
	if(document.getElementById('cscont'+n)) document.getElementById('cscont'+n).value='';
	setTimeout("window.location.reload()",1000);
}
*/
