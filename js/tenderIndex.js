function changeTenderStatus(tenderid, status)
{
    $.ajax({
        url: 'index.php?r=tender-ajax%2Ftoggle-tender-status',
        type: 'POST',
        data : { 
            tenderid: tenderid,
            status: +status,
        },
        success: function(data){
            console.log(data);
            
            $('#tender_'+tenderid).remove();
            
            $.toast({
                text : 'Статус заявки тендера изменён! :)',
                heading: 'Успешно',
                showHideTransition: 'slide',
                loader: false,
                loaderBg: '#9EC600',
                position: 'top-right',
                bgColor: '#1BA261'
            });
        }});
}

function toSpecialist(tenderid, manager_id, userid, fullname){
    $.ajax({
        url: 'index.php?r=tender-ajax%2Fto-specialist',
        type: 'POST',
        data : { 
            tenderid: tenderid,
            manager_id: manager_id,
            userid: userid,
            fullname: fullname
        },
        success: function(data){
            console.log(data);
            $.toast({
                text : 'Лот №  успешно делегирован менеджеру',
                heading: 'Успешно',
                showHideTransition: 'slide',
                loader: false,
                loaderBg: '#9EC600',
                position: 'top-right',
                bgColor: '#1BA261'
            });
        }});
}
function search()
{
    $.pjax.reload({container: "#tenders", url: "index.php?r=tender-ajax/search&q=" + $('#search').val()});

}

document.getElementById("search").onkeypress = function(event){
    if (event.keyCode == 13 || event.which == 13){
        search();
    }
};