$( document ).ready(function() {
	function blink(selector) {
		$(selector).fadeOut('slow', function() {
			$(this).fadeIn('slow', function() {
				blink(this);
			});
		});
	}
	setInterval(function(){ 
		blink(".blink");
	}, 1500);

	$("#main_table tr").click(function(){
		$(this).addClass("selected").siblings().removeClass("selected");
	})

	$( '.nav-tabs a' ).on( 'click', function () {
		$( '.nav-tabs' ).find( 'li.active' ).removeClass( 'active' );
		$( this ).parent( 'li' ).addClass( 'active' );
	});

});

