<?php 

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use Yii;
use yii\helpers\ArrayHelper;

class TenderFile extends ActiveRecord
{
	public $fileid;
	public $tenderid;
	public $path;
	public $userid;
	public $tmp_code;
	public $file;
	public $response;

	public function rules(){
		return [
			[['file'], 'file'],
			[['tmp_code'],'integer']
		];
	}

	public static function tableName()
	{
        return 'tender_files';
    }

    public function ajaxUploadFile()
    {

    	$this->file = $_FILES['file'];
    	$this->userid = Yii::$app->auth->user()['userid'];
    	$this->tmp_code = Yii::$app->request->post('tmp_code');

    	if ($this->validate()) {
    		@mkdir('documents/tender');
    		@mkdir('documents/tender/'.$this->tmp_code);

    		copy($this->file['tmp_name'],'documents/tender/'.$this->tmp_code.'/'.Yii::$app->File->pcgbasename($this->file['name']));

    		Yii::$app->db->createCommand()->insert('tender_files', [
    			'path' => 'documents/tender/'.$this->tmp_code.'/'.Yii::$app->File->pcgbasename($this->file['name']),
    			'userid' => $this->userid,
    			'tmp_code' => $this->tmp_code,
    		])->execute();

    		$this->fileid = Yii::$app->db->getLastInsertID();

    		$this->response = (new \yii\db\Query())
	    		->select('*')
	    		->from('tender_files')
	    		->where(['fileid'=>$this->fileid, 'tmp_code'=>$this->tmp_code])
	    		->one();

	    	$this->response['fileName'] = Yii::$app->File->pcgbasename($this->response['path']);

    		return true;
    	}
    }


    public function ajaxUpdateFile()
    {
    	$this->file = $_FILES['file'];
    	$this->userid = Yii::$app->auth->user()['userid'];
        $this->tenderid = Yii::$app->request->post('tenderid');

    	if ($this->validate()) {

            $code = (new \yii\db\Query())
                ->select('file_code')
                ->from('tenders')
                ->where(['tenderid'=>$this->tenderid])
                ->one();

    		@mkdir('documents/tender');
    		@mkdir('documents/tender/'.$code['file_code']);

    		copy($this->file['tmp_name'],'documents/tender/'.$code['file_code'].'/'.Yii::$app->File->pcgbasename($this->file['name']));

    		Yii::$app->db->createCommand()->insert('tender_files', [
    			'path' => 'documents/tender/'.$code['file_code'].'/'.Yii::$app->File->pcgbasename($this->file['name']),
                'tenderid' => $this->tenderid,
    			'userid' => $this->userid,
    		])->execute();

    		$this->fileid = Yii::$app->db->getLastInsertID();

    		$this->response = (new \yii\db\Query())
	    		->select('*')
	    		->from('tender_files')
	    		->where(['fileid'=>$this->fileid])
	    		->one();

	    	$this->response['fileName'] = Yii::$app->File->pcgbasename($this->response['path']);

    		return true;
    	}
    }

    public function uploadFile($tenderid)
    {
    	$this->userid = Yii::$app->auth->user()['userid'];
    	$this->tmp_code = Yii::$app->request->post('tmp_code');

    	$paths = (new \yii\db\Query())
        		->select('path')
        		->from('tender_files')
        		->where(['tenderid'=> null, 'userid'=>$this->userid])
        		->andWhere(['!=', 'tmp_code', $this->tmp_code])
        		->all();

		foreach ($paths as $path) {
			Yii::$app->Directory->delete(Yii::$app->Directory->get($path['path'],1));
		}

		Yii::$app->db->createCommand("
			DELETE FROM tender_files 
			WHERE tenderid IS NULL AND userid = ".$this->userid." AND tmp_code != ".$this->tmp_code."
			")->execute();

		Yii::$app->db->createCommand()
        	->update('tender_files', ['tmp_code' => '', 'tenderid' => $tenderid], 
             	'tmp_code = '.$this->tmp_code)
             ->execute();

        Yii::$app->db->createCommand()
             ->update('tenders', ['file_code' => $this->tmp_code], 
                'tenderid = '.$tenderid)
             ->execute();
    }

    public function ajaxRemoveFile($id)
    {
    	$this->file = (new \yii\db\Query())
		->select('*')
		->from('tender_files')
		->where(['fileid'=>$id])
		->one();

		@unlink($this->file['path']);

		return (new \yii\db\Query())
		->createCommand()
		->delete('tender_files', ['fileid'=>$id])
		->execute();
    }

    public static function download($fileid)
    {
    	$file = (new \yii\db\Query())
		->select(['path'])
		->from('tender_files')
		->where(['fileid'=>$fileid])
		->one();

		return Yii::$app->File->get($file['path']);
    }
}