<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\Auth;

class ManagerHistory extends ActiveRecord
{

	public $id;
	public $mh_lotid;
	public $mh_depid;
	public $mh_userid;
	public $mh_user2id;
	public $mh_user3id;
	public $mh_reason;
	public $mh_created_at;
	public $mh_user4id;
	public $mh_updated_at;
	public $mh_status;
	public $response;

	public static function tableName()
	{
		return 'tender_lot_manager_histories';
	}

	public static function get($id)
	{
		return (new \yii\db\Query())
		->select(['*'])
		->from('tender_lot_manager_histories')
		->where(['mh_lotid'=>$id])
		->orderBy(['id' => SORT_DESC])
		->all();
	}

	private function clean($data)
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = strip_tags($data);
		$data = htmlspecialchars($data);

		return $data;
	}


	public function changeManager()
	{
		$this->mh_userid = Yii::$app->auth->user()['userid'];
		$this->mh_lotid = Yii::$app->request->post('lotid');
		$this->mh_user2id = Yii::$app->request->post('curManager');
		$this->mh_depid = Yii::$app->auth->user()['depid'];
		$this->mh_user3id = Yii::$app->request->post('nextManager');
		$this->mh_reason = $this->clean(Yii::$app->request->post('reason'));
		$this->mh_created_at = Yii::$app->params['currentTime'];

		if ($this->validate()) {

			$transaction = Yii::$app->db->transaction(function ($db){

				$db->createCommand()->insert('tender_lot_manager_histories', [
					'mh_userid' => $this->mh_userid,
					'mh_lotid' => $this->mh_lotid,
					'mh_user2id' => $this->mh_user2id,
					'mh_depid' => $this->mh_depid,
					'mh_user3id' => $this->mh_user3id,
					'mh_reason' => $this->mh_reason,
					'mh_created_at' => $this->mh_created_at,
				])->execute();

				$this->id = Yii::$app->db->getLastInsertID();


			});

			$this->response = (new \yii\db\Query())
			->select('*')
			->from('tender_lot_manager_histories')
			->where(['id'=>$this->id])
			->one();

			$this->response['userid_fullname'] = Yii::$app->UserComponent->getById($this->response['mh_userid'])['fullname'];
			$this->response['user2id_fullname'] = Yii::$app->UserComponent->getById($this->response['mh_user2id'])['fullname'];
			$this->response['user3id_fullname'] = Yii::$app->UserComponent->getById($this->response['mh_user3id'])['fullname'];
			$this->response['created_at'] = date('d-m-Y', $this->response['mh_created_at']);
			$this->response['dname'] = Yii::$app->UserComponent->getById($this->response['mh_userid'])['dname'];


      Yii::$app->MailComponent->sendReqToChangeManager($this->mh_lotid);
			return true;
		}

		return false;
	}


	public function approveChangeManager()
	{
		$this->mh_user4id = Yii::$app->auth->user()['userid'];
		$this->mh_updated_at = Yii::$app->params['currentTime'];
		$this->mh_status = Yii::$app->request->post('checked');
		$this->id = Yii::$app->request->post('rowId');
		$this->mh_lotid = Yii::$app->request->post('lotid');
		$this->mh_user3id = Yii::$app->request->post('user3id');
		$this->mh_user2id = Yii::$app->request->post('user2id');
		
		if ($this->mh_status == 1) {

			$check = (new \yii\db\Query())
							->select(['l_dealid', 'mh_user3id', 't_csid', 'l_tenderid', 'depid'])
							->from('tender_lots')
							->join('inner join', 'tender_lot_manager_histories', 'tender_lot_manager_histories.mh_lotid = tender_lots.lotid')
							->join('inner join', 'tenders', 'tenders.tenderid = tender_lots.l_tenderid')
							->join('inner join', 'tender_lot_dep', 'tender_lot_dep.dep_lotid = tender_lots.lotid')
							->where(['tender_lot_manager_histories.id' => $this->id])
							->one();

			$lots = (new \yii\db\Query())
                ->select('m_lotid')
                ->from('tender_lot_dep')
                ->join('inner join', 'tender_lot_managers', 'tender_lot_managers.m_lotid = tender_lot_dep.dep_lotid')
                ->join('inner join', 'tender_lots', 'tender_lots.lotid = tender_lot_managers.m_lotid')
                ->join('inner join', 'tenders', 'tender_lots.l_tenderid = tenders.tenderid')
                ->where(['tenders.tenderid' => $check['l_tenderid'], 'tender_lot_dep.depid' => $check['depid']])
                ->all();

			$transaction = Yii::$app->db->transaction(function ($db) use ($lots) {

				foreach ($lots as $key => $value) {
					$db->createCommand()->update('tender_lot_manager_histories', [
						'mh_user4id' => $this->mh_user4id,
						'mh_updated_at' => 0,
						'mh_status' => 0,
					], 'mh_lotid ='.$value['m_lotid'])->execute();

					$db->createCommand()->update('tender_lot_manager_histories', [
						'mh_user4id' => $this->mh_user4id,
						'mh_updated_at' => $this->mh_updated_at,
						'mh_status' => $this->mh_status,
					], 'id ='.$this->id)->execute();

					$db->createCommand()->update('tender_lot_managers', [
						'm_managerid' => $this->mh_user3id,
					], 'm_lotid ='.$value['m_lotid'])->execute();		
				}

			});

			if ($check['t_csid'] != 0) {

				$acclist = (new \yii\db\Query())
									->select(['userid'])
									->from('acclist')
									->where(['userid' => $check['mh_user3id'], 'csid' => $check['t_csid']])
									->one();

			if (!$acclist) {
					Yii::$app->db->createCommand()->insert('acclist', [
						'userid' => $check['mh_user3id'],
						'csid' => $check['t_csid'],
						'fullacc' => 1
					])->execute();
				}
			}

			if ($check['l_dealid'] != 0) {

					Yii::$app->db->createCommand()->update('deals', [
							'userid' => $check['mh_user3id'],
					], 'dealid ='.$check['l_dealid'])->execute();

			}

		} else {

				Yii::$app->db->createCommand()->update('tender_lot_manager_histories', [
					'mh_user4id' => $this->mh_user4id,
					'mh_updated_at' => 0,
					'mh_status' => 0,
				], 'mh_lotid ='.$this->mh_lotid)->execute();

				Yii::$app->db->createCommand()->update('tender_lot_managers', [
					'm_managerid' => $this->mh_user2id,
				], 'm_lotid ='.$this->mh_lotid)->execute();

		}
	}
}