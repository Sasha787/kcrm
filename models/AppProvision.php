<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

use app\models\Auth;
use app\models\Tender;
use app\models\Lot;
use app\models\Contragent;
use yii\helpers\ArrayHelper;


class AppProvision extends ActiveRecord
{
	public static $response;

	public static function tableName()
	{
        return 'tender_lot_application_provisions';
    }

    public static function getById($lotid)
    {
    	return (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_application_provisions')
                ->where(['tender_lot_application_provisions.ap_lotid' => $lotid])
                ->one();
    }

    public static function isNeed()
    {
    	$ap_lotid = Yii::$app->request->post('lotid');
    	$app_is_need = Yii::$app->request->post('checked');

    	$check = (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_application_provisions')
                ->where(['tender_lot_application_provisions.ap_lotid' => $ap_lotid])
                ->one();

        $tenderid = (new \yii\db\Query())
                ->select(['l_tenderid'])
                ->from('tender_lots')
                ->where(['tender_lots.lotid' => $ap_lotid])
                ->one();

        Yii::$app->db->createCommand()->update('tenders', [
                'updated_at' => strtotime(date('d-m-Y H:m:s')." +6 hours"),
            ], 'tenderid = '.$tenderid['l_tenderid'])->execute();

        $budget = Lot::getBudget($ap_lotid);

        $percent = Lot::getPercent($ap_lotid);

        $ap_amount = (float)(($budget * $percent)/100);


        if ($check) {
        	Yii::$app->db->createCommand()->update('tender_lot_application_provisions', [
                'ap_is_need' => $app_is_need,
                'ap_userid' => Yii::$app->auth->user()['userid'],
                'ap_amount' => $ap_amount,
            ], 'tender_lot_application_provisions.ap_lotid = '.$ap_lotid)->execute();

        } else {
        	Yii::$app->db->createCommand()->insert('tender_lot_application_provisions', [
              'ap_lotid' => $ap_lotid,
              'ap_is_need' => $app_is_need,
              'ap_amount' => $ap_amount,
              'ap_userid' => Yii::$app->auth->user()['userid'],
            ])->execute();

        }

        static::$response = ['ap_amount' => $ap_amount];
        return true;
    }

    public static function isExist()
    {
    	$ap_lotid = Yii::$app->request->post('lotid');
    	$app_is_need = Yii::$app->request->post('checked');

    	$check = (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_application_provisions')
                ->where(['tender_lot_application_provisions.ap_lotid' => $ap_lotid])
                ->one();

        $tenderid = (new \yii\db\Query())
                ->select('l_tenderid')
                ->from('tender_lots')
                ->where(['tender_lots.lotid' => $ap_lotid])
                ->one();

        Yii::$app->db->createCommand()->update('tenders', [
                'updated_at' => strtotime(date('d-m-Y H:m:s')." +6 hours"),
            ], 'tenderid = '.$tenderid['l_tenderid'])->execute();

        if ($check) {
        	$q = Yii::$app->db->createCommand()->update('tender_lot_application_provisions', [
                'ap_is_exist' => $app_is_need,
                'ap_managerid' => Yii::$app->auth->user()['userid'],
            ], 'tender_lot_application_provisions.ap_lotid = '.$ap_lotid)->execute();

            if ($q) {
            	return true;
            }
        } else {
        	$q = Yii::$app->db->createCommand()->insert('tender_lot_application_provisions', [
              'ap_lotid' => $ap_lotid,
              'ap_is_exist' => $app_is_need,
              'ap_managerid' => Yii::$app->auth->user()['userid'],
            ])->execute();

            if ($q) {
            	return true;
            }
        }

        return false;
    }

    public static function isReturn()
    {
    	$ap_lotid = Yii::$app->request->post('lotid');
    	$app_is_need = Yii::$app->request->post('checked');

    	$check = (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_application_provisions')
                ->where(['tender_lot_application_provisions.ap_lotid' => $ap_lotid])
                ->one();

        $tenderid = (new \yii\db\Query())
                ->select('l_tenderid')
                ->from('tender_lots')
                ->where(['tender_lots.lotid' => $ap_lotid])
                ->one();

        Yii::$app->db->createCommand()->update('tenders', [
                'updated_at' => strtotime(date('d-m-Y H:m:s')." +6 hours"),
            ], 'tenderid = '.$tenderid['l_tenderid'])->execute();

        if ($check) {
        	$q = Yii::$app->db->createCommand()->update('tender_lot_application_provisions', [
                'ap_is_return' => $app_is_need,
            ], 'tender_lot_application_provisions.ap_lotid = '.$ap_lotid)->execute();

            if ($q) {
            	return true;
            }
        } else {
        	$q = Yii::$app->db->createCommand()->insert('tender_lot_application_provisions', [
              'ap_lotid' => $ap_lotid,
              'ap_is_return' => $app_is_need,
            ])->execute();

            if ($q) {
            	return true;
            }
        }

        return false;
    }

    public static function changeAmount($lotid = 0, $ap_amount = 0)
    {

        if ($lotid != 0 && $ap_amount != 0) {
            $ap_amount = $ap_amount;
            $ap_lotid = $lotid;
        } else {
           $ap_amount = Yii::$app->request->post('ap_amount_input');
           $ap_lotid = Yii::$app->request->post('lotid');
        }
  

        $check = (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_application_provisions')
                ->where(['tender_lot_application_provisions.ap_lotid' => $ap_lotid])
                ->one();
                
        $tenderid = (new \yii\db\Query())
                ->select('l_tenderid')
                ->from('tender_lots')
                ->where(['tender_lots.lotid' => $ap_lotid])
                ->one();

        Yii::$app->db->createCommand()->update('tenders', [
                'updated_at' => strtotime(date('d-m-Y H:m:s')." +6 hours"),
            ], 'tenderid = '.$tenderid['l_tenderid'])->execute();

        if ($check) {
            Yii::$app->db->createCommand()->update('tender_lot_application_provisions', [
                'ap_amount' => $ap_amount,
            ], 'tender_lot_application_provisions.ap_lotid = '.$ap_lotid)->execute();

          
            self::$response = $ap_amount;
            return true;

        } else {
            Yii::$app->db->createCommand()->insert('tender_lot_application_provisions', [
              'ap_lotid' => $ap_lotid,
              'ap_amount' => $ap_amount,
            ])->execute();

            
            self::$response = $ap_amount;
            return true;
           
        }
    }

    
}