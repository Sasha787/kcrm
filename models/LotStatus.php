<?php

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

/**
 * 
 */
class LotStatus extends ActiveRecord
{
	public static function tableName()
	{
        return 'tender_lot_statuses';
    }

    public static function getAll()
    {
    	return (new \yii\db\Query())
            ->select(['lid','status_title'])
            ->from('tender_lot_statuses')
            ->where(['is_active' => 1])
            ->all();
    }
}