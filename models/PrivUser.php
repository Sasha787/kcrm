<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;


class PrivUser extends ActiveRecord
{

	public static function tableName()
	{
		return 'privusers';
	}

	public static function getByDepid($depid)
	{
		return (new \yii\db\Query())
							->select(['userid'])
							->from('privusers')
							->where(['depid' => $depid, 'tender'=>4, 'isactive'=>1])
							->all();
	}

}