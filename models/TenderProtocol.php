<?php 
namespace app\models;


use Yii;
use yii\db\ActiveRecord;
use app\models\Auth;

use yii\web\UploadedFile;

class TenderProtocol extends ActiveRecord
{
    public $protocol_id;
    public $protocol_tenderid;
    public $protocol_dictionary_id;
    public $protocol_title;
    public $protocol_userid;
    public $protocol_date;
    public $response;

    public static function tableName()
    {
        return 'tender_protocols';
    }

    public static function getAll($tenderid)
    {
        return (new \yii\db\Query())
                    ->select(['*'])
                    ->from('tender_protocols')
                    ->where(['protocol_tenderid'=>$tenderid])
                    ->all();
    }


    public function uploadAjaxFile()
    {
        $this->file = $_FILES['file'];
        $this->prt_id = Yii::$app->request->post('prt_id');
        $this->file_code = Yii::$app->request->post('file_code');

        @mkdir('documents/lot');
        @mkdir('documents/lot/'.$this->file_code);
        @mkdir('documents/lot/'.$this->file_code.'/protocols');

        copy($this->file['tmp_name'],'documents/lot/'.$this->file_code.'/protocols/'.Yii::$app->File->pcgbasename($this->file['name']));

        $file_title = Yii::$app->File->pcgbasename($this->file['name']);

        Yii::$app->db->createCommand()->insert('tender_lot_protocol_files', [
                'pf_prt_id' => $this->prt_id,
                'pf_path' => 'documents/lot/'.$this->file_code.'/protocols/'.Yii::$app->File->pcgbasename($this->file['name']),
                'pf_filename' => $file_title,
            ])->execute();

        $pf_id = Yii::$app->db->getLastInsertID();

        $this->response = (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_protocol_files')
                ->where(['pf_id'=>$pf_id])
                ->one();

            return true;
    }

    public static function getFiles($lotid)
    {
        return (new \yii\db\Query())
                    ->select(['*'])
                    ->from('tender_lot_protocol_files')
                    ->join('inner join', 'tender_lot_protocols', 'tender_lot_protocols.id = tender_lot_protocol_files.pf_prt_id')
                    ->where(['tender_lot_protocols.prt_lotid'=>$lotid])
                    ->all();
    }

    public function create($tenderid)
    {
        $this->protocol_dictionary_id = Yii::$app->request->post('pd_id');
        $this->protocol_userid = Yii::$app->auth->user()['userid'];
        $this->protocol_title =  Yii::$app->request->post('protocol_title');

        $query = Yii::$app->db->createCommand()->insert('tender_protocols', [
                  'protocol_tenderid' => $tenderid,
                  'protocol_dictionary_id' => $this->protocol_dictionary_id,
                  'protocol_title' => $this->protocol_title,
                  'protocol_userid' => $this->protocol_userid,
                  'protocol_date' => Yii::$app->params['currentTime'],
                ])->execute();

        //return json_encode('test');

        $this->protocol_id = Yii::$app->db->getLastInsertID();

        if ($query) {
            $this->response = [
                'protocol_id' => $this->protocol_id,
                'protocol_dictionary_id' => $this->protocol_dictionary_id,
                'protocol_title' => $this->protocol_title,
            ];
        }
        return true;
    }

    public static function download($id)
    {
        $file = (new \yii\db\Query())
        ->select(['pf_path'])
        ->from('tender_lot_protocol_files')
        ->where(['pf_id'=>$id])
        ->one();

        return Yii::$app->File->get($file['pf_path']);
    }

    public static function removeFile($id)
    {
        $file = (new \yii\db\Query())
        ->select(['pf_path'])
        ->from('tender_lot_protocol_files')
        ->where(['pf_id'=>$id])
        ->one();

        @unlink($file['pf_path']);

        return (new \yii\db\Query())
        ->createCommand()
        ->delete('tender_lot_protocol_files', ['pf_id'=>$id])
        ->execute();
    }

    public static function removeProtocol($id)
    {
        $q = (new \yii\db\Query())
        ->createCommand()
        ->delete('tender_protocols', ['protocol_id'=>$id])
        ->execute();

        $q2 = (new \yii\db\Query())
        ->select(['protocol_file_path'])
        ->from('tender_protocol_files')
        ->where(['protocol_id'=>$id])
        ->all();

        $q3 = (new \yii\db\Query())
        ->createCommand()
        ->delete('tender_protocol_files', ['protocol_id'=>$id])
        ->execute();

        foreach ($q2 as $key => $value) {
            @unlink($value['protocol_file_path']);
        }

        return true;
    }
}