<?php 
namespace app\models;


use Yii;
use yii\db\ActiveRecord;


class ProtocolDictionary extends ActiveRecord
{
 
    public static function tableName()
    {
        return 'tender_protocol_dictionary';
    }

    public static function getAll()
    {
        return (new \yii\db\Query())
                    ->select(['*'])
                    ->from('tender_protocol_dictionary')
                    ->where(['pd_is_active'=>1])
                    ->all();
    }
}