<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

class LotSign extends ActiveRecord
{
	public $userid;
	public $depid;
	public $fullname;
	public $dname;
	public $user;
	public $lotid;
    public $s_risk_id;
	public $sid;
	public $response;


	public static function tableName()
	{
        return 'tender_lot_signed';
    }

    public function rules()
	{
		return [
			[['userid', 'depid', 'fullname', 'dname', 'lotid'], 'required'],
		];
	}

	public static function all($lotid)
	{  
		return (new \yii\db\Query())
    		->select('*')
    		->from('tender_lot_signed')
    		->where(['s_lotid'=>$lotid])
    		->all();
	}

    public static function allOfRisk($lotid, $riskId)
    {  
        return (new \yii\db\Query())
            ->select('*')
            ->from('tender_lot_signed')
            ->where(['s_lotid'=>$lotid, 's_risk_id'=>$riskId])
            ->all();
    }

    public function addUser()
    {	
    	$this->userid = Yii::$app->request->post('userid');
    	$this->user = Yii::$app->UserComponent->getById($this->userid);
    	$this->depid = $this->user['depid'];
    	$this->dname = $this->user['dname'];
    	$this->fullname = $this->user['fullname'];
    	$this->lotid = Yii::$app->request->post('lotid');
        $this->s_risk_id = Yii::$app->request->post('riskId');


    	if ($this->validate()) {

            $tenderid = (new \yii\db\Query())
            ->select(['l_tenderid', 'lot_number'])
            ->from('tender_lots')
            ->where(['tender_lots.lotid' => $this->lotid])
            ->one();

            Yii::$app->db->createCommand()->update('tenders', [
                'updated_at' => Yii::$app->params['currentTime'],
            ], 'tenderid = '.$tenderid['l_tenderid'])->execute();

    		$check = (new \yii\db\Query())
	    		->select('*')
	    		->from('tender_lot_signed')
	    		->where(['s_lotid'=>$this->lotid, 's_userid' => $this->userid, 's_risk_id' => $this->s_risk_id])
	    		->one();

	    	if ($check) {
	    		return false;
	    	}

    		$q = Yii::$app->db->createCommand()->insert('tender_lot_signed', [
    			's_lotid' => $this->lotid,
                's_risk_id' => $this->s_risk_id,
    			's_userid' => $this->userid,
    			's_username' => $this->fullname,
    			's_depid' => $this->depid,
    			's_dname' => $this->dname,
    			'is_signed' => 0,
    			's_date_received' => strtotime(date('d-m-Y')),
    			's_date_signed' => 0,
    			's_comment' => ''
    		])->execute();

    		$this->sid = Yii::$app->db->getLastInsertID();


    		$this->response = (new \yii\db\Query())
	    		->select('*')
	    		->from('tender_lot_signed')
	    		->where(['sid'=>$this->sid])
	    		->one();

	    	$this->response['s_date_received'] = date('d-m-Y', $this->response['s_date_received']);
	    	$this->response['s_date_signed'] = 'Еще не подписал';

	    	return true;
    	} else {
    		return false;
    	}
    }

    public function rmUser($sid)
    {
    	$this->response = (new \yii\db\Query())
    	->createCommand()
    	->delete('tender_lot_signed', ['sid'=>$sid])
    	->execute();

        return true;
    }

    public static function isNeed($lotid)
    {
    	$check = (new \yii\db\Query())
	    		->select('s_date_signed')
	    		->from('tender_lot_signed')
	    		->where(['s_lotid'=>$lotid, 's_userid' => Yii::$app->auth->user()['userid']])
	    		->one();

	   	if (!isset($check['s_date_signed']) || $check['s_date_signed'] != 0) {
	   		return false;
	   	}
	   	return true;
    }

    public static function sign($riskId, $lotId)
    {
    	$sign = Yii::$app->request->post('is_sign');
    	$comment = Yii::$app->request->post('comment');
    	$userid = Yii::$app->auth->user()['userid'];

    	$comment = trim($comment);
    	$comment = stripslashes($comment);
    	$comment = htmlspecialchars($comment);

        $tenderid = (new \yii\db\Query())
            ->select('l_tenderid')
            ->from('tender_lots')
            ->where(['tender_lots.lotid' => $lotId])
            ->one();

        Yii::$app->db->createCommand()->update('tenders', [
            'updated_at' => Yii::$app->params['currentTime'],
        ], 'tenderid = '.$tenderid['l_tenderid'])->execute();

    	$q = Yii::$app->db->createCommand()->update('tender_lot_signed', [
                'is_signed' => $sign,
                's_comment' => $comment,
                's_date_signed' => strtotime(date('d-m-Y')),
            ], ['s_lotid'=>$lotId, 's_userid'=>$userid, 's_risk_id' => $riskId])->execute();

    	if ($q) {
    		return true;
    	} else {
    		return false;
    	}
    }
}