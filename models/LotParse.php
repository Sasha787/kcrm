<?php
namespace app\models;


use Yii;
use yii\db\ActiveRecord;
use app\models\Auth;

class LotParse extends ActiveRecord
{
	public $id;
	public $parse_from;
	public $lot_title;
	public $add_info;
	public $total_amount;
	public $lot_status;
	public $lot_link;
	public $cs_bin;
	public $cs_link;
	public $userid;

	public static function tableName()
	{
		return 'tender_parse_lots';
	}

	public static function clean($data)
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = strip_tags($data);
		$data = htmlspecialchars($data);

		return $data;
	}

	public function complete()
	{
		$this->id = Yii::$app->request->post('id');
		$this->userid = Yii::$app->auth->user()['userid'];

		Yii::$app->db->createCommand()->update('tender_parse_lots', [
			'is_completed' => 1,
			'userid' => $this->userid,
		], 'id = '.$this->id)->execute();

		
		return true;
	}

	public static function getAll()
	{
		return (new \yii\db\Query())
		->select('*')
		->from('tender_parse_lots')
		->all();
	}

	public static function addFromMitwork($data)
	{
		foreach ($data as $key => $value) {
			try {
				$check_id = (new \yii\db\Query())
				->select('id_from_site')
				->from('tender_parse_lots')
				->where(['parsed_from' => 'https://eep.mitwork.kz/', 'id_from_site' => $value['id']])
				->one();

				if (!$check_id) {
					Yii::$app->db->createCommand()->insert('tender_parse_lots', [
						'parsed_from' => 'https://eep.mitwork.kz/',
						'lot_title' => self::clean($value['title']),
						'add_info' => self::clean($value['add_info']),
						'total_amount' => self::clean($value['price']),
						'lot_status' => self::clean($value['status']),
						'lot_link' => self::clean($value['link_to_lot']),
						'cs_bin' => self::clean($value['bin']),
						'cs_link' => self::clean($value['link_to_cs']),
						'id_from_site' => self::clean($value['id']),
						'created_at' => strtotime(date('d-m-Y')),
					])->execute();
				}
				unset($check_id);
			} catch (Exception $e) {
				echo 'error: ',  $e->getMessage(), "\n";
			}
		}
	}
}