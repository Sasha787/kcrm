<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;


class RiskFile extends ActiveRecord
{
	public $rf_id;
	public $rf_lotid;
    public $rf_risk_id;
	public $rf_path;
	public $rf_filename;
    public $file;
    public $code;
    public $response;

	public static function tableName()
	{
        return 'tender_lot_risk_files';
    }

    // public function rules()
    // {
    //     return [
    //         [['lr_lotid', 'lr_userid', 'lr_user_depid'], 'required'],
    //     ];
    // }

    public function upload()
    {
        $this->file = $_FILES['file'];
        $this->rf_lotid = Yii::$app->request->post('lotid');
        $this->rf_risk_id = Yii::$app->request->post('riskId');
        $this->code = Yii::$app->request->post('code');

        @mkdir('documents/lot');
        @mkdir('documents/lot/'.$this->code);
        @mkdir('documents/lot/'.$this->code.'/risks');
        @mkdir('documents/lot/'.$this->code.'/risks/'.$this->rf_risk_id);

        $this->rf_path = 'documents/lot/'.$this->code.'/risks/'.$this->rf_risk_id.'/'.Yii::$app->File->pcgbasename($this->file['name']);
        $this->rf_filename = Yii::$app->File->pcgbasename($this->file['name']);

        copy($this->file['tmp_name'], $this->rf_path);

       
        $query = Yii::$app->db->createCommand()->insert('tender_lot_risk_files', [
                'rf_lotid' => $this->rf_lotid,
                'rf_risk_id' => $this->rf_risk_id,
                'rf_path' => $this->rf_path,
                'rf_filename' => $this->rf_filename,
            ])->execute();

        if ($query) {
            $this->rf_id = Yii::$app->db->getLastInsertID();

            $this->response = [
                'id' => $this->rf_id,
                'fileName' => $this->rf_filename,
                'risk_id' => $this->rf_risk_id,
            ];

            return true;
        }
        
        return false;
            
    }

    public static function get($lotid)
    {
        return (new \yii\db\Query())
                    ->select('*')
                    ->from('tender_lot_risk_files')
                    ->where(['rf_lotid' => $lotid])
                    ->all();
    }

    public static function remove($id)
    {
        $file = (new \yii\db\Query())
                    ->select('rf_path')
                    ->from('tender_lot_risk_files')
                    ->where(['rf_id'=>$id])
                    ->one();

        @unlink($file['rf_path']);

        return (new \yii\db\Query())
                ->createCommand()
                ->delete('tender_lot_risk_files', ['rf_id'=>$id])
                ->execute();
    }

    public static function download($id)
    {
        $file = (new \yii\db\Query())
                ->select(['rf_path'])
                ->from('tender_lot_risk_files')
                ->where(['rf_id'=>$id])
                ->one();

        return Yii::$app->File->get($file['rf_path']);
    }

}
