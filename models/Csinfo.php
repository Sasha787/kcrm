<?php

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;


class Csinfo extends ActiveRecord
{
	public static function tableName()
	{
        return 'csinfo';
    }

    public static function getBinAndName()
    {
    	return (new \yii\db\Query())
            ->select(['csid','csname', 'csbin'])
            ->from('csinfo')
            ->where(['not', ['csbin' => '']])
            ->andWhere(['cscactive' => 1])
            ->all();
    }
}