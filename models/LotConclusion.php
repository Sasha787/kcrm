<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

class LotConclusion extends ActiveRecord
{
	public static function tableName()
	{
        return 'tender_conclusions';
    }

    public static function getAll()
    {
    	return (new \yii\db\Query())
            ->select('*')
            ->from('tender_conclusions')
            ->where(['is_active' => 1])
            ->all();	
    }
}