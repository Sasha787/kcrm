<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;


class LotPriceOffer extends ActiveRecord
{
	public $po_id;
    public $tenderid;
	public $po_lotid;
	public $po_price;
	public $po_discount;
	public $po_managerid;
	public $po_total;
	public $response;

	public static function tableName()
	{
        return 'tender_lot_price_offer';
    }

    public function rules()
    {
        return [
            [['po_lotid', 'po_price', 'po_discount', 'po_managerid'], 'required'],
            [['po_price', 'po_discount'], 'double', 'min'=>0],
        ];
    }

    public function change($id)
    {
    	$this->po_lotid = $id;
    	$this->po_price = Yii::$app->request->post('po_price');
    	$this->po_discount = Yii::$app->request->post('po_discount');
    	$this->po_managerid = Yii::$app->auth->user()['userid'];

    	if ($this->validate()) {

    		$this->po_total = $this->po_price*(1-($this->po_discount/100));

            $this->tenderid = (new \yii\db\Query())
                        ->select('l_tenderid')
                        ->from('tender_lots')
                        ->where(['tender_lots.lotid' => $this->po_lotid])
                        ->one();

            Yii::$app->db->createCommand()->update('tenders', [
                'updated_at' => Yii::$app->params['currentTime'],
            ], 'tenderid = '.$this->tenderid['l_tenderid'])->execute();
        
    		Yii::$app->db->createCommand()->update('tender_lot_price_offer', [
                'po_price' => $this->po_price,
                'po_discount' => $this->po_discount,
                'po_managerid' => $this->po_managerid,
                'po_total' => $this->po_total,
            ], 'po_lotid ='.$this->po_lotid)->execute();

        	$this->response = (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_price_offer')
                ->where(['po_lotid' => $this->po_lotid])
                ->one();

            $this->response['po_price_number'] = $this->response['po_price'];
            $this->response['po_price_discount_number'] = $this->response['po_discount'];
            $this->response['po_price'] = number_format($this->response['po_price'], 2, ',', ' ').' тг';
            $this->response['po_total'] = number_format($this->response['po_total'], 2, ',', ' ').' тг';
            $this->response['po_discount'] = $this->response['po_discount'].'%';


        	return true;
         
    	} else {

    		return false;
    	}
    	
    }
}