<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

class LotPercent extends ActiveRecord
{
	public static function tableName()
	{
        return 'tender_percents';
    }

    public static function getAll()
    {
    	return (new \yii\db\Query())
            ->select('*')
            ->from('tender_percents')
            ->where(['is_active' => 1])
            ->all();	
    }
}