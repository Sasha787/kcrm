<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

class DepList extends ActiveRecord
{
	public static function tableName()
	{
        return 'deplist';
    }

    public static function all()
    {
    	return Yii::$app->db2->createCommand('SELECT depid, dname FROM deplist WHERE depid2 = 0')
		      	->queryAll();
    }
}