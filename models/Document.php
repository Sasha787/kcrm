<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;


class Document extends ActiveRecord
{
	public $ld_id;
	public $lotid;
	public $doc_title;
	public $depid;
	public $dname;
	public $is_ok;
	public $response;
    public $file;
    public $file_code;
    public $ld_comment;

	public static function tableName()
	{
        return 'tender_lot_documents';
    }

    public function rules()
	{
		return [
			[['doc_title', 'depid', 'dname'], 'required'],
            [['file'], 'file']
		];
	}

    public static function all()
    {
    	return (new \yii\db\Query())
                ->select(['*'])
                ->from('tender_lot_type_documents')
                ->where(['td_depid'=>3, 'is_active' => 1])
                ->all();
    }

    public static function getDocFiles($lotid)
    {
        return (new \yii\db\Query())
                ->select(['*'])
                ->from('tender_lot_doc_files')
                ->join('join', 'tender_lot_documents', 'tender_lot_documents.ld_id = tender_lot_doc_files.df_ld_id')
                ->where(['tender_lot_documents.ld_lotid'=>$lotid])
                ->all();
    }

    public function create($lotid)
    {
    	$this->doc_title = Yii::$app->request->post('doc_title');
    	$this->depid = Yii::$app->request->post('depid');
    	$this->dname = Yii::$app->request->post('dname');

    	if ($this->validate()) {

            $tenderid = (new \yii\db\Query())
            ->select('l_tenderid')
            ->from('tender_lots')
            ->where(['tender_lots.lotid' => $lotid])
            ->one();

            Yii::$app->db->createCommand()->update('tenders', [
                'updated_at' => Yii::$app->params['currentTime'],
            ], 'tenderid = '.$tenderid['l_tenderid'])->execute();

    		$q = Yii::$app->db->createCommand()->insert('tender_lot_documents', [
                'ld_lotid' => $lotid,
                'ld_doc_title' => $this->doc_title,
                'ld_depid' => $this->depid,
                'ld_dname' => $this->dname,
            ])->execute();

    		$this->ld_id = Yii::$app->db->getLastInsertID();

            if ($q) {

            	$this->response = (new \yii\db\Query())
		            	->select('*')
		            	->from('tender_lot_documents')
		            	->where(['ld_id'=>$this->ld_id])
		            	->one();

            	return true;
            } else {
            	return false;
            }
            
    	} 	
    }

    public static function getById($lotid)
    {
    	return (new \yii\db\Query())
            	->select('*')
            	->from('tender_lot_documents')
            	->where(['ld_lotid'=>$lotid])
            	->all();
    }

    public function getDocuments()
    {
        $this->depid = Yii::$app->request->post('depid');

        $q = (new \yii\db\Query())
                ->select(['td_id', 'td_title'])
                ->from('tender_lot_type_documents')
                ->where(['td_depid'=>$this->depid, 'is_active' => 1])
                ->all();


        $this->response = ArrayHelper::map($q, 'td_id', 'td_title');      
        return $this->response;
    }

    public function createOwnDoc($lotid)
    {
        $this->doc_title = Yii::$app->request->post('doc_title');
        $this->depid = Yii::$app->request->post('depid');

        $q = Yii::$app->db->createCommand()->insert('tender_lot_type_documents', [
                'td_title' => $this->doc_title,
                'td_depid' => $this->depid,
                'is_active' => 1,
            ])->execute();

        if ($q) {
            if ($this->create($lotid)) {
                return true;
            }  
        } 

        return false;
    }

    public function ajaxUploadFile()
    {
        $this->file = $_FILES['file'];
        $this->ld_id = Yii::$app->request->post('ld_id');
        $this->file_code = Yii::$app->request->post('file_code');

        @mkdir('documents/lot');
        @mkdir('documents/lot/'.$this->file_code);
        @mkdir('documents/lot/'.$this->file_code.'/checklist');

        copy($this->file['tmp_name'],'documents/lot/'.$this->file_code.'/checklist/'.Yii::$app->File->pcgbasename($this->file['name']));

        $file_title = Yii::$app->File->pcgbasename($this->file['name']);

        Yii::$app->db->createCommand()->insert('tender_lot_doc_files', [
                'df_ld_id' => $this->ld_id,
                'df_path' => 'documents/lot/'.$this->file_code.'/checklist/'.Yii::$app->File->pcgbasename($this->file['name']),
                'df_file_title' => $file_title,
            ])->execute();

        $df_id = Yii::$app->db->getLastInsertID();

        $this->response = (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_doc_files')
                ->where(['df_id'=>$df_id])
                ->one();

            return true;
    }

    public static function download($id)
    {
        $file = (new \yii\db\Query())
        ->select(['df_path'])
        ->from('tender_lot_doc_files')
        ->where(['df_id'=>$id])
        ->one();

        return Yii::$app->File->get($file['df_path']);
    }

    public static function removeFile($id)
    {
        $file = (new \yii\db\Query())
        ->select('*')
        ->from('tender_lot_doc_files')
        ->where(['df_id'=>$id])
        ->one();

        @unlink($file['df_path']);

        return (new \yii\db\Query())
        ->createCommand()
        ->delete('tender_lot_doc_files', ['df_id'=>$id])
        ->execute();
    }

    public function comment()
    {
        $this->ld_comment = Yii::$app->request->post('new_comment');
        $this->ld_id = Yii::$app->request->post('id');

        Yii::$app->db->createCommand()
            ->update('tender_lot_documents', ['ld_comment' => $this->ld_comment], 
                'ld_id = '. $this->ld_id)
             ->execute();

        $this->response = (new \yii\db\Query())
                     ->select(['ld_comment'])
                     ->from('tender_lot_documents')
                     ->where(['ld_id'=>$this->ld_id])
                     ->one();

        return true;
    }

    public static function removeChecklist($id)
    {
        $q = (new \yii\db\Query())
        ->createCommand()
        ->delete('tender_lot_documents', ['ld_id'=>$id])
        ->execute();

        $q2 = (new \yii\db\Query())
        ->select(['df_path'])
        ->from('tender_lot_doc_files')
        ->where(['df_ld_id'=>$id])
        ->all();

        $q3 = (new \yii\db\Query())
        ->createCommand()
        ->delete('tender_lot_doc_files', ['df_ld_id'=>$id])
        ->execute();

        foreach ($q2 as $key => $value) {
            @unlink($value['df_path']);
        }

        return true;
    }

}