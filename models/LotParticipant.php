<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;


class LotParticipant extends ActiveRecord
{
	public $lp_id;
	public $lp_lotid;
    public $lp_competitor_id;
	public $lp_title;
	public $lp_price;
	public $lp_discount;
	public $lp_total;
	public $response;

	public static function tableName()
	{
        return 'tender_lot_participants';
    }

    public function rules()
    {
        return [
            [['lp_title', 'lp_price', 'lp_discount'], 'required'],
            [['lp_price', 'lp_discount'], 'double', 'min'=>0],
        ];
    }

    public function add()
    {
        $tenderid = (new \yii\db\Query())
                        ->select('l_tenderid')
                        ->from('tender_lots')
                        ->where(['tender_lots.lotid' => $this->lp_lotid])
                        ->one();

        Yii::$app->db->createCommand()->update('tenders', [
            'updated_at' => Yii::$app->params['currentTime'],
        ], 'tenderid = '.$tenderid['l_tenderid'])->execute();

    	if ($this->validate()) {

        	$this->lp_total = $this->lp_price*(1-($this->lp_discount/100));

    		Yii::$app->db->createCommand()->insert('tender_lot_participants', [
                'lp_title' => $this->lp_title,
                'lp_lotid' => $this->lp_lotid,
                'lp_competitor_id' => $this->lp_competitor_id,
                'lp_price' => $this->lp_price,
                'lp_discount' => $this->lp_discount,
                'lp_total' => $this->lp_total,
            ])->execute();

    		$this->lp_id = Yii::$app->db->getLastInsertID();

    		$this->response = (new \yii\db\Query())
                            ->select('*')
                            ->from('tender_lot_participants')
                            ->where(['lp_id' => $this->lp_id])
                            ->one();

            return true;
    	} else {
    		return false;
    	}
    }

    public static function remove($id)
    {   
    	return  (new \yii\db\Query())
                    ->createCommand()
                    ->delete('tender_lot_participants', ['lp_id'=>$id])
                    ->execute();
    }

    public static function winner($id)
    {
    	$is_checked = Yii::$app->request->post('is_checked');
        
    	return Yii::$app->db->createCommand()->update('tender_lot_participants', [
             'lp_is_win' => $is_checked,
         ], 'lp_id = '.$id)->execute();
    }

}
