<?php

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

use app\models\Auth;
use app\models\Tender;
use app\models\Contragent;
use yii\helpers\ArrayHelper;


class LotDep extends ActiveRecord
{	
	public $id;
	public $dep_lotid;
	public $depid;
	public $created_at;
	public $userid;
	public $username;
	public $response;

	public static function tableName()
	{
		return 'tender_lot_dep';
	}

	public static function getLotDepId($lotid)
	{
		return (new \yii\db\Query())
						->select('depid')
						->from('tender_lot_dep')
						->where(['tender_lot_dep.dep_lotid' => $lotid])
						->one();
	}

	public function assignDepartment()
	{
		$this->dep_lotid = Yii::$app->request->post('lotid');
		$this->depid = Yii::$app->request->post('curDep');
		$this->userid = Yii::$app->request->post('userid');
		$this->username = Yii::$app->request->post('fullname');

		$depids = Yii::$app->request->post('deps');

		$managers = User::getManagersByDepId($depids);

		$tender_lots = (new \yii\db\Query())
                ->select(['tender_lots.l_tenderid', 'tender_lots.lot_number', 'tender_lots.l_tenderid', 'tender_lots.lot_name', 'tenders.t_csid', 'tenders.sector'])
                ->from('tender_lots')
                ->join('inner join', 'tenders', 'tender_lots.l_tenderid = tenders.tenderid')
                ->where(['tender_lots.lotid' => $this->dep_lotid])
                ->one();

		Yii::$app->db->transaction(function($db) {

			$db->createCommand()->insert('tender_lot_dep', [
				'dep_lotid' => $this->dep_lotid,
				'depid' => $this->depid,
				'created_at' => strtotime(date('d-m-Y')),
				'userid' => $this->userid,
				'username' => $this->username,
			])->execute();

			$db->createCommand()->update('tender_lots', [
					'is_delegated' => 1,
			], 'lotid = '.$this->dep_lotid)->execute();

		});

		Yii::$app->MailComponent->sendDepMail($this->depid, $tender_lots['l_tenderid'], $tender_lots['lot_number'], $tender_lots['lot_name']); 

		$this->response = ['status'=>200, 'managers'=>$managers];
		return true;
		
	}

	public function reAssignDepartment()
	{
		$this->dep_lotid = Yii::$app->request->post('lotid');
		$this->depid = Yii::$app->request->post('curDep');

		$depids = Yii::$app->request->post('deps');

		$q = (new \yii\db\Query())
            ->createCommand()
            ->delete('tender_lot_dep', ['dep_lotid'=>$this->dep_lotid, 'depid'=>$this->depid])
            ->execute();
            
    if ($q) {

    	$managers = User::getManagersByDepId($depids);
    	$this->response = ['status' => 1, 'managers'=>$managers];

    	return true;
    }
	}
}
