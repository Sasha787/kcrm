<?php 
namespace app\models;


use Yii;
use yii\db\ActiveRecord;
use app\models\Auth;

use yii\web\UploadedFile;

class ProtocolFile extends ActiveRecord
{
    public $file;
    public $protocol_file_id;
    public $protocol_id;
    public $protocol_file_path;
    public $protocol_file_name;
    public $file_code;
    public $response;

    public static function tableName()
    {
        return 'tender_protocol_files';
    }

    public static function getAll($tenderid)
    {
        return (new \yii\db\Query())
                    ->select(['*'])
                    ->from('tender_protocol_files')
                    ->join('inner join', 'tender_protocols', 'tender_protocols.protocol_id = tender_protocol_files.protocol_id')
                    ->where(['tender_protocols.protocol_tenderid'=>$tenderid])
                    ->all();
    }

    public function uploadAjaxFile()
    {
        $this->file = $_FILES['file'];
        $this->protocol_id = Yii::$app->request->post('protocol_id');
        $this->file_code = Yii::$app->request->post('file_code');

        @mkdir('documents/tender');
        @mkdir('documents/tender/'.$this->file_code);
        @mkdir('documents/tender/'.$this->file_code.'/protocols');

        copy($this->file['tmp_name'],'documents/tender/'.$this->file_code.'/protocols/'.Yii::$app->File->pcgbasename($this->file['name']));

        $this->protocol_file_name = Yii::$app->File->pcgbasename($this->file['name']);
        $this->protocol_file_path =  'documents/tender/'.$this->file_code.'/protocols/'.Yii::$app->File->pcgbasename($this->file['name']);

        $query = Yii::$app->db->createCommand()->insert('tender_protocol_files', [
                'protocol_id' => $this->protocol_id,
                'protocol_file_path' => $this->protocol_file_path,
                'protocol_file_name' => $this->protocol_file_name,
            ])->execute();

        $this->protocol_file_id = Yii::$app->db->getLastInsertID();

        if ($query) {

            $this->response = [
                'protocol_file_id' => $this->protocol_file_id, 
                'protocol_id' => $this->protocol_id,
                'protocol_file_path' => $this->protocol_file_path,
                'protocol_file_name' => $this->protocol_file_name,
            ];
            return true;

        }

        return false;
            
    }

    public static function download($id)
    {
        $file = (new \yii\db\Query())
        ->select(['protocol_file_path'])
        ->from('tender_protocol_files')
        ->where(['protocol_file_id'=>$id])
        ->one();

        return Yii::$app->File->get($file['protocol_file_path']);
    }

    public static function removeFile($id)
    {
        $file = (new \yii\db\Query())
        ->select(['protocol_file_path'])
        ->from('tender_protocol_files')
        ->where(['protocol_file_id'=>$id])
        ->one();

        @unlink($file['protocol_file_path']);

        return (new \yii\db\Query())
        ->createCommand()
        ->delete('tender_protocol_files', ['protocol_file_id'=>$id])
        ->execute();
    }

}