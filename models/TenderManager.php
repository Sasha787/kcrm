<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * 
 */
class TenderManager extends ActiveRecord
{

	public $id;
	public $tm_tenderid;
	public $tm_managerid;
	public $tm_userid;
	public $tm_userfullname;
	public $tm_created_at;
	public $response;
	
	public function rules(){
		return [
			[['tm_tenderid', 'tm_managerid', 'tm_userid', 'tm_userfullname'], 'required'],
		];
	}

	public static function tableName()
	{
        return 'tender_managers';
    }

    public function ajaxToSpecialist()
    {

    	$this->tm_tenderid = Yii::$app->request->post('tenderid');
    	$this->tm_managerid = Yii::$app->request->post('manager_id');
    	$this->tm_userid = Yii::$app->request->post('userid');
    	$this->tm_userfullname = Yii::$app->request->post('fullname');

    	if ($this->validate()) {

    		$check_if_exist = (new \yii\db\Query())
		                ->select('*')
		                ->from('tender_managers')
		                ->where(['tm_tenderid' => $this->tm_tenderid])
		                ->one();

		    if ($check_if_exist) {
		    	Yii::$app->db->createCommand()->update('tender_managers', [
		    		'tm_managerid' => $this->tm_managerid,
		    		'tm_userid' => $this->tm_userid,
		    		'tm_userfullname' => $this->tm_userfullname,
		    		'tm_created_at' => Yii::$app->params['currentTime'],
                ], 'tm_tenderid ='.$this->tm_tenderid)->execute();
		    } else {
		    	Yii::$app->db->createCommand()->insert('tender_managers', [
		    		'tm_tenderid' => $this->tm_tenderid,
		    		'tm_managerid' => $this->tm_managerid,
		    		'tm_userid' => $this->tm_userid,
		    		'tm_userfullname' => $this->tm_userfullname,
		    		'tm_created_at' => Yii::$app->params['currentTime'],
		    	])->execute();
		    }
    		return true;
    	} else {
    		return false;
    	}
    }
}