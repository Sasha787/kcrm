<?php 

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class LotChat extends ActiveRecord
{
    public $id;
    public $lotid;
    public $userid;
    public $message;
    public $createdAt;
    public $updatedAt;
    
    public static function tableName()
    {
    	return 'tender_lot_chat';
    }

    public static function clean($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    public static function getLotMessages($lotId)
    {
        $messages = (new \yii\db\Query())
        ->select(['tender_lot_chat.id', 'tender_lot_chat.lotid', 'tender_lot_chat.userid', 'u.fullname', 'tender_lot_chat.message', 'tender_lot_chat.created_at',
        'tender_lot_chat.updated_at'])
        ->from('tender_lot_chat')
        ->join('inner join', 'contracts.users u', 'tender_lot_chat.userid = u.userid')
        ->where(['tender_lot_chat.lotid' => [$lotId]])
        ->orderBy('tender_lot_chat.created_at', 'ASC')
        ->all();

        return $messages;
    }

    public function add()
    {
        $query = Yii::$app->db->createCommand()->insert('tender_lot_chat', [
            'lotid' => $this->lotid,
            'userid' => $this->userid,
            'message' => $this->message,
            'created_at' => Yii::$app->params['currentTime'],
            'updated_at' => Yii::$app->params['currentTime'],

        ])->execute();

        if ($query) {
            return true;
        }

        return false;
    }
}
