<?php 
namespace app\models;


use Yii;
use yii\db\ActiveRecord;
use app\models\Auth;

use yii\web\UploadedFile;

class LotProtocol extends ActiveRecord
{
    public $file;
    public $prt_id;
    public $file_code;
    public $response;

    public static function tableName()
    {
        return 'tender_lot_protocols';
    }

    public static function getAll()
    {
        return (new \yii\db\Query())
                    ->select(['*'])
                    ->from('tender_protocol_dictionary')
                    ->where(['pd_is_active'=>1])
                    ->all();
    }

    public static function getLotProtocols($lotid)
    {
        return (new \yii\db\Query())
                    ->select(['*'])
                    ->from('tender_lot_protocols')
                    ->where(['prt_lotid'=>$lotid])
                    ->all();
    }


    public function uploadAjaxFile()
    {
        $this->file = $_FILES['file'];
        $this->prt_id = Yii::$app->request->post('prt_id');
        $this->file_code = Yii::$app->request->post('file_code');

        @mkdir('documents/lot');
        @mkdir('documents/lot/'.$this->file_code);
        @mkdir('documents/lot/'.$this->file_code.'/protocols');

        copy($this->file['tmp_name'],'documents/lot/'.$this->file_code.'/protocols/'.Yii::$app->File->pcgbasename($this->file['name']));

        $file_title = Yii::$app->File->pcgbasename($this->file['name']);

        Yii::$app->db->createCommand()->insert('tender_lot_protocol_files', [
                'pf_prt_id' => $this->prt_id,
                'pf_path' => 'documents/lot/'.$this->file_code.'/protocols/'.Yii::$app->File->pcgbasename($this->file['name']),
                'pf_filename' => $file_title,
            ])->execute();

        $pf_id = Yii::$app->db->getLastInsertID();

        $this->response = (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_protocol_files')
                ->where(['pf_id'=>$pf_id])
                ->one();

            return true;
    }

    public static function getFiles($lotid)
    {
        return (new \yii\db\Query())
                    ->select(['*'])
                    ->from('tender_lot_protocol_files')
                    ->join('inner join', 'tender_lot_protocols', 'tender_lot_protocols.id = tender_lot_protocol_files.pf_prt_id')
                    ->where(['tender_lot_protocols.prt_lotid'=>$lotid])
                    ->all();
    }

    public function create($lotid)
    {
        $pd_id = Yii::$app->request->post('pd_id');
        $userid = Yii::$app->auth->user()['userid'];
        $protocol_title =  Yii::$app->request->post('protocol_title');

        Yii::$app->db->createCommand()->insert('tender_lot_protocols', [
                  'prt_lotid' => $lotid,
                  'prt_pd_id' => $pd_id,
                  'prt_title' => $protocol_title,
                  'prt_userid' => $userid,
                  'prt_created_at' => Yii::$app->params['currentTime'],
                ])->execute();

        $prt_id = Yii::$app->db->getLastInsertID();

        $this->response = (new \yii\db\Query())
                        ->select('*')
                        ->from('tender_lot_protocols')
                        ->where(['id'=>$prt_id])
                        ->one();
        return true;
    }

    public static function download($id)
    {
        $file = (new \yii\db\Query())
        ->select(['pf_path'])
        ->from('tender_lot_protocol_files')
        ->where(['pf_id'=>$id])
        ->one();

        return Yii::$app->File->get($file['pf_path']);
    }

    public static function removeFile($id)
    {
        $file = (new \yii\db\Query())
        ->select(['pf_path'])
        ->from('tender_lot_protocol_files')
        ->where(['pf_id'=>$id])
        ->one();

        @unlink($file['pf_path']);

        return (new \yii\db\Query())
        ->createCommand()
        ->delete('tender_lot_protocol_files', ['pf_id'=>$id])
        ->execute();
    }

    public static function removeProtocol($id)
    {
        $q = (new \yii\db\Query())
        ->createCommand()
        ->delete('tender_lot_protocols', ['id'=>$id])
        ->execute();

        $q2 = (new \yii\db\Query())
        ->select(['pf_path'])
        ->from('tender_lot_protocol_files')
        ->where(['pf_prt_id'=>$id])
        ->all();

        $q3 = (new \yii\db\Query())
        ->createCommand()
        ->delete('tender_lot_protocol_files', ['pf_prt_id'=>$id])
        ->execute();

        foreach ($q2 as $key => $value) {
            @unlink($value['pf_path']);
        }

        return true;
    }
}