<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

class Contragent extends ActiveRecord
{
	public $depid;
	public $cdate;
	public $cuserid;
	public $upd;
	public $csid;
	public $csinfoid;
	public $ciid;

	public static function tableName()
    {
        return 'cslist';
    }

	public function rules()
    {
        return [
            [['depid', 'cdate', 'cuserid', 'upd'], 'required'],
        ];
    }

	public function create($csname, $csbin, $managerid, $manager_depid, $origid, $ognid)
	{
				$this->depid = $manager_depid;
				$this->cdate = Yii::$app->params['currentTime'];
				$this->cuserid = $managerid;
				$this->upd = Yii::$app->params['currentTime'];

				if ($this->validate()) {
					

					$transaction = Yii::$app->db->transaction(function ($db) use ($csname, $csbin, $origid, $ognid){

						$db->createCommand()->insert('cslist', [
							'depid' => $this->depid,
							'cdate' => $this->cdate,
							'cuserid' => $this->cuserid,
							'iscc' => 0,
							'upd' => $this->upd,
							'isactive' => 1,
						])->execute();

						$this->csid = Yii::$app->db->getLastInsertID();

						Yii::$app->db->createCommand()->insert('acclist', [
							'userid' => $this->cuserid,
							'csid' => $this->csid,
							'fullacc' => 1,
						])->execute();

						Yii::$app->db->createCommand()->insert('csstatus', [
							'csid' => $this->csid,
							'csstatid' => 0,
							'cssdate' => $this->cdate,
							'csman' => 0,
							'cssactive' => 1,
						])->execute();

						Yii::$app->db->createCommand()->insert('csinfo', [
							'csid' => $this->csid,
							'userid' => $this->cuserid,
							'csdate' => $this->cdate,
							'csname' => $csname,
							'csognid' => $ognid,
							'csbin' => $csbin,
							'csorigid' => $origid,
							'cscactive' => 1,
						])->execute();

						$this->csinfoid = Yii::$app->db->getLastInsertID();

						Yii::$app->db->createCommand()->insert('cscont', [
							'csinfoid' => $this->csinfoid,
							'csid' => $this->csid,
							'userid' => $this->cuserid,
							'cscdate' => $this->cdate,
							'cscname' => '',
							'cscname_f' => '',
							'cscname_t' => '',
							'cscname2' => '',
							'docsign_kz' => '',
							'docsign_ru' => '',
							'docsign_en' => '',
							'cscpos' => '',
							'cscpos2' => '',
							'docpos_kz' => '',
							'docpos_ru' => '',
							'docpos_en' => '',
							'cscrem' => '',
							'cscrem2' => '',
							'imgsrc' => '',
							'cscactive' => 1,
						])->execute();

						$this->ciid = Yii::$app->db->getLastInsertID();

						Yii::$app->db->createCommand()->insert('cscontchild', [
							'ciid' => $this->csinfoid,
							'ciidch' => 0,
							'cscontid' => 0,
							'cscont' => '',
							'cscrem' => '',
							'cscrem2' => '',
							'cscactive' => 1,
						])->execute();

					});

		      return true;
				} else {
					return false;
				}
	}

	public function createCsList()
	{
		Yii::$app->db->createCommand()->insert('cslist', [
			'depid' => $this->depid,
			'cdate' => $this->cdate,
			'cuserid' => $this->cuserid,
			'iscc' => 0,
			'upd' => $this->upd,
			'isactive' => 1,
		])->execute();

		$this->csid = Yii::$app->db->getLastInsertID();
	}

	public function createAccList()
	{
		Yii::$app->db->createCommand()->insert('acclist', [
			'userid' => $this->cuserid,
			'csid' => $this->csid,
			'fullacc' => 1,
		])->execute();
	}

	public function createCsStatus()
	{
		Yii::$app->db->createCommand()->insert('csstatus', [
			'csid' => $this->csid,
			'csstatid' => 0,
			'cssdate' => $this->cdate,
			'csman' => 0,
			'cssactive' => 1,
		])->execute();
	}

	public function createCsInfo($csname, $csbin)
	{
		Yii::$app->db->createCommand()->insert('csinfo', [
			'csid' => $this->csid,
			'userid' => $this->cuserid,
			'csdate' => $this->cdate,
			'csname' => $csname,
			'csbin' => $csbin,
			'csorigid' => 1,
			'cscactive' => 1,
		])->execute();

		$this->csinfoid = Yii::$app->db->getLastInsertID();
	}

	public function createCsCont()
	{
		Yii::$app->db->createCommand()->insert('cscont', [
			'csinfoid' => $this->csinfoid,
			'csid' => $this->csid,
			'userid' => $this->cuserid,
			'cscdate' => $this->cdate,
			'cscname' => '',
			'cscname_f' => '',
			'cscname_t' => '',
			'cscname2' => '',
			'docsign_kz' => '',
			'docsign_ru' => '',
			'docsign_en' => '',
			'cscpos' => '',
			'cscpos2' => '',
			'docpos_kz' => '',
			'docpos_ru' => '',
			'docpos_en' => '',
			'cscrem' => '',
			'cscrem2' => '',
			'imgsrc' => '',
			'cscactive' => 1,
		])->execute();

		$this->ciid = Yii::$app->db->getLastInsertID();
	}

	public function createCsContChild()
	{
		Yii::$app->db->createCommand()->insert('cscontchild', [
			'ciid' => $this->csinfoid,
			'ciidch' => 0,
			'cscontid' => 0,
			'cscont' => '',
			'cscrem' => '',
			'cscrem2' => '',
			'cscactive' => 1,
		])->execute();
	}
}