<?php

namespace app\models;

use Yii;

use  yii\db\ActiveRecord;

class User extends ActiveRecord
{
   public static function tableName()
    {
        return 'users';
    }

    public static function all()
    {

    	 return (new \yii\db\Query())
										->select('u.userid,u.login,u.fullname,u.position,u.depid,u.email')
										->from('contracts.users u')
										->where(['blocked'=>0])
										->andWhere(['!=', 'userid', 0])
										->orderBy(['u.fullname' => SORT_ASC])
										->all();
       // return Yii::$app->db2->createCommand("SELECT users.userid,users.login,users.fullname,users.position,users.depid,users.email FROM contracts.users WHERE blocked=0 AND userid != 0 ORDER BY users.fullname")->queryAll();
    }

    public static function getByDepid()
    {
	    	$managerids = (new \yii\db\Query())
					->select('userid')
					->from('acclist')
					->where(['fullacc'=>1, 'isblocked'=>0])
					->distinct();

				$user_role = Yii::$app->auth->tenderPermission()['depid'];

				if ($user_role[0] == 0) {
					return (new \yii\db\Query())
										->select('u.userid,u.login,u.fullname,u.position,u.depid,u.email')
										->from('contracts.users u')
										->where(['blocked'=>0])
										->andWhere([
											'in', 'userid', $managerids
										])
										->orderBy(['u.fullname' => SORT_ASC])
										->all();
				} else {
					return (new \yii\db\Query())
										->select('*')
										->from('contracts.users u')
										->where(['blocked'=>0])
										->andWhere([
											'in', 'userid', $managerids
										])
										->andWhere([ 'in', 'depid', $user_role
										])
										->orderBy(['u.fullname' => SORT_ASC])
										->all();
				}
    }

    public static function getOnlyManagers($depid = 0)
    {
    		$managerids = (new \yii\db\Query())
								->select('userid')
								->from('acclist')
								->where(['fullacc'=>1, 'isblocked'=>0])
								->distinct();

				if ($depid == 0) {
					return (new \yii\db\Query())
										->select('u.userid,u.login,u.fullname,u.position,u.depid,u.email')
										->from('contracts.users u')
										->where(['blocked'=>0])
										->andWhere([
											'in', 'userid', $managerids
										])
										->orderBy(['u.fullname' => SORT_ASC])
										->all();
				} else {
					return (new \yii\db\Query())
										->select('u.userid,u.login,u.fullname,u.position,u.depid,u.email')
										->from('contracts.users u')
										->where(['blocked'=>0])
										->andWhere([
											'in', 'userid', $managerids
										])
										->andWhere(['u.depid' => $depid])
										->orderBy(['u.fullname' => SORT_ASC])
										->all();
				}
				
    }

    public static function getManagersByDepId($deps = array())
    {
    	$managerids = (new \yii\db\Query())
								->select('userid')
								->from('acclist')
								->where(['fullacc'=>1, 'isblocked'=>0])
								->distinct();

			if (!empty($deps)) {

				return (new \yii\db\Query())
								->select('u.userid,u.login,u.fullname,u.position,u.depid,u.email')
								->from('contracts.users u')
								->where(['blocked'=>0])
								->andWhere([
									'in', 'userid', $managerids
								])
								->andWhere(['in', 'u.depid', $deps])
								->orderBy(['u.fullname' => SORT_ASC])
								->all();

			} else {

					return (new \yii\db\Query())
										->select('u.userid,u.login,u.fullname,u.position,u.depid,u.email')
										->from('contracts.users u')
										->where(['blocked'=>0])
										->andWhere([
											'in', 'userid', $managerids
										])
										->orderBy(['u.fullname' => SORT_ASC])
										->all();

			}
    }

    public static function getDrtpSpecialists()
    {
    		return (new \yii\db\Query())
										->select('u.userid,u.login,u.fullname,u.position,u.depid,u.email')
										->from('contracts.users u')
										->join('left join', 'kcrm.privusers p', 'u.userid = p.userid')
										->where(['u.blocked'=>0])
										->andWhere(['or',
											['tender' => 1],
											['tender' => 2]
										])
										->orderBy(['u.fullname' => SORT_ASC])
										->groupBy(['u.fullname'])
										->all();
    }

}
