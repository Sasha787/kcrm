<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

class TenderOrig extends ActiveRecord
{
	public static function tableName()
    {
        return 'csoriglist';
    }

    public static function getAll()
    {
    	return (new \yii\db\Query())
		    	->select(['origname', 'origid'])
		    	->from('csoriglist')
                ->where(['oactive'=>1])
		    	->all();
    }
}