<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

use app\models\Auth;
use app\models\Tender;
use app\models\Contragent;
use yii\helpers\ArrayHelper;


class AgreementProvision extends ActiveRecord
{
    public static $response;

	public static function tableName()
	{
        return 'tender_lot_agreement_provisions';
    }

    public static function getById($lotid)
    {
    	return (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_agreement_provisions')
                ->where(['tender_lot_agreement_provisions.lap_lotid' => $lotid])
                ->one();
    }

    public static function isNeed()
    {
    	$lap_lotid = Yii::$app->request->post('lotid');
    	$app_is_need = Yii::$app->request->post('checked');

    	$check = (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_agreement_provisions')
                ->where(['tender_lot_agreement_provisions.lap_lotid' => $lap_lotid])
                ->one();

        $tenderid = (new \yii\db\Query())
                ->select('l_tenderid')
                ->from('tender_lots')
                ->where(['tender_lots.lotid' => $lap_lotid])
                ->one();

        Yii::$app->db->createCommand()->update('tenders', [
                'updated_at' => strtotime(date('d-m-Y H:m:s')." +6 hours"),
            ], 'tenderid = '.$tenderid['l_tenderid'])->execute();

        if ($check) {
        	$q = Yii::$app->db->createCommand()->update('tender_lot_agreement_provisions', [
                'lap_is_need' => $app_is_need,
                'lap_userid' => Yii::$app->auth->user()['userid'],
            ], 'tender_lot_agreement_provisions.lap_lotid = '.$lap_lotid)->execute();

            if ($q) {
            	return true;
            }
        } else {
        	$q = Yii::$app->db->createCommand()->insert('tender_lot_agreement_provisions', [
              'lap_lotid' => $lap_lotid,
              'lap_is_need' => $app_is_need,
              'lap_userid' => Yii::$app->auth->user()['userid'],
            ])->execute();

            if ($q) {
            	return true;
            }
        }

        return false;
    }

    public static function isExist()
    {
    	$lap_lotid = Yii::$app->request->post('lotid');
    	$app_is_need = Yii::$app->request->post('checked');

    	$check = (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_agreement_provisions')
                ->where(['tender_lot_agreement_provisions.lap_lotid' => $lap_lotid])
                ->one();

        $tenderid = (new \yii\db\Query())
                ->select('l_tenderid')
                ->from('tender_lots')
                ->where(['tender_lots.lotid' => $lap_lotid])
                ->one();

        Yii::$app->db->createCommand()->update('tenders', [
                'updated_at' => strtotime(date('d-m-Y H:m:s')." +6 hours"),
            ], 'tenderid = '.$tenderid['l_tenderid'])->execute();

        if ($check) {
        	$q = Yii::$app->db->createCommand()->update('tender_lot_agreement_provisions', [
                'lap_is_exist' => $app_is_need,
                'lap_managerid' => Yii::$app->auth->user()['userid'],
            ], 'tender_lot_agreement_provisions.lap_lotid = '.$lap_lotid)->execute();

            if ($q) {
            	return true;
            }
        } else {
        	$q = Yii::$app->db->createCommand()->insert('tender_lot_agreement_provisions', [
              'lap_lotid' => $lap_lotid,
              'lap_is_exist' => $app_is_need,
              'lap_managerid' => Yii::$app->auth->user()['userid'],
            ])->execute();

            if ($q) {
            	return true;
            }
        }

        return false;
    }

    public static function isReturn()
    {
    	$lap_lotid = Yii::$app->request->post('lotid');
    	$app_is_need = Yii::$app->request->post('checked');

    	$check = (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_agreement_provisions')
                ->where(['tender_lot_agreement_provisions.lap_lotid' => $lap_lotid])
                ->one();

        $tenderid = (new \yii\db\Query())
                ->select('l_tenderid')
                ->from('tender_lots')
                ->where(['tender_lots.lotid' => $lap_lotid])
                ->one();

        Yii::$app->db->createCommand()->update('tenders', [
                'updated_at' => strtotime(date('d-m-Y H:m:s')." +6 hours"),
            ], 'tenderid = '.$tenderid['l_tenderid'])->execute();

        if ($check) {
        	$q = Yii::$app->db->createCommand()->update('tender_lot_agreement_provisions', [
                'lap_is_return' => $app_is_need,
            ], 'tender_lot_agreement_provisions.lap_lotid = '.$lap_lotid)->execute();

            if ($q) {
            	return true;
            }
        } else {
        	$q = Yii::$app->db->createCommand()->insert('tender_lot_agreement_provisions', [
              'lap_lotid' => $lap_lotid,
              'lap_is_return' => $app_is_need,
            ])->execute();

            if ($q) {
            	return true;
            }
        }

        return false;
    }

    public static function changeAmount()
    {
        $lap_amount = Yii::$app->request->post('lap_amount_input');
        $lap_lotid = Yii::$app->request->post('lotid');

        $check = (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_agreement_provisions')
                ->where(['tender_lot_agreement_provisions.lap_lotid' => $lap_lotid])
                ->one();

        $tenderid = (new \yii\db\Query())
                ->select('l_tenderid')
                ->from('tender_lots')
                ->where(['tender_lots.lotid' => $lap_lotid])
                ->one();

        Yii::$app->db->createCommand()->update('tenders', [
                'updated_at' => strtotime(date('d-m-Y H:m:s')." +6 hours"),
            ], 'tenderid = '.$tenderid['l_tenderid'])->execute();
        
        if ($check) {
            Yii::$app->db->createCommand()->update('tender_lot_agreement_provisions', [
                'lap_amount' => $lap_amount,
            ], 'tender_lot_agreement_provisions.lap_lotid = '.$lap_lotid)->execute();

           
            self::$response = $lap_amount;
            return true;
        
        } else {
            Yii::$app->db->createCommand()->insert('tender_lot_agreement_provisions', [
              'lap_lotid' => $lap_lotid,
              'lap_amount' => $lap_amount,
            ])->execute();

            self::$response = $lap_amount;
            return true;
   
        }

    }
}