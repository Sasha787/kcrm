<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

class TenderSector extends ActiveRecord
{
	public static function tableName()
    {
        return 'tender_sectors';
    }

    public static function getAll()
    {
    	return (new \yii\db\Query())
		    	->select('*')
		    	->from('tender_sectors')
                ->where(['is_active'=>1])
		    	->all();
    }
}