<?php 
 
namespace app\models;

use Yii;


class Auth extends User
{


    public function user()
    {

        $token = $_COOKIE['userid'];

        $userid = (new \yii\db\Query())
            ->select('userid')
            ->from('usercfg')
            ->where(['sid' => $token])
            ->one();

        return Yii::$app->UserComponent->getById($userid['userid']);
    }

    public function userConfig()
    {
        $token = $_COOKIE['userid'];

        return (new \yii\db\Query())
            ->select('*')
            ->from('usercfg')
            ->where(['sid' => $token])
            ->one();
    }

    public static function tenderPermission()
    {
        $token = $_COOKIE['userid'];

        $userid = (new \yii\db\Query())
            ->select('userid')
            ->from('usercfg')
            ->where(['sid' => $token])
            ->one();

       return Yii::$app->UserComponent->getTenderPermissionById($userid['userid']);
    }

    public static function isGuest($token)
    {
        if (isset($_COOKIE['userid'])) {

            $userid = (new \yii\db\Query())
            ->select('userid')
            ->from('usercfg')
            ->where(['sid' => $token])
            ->one();

            if ($userid) {
                return false;
            } else {
                setcookie("userid", "", time()-3600);
                return true;
            }
            
        } else {
            return true;
        }   
    }

    public static function login($userid)
    {
        $sid=round(mt_rand(1,9));

        for($i=0;$i<13;$i++) $sid.=round(mt_rand(0,9));

        $sidexp=time()+43200+3600;

        setcookie("userid","$sid",$sidexp, "/");

        $query = Yii::$app->db->createCommand()->update('usercfg', [
                   'sid' => $sid ,
                   'sidexp' => $sidexp,
               ], 'userid = '.$userid)->execute();

        if ($query) {
            return true;
        }

        return false;
    }
}
