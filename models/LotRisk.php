<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;
date_default_timezone_set('Asia/Almaty');

class LotRisk extends ActiveRecord
{
	public $lr_id;
	public $lr_lotid;
	public $lr_title;
	public $lr_userid;
	public $lr_user_depid;
	public $lr_fullname;
	public $lr_dname;
    public $lr_isActive;
    public $lr_updated_at;
    public $response;

	public static function tableName()
	{
        return 'tender_lot_risks';
    }

    public function rules()
    {
        return [
            [['lr_id', 'lr_userid', 'lr_user_depid'], 'required'],
        ];
    }

    public static function create($lotid)
    {   
        $currentTime = Yii::$app->params['currentTime'];

        $query = Yii::$app->db->createCommand()->insert('tender_lot_risks', [
                   'lr_lotid' => $lotid,
                   'lr_updated_at' => $currentTime,
               ])->execute();

        if ($query) {
            return true;
        }

        return false;
    }

    public function add($riskId)
    {
       $this->lr_lotid = Yii::$app->request->post('lotId');
       $this->lr_id = $riskId;
       $this->lr_title = Yii::$app->request->post('risk');
       $this->lr_userid = Yii::$app->auth->user()['userid'];
       $this->lr_user_depid = Yii::$app->auth->user()['depid'];
       $this->lr_fullname = Yii::$app->auth->user()['fullname'];
       $this->lr_dname = Yii::$app->auth->user()['dname'];
       $this->lr_updated_at = Yii::$app->params['currentTime'];

    	if ($this->validate()) {

            $tenderid = (new \yii\db\Query())
            ->select('l_tenderid')
            ->from('tender_lots')
            ->where(['tender_lots.lotid' => $this->lr_lotid])
            ->one();

            Yii::$app->db->createCommand()->update('tenders', [
                'updated_at' => Yii::$app->params['currentTime'],
            ], 'tenderid = '.$tenderid['l_tenderid'])->execute();


            Yii::$app->db->createCommand()->update('tender_lot_risks', [
             'lr_title' => trim($this->lr_title),
             'lr_userid' => $this->lr_userid,
             'lr_user_depid' => $this->lr_user_depid,
             'lr_fullname' => $this->lr_fullname,
             'lr_dname' => $this->lr_dname,
             'lr_updated_at' => $this->lr_updated_at,
         ], 'lr_id = '.$this->lr_id)->execute();

            $this->response = (new \yii\db\Query())
                            ->select('*')
                            ->from('tender_lot_risks')
                            ->where(['lr_id' => $this->lr_id])
                            ->one();

            $this->response['lr_updated_at'] = date('d-m-Y H:i', $this->response['lr_updated_at']);
            return true;
    	} else {
    		return false;
    	}
    }

    public static function get($id)
    {
    	return (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_risks')
                ->where(['lr_lotid' => $id])
                ->orderBy(['lr_updated_at' => SORT_DESC])
                ->all();

        // if (!$risk) {
        //     $risk['lr_title'] = NULL;
        //     $risk['lr_fullname'] = NULL;
        //     $risk['lr_updated_at'] = 0;
        //     $risk['lr_isActive'] = 0;
        // }
        // $risk['lr_updated_at'] = date('d-m-Y', $risk['lr_updated_at']);

        return $risk;
    }

    public function isActive()
    {
        $this->lr_isActive = Yii::$app->request->post('isActive');
        $this->lr_lotid = Yii::$app->request->post('lotid');
        $this->lr_id = Yii::$app->request->post('riskId');

        if ($this->lr_isActive == 1) {
            
            $check = (new \yii\db\Query())
                    ->select('sid')
                    ->from('tender_lot_signed')
                    ->where(['s_lotid' => $this->lr_lotid, 's_risk_id'=>$this->lr_id])
                    ->all();

            if ($check) {
                
                $query = Yii::$app->db->createCommand()->update('tender_lot_risks', [
                    'lr_isActive' => $this->lr_isActive,
                ], ['lr_id' => $this->lr_id])->execute();

                $query2 =Yii::$app->db->createCommand()->update('tender_lot_signed', [
                    's_date_received' => Yii::$app->params['currentTime'],
                ], ['s_lotid' => $this->lr_lotid, 's_risk_id' => $this->lr_id])->execute();

                if ($query && $query2) {
                    $this->response = ['status' => 200, 'res' => 'Риск отправлен на согласование!'];
                    return true;
                } 
            }
            $this->response = ['status' => 'warning', 'res' => 'Добавьте подписантов'];
            return true;
        } else {

            $query = Yii::$app->db->createCommand()->update('tender_lot_risks', [
                    'lr_isActive' => $this->lr_isActive,
            ], ['lr_lotid' => $this->lr_lotid])->execute();

            $query2 =Yii::$app->db->createCommand()->update('tender_lot_signed', [
                'is_signed' => 0,
                's_date_received' => 0,
                's_date_signed' => 0, 
                's_comment' => '',
            ], ['s_lotid' => $this->lr_lotid, 's_risk_id' => $this->lr_id])->execute();

            if ($query) {
                $this->response = ['status' => 200, 'res' => 'Согласование отозвано!'];
                return true;
            } 
            return false;
        }
        return false;

    }

}
