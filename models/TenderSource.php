<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

class TenderSource extends ActiveRecord
{
	public static function tableName()
	{
        return 'tenders_sources';
    }

    public static function getAll()
    {
    	return (new \yii\db\Query())
            ->select('*')
            ->from('tender_sources')
            ->where(['is_active' => 1])
            ->all();
    }
}