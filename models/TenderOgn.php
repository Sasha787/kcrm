<?php 

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

class TenderOgn extends ActiveRecord
{
	public static function tableName()
    {
        return 'csognlist';
    }

    public static function getAll()
    {
    	return (new \yii\db\Query())
		    	->select(['ognname', 'ognid'])
		    	->from('csognlist')
                ->where(['oactive'=>1])
                ->andWhere(['not', ['ognid' => 0]])
		    	->all();
    }
}