<?php

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

/**
 * 
 */
class LotIndication extends ActiveRecord
{
	
	public static function tableName()
	{
        return 'tender_lot_indications';
    }

    public static function getAll()
    {
    	return (new \yii\db\Query())
            ->select(['indicationid','indication_title'])
            ->from('tender_lot_indications')
            ->where(['is_active' => 1])
            ->all();
    }
}