<?php 

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use Yii;
use yii\helpers\ArrayHelper;

class LotFile extends ActiveRecord
{
    public $fileid;
    public $lotid;
    public $path;
    public $userid;
    public $tmp_code;
    public $file;
    public $response;

    public function rules(){
        return [
            [['file'], 'file'],
            [['tmp_code'],'integer']
        ];
    }

    public static function tableName()
    {
        return 'lot_files';
    }

    public function ajaxUploadFile()
    {
        $this->file = $_FILES['file'];
        $this->userid = Yii::$app->auth->user()['userid'];
        $this->tmp_code = Yii::$app->request->post('tmp_code');

        if ($this->validate()) {
            @mkdir('documents/lot');
            @mkdir('documents/lot/'.$this->tmp_code);

            copy($this->file['tmp_name'],'documents/lot/'.$this->tmp_code.'/'.Yii::$app->File->pcgbasename($this->file['name']));

            Yii::$app->db->createCommand()->insert('tender_lot_files', [
                'f_path' => 'documents/lot/'.$this->tmp_code.'/'.Yii::$app->File->pcgbasename($this->file['name']),
                'f_userid' => $this->userid,
                'tmp_code' => $this->tmp_code,
            ])->execute();

            $this->fileid = Yii::$app->db->getLastInsertID();

            $this->response = (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_files')
                ->where(['fileid'=>$this->fileid, 'tmp_code'=>$this->tmp_code])
                ->one();

            $this->response['fileName'] = Yii::$app->File->pcgbasename($this->response['f_path']); 

            return true;
        }
    }


    public function ajaxUpdateFile()
    {
        $this->file = $_FILES['file'];
        $this->userid = Yii::$app->auth->user()['userid'];
        $this->lotid = Yii::$app->request->post('lotid');

        if ($this->validate()) {

            $tenderid = (new \yii\db\Query())
            ->select('l_tenderid')
            ->from('tender_lots')
            ->where(['tender_lots.lotid' => $this->lotid])
            ->one();

            Yii::$app->db->createCommand()->update('tenders', [
                'updated_at' => Yii::$app->params['currentTime'],
            ], 'tenderid = '.$tenderid['l_tenderid'])->execute();
            
            $code = (new \yii\db\Query())
                ->select('file_code')
                ->from('tender_lots')
                ->where(['lotid'=>$this->lotid])
                ->one();

            @mkdir('documents/lot');
            @mkdir('documents/lot/'.$code['file_code']);

            copy($this->file['tmp_name'],'documents/lot/'.$code['file_code'].'/'.Yii::$app->File->pcgbasename($this->file['name']));

            Yii::$app->db->createCommand()->insert('tender_lot_files', [
                'f_path' => 'documents/lot/'.$code['file_code'].'/'.Yii::$app->File->pcgbasename($this->file['name']),
                'f_lotid' => $this->lotid,
                'f_userid' => $this->userid,
            ])->execute();

            $this->fileid = Yii::$app->db->getLastInsertID();

            $this->response = (new \yii\db\Query())
                ->select('*')
                ->from('tender_lot_files')
                ->where(['fileid'=>$this->fileid])
                ->one();

            $this->response['fileName'] = Yii::$app->File->pcgbasename($this->response['f_path']);

            return true;
        }
    }

    public function uploadFile($lotid)
    {
        $this->userid = Yii::$app->auth->user()['userid'];
        $this->tmp_code = Yii::$app->request->post('tmp_code');

        $paths = (new \yii\db\Query())
        ->select('f_path')
        ->from('tender_lot_files')
        ->where(['f_lotid'=> null, 'f_userid'=>$this->userid])
        ->andWhere(['!=', 'tmp_code', $this->tmp_code])
        ->all();

        foreach ($paths as $path) {
            Yii::$app->Directory->delete(Yii::$app->Directory->get($path['f_path'],1));
        }

        Yii::$app->db->createCommand("
            DELETE FROM tender_lot_files 
            WHERE f_lotid IS NULL AND f_userid = ".$this->userid." AND tmp_code != ".$this->tmp_code."
            ")->execute();

        Yii::$app->db->createCommand()
            ->update('tender_lot_files', ['tmp_code' => '', 'f_lotid' => $lotid], 
                'tmp_code = '.$this->tmp_code)
             ->execute();

        Yii::$app->db->createCommand()
             ->update('tender_lots', ['file_code' => $this->tmp_code], 
                'lotid = '.$lotid)
             ->execute();
    }

    public function ajaxRemoveFile($id)
    {
        $this->file = (new \yii\db\Query())
        ->select('*')
        ->from('tender_lot_files')
        ->where(['fileid'=>$id])
        ->one();

        @unlink($this->file['f_path']);

        return (new \yii\db\Query())
        ->createCommand()
        ->delete('tender_lot_files', ['fileid'=>$id])
        ->execute();
    }

    public static function download($fileid)
    {
        $file = (new \yii\db\Query())
        ->select(['f_path'])
        ->from('tender_lot_files')
        ->where(['fileid'=>$fileid])
        ->one();

        return Yii::$app->File->get($file['f_path']);
    }
}