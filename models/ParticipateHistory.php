<?php 

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

use app\models\Auth;
use app\models\Tender;
use app\models\Contragent;
use yii\helpers\ArrayHelper;
use yii\base\Model;


class ParticipateHistory extends ActiveRecord
{
	public $id;
	public $lotid;
	public $isParticipate;
	public $managerId;
	public $date;
	public $reason;
	public $response;

	public static function tableName()
	{
		return 'tender_participate_history';
	}

	public static function clean($data)
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	public static function isNeedMakeSolution($lotid)
	{
		return (new \yii\db\Query())
								->select('id')
								->from('tender_participate_history')
								->where(['tender_participate_history.lotid' => $lotid, 'tender_participate_history.isActive' => 1])
								->one();
	}

	public static function isNeedApproveSolution($lotid)
	{
		$userid = Yii::$app->auth->user()['userid'];

		return (new \yii\db\Query())
								->select('id')
								->from('tender_participate_history')
								->where(['tender_participate_history.lotid' => $lotid, 'tender_participate_history.isParticipate' => 0, 'tender_participate_history.isActive' => 1])
								->andWhere(['!=', 'participateManagerid', $userid])
								->one();

	}

	public function isParticipate()
	{
		$this->isParticipate = Yii::$app->request->post('decision');
		$this->lotid = Yii::$app->request->post('lotid');
		$this->reason = static::clean(Yii::$app->request->post('reason'));
		$this->date = Yii::$app->params['currentTime'];
		$this->managerId = Yii::$app->auth->user()['userid'];


		Yii::$app->db->createCommand()->update('tender_participate_history', [
          'isActive' => 0,
    ], 'lotid = '.$this->lotid)->execute();

		$query = Yii::$app->db->createCommand()->insert('tender_participate_history', [
                'isParticipate' => $this->isParticipate,
                'lotid' => $this->lotid,
                'participateReason' => $this->reason,
                'participateManagerid' => $this->managerId,
                'participateDate' => $this->date,
                'isActive' => 1,
    ])->execute();

		if ($query) {
			return true;
		}

		return false;
	}

	public static function getAllSolutions($lotid)
	{
		return (new \yii\db\Query())
								->select('*')
								->from('tender_participate_history')
								->where(['tender_participate_history.lotid' => $lotid])
								->orderBy(['tender_participate_history.participateDate' => SORT_DESC, 'tender_participate_history.isActive' => SORT_DESC])
								->all();
	}
}