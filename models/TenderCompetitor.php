<?php 

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use Yii;
use yii\helpers\ArrayHelper;

class TenderCompetitor extends ActiveRecord
{
	public $competitor_id;
    public $competitor_title;
    public $competitor_date;

	// public function rules(){
	// 	return [
	// 		[['file'], 'file'],
	// 		[['tmp_code'],'integer']
	// 	];
	// }

	public static function tableName()
	{
        return 'tender_competitors';
    }

    public static function getAll()
    {
        return (new \yii\db\Query())
                ->select(['competitor_id', 'competitor_title'])
                ->from('tender_competitors')
                ->all();
    }


    public function add()
    {
        $query = Yii::$app->db->createCommand()->insert('tender_competitors', [
            'competitor_title' => $this->competitor_title,
            'competitor_date' => Yii::$app->params['currentTime'],
        ])->execute();

        if ($query) {
            $this->competitor_id = Yii::$app->db->getLastInsertID();
            return true;
        }

        return false;
    }


    public static function reportAll()
    {
        $competitor = base64_decode(Yii::$app->request->get('competitor'));
        $contragent = base64_decode(Yii::$app->request->get('contragent'));
        

        $winner = (new \yii\db\Query())
                    ->select(['tender_lot_participants.lp_title as winner_title', 
                        'tender_lot_participants.lp_lotid'])
                    ->from('tender_lot_participants')
                    ->where(['tender_lot_participants.lp_is_win' => 1])
                    ->groupBy('lp_lotid');

        $query = (new \yii\db\Query())
                    ->select(['tender_lots.lotid', 'tenders.t_csbin', 'tenders.purchase_name', 'tenders.purchase_number', 'tenders.t_csname', 
                        'tender_lots.lot_number', 'tender_lots.budget', 'tender_lot_participants.lp_title', 'tender_lot_participants.lp_price',
                        'tender_lot_participants.lp_discount', 'tender_lot_participants.lp_total', 
                        'tender_lot_participants.lp_is_win', 'tender_lots.lot_conclusion', 'winner.winner_title'])
                    ->from('tender_lot_participants')
                    ->join('left join', ['winner' => $winner], 'tender_lot_participants.lp_lotid = winner.lp_lotid')
                    ->join('left join', 'tender_lots', 'tender_lot_participants.lp_lotid = tender_lots.lotid')
                    ->join('left join', 'tenders', 'tender_lots.l_tenderid = tenders.tenderid');
                    

        if ($competitor != 0) {
            $query->andWhere(['tender_lot_participants.lp_competitor_id' => $competitor]);
        }

        if ($contragent != 0) {
            $query->andWhere(['tenders.t_csbin' => $contragent]);
        }

        return $query->all();
    }

}