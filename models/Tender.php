<?php 
namespace app\models;


use Yii;
use yii\db\ActiveRecord;
use app\models\Auth;

include($_SERVER['DOCUMENT_ROOT'].'/apps/sandbox.inc');

class Tender extends ActiveRecord
{
    public $id;
    public $tenderid;
    public $csid;
    public $senddate;
    public $accept_solution;
    public $date_end_discuss;
    public $date_purchase;
    public $consortium;
    public $source_get;
    public $t_csbin;
    public $t_csname;
    public $origid;
    public $ognid;
    public $t_purchases;
    public $tender_userid;
    public $purchase_number;
    public $purchase_name;
    public $file_code;
    public $error;
    public $updated_at;
    public $procurement_id;


    public function rules()
    {
        return [
            [['t_csname', 't_csbin'], 'required'],
            [['consortium'], 'string'],
        ];
    }
    public static function tableName()
    {
        return 'tenders';
    }

    public static function getAll()
    {
        $userid = Yii::$app->auth->user()['userid'];
        $user_role = Yii::$app->auth->tenderPermission()['tender'];
        $user_role_depid = Yii::$app->auth->tenderPermission()['depid'];
        $user_role_hidden = Yii::$app->auth->tenderPermission()['tender_hidden'];

        $isParticipate = base64_decode(Yii::$app->request->get('isParticipate'));

        $subquery_true = (new \yii\db\Query())
        			->select(['count(actions.aid) as "aid_true"', 'tender_lots.l_tenderid as l_tenderid'])
        			->from('actions')
        			->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
        			->join('inner join', 'tender_lot_agreement_provisions', 'tender_lots.lotid = tender_lot_agreement_provisions.lap_lotid')
        			->where(['>=', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['<', 'actions.act2id', Yii::$app->params['cfg_cancel_stage']])
        			->andWhere(['lap_is_need'=>1, 'lap_is_exist'=>1, 'isactive'=>1, 'isdel'=>0])
        			->groupBy(['actions.dealid']);

        //   Данные запросы проверяют, есть ли в тендере лоты, где нужно было внести обеспечения и они были внесены 
       	$lot_counts_true = (new \yii\db\Query())
       		->select(['count(aid_true) as lot_counts_true', 'l_tenderid'])
       		->from(['T' => $subquery_true])
       		->groupBy(['l_tenderid']);

      	$subquery_false = (new \yii\db\Query())
        			->select(['count(actions.aid) as "aid_false"', 'tender_lots.l_tenderid as l_tenderid'])
        			->from('actions')
        			->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
        			->join('inner join', 'tender_lot_agreement_provisions', 'tender_lots.lotid = tender_lot_agreement_provisions.lap_lotid')
        			->where(['>=', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['<', 'actions.act2id', Yii::$app->params['cfg_cancel_stage']])
        			->andWhere(['lap_is_need'=>1, 'lap_is_exist'=>0, 'isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);
                    
        //   Данные запросы проверяют, есть ли в тендере лоты, где нужно было внести обеспечения и они не были внесены 
       	$lot_counts_false = (new \yii\db\Query())
       		->select(['count(aid_false) as lot_counts_false', 'l_tenderid'])
       		->from(['T' => $subquery_false])
       		->groupBy(['l_tenderid']);

       	$subquery_neutral = (new \yii\db\Query())
        			->select(['count(actions.aid) as "aid_neutral"', 'tender_lots.l_tenderid as l_tenderid'])
        			->from('actions')
        			->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
        			->where(['<', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
        			->andWhere(['isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);
                    
        //   Данные запросы проверяют, есть ли в тендере лоты, которые не дошли до стадии договора
       	$lot_counts_neutral = (new \yii\db\Query())
       		->select(['count(aid_neutral) as lot_counts_neutral', 'l_tenderid'])
       		->from(['T' => $subquery_neutral])
       		->groupBy(['l_tenderid']);

       	$lot_counts_signed_agreements_subquery = (new \yii\db\Query())
        			->select(['count(actions.aid) as "aid_signed_agreements"', 'tender_lots.l_tenderid as l_tenderid'])
        			->from('actions')
        			->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
        			->where(['>=', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['<', 'actions.act2id', Yii::$app->params['cfg_cancel_stage']])
        			->andWhere(['isactive'=>1, 'isdel'=>0])
        			->groupBy(['actions.dealid']);

        // проверка, есть ли лот в тендере, где подписан договор
      	$lot_counts_signed_agreements = (new \yii\db\Query())
		       		->select(['count(aid_signed_agreements) as lot_counts_signed_agreements', 'l_tenderid'])
		       		->from(['T' => $lot_counts_signed_agreements_subquery])
		       		->groupBy(['l_tenderid']);

        $lot_counts_tv = (new \yii\db\Query())
                    ->select(['count(tender_lots.lotid) as lot_counts_tv', 'tender_lots.l_tenderid'])
                    ->from('tender_lots')
                    ->join('inner join', 'treqform', 'treqform.dealid = tender_lots.l_dealid')
                    ->where(['treqform.issucc' => 1, 'treqform.isactive' => 1])
                    ->groupBy(['l_tenderid']);

        $lot_participate_true = (new \yii\db\Query())
                    ->select(['count(tender_participate_history.lotid) as lot_participate_count_true', 'tender_lots.l_tenderid'])
                    ->from('tender_participate_history')
                    ->join('inner join', 'tender_lots', 'tender_lots.lotid = tender_participate_history.lotid')
                    ->where(['tender_participate_history.isActive' => 1, 'tender_participate_history.isParticipate' => 1])
                    ->groupBy(['l_tenderid']);

        $lot_participate_false = (new \yii\db\Query())
                    ->select(['count(tender_participate_history.lotid) as lot_participate_count_false', 'tender_lots.l_tenderid'])
                    ->from('tender_participate_history')
                    ->join('inner join', 'tender_lots', 'tender_lots.lotid = tender_participate_history.lotid')
                    ->where(['tender_participate_history.isActive' => 1, 'tender_participate_history.isParticipate' => 0])
                    ->groupBy(['l_tenderid']);

        $common_query = (new \yii\db\Query())
                    ->select(['tenders.tenderid', 'lot_counts_tv.lot_counts_tv', 
                        'tenders.t_csbin', 'tenders.t_csname', 'lot_participate_true.lot_participate_count_true',
                        'lot_participate_false.lot_participate_count_false', 'tenders.purchase_number', 'tenders.accept_solution', 
                        'tenders.tender_date_purchase', 'tender_managers.tm_managerid', 'tender_managers.tm_userfullname',
                        'tenders.application_status', 'tenders.application_status_date', 'lot_counts_true.lot_counts_true',
                        'lot_counts_false.lot_counts_false', 'lot_counts_neutral.lot_counts_neutral', 'lot_counts_signed_agreements.lot_counts_signed_agreements',
                        'tenders.senddate', 'tender_sources.source_title', 'tenders.tender_username', 'tenders.tender_userid',
                        'tender_managers.tm_created_at', 'tenders.application_status_userid', 'tenders.updated_at'])
                    ->from('tenders')
                    ->join('left join', 'tender_lots', 'tenders.tenderid = tender_lots.l_tenderid')
                    ->join('left join', 'tender_purchases', 'tenders.purchase_type = tender_purchases.purchaseid')
                    ->join('left join', 'tender_sources', 'tenders.source_get = tender_sources.sourceid')
                    ->join('left join', 'tender_lot_dep', 'tender_lot_dep.dep_lotid = tender_lots.lotid')
                    ->join('left join', 'tender_managers', 'tenders.tenderid = tender_managers.tm_tenderid')
                    ->join('left join', 'tender_lot_signed', 'tender_lot_signed.s_lotid = tender_lots.lotid')
                    ->join('left join', 'tender_lot_managers', 'tender_lot_managers.m_lotid = tender_lots.lotid')
                    ->join('left join', 'tender_lot_documents', 'tender_lot_documents.ld_lotid = tender_lots.lotid')
                    ->join('left join', ['lot_participate_true'=>$lot_participate_true], 'tenders.tenderid = lot_participate_true.l_tenderid')
                    ->join('left join', ['lot_participate_false'=>$lot_participate_false], 'tenders.tenderid = lot_participate_false.l_tenderid')
                    ->join('left join', ['lot_counts_true'=>$lot_counts_true], 'lot_counts_true.l_tenderid = tenders.tenderid')
                    ->join('left join', ['lot_counts_signed_agreements'=>$lot_counts_signed_agreements], 'lot_counts_signed_agreements.l_tenderid = tenders.tenderid')
                    ->join('left join', ['lot_counts_false'=>$lot_counts_false], 'lot_counts_false.l_tenderid = tenders.tenderid')
                    ->join('left join', ['lot_counts_neutral'=>$lot_counts_neutral], 'lot_counts_neutral.l_tenderid = tenders.tenderid')
                    ->join('left join', ['lot_counts_tv' => $lot_counts_tv], 'lot_counts_tv.l_tenderid = tenders.tenderid')
                    ->where(['tenders.application_status' => 0]);

        if ($isParticipate == 2) {
            $common_query->andWhere(['>', 'lot_participate_count_true', 0]);
        }

        if ($isParticipate == 1) {
            $common_query->andWhere(['>', 'lot_participate_count_false', 0]);
        }

       	//Если супер админ 
        if ($user_role == 1) {
        	// и если depid в privusers 0 , то он видит все подразделения тенедера
        	if ($user_role_depid[0] == 0) {
        		// и если tender hidden в privusers 1, то он видит и закрытые тендеры
        		if ($user_role_hidden == 1) {
        			return $common_query
				                ->groupBy('tenders.tenderid')
				                ->all();
        		}
        		// если tender hidden 0, то он не видит закрытые тендеры
        		else {
        			return $common_query
			                ->andWhere(['!=','tenders.purchase_type', 13])
			                ->groupBy('tenders.tenderid')
			                ->all();
        		}
        	// и если depid в privusers не нуль , то он видит все тендеры с соответствующими подразделениями
        	} else {

        		if ($user_role_hidden == 1) {
        			return $common_query
			                ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
		            	    ->groupBy('tenders.tenderid')
			                ->all();
        		} else {
        			return $common_query
			                ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
			                ->andWhere(['!=','tenders.purchase_type', 13])
			                ->groupBy('tenders.tenderid')
			                ->all();
        		}
        	}
        	// если tender = 2, то это специалист, который видит все тенедеры , но не может назначать специалистов на тендер  
        } elseif ($user_role == 2) {

        	// если depid у данного юзера 0 , то он видит все тенедеры всех подраздлениях 
        	if ($user_role_depid[0] == 0) {
        		// и если tender hidden в privusers 1, то он видит и закрытые тендеры
        		if ($user_role_hidden == 1) {
        			return $common_query
			                ->groupBy('tenders.tenderid')
			                ->all();
		                // и если tender hidden в privusers 0, то он не видит закрытые тендеры
        		} else {
        			return $common_query
			                ->andWhere(['!=','tenders.purchase_type', 13])
			                ->groupBy('tenders.tenderid')
			                ->all();
        		}
        	// отобразить тендеры, которые имеют лоты с соответсвующими depid
        	} else {
        		// отобразить тендеры, которые имеют лоты с соответсвующими depid (с закртыми)
        		if ($user_role_hidden == 1) {
        			return $common_query
			                ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
			                ->groupBy('tenders.tenderid')
			                ->all();
		        // отобразить тендеры, которые имеют лоты с соответсвующими depid (без закртых)
        		} else {
        			return $common_query
		        			->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
		        			->andWhere(['!=','tenders.purchase_type', 13])
		        			->groupBy('tenders.tenderid')
		        			->all();
        		}	
        	}
       	//  если tender в Privuser у авторизованного пользоватлея равен 3, то это привилегированный пользователь который видит только тендера, который у него в depid  
        } elseif ($user_role == 3) {
        	// отобразить все тендеры, независимо от подраздления
        	if ($user_role_depid[0] == 0) {
        		// также отбобразить и закртые тендеры
        		if ($user_role_hidden == 1) {
        			return $common_query
		        			->groupBy('tenders.tenderid')
		        			->all();
        		} else {
        			return $common_query
		        			->andWhere(['!=','tenders.purchase_type', 13])
		        			->groupBy('tenders.tenderid')
		        			->all();
        		}
        	// отобразить тендеры, в зависимости от подраздления пользователя	
        	} else {
        		// вместе с закрытыми тендерами
        		if ($user_role_hidden == 1) {
        			return $common_query
		        			->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
		        			->groupBy('tenders.tenderid')
		        			->all();
        		} else {
        			return $common_query
	        			->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
	        			->andWhere(['!=','tenders.purchase_type', 13])
	        			->groupBy('tenders.tenderid')
	        			->all();
        		}
        	}
        } elseif ($user_role == 4) {
            // отобразить все тендеры, независимо от подраздления
            if ($user_role_depid[0] == 0) {
                // также отбобразить и закртые тендеры
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->groupBy('tenders.tenderid')
                            ->all();
                } else {
                    return $common_query
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            // отобразить тендеры, в зависимости от подраздления пользователя   
            } else {
                // вместе с закрытыми тендерами
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_documents.ld_depid', $user_role_depid])
                            ->groupBy('tenders.tenderid')
                            ->all();
                } else {
                    return $common_query
                        ->andWhere(['in', 'tender_lot_documents.ld_depid', $user_role_depid])
                        ->andWhere(['!=','tenders.purchase_type', 13])
                        ->groupBy('tenders.tenderid')
                        ->all();
                }
            }
        } else {
            return 	$common_query
		                ->andWhere(['or',
		                	['tender_lot_signed.s_userid' => $userid],
		                	['tender_lot_managers.m_managerid' => $userid],
		            	])
		                ->groupBy(['tenders.tenderid'])
		                ->all();
        }
    }

    public static function getFiled()
    {
        $userid = Yii::$app->auth->user()['userid'];
        $user_role = Yii::$app->auth->tenderPermission()['tender'];
        $user_role_depid = Yii::$app->auth->tenderPermission()['depid'];
        $user_role_hidden = Yii::$app->auth->tenderPermission()['tender_hidden'];

        $subquery_true = (new \yii\db\Query())
                    ->select(['count(actions.aid) as "aid_true"', 'tender_lots.l_tenderid as l_tenderid'])
                    ->from('actions')
                    ->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
                    ->join('inner join', 'tender_lot_agreement_provisions', 'tender_lots.lotid = tender_lot_agreement_provisions.lap_lotid')
                    ->where(['>=', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['<', 'actions.act2id', Yii::$app->params['cfg_cancel_stage']])
                    ->andWhere(['lap_is_need'=>1, 'lap_is_exist'=>1, 'isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);

        //   Данные запросы проверяют, есть ли в тендере лоты, где нужно было внести обеспечения и они были внесены 
        $lot_counts_true = (new \yii\db\Query())
            ->select(['count(aid_true) as lot_counts_true', 'l_tenderid'])
            ->from(['T' => $subquery_true])
            ->groupBy(['l_tenderid']);

        $subquery_false = (new \yii\db\Query())
                    ->select(['count(actions.aid) as "aid_false"', 'tender_lots.l_tenderid as l_tenderid'])
                    ->from('actions')
                    ->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
                    ->join('inner join', 'tender_lot_agreement_provisions', 'tender_lots.lotid = tender_lot_agreement_provisions.lap_lotid')
                    ->where(['>=', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['<', 'actions.act2id', Yii::$app->params['cfg_cancel_stage']])
                    ->andWhere(['lap_is_need'=>1, 'lap_is_exist'=>0, 'isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);
        //   Данные запросы проверяют, есть ли в тендере лоты, где нужно было внести обеспечения и они не были внесены 
        $lot_counts_false = (new \yii\db\Query())
            ->select(['count(aid_false) as lot_counts_false', 'l_tenderid'])
            ->from(['T' => $subquery_false])
            ->groupBy(['l_tenderid']);

        $subquery_neutral = (new \yii\db\Query())
                    ->select(['count(actions.aid) as "aid_neutral"', 'tender_lots.l_tenderid as l_tenderid'])
                    ->from('actions')
                    ->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
                    ->where(['<', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);
        //   Данные запросы проверяют, есть ли в тендере лоты, которые не дошли до стадии договора
        $lot_counts_neutral = (new \yii\db\Query())
            ->select(['count(aid_neutral) as lot_counts_neutral', 'l_tenderid'])
            ->from(['T' => $subquery_neutral])
            ->groupBy(['l_tenderid']);


        $lot_counts_signed_agreements_subquery = (new \yii\db\Query())
                    ->select(['count(actions.aid) as "aid_signed_agreements"', 'tender_lots.l_tenderid as l_tenderid'])
                    ->from('actions')
                    ->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
                    ->where(['>=', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['<', 'actions.act2id', Yii::$app->params['cfg_cancel_stage']])
                    ->andWhere(['isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);

        // проверка, есть ли лот в тендере, где подписан договор
        $lot_counts_signed_agreements = (new \yii\db\Query())
                    ->select(['count(aid_signed_agreements) as lot_counts_signed_agreements', 'l_tenderid'])
                    ->from(['T' => $lot_counts_signed_agreements_subquery])
                    ->groupBy(['l_tenderid']);

        $lot_counts_tv = (new \yii\db\Query())
                    ->select(['count(tender_lots.lotid) as lot_counts_tv', 'tender_lots.l_tenderid'])
                    ->from('tender_lots')
                    ->join('inner join', 'treqform', 'treqform.dealid = tender_lots.l_dealid')
                    ->where(['treqform.issucc' => 1, 'treqform.isactive' => 1])
                    ->groupBy(['l_tenderid']);

        $lot_participate_true = (new \yii\db\Query())
                    ->select(['count(tender_participate_history.lotid) as lot_participate_count_true', 'tender_lots.l_tenderid'])
                    ->from('tender_participate_history')
                    ->join('inner join', 'tender_lots', 'tender_lots.lotid = tender_participate_history.lotid')
                    ->where(['tender_participate_history.isActive' => 1, 'tender_participate_history.isParticipate' => 1])
                    ->groupBy(['l_tenderid']);

        $lot_participate_false = (new \yii\db\Query())
                    ->select(['count(tender_participate_history.lotid) as lot_participate_count_false', 'tender_lots.l_tenderid'])
                    ->from('tender_participate_history')
                    ->join('inner join', 'tender_lots', 'tender_lots.lotid = tender_participate_history.lotid')
                    ->where(['tender_participate_history.isActive' => 1, 'tender_participate_history.isParticipate' => 0])
                    ->groupBy(['l_tenderid']);

      
        $common_query = (new \yii\db\Query())
                ->select(['tenders.tenderid', 'lot_counts_tv.lot_counts_tv', 
                    'tenders.t_csbin', 'tenders.t_csname', 'lot_participate_true.lot_participate_count_true',
                    'lot_participate_false.lot_participate_count_false', 'tenders.purchase_number', 'tenders.accept_solution', 
                    'tenders.tender_date_purchase', 'tender_managers.tm_managerid', 'tender_managers.tm_userfullname',
                    'tenders.application_status', 'tenders.application_status_date', 'lot_counts_true.lot_counts_true',
                    'lot_counts_false.lot_counts_false', 'lot_counts_neutral.lot_counts_neutral', 'lot_counts_signed_agreements.lot_counts_signed_agreements',
                    'tenders.senddate', 'tender_sources.source_title', 'tenders.tender_username', 'tenders.tender_userid',
                    'tender_managers.tm_created_at', 'tenders.application_status_userid', 'tenders.updated_at'])
                ->from('tenders')
                ->join('left join', 'tender_lots', 'tenders.tenderid = tender_lots.l_tenderid')
                ->join('left join', 'tender_purchases', 'tenders.purchase_type = tender_purchases.purchaseid')
                ->join('left join', 'tender_sources', 'tenders.source_get = tender_sources.sourceid')
                ->join('left join', 'tender_lot_dep', 'tender_lot_dep.dep_lotid = tender_lots.lotid')
                ->join('left join', 'tender_managers', 'tenders.tenderid = tender_managers.tm_tenderid')
                ->join('left join', 'tender_lot_signed', 'tender_lot_signed.s_lotid = tender_lots.lotid')
                ->join('left join', 'tender_lot_managers', 'tender_lot_managers.m_lotid = tender_lots.lotid')
                ->join('left join', 'tender_lot_documents', 'tender_lot_documents.ld_lotid = tender_lots.lotid')
                ->join('left join', ['lot_participate_true'=>$lot_participate_true], 'tenders.tenderid = lot_participate_true.l_tenderid')
                ->join('left join', ['lot_participate_false'=>$lot_participate_false], 'tenders.tenderid = lot_participate_false.l_tenderid')
                ->join('left join', ['lot_counts_true'=>$lot_counts_true], 'lot_counts_true.l_tenderid = tenders.tenderid')
                ->join('left join', ['lot_counts_signed_agreements'=>$lot_counts_signed_agreements], 'lot_counts_signed_agreements.l_tenderid = tenders.tenderid')
                ->join('left join', ['lot_counts_false'=>$lot_counts_false], 'lot_counts_false.l_tenderid = tenders.tenderid')
                ->join('left join', ['lot_counts_neutral'=>$lot_counts_neutral], 'lot_counts_neutral.l_tenderid = tenders.tenderid')
                ->join('left join', ['lot_counts_tv' => $lot_counts_tv], 'lot_counts_tv.l_tenderid = tenders.tenderid')
                ->where(['tenders.application_status' => 1]);
        
                
        //Если супер админ 
        if ($user_role == 1) {
            // и если depid в privusers 0 , то он видит все подразделения тенедера
            if ($user_role_depid[0] == 0) {
                // и если tender hidden в privusers 1, то он видит и закрытые тендеры
                if ($user_role_hidden == 1) {
                    return $common_query
                                ->groupBy('tenders.tenderid')
                                ->all();
                }
                // если tender hidden 0, то он не видит закрытые тендеры
                else {
                    return $common_query
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            // и если depid в privusers не нуль , то он видит все тендеры с соответствующими подразделениями
            } else {

                if ($user_role_hidden == 1) {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->groupBy('tenders.tenderid')
                            ->all();
                } else {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            }
            // если tender = 2, то это специалист, который видит все тенедеры , но не может назначать специалистов на тендер  
        } elseif ($user_role == 2) {

            // если depid у данного юзера 0 , то он видит все тенедеры всех подраздлениях 
            if ($user_role_depid[0] == 0) {
                // и если tender hidden в privusers 1, то он видит и закрытые тендеры
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->groupBy('tenders.tenderid')
                            ->all();
                        // и если tender hidden в privusers 0, то он не видит закрытые тендеры
                } else {
                    return $common_query
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            // отобразить тендеры, которые имеют лоты с соответсвующими depid
            } else {
                // отобразить тендеры, которые имеют лоты с соответсвующими depid (с закртыми)
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->groupBy('tenders.tenderid')
                            ->all();
                // отобразить тендеры, которые имеют лоты с соответсвующими depid (без закртых)
                } else {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }   
            }
        //  если tender в Privuser у авторизованного пользоватлея равен 3, то это привилегированный пользователь который видит только тендера, который у него в depid  
        } elseif ($user_role == 3) {
            // отобразить все тендеры, независимо от подраздления
            if ($user_role_depid[0] == 0) {
                // также отбобразить и закртые тендеры
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->groupBy('tenders.tenderid')
                            ->all();
                } else {
                    return $common_query
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            // отобразить тендеры, в зависимости от подраздления пользователя   
            } else {
                // вместе с закрытыми тендерами
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->groupBy('tenders.tenderid')
                            ->all();
                } else {
                    return $common_query
                        ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                        ->andWhere(['!=','tenders.purchase_type', 13])
                        ->groupBy('tenders.tenderid')
                        ->all();
                }
            }
        } else {
            return  $common_query
                        ->andWhere(['or',
                            ['tender_lot_signed.s_userid' => $userid],
                            ['tender_lot_managers.m_managerid' => $userid],
                            ['in', 'tender_lot_documents.ld_depid', $user_role_depid]
                        ])
                        ->groupBy(['tenders.tenderid'])
                        ->all();
        }  
    }

    public static function getWorked()
    {

        $userid = Yii::$app->auth->user()['userid'];
        $user_role = Yii::$app->auth->tenderPermission()['tender'];
        $user_role_depid = Yii::$app->auth->tenderPermission()['depid'];
        $user_role_hidden = Yii::$app->auth->tenderPermission()['tender_hidden'];

        $subquery_true = (new \yii\db\Query())
                    ->select(['count(actions.aid) as "aid_true"', 'tender_lots.l_tenderid as l_tenderid'])
                    ->from('actions')
                    ->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
                    ->join('inner join', 'tender_lot_agreement_provisions', 'tender_lots.lotid = tender_lot_agreement_provisions.lap_lotid')
                    ->where(['>=', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['<', 'actions.act2id', Yii::$app->params['cfg_cancel_stage']])
                    ->andWhere(['lap_is_need'=>1, 'lap_is_exist'=>1, 'isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);

        //   Данные запросы проверяют, есть ли в тендере лоты, где нужно было внести обеспечения и они были внесены 
        $lot_counts_true = (new \yii\db\Query())
            ->select(['count(aid_true) as lot_counts_true', 'l_tenderid'])
            ->from(['T' => $subquery_true])
            ->groupBy(['l_tenderid']);

        $subquery_false = (new \yii\db\Query())
                    ->select(['count(actions.aid) as "aid_false"', 'tender_lots.l_tenderid as l_tenderid'])
                    ->from('actions')
                    ->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
                    ->join('inner join', 'tender_lot_agreement_provisions', 'tender_lots.lotid = tender_lot_agreement_provisions.lap_lotid')
                    ->where(['>=', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['<', 'actions.act2id', Yii::$app->params['cfg_cancel_stage']])
                    ->andWhere(['lap_is_need'=>1, 'lap_is_exist'=>0, 'isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);
        //   Данные запросы проверяют, есть ли в тендере лоты, где нужно было внести обеспечения и они не были внесены 
        $lot_counts_false = (new \yii\db\Query())
            ->select(['count(aid_false) as lot_counts_false', 'l_tenderid'])
            ->from(['T' => $subquery_false])
            ->groupBy(['l_tenderid']);

        $subquery_neutral = (new \yii\db\Query())
                    ->select(['count(actions.aid) as "aid_neutral"', 'tender_lots.l_tenderid as l_tenderid'])
                    ->from('actions')
                    ->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
                    ->where(['<', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);
        //   Данные запросы проверяют, есть ли в тендере лоты, которые не дошли до стадии договора
        $lot_counts_neutral = (new \yii\db\Query())
            ->select(['count(aid_neutral) as lot_counts_neutral', 'l_tenderid'])
            ->from(['T' => $subquery_neutral])
            ->groupBy(['l_tenderid']);


        $lot_counts_signed_agreements_subquery = (new \yii\db\Query())
                    ->select(['count(actions.aid) as "aid_signed_agreements"', 'tender_lots.l_tenderid as l_tenderid'])
                    ->from('actions')
                    ->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
                    ->where(['>=', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['<', 'actions.act2id', Yii::$app->params['cfg_cancel_stage']])
                    ->andWhere(['isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);

        // проверка, есть ли лот в тендере, где подписан договор
        $lot_counts_signed_agreements = (new \yii\db\Query())
                    ->select(['count(aid_signed_agreements) as lot_counts_signed_agreements', 'l_tenderid'])
                    ->from(['T' => $lot_counts_signed_agreements_subquery])
                    ->groupBy(['l_tenderid']);

        $lot_counts_tv = (new \yii\db\Query())
                    ->select(['count(tender_lots.lotid) as lot_counts_tv', 'tender_lots.l_tenderid'])
                    ->from('tender_lots')
                    ->join('inner join', 'treqform', 'treqform.dealid = tender_lots.l_dealid')
                    ->where(['treqform.issucc' => 1, 'treqform.isactive' => 1])
                    ->groupBy(['l_tenderid']);

        $lot_participate_true = (new \yii\db\Query())
                    ->select(['count(tender_participate_history.lotid) as lot_participate_count_true', 'tender_lots.l_tenderid'])
                    ->from('tender_participate_history')
                    ->join('inner join', 'tender_lots', 'tender_lots.lotid = tender_participate_history.lotid')
                    ->where(['tender_participate_history.isActive' => 1, 'tender_participate_history.isParticipate' => 1])
                    ->groupBy(['l_tenderid']);

        $lot_participate_false = (new \yii\db\Query())
                    ->select(['count(tender_participate_history.lotid) as lot_participate_count_false', 'tender_lots.l_tenderid'])
                    ->from('tender_participate_history')
                    ->join('inner join', 'tender_lots', 'tender_lots.lotid = tender_participate_history.lotid')
                    ->where(['tender_participate_history.isActive' => 1, 'tender_participate_history.isParticipate' => 0])
                    ->groupBy(['l_tenderid']);

      
        $common_query = (new \yii\db\Query())
                ->select(['tenders.tenderid', 'lot_counts_tv.lot_counts_tv', 
                        'tenders.t_csbin', 'tenders.t_csname', 'lot_participate_true.lot_participate_count_true',
                        'lot_participate_false.lot_participate_count_false', 'tenders.purchase_number', 'tenders.accept_solution', 
                        'tenders.tender_date_purchase', 'tender_managers.tm_managerid', 'tender_managers.tm_userfullname',
                        'tenders.application_status', 'tenders.application_status_date', 'lot_counts_true.lot_counts_true',
                        'lot_counts_false.lot_counts_false', 'lot_counts_neutral.lot_counts_neutral', 'lot_counts_signed_agreements.lot_counts_signed_agreements',
                        'tenders.senddate', 'tender_sources.source_title', 'tenders.tender_username', 'tenders.tender_userid',
                        'tender_managers.tm_created_at', 'tenders.application_status_userid', 'tenders.updated_at'])
                ->from('tenders')
                ->join('left join', 'tender_lots', 'tenders.tenderid = tender_lots.l_tenderid')
                ->join('left join', 'tender_purchases', 'tenders.purchase_type = tender_purchases.purchaseid')
                ->join('left join', 'tender_sources', 'tenders.source_get = tender_sources.sourceid')
                ->join('left join', 'tender_lot_dep', 'tender_lot_dep.dep_lotid = tender_lots.lotid')
                ->join('left join', 'tender_managers', 'tenders.tenderid = tender_managers.tm_tenderid')
                ->join('left join', 'tender_lot_signed', 'tender_lot_signed.s_lotid = tender_lots.lotid')
                ->join('left join', 'tender_lot_managers', 'tender_lot_managers.m_lotid = tender_lots.lotid')
                ->join('left join', 'tender_lot_documents', 'tender_lot_documents.ld_lotid = tender_lots.lotid')
                ->join('left join', ['lot_participate_true'=>$lot_participate_true], 'tenders.tenderid = lot_participate_true.l_tenderid')
                ->join('left join', ['lot_participate_false'=>$lot_participate_false], 'tenders.tenderid = lot_participate_false.l_tenderid')
                ->join('left join', ['lot_counts_true'=>$lot_counts_true], 'lot_counts_true.l_tenderid = tenders.tenderid')
                ->join('left join', ['lot_counts_signed_agreements'=>$lot_counts_signed_agreements], 'lot_counts_signed_agreements.l_tenderid = tenders.tenderid')
                ->join('left join', ['lot_counts_false'=>$lot_counts_false], 'lot_counts_false.l_tenderid = tenders.tenderid')
                ->join('left join', ['lot_counts_neutral'=>$lot_counts_neutral], 'lot_counts_neutral.l_tenderid = tenders.tenderid')
                ->join('left join', ['lot_counts_tv' => $lot_counts_tv], 'lot_counts_tv.l_tenderid = tenders.tenderid')
                ->where(['tenders.application_status' => 2]);
        
        

        
                        
        //Если супер админ 
        if ($user_role == 1) {
            // и если depid в privusers 0 , то он видит все подразделения тенедера
            if ($user_role_depid[0] == 0) {
                // и если tender hidden в privusers 1, то он видит и закрытые тендеры
                if ($user_role_hidden == 1) {
                    return $common_query
                                ->groupBy('tenders.tenderid')
                                ->all();
                }
                // если tender hidden 0, то он не видит закрытые тендеры
                else {
                    return $common_query
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            // и если depid в privusers не нуль , то он видит все тендеры с соответствующими подразделениями
            } else {

                if ($user_role_hidden == 1) {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->groupBy('tenders.tenderid')
                            ->all();
                } else {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            }
            // если tender = 2, то это специалист, который видит все тенедеры , но не может назначать специалистов на тендер  
        } elseif ($user_role == 2) {

            // если depid у данного юзера 0 , то он видит все тенедеры всех подраздлениях 
            if ($user_role_depid[0] == 0) {
                // и если tender hidden в privusers 1, то он видит и закрытые тендеры
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->groupBy('tenders.tenderid')
                            ->all();
                        // и если tender hidden в privusers 0, то он не видит закрытые тендеры
                } else {
                    return $common_query
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            // отобразить тендеры, которые имеют лоты с соответсвующими depid
            } else {
                // отобразить тендеры, которые имеют лоты с соответсвующими depid (с закртыми)
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->groupBy('tenders.tenderid')
                            ->all();
                // отобразить тендеры, которые имеют лоты с соответсвующими depid (без закртых)
                } else {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }   
            }
        //  если tender в Privuser у авторизованного пользоватлея равен 3, то это привилегированный пользователь который видит только тендера, который у него в depid  
        } elseif ($user_role == 3) {
            // отобразить все тендеры, независимо от подраздления
            if ($user_role_depid[0] == 0) {
                // также отбобразить и закртые тендеры
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->groupBy('tenders.tenderid')
                            ->all();
                } else {
                    return $common_query
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            // отобразить тендеры, в зависимости от подраздления пользователя   
            } else {
                // вместе с закрытыми тендерами
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->groupBy('tenders.tenderid')
                            ->all();
                } else {
                    return $common_query
                        ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                        ->andWhere(['!=','tenders.purchase_type', 13])
                        ->groupBy('tenders.tenderid')
                        ->all();
                }
            }
        } else {
            return  $common_query
                        ->andWhere(['or',
                            ['tender_lot_signed.s_userid' => $userid],
                            ['tender_lot_managers.m_managerid' => $userid],
                            ['in', 'tender_lot_documents.ld_depid', $user_role_depid]
                        ])
                        ->groupBy(['tenders.tenderid'])
                        ->all();
        }   

    }

    public static function getUnWorked()
    {
        $userid = Yii::$app->auth->user()['userid'];
        $user_role = Yii::$app->auth->tenderPermission()['tender'];
        $user_role_depid = Yii::$app->auth->tenderPermission()['depid'];
        $user_role_hidden = Yii::$app->auth->tenderPermission()['tender_hidden'];

        $subquery_true = (new \yii\db\Query())
                    ->select(['count(actions.aid) as "aid_true"', 'tender_lots.l_tenderid as l_tenderid'])
                    ->from('actions')
                    ->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
                    ->join('inner join', 'tender_lot_agreement_provisions', 'tender_lots.lotid = tender_lot_agreement_provisions.lap_lotid')
                    ->where(['>=', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['<', 'actions.act2id', Yii::$app->params['cfg_cancel_stage']])
                    ->andWhere(['lap_is_need'=>1, 'lap_is_exist'=>1, 'isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);

        //   Данные запросы проверяют, есть ли в тендере лоты, где нужно было внести обеспечения и они были внесены 
        $lot_counts_true = (new \yii\db\Query())
            ->select(['count(aid_true) as lot_counts_true', 'l_tenderid'])
            ->from(['T' => $subquery_true])
            ->groupBy(['l_tenderid']);

        $subquery_false = (new \yii\db\Query())
                    ->select(['count(actions.aid) as "aid_false"', 'tender_lots.l_tenderid as l_tenderid'])
                    ->from('actions')
                    ->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
                    ->join('inner join', 'tender_lot_agreement_provisions', 'tender_lots.lotid = tender_lot_agreement_provisions.lap_lotid')
                    ->where(['>=', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['<', 'actions.act2id', Yii::$app->params['cfg_cancel_stage']])
                    ->andWhere(['lap_is_need'=>1, 'lap_is_exist'=>0, 'isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);
        //   Данные запросы проверяют, есть ли в тендере лоты, где нужно было внести обеспечения и они не были внесены 
        $lot_counts_false = (new \yii\db\Query())
            ->select(['count(aid_false) as lot_counts_false', 'l_tenderid'])
            ->from(['T' => $subquery_false])
            ->groupBy(['l_tenderid']);

        $subquery_neutral = (new \yii\db\Query())
                    ->select(['count(actions.aid) as "aid_neutral"', 'tender_lots.l_tenderid as l_tenderid'])
                    ->from('actions')
                    ->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
                    ->where(['<', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);
        //   Данные запросы проверяют, есть ли в тендере лоты, которые не дошли до стадии договора
        $lot_counts_neutral = (new \yii\db\Query())
            ->select(['count(aid_neutral) as lot_counts_neutral', 'l_tenderid'])
            ->from(['T' => $subquery_neutral])
            ->groupBy(['l_tenderid']);


        $lot_counts_signed_agreements_subquery = (new \yii\db\Query())
                    ->select(['count(actions.aid) as "aid_signed_agreements"', 'tender_lots.l_tenderid as l_tenderid'])
                    ->from('actions')
                    ->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
                    ->where(['>=', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['<', 'actions.act2id', Yii::$app->params['cfg_cancel_stage']])
                    ->andWhere(['isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);

        // проверка, есть ли лот в тендере, где подписан договор
        $lot_counts_signed_agreements = (new \yii\db\Query())
                    ->select(['count(aid_signed_agreements) as lot_counts_signed_agreements', 'l_tenderid'])
                    ->from(['T' => $lot_counts_signed_agreements_subquery])
                    ->groupBy(['l_tenderid']);

        $lot_counts_tv = (new \yii\db\Query())
                    ->select(['count(tender_lots.lotid) as lot_counts_tv', 'tender_lots.l_tenderid'])
                    ->from('tender_lots')
                    ->join('inner join', 'treqform', 'treqform.dealid = tender_lots.l_dealid')
                    ->where(['treqform.issucc' => 1, 'treqform.isactive' => 1])
                    ->groupBy(['l_tenderid']);

        $lot_participate_true = (new \yii\db\Query())
                    ->select(['count(tender_participate_history.lotid) as lot_participate_count_true', 'tender_lots.l_tenderid'])
                    ->from('tender_participate_history')
                    ->join('inner join', 'tender_lots', 'tender_lots.lotid = tender_participate_history.lotid')
                    ->where(['tender_participate_history.isActive' => 1, 'tender_participate_history.isParticipate' => 1])
                    ->groupBy(['l_tenderid']);

        $lot_participate_false = (new \yii\db\Query())
                    ->select(['count(tender_participate_history.lotid) as lot_participate_count_false', 'tender_lots.l_tenderid'])
                    ->from('tender_participate_history')
                    ->join('inner join', 'tender_lots', 'tender_lots.lotid = tender_participate_history.lotid')
                    ->where(['tender_participate_history.isActive' => 1, 'tender_participate_history.isParticipate' => 0])
                    ->groupBy(['l_tenderid']);

      
        $common_query = (new \yii\db\Query())
                ->select(['tenders.tenderid', 'lot_counts_tv.lot_counts_tv', 
                        'tenders.t_csbin', 'tenders.t_csname', 'lot_participate_true.lot_participate_count_true',
                        'lot_participate_false.lot_participate_count_false', 'tenders.purchase_number', 'tenders.accept_solution', 
                        'tenders.tender_date_purchase', 'tender_managers.tm_managerid', 'tender_managers.tm_userfullname',
                        'tenders.application_status', 'tenders.application_status_date', 'lot_counts_true.lot_counts_true',
                        'lot_counts_false.lot_counts_false', 'lot_counts_neutral.lot_counts_neutral', 'lot_counts_signed_agreements.lot_counts_signed_agreements',
                        'tenders.senddate', 'tender_sources.source_title', 'tenders.tender_username', 'tenders.tender_userid',
                        'tender_managers.tm_created_at', 'tenders.application_status_userid', 'tenders.updated_at'])
                ->from('tenders')
                ->join('left join', 'tender_lots', 'tenders.tenderid = tender_lots.l_tenderid')
                ->join('left join', 'tender_purchases', 'tenders.purchase_type = tender_purchases.purchaseid')
                ->join('left join', 'tender_sources', 'tenders.source_get = tender_sources.sourceid')
                ->join('left join', 'tender_lot_dep', 'tender_lot_dep.dep_lotid = tender_lots.lotid')
                ->join('left join', 'tender_managers', 'tenders.tenderid = tender_managers.tm_tenderid')
                ->join('left join', 'tender_lot_signed', 'tender_lot_signed.s_lotid = tender_lots.lotid')
                ->join('left join', 'tender_lot_managers', 'tender_lot_managers.m_lotid = tender_lots.lotid')
                ->join('left join', 'tender_lot_documents', 'tender_lot_documents.ld_lotid = tender_lots.lotid')
                ->join('left join', ['lot_participate_true'=>$lot_participate_true], 'tenders.tenderid = lot_participate_true.l_tenderid')
                ->join('left join', ['lot_participate_false'=>$lot_participate_false], 'tenders.tenderid = lot_participate_false.l_tenderid')
                ->join('left join', ['lot_counts_true'=>$lot_counts_true], 'lot_counts_true.l_tenderid = tenders.tenderid')
                ->join('left join', ['lot_counts_signed_agreements'=>$lot_counts_signed_agreements], 'lot_counts_signed_agreements.l_tenderid = tenders.tenderid')
                ->join('left join', ['lot_counts_false'=>$lot_counts_false], 'lot_counts_false.l_tenderid = tenders.tenderid')
                ->join('left join', ['lot_counts_neutral'=>$lot_counts_neutral], 'lot_counts_neutral.l_tenderid = tenders.tenderid')
                ->join('left join', ['lot_counts_tv' => $lot_counts_tv], 'lot_counts_tv.l_tenderid = tenders.tenderid')
                ->where(['tenders.application_status' => 3]);
        
        

        
                        
        //Если супер админ 
        if ($user_role == 1) {
            // и если depid в privusers 0 , то он видит все подразделения тенедера
            if ($user_role_depid[0] == 0) {
                // и если tender hidden в privusers 1, то он видит и закрытые тендеры
                if ($user_role_hidden == 1) {
                    return $common_query
                                ->groupBy('tenders.tenderid')
                                ->all();
                }
                // если tender hidden 0, то он не видит закрытые тендеры
                else {
                    return $common_query
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            // и если depid в privusers не нуль , то он видит все тендеры с соответствующими подразделениями
            } else {

                if ($user_role_hidden == 1) {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->groupBy('tenders.tenderid')
                            ->all();
                } else {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            }
            // если tender = 2, то это специалист, который видит все тенедеры , но не может назначать специалистов на тендер  
        } elseif ($user_role == 2) {

            // если depid у данного юзера 0 , то он видит все тенедеры всех подраздлениях 
            if ($user_role_depid[0] == 0) {
                // и если tender hidden в privusers 1, то он видит и закрытые тендеры
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->groupBy('tenders.tenderid')
                            ->all();
                        // и если tender hidden в privusers 0, то он не видит закрытые тендеры
                } else {
                    return $common_query
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            // отобразить тендеры, которые имеют лоты с соответсвующими depid
            } else {
                // отобразить тендеры, которые имеют лоты с соответсвующими depid (с закртыми)
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->groupBy('tenders.tenderid')
                            ->all();
                // отобразить тендеры, которые имеют лоты с соответсвующими depid (без закртых)
                } else {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }   
            }
        //  если tender в Privuser у авторизованного пользоватлея равен 3, то это привилегированный пользователь который видит только тендера, который у него в depid  
        } elseif ($user_role == 3) {
            // отобразить все тендеры, независимо от подраздления
            if ($user_role_depid[0] == 0) {
                // также отбобразить и закртые тендеры
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->groupBy('tenders.tenderid')
                            ->all();
                } else {
                    return $common_query
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            // отобразить тендеры, в зависимости от подраздления пользователя   
            } else {
                // вместе с закрытыми тендерами
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->groupBy('tenders.tenderid')
                            ->all();
                } else {
                    return $common_query
                        ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                        ->andWhere(['!=','tenders.purchase_type', 13])
                        ->groupBy('tenders.tenderid')
                        ->all();
                }
            }
        } else {
            return  $common_query
                        ->andWhere(['or',
                            ['tender_lot_signed.s_userid' => $userid],
                            ['tender_lot_managers.m_managerid' => $userid],
                            ['in', 'tender_lot_documents.ld_depid', $user_role_depid]
                        ])
                        ->groupBy(['tenders.tenderid'])
                        ->all();
        }  
    }

    public static function search($q = NULL)
    {
        $userid = Yii::$app->auth->user()['userid'];
        $user_role = Yii::$app->auth->tenderPermission()['tender'];
        $user_role_depid = Yii::$app->auth->tenderPermission()['depid'];
        $user_role_hidden = Yii::$app->auth->tenderPermission()['tender_hidden'];

        $subquery_true = (new \yii\db\Query())
                    ->select(['count(actions.aid) as "aid_true"', 'tender_lots.l_tenderid as l_tenderid'])
                    ->from('actions')
                    ->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
                    ->join('inner join', 'tender_lot_agreement_provisions', 'tender_lots.lotid = tender_lot_agreement_provisions.lap_lotid')
                    ->where(['>=', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['<', 'actions.act2id', Yii::$app->params['cfg_cancel_stage']])
                    ->andWhere(['lap_is_need'=>1, 'lap_is_exist'=>1, 'isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);

        //   Данные запросы проверяют, есть ли в тендере лоты, где нужно было внести обеспечения и они были внесены 
        $lot_counts_true = (new \yii\db\Query())
            ->select(['count(aid_true) as lot_counts_true', 'l_tenderid'])
            ->from(['T' => $subquery_true])
            ->groupBy(['l_tenderid']);

        $subquery_false = (new \yii\db\Query())
                    ->select(['count(actions.aid) as "aid_false"', 'tender_lots.l_tenderid as l_tenderid'])
                    ->from('actions')
                    ->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
                    ->join('inner join', 'tender_lot_agreement_provisions', 'tender_lots.lotid = tender_lot_agreement_provisions.lap_lotid')
                    ->where(['>=', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['<', 'actions.act2id', Yii::$app->params['cfg_cancel_stage']])
                    ->andWhere(['lap_is_need'=>1, 'lap_is_exist'=>0, 'isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);
        //   Данные запросы проверяют, есть ли в тендере лоты, где нужно было внести обеспечения и они не были внесены 
        $lot_counts_false = (new \yii\db\Query())
            ->select(['count(aid_false) as lot_counts_false', 'l_tenderid'])
            ->from(['T' => $subquery_false])
            ->groupBy(['l_tenderid']);

        $subquery_neutral = (new \yii\db\Query())
                    ->select(['count(actions.aid) as "aid_neutral"', 'tender_lots.l_tenderid as l_tenderid'])
                    ->from('actions')
                    ->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
                    ->where(['<', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);
        //   Данные запросы проверяют, есть ли в тендере лоты, которые не дошли до стадии договора
        $lot_counts_neutral = (new \yii\db\Query())
            ->select(['count(aid_neutral) as lot_counts_neutral', 'l_tenderid'])
            ->from(['T' => $subquery_neutral])
            ->groupBy(['l_tenderid']);


        $lot_counts_signed_agreements_subquery = (new \yii\db\Query())
                    ->select(['count(actions.aid) as "aid_signed_agreements"', 'tender_lots.l_tenderid as l_tenderid'])
                    ->from('actions')
                    ->join('inner join', 'tender_lots', 'tender_lots.l_dealid = actions.dealid')
                    ->where(['>=', 'actions.act2id', Yii::$app->params['cfg_contr_act2id']])
                    ->andWhere(['<', 'actions.act2id', Yii::$app->params['cfg_cancel_stage']])
                    ->andWhere(['isactive'=>1, 'isdel'=>0])
                    ->groupBy(['actions.dealid']);

        // проверка, есть ли лот в тендере, где подписан договор
        $lot_counts_signed_agreements = (new \yii\db\Query())
                    ->select(['count(aid_signed_agreements) as lot_counts_signed_agreements', 'l_tenderid'])
                    ->from(['T' => $lot_counts_signed_agreements_subquery])
                    ->groupBy(['l_tenderid']);

        $lot_counts_tv = (new \yii\db\Query())
                    ->select(['count(tender_lots.lotid) as lot_counts_tv', 'tender_lots.l_tenderid'])
                    ->from('tender_lots')
                    ->join('inner join', 'treqform', 'treqform.dealid = tender_lots.l_dealid')
                    ->where(['treqform.issucc' => 1, 'treqform.isactive' => 1])
                    ->groupBy(['l_tenderid']);

        $lot_participate_true = (new \yii\db\Query())
                    ->select(['count(tender_participate_history.lotid) as lot_participate_count_true', 'tender_lots.l_tenderid'])
                    ->from('tender_participate_history')
                    ->join('inner join', 'tender_lots', 'tender_lots.lotid = tender_participate_history.lotid')
                    ->where(['tender_participate_history.isActive' => 1, 'tender_participate_history.isParticipate' => 1])
                    ->groupBy(['l_tenderid']);

        $lot_participate_false = (new \yii\db\Query())
                    ->select(['count(tender_participate_history.lotid) as lot_participate_count_false', 'tender_lots.l_tenderid'])
                    ->from('tender_participate_history')
                    ->join('inner join', 'tender_lots', 'tender_lots.lotid = tender_participate_history.lotid')
                    ->where(['tender_participate_history.isActive' => 1, 'tender_participate_history.isParticipate' => 0])
                    ->groupBy(['l_tenderid']);

     
        $common_query = (new \yii\db\Query())
                    ->select(['tenders.tenderid', 'lot_counts_tv.lot_counts_tv', 
                        'tenders.t_csbin', 'tenders.t_csname', 'lot_participate_true.lot_participate_count_true',
                        'lot_participate_false.lot_participate_count_false', 'tenders.purchase_number', 'tenders.accept_solution', 
                        'tenders.tender_date_purchase', 'tender_managers.tm_managerid', 'tender_managers.tm_userfullname',
                        'tenders.application_status', 'tenders.application_status_date', 'lot_counts_true.lot_counts_true',
                        'lot_counts_false.lot_counts_false', 'lot_counts_neutral.lot_counts_neutral', 'lot_counts_signed_agreements.lot_counts_signed_agreements',
                        'tenders.senddate', 'tender_sources.source_title', 'tenders.tender_username', 'tenders.tender_userid',
                        'tender_managers.tm_created_at', 'tenders.application_status_userid', 'tenders.updated_at'])
                    ->from('tenders')
                    ->join('left join', 'tender_lots', 'tenders.tenderid = tender_lots.l_tenderid')
                    ->join('left join', 'tender_purchases', 'tenders.purchase_type = tender_purchases.purchaseid')
                    ->join('left join', 'tender_sources', 'tenders.source_get = tender_sources.sourceid')
                    ->join('left join', 'tender_lot_dep', 'tender_lot_dep.dep_lotid = tender_lots.lotid')
                    ->join('left join', 'tender_managers', 'tenders.tenderid = tender_managers.tm_tenderid')
                    ->join('left join', 'tender_lot_signed', 'tender_lot_signed.s_lotid = tender_lots.lotid')
                    ->join('left join', 'tender_lot_managers', 'tender_lot_managers.m_lotid = tender_lots.lotid')
                    ->join('left join', 'tender_lot_documents', 'tender_lot_documents.ld_lotid = tender_lots.lotid')
                    ->join('left join', ['lot_participate_true'=>$lot_participate_true], 'tenders.tenderid = lot_participate_true.l_tenderid')
                    ->join('left join', ['lot_participate_false'=>$lot_participate_false], 'tenders.tenderid = lot_participate_false.l_tenderid')
                    ->join('left join', ['lot_counts_true'=>$lot_counts_true], 'lot_counts_true.l_tenderid = tenders.tenderid')
                    ->join('left join', ['lot_counts_signed_agreements'=>$lot_counts_signed_agreements], 'lot_counts_signed_agreements.l_tenderid = tenders.tenderid')
                    ->join('left join', ['lot_counts_false'=>$lot_counts_false], 'lot_counts_false.l_tenderid = tenders.tenderid')
                    ->join('left join', ['lot_counts_neutral'=>$lot_counts_neutral], 'lot_counts_neutral.l_tenderid = tenders.tenderid')
                    ->join('left join', ['lot_counts_tv' => $lot_counts_tv], 'lot_counts_tv.l_tenderid = tenders.tenderid')
                    ->where([
                        'or',
                        ['like', 'tenders.purchase_number', $q],
                        ['like', 'tenders.t_csbin', $q],
                        ['like', 'tenders.t_csname', $q],
                        ['like', 'tender_lots.lotid', $q]
                    ]);
                        
        //Если супер админ 
        if ($user_role == 1) {
            // и если depid в privusers 0 , то он видит все подразделения тенедера
            if ($user_role_depid[0] == 0) {
                // и если tender hidden в privusers 1, то он видит и закрытые тендеры
                if ($user_role_hidden == 1) {
                    return $common_query
                                ->groupBy('tenders.tenderid')
                                ->all();
                }
                // если tender hidden 0, то он не видит закрытые тендеры
                else {
                    return $common_query
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            // и если depid в privusers не нуль , то он видит все тендеры с соответствующими подразделениями
            } else {

                if ($user_role_hidden == 1) {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->groupBy('tenders.tenderid')
                            ->all();
                } else {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            }
            // если tender = 2, то это специалист, который видит все тенедеры , но не может назначать специалистов на тендер  
        } elseif ($user_role == 2) {

          // Yii::$app->db->createCommand('UPDATE tenders, treqform, tender_lots SET 
          //                               tenders.updated_at = treqform.fdate WHERE tenders.tenderid = 
          //                               tender_lots.l_tenderid AND tender_lots.l_dealid = treqform.dealid AND 
          //                               tenders.updated_at < treqform.fdate AND treqform.isactive = 1')
          //                             ->execute();
            // если depid у данного юзера 0 , то он видит все тенедеры всех подраздлениях 
            if ($user_role_depid[0] == 0) {
                // и если tender hidden в privusers 1, то он видит и закрытые тендеры
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->groupBy('tenders.tenderid')
                            ->all();
                        // и если tender hidden в privusers 0, то он не видит закрытые тендеры
                } else {
                    return $common_query
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            // отобразить тендеры, которые имеют лоты с соответсвующими depid
            } else {
                // отобразить тендеры, которые имеют лоты с соответсвующими depid (с закртыми)
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->groupBy('tenders.tenderid')
                            ->all();
                // отобразить тендеры, которые имеют лоты с соответсвующими depid (без закртых)
                } else {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }   
            }
        //  если tender в Privuser у авторизованного пользоватлея равен 3, то это привилегированный пользователь который видит только тендера, который у него в depid  
        } elseif ($user_role == 3) {
            // отобразить все тендеры, независимо от подраздления
            if ($user_role_depid[0] == 0) {
                // также отбобразить и закртые тендеры
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->groupBy('tenders.tenderid')
                            ->all();
                } else {
                    return $common_query
                            ->andWhere(['!=','tenders.purchase_type', 13])
                            ->groupBy('tenders.tenderid')
                            ->all();
                }
            // отобразить тендеры, в зависимости от подраздления пользователя   
            } else {
                // вместе с закрытыми тендерами
                if ($user_role_hidden == 1) {
                    return $common_query
                            ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                            ->groupBy('tenders.tenderid')
                            ->all();
                } else {
                    return $common_query
                        ->andWhere(['in', 'tender_lot_dep.depid', $user_role_depid])
                        ->andWhere(['!=','tenders.purchase_type', 13])
                        ->groupBy('tenders.tenderid')
                        ->all();
                }
            }
        } else {
            return  $common_query
                        ->andWhere(['or',
                            ['tender_lot_signed.s_userid' => $userid],
                            ['tender_lot_managers.m_managerid' => $userid],
                            ['in', 'tender_lot_documents.ld_depid', $user_role_depid]
                        ])
                        ->groupBy(['tenders.tenderid'])
                        ->all();
        }   
    }


    public static function info($id)
    {
        $info = (new \yii\db\Query())
                ->select('*')
                ->from('tenders')
                ->join('left join', 'tender_purchases', 'tenders.purchase_type = tender_purchases.purchaseid')
                ->join('left join', 'tender_sources', 'tenders.source_get = tender_sources.sourceid')
                ->join('left join', 'tender_sectors', 'tenders.sector = tender_sectors.sectorid')
                ->join('left join', 'csoriglist', 'csoriglist.origid = tenders.t_csorigid')
                ->join('left join', 'csognlist', 'csognlist.ognid = tenders.t_csognid')
                ->where(['tenderid'=>$id])
                ->one();

        $tender_files = (new \yii\db\Query())
        ->select('*')
        ->from('tender_files')
        ->where(['tenderid'=>$id])
        ->all();

        $info['files'] = $tender_files;

        $info['tender_purchase_changed'] = (new \yii\db\Query())
                ->select('tender_purchases.purchase_title')
                ->from('tender_purchases')
                ->join('inner join', 'tenders', 'tenders.purchase_type_changed = tender_purchases.purchaseid')
                ->where(['tenderid'=>$id])
                ->one();


        return $info;
    }

    public static function getPurchases()
    {
        return (new \yii\db\Query())
        ->select('*')
        ->from('tender_purchases')
        ->all();
    }

    public function store()
    {
       $this->csid = Yii::$app->request->post('t_csid');
       $this->senddate = strtotime(Yii::$app->request->post('senddate'));
       $this->accept_solution = strtotime(Yii::$app->request->post('accept_solution'));
       $this->consortium = Yii::$app->request->post('consortium');
       $this->source_get = Yii::$app->request->post('source_get');
       $this->date_end_discuss = strtotime(Yii::$app->request->post('date_end_discuss'));
       $this->date_purchase = strtotime(Yii::$app->request->post('date_purchase'));
       $this->t_csbin = preg_replace('/\s+/','',Yii::$app->request->post('t_csbin_text'));
       $this->t_csname = Yii::$app->request->post('t_csname_text');
       $this->origid = Yii::$app->request->post('origid');
       $this->ognid = Yii::$app->request->post('ognid');
       $this->purchase_type = Yii::$app->request->post('t_purchase');
       $this->purchase_type_changed = Yii::$app->request->post('purchase_type_changed');
       $this->purchase_number = Yii::$app->request->post('purchase_number');
       $this->tender_userid = Yii::$app->auth->user()['userid'];
       $this->tender_username = Yii::$app->auth->user()['fullname'];
       $this->purchase_name = Yii::$app->request->post('purchase_name');
       $this->sector = Yii::$app->request->post('sector');
       $this->file_code = Yii::$app->request->post('tmp_code');
       $this->updated_at = Yii::$app->params['currentTime'];
        

        if (strlen($this->t_csbin) != 12) {
            $this->error = "БИН введен некорректно!";
            return false;
        }

        if ($this->validate()) {

            $bin = (new \yii\db\Query())
            ->select('csid')
            ->from('csinfo')
            ->where(['csid' => $this->csid])
            ->one();

            $bin2 = (new \yii\db\Query())
            ->select('csid')
            ->from(Yii::$app->params['cfg_sandbox_db'].'.csinfo')
            ->where(['csid' => $this->csid])
            ->one();

            if (isset($bin['csid']) || isset($bin2['csid'])) {
                $q = Yii::$app->db->createCommand()->insert('tenders', [
                    'senddate' => $this->senddate,
                    'accept_solution' => $this->accept_solution,
                    'consortium' => $this->consortium,
                    'tender_date_purchase' => $this->date_purchase,
                    'date_end_discuss' => $this->date_end_discuss,
                    'source_get' => $this->source_get,
                    't_csid' => $this->csid,
                    't_csbin' => $this->t_csbin,
                    't_csname' => $this->t_csname,
                    't_csorigid' => $this->origid,
                    't_csognid' => $this->ognid,
                    'tender_userid' => $this->tender_userid,
                    'tender_username' => $this->tender_username,
                    'purchase_type' => $this->purchase_type,
                    'purchase_type_changed' => $this->purchase_type_changed, 
                    'purchase_number' => $this->purchase_number,
                    'purchase_name' => $this->purchase_name,
                    'sector' => $this->sector,
                    'file_code' => $this->file_code,
                    'updated_at' => $this->updated_at,
                ])->execute();
            } else {
                $q = Yii::$app->db->createCommand()->insert('tenders', [
                    'senddate' => $this->senddate,
                    'accept_solution' => $this->accept_solution,
                    'consortium' => $this->consortium,
                    'source_get' => $this->source_get,
                    'tender_date_purchase' => $this->date_purchase,
                    'date_end_discuss' => $this->date_end_discuss,
                    't_csbin' => $this->t_csbin,
                    't_csname' => $this->t_csname,
                    't_csorigid' => $this->origid,
                    't_csognid' => $this->ognid,
                    'tender_userid' => $this->tender_userid,
                    'tender_username' => $this->tender_username, 
                    'purchase_type' => $this->purchase_type,
                    'purchase_type_changed' => $this->purchase_type_changed,  
                    'purchase_number' => $this->purchase_number,
                    'purchase_name' => $this->purchase_name,
                    'file_code' => $this->file_code,
                    'sector' => $this->sector, 
                    'updated_at' => $this->updated_at,
                ])->execute();
            }

            $this->id = Yii::$app->db->getLastInsertID();
            return true;
        } else {
           $this->error = 'Заполните все необходимые поля'; 
           return false;
        }
    }

    public function edit($id)
    {
        //return var_dump(Yii::$app->params);

        $this->csid = Yii::$app->request->post('t_csid');
        $this->senddate = strtotime(Yii::$app->request->post('senddate'));
        $this->source_get = Yii::$app->request->post('source_get');
        $this->t_csbin = preg_replace('/\s+/','',Yii::$app->request->post('t_csbin_text'));
        $this->accept_solution = strtotime(Yii::$app->request->post('accept_solution'));
        $this->consortium = Yii::$app->request->post('consortium');
        $this->t_csname = Yii::$app->request->post('t_csname_text');
        $this->origid = Yii::$app->request->post('origid');
        $this->ognid = Yii::$app->request->post('ognid');
        $this->purchase_type = Yii::$app->request->post('t_purchase');
        $this->purchase_type_changed = Yii::$app->request->post('purchase_type_changed');
        $this->purchase_number = Yii::$app->request->post('purchase_number');
        $this->purchase_name = Yii::$app->request->post('purchase_name');
        $this->sector = Yii::$app->request->post('sector');
        $this->date_end_discuss = strtotime(Yii::$app->request->post('date_end_discuss'));
        $this->date_purchase = strtotime(Yii::$app->request->post('date_purchase'));
        $this->updated_at = Yii::$app->params['currentTime'];
        $this->procurement_id = Yii::$app->request->post('procurement_id');

        if (strlen($this->t_csbin) != 12) {
            $this->error = "БИН введен некорректно!";
            return false;
        }

        if ($this->validate()) {

            $bin = (new \yii\db\Query())
                    ->select('csid')
                    ->from('csinfo')
                    ->where(['csid' => $this->csid])
                    ->one();

            $bin2 = (new \yii\db\Query())
                    ->select('csid')
                    ->from(Yii::$app->params['cfg_sandbox_db'].'.csinfo')
                    ->where(['csid' => $this->csid])
                    ->one();

            if (isset($bin['csid']) || isset($bin2['csid'])) {

                $q = Yii::$app->db->createCommand()->update('tenders', [
                    'senddate' => $this->senddate,
                    'accept_solution' => $this->accept_solution,
                    'consortium' => $this->consortium,
                    'source_get' => $this->source_get,
                    't_csid' => $this->csid,
                    't_csbin' => $this->t_csbin,
                    't_csname' => $this->t_csname, 
                    't_csorigid' => $this->origid,
                    't_csognid' => $this->ognid,
                    'purchase_type' => $this->purchase_type,
                    'purchase_type_changed' => $this->purchase_type_changed,  
                    'purchase_number' => $this->purchase_number,
                    'purchase_name' => $this->purchase_name,
                    'sector' => $this->sector,
                    'tender_date_purchase' => $this->date_purchase,
                    'date_end_discuss' => $this->date_end_discuss,
                    'updated_at' => $this->updated_at,
                    'procurement_id' => $this->procurement_id
                ], 'tenderid ='.$id)->execute();
                
            } else {
                $q = Yii::$app->db->createCommand()->update('tenders', [
                    'senddate' => $this->senddate,
                    'accept_solution' => $this->accept_solution,
                    'consortium' => $this->consortium,
                    'source_get' => $this->source_get,
                    't_csid' => 0,
                    't_csbin' => $this->t_csbin,
                    't_csname' => $this->t_csname,
                    't_csorigid' => $this->origid, 
                    't_csognid' => $this->ognid,
                    'purchase_type' => $this->purchase_type,
                    'purchase_type_changed' => $this->purchase_type_changed, 
                    'purchase_number' => $this->purchase_number,
                    'purchase_name' => $this->purchase_name,
                    'sector' => $this->sector,
                    'tender_date_purchase' => $this->date_purchase,
                    'date_end_discuss' => $this->date_end_discuss,
                    'updated_at' => $this->updated_at,
                    'procurement_id' => $this->procurement_id
                ], 'tenderid ='.$id)->execute();
            }
            

            return true;
        } else {
            $this->error = 'Заполните все необходимые поля'; 
            return false; 
        }
       
    }

    public function isHasContragent($id)
    {
        $tender = (new \yii\db\Query())
                ->select(['tenders.t_csbin', 'tenders.t_csid', 'tenders.tenderid', 'tenders.purchase_name', 'tenders.t_csname', 'tenders.t_csorigid', 'tenders.t_csognid'])
                ->from('tender_lots')
                ->join('left join', 'tenders', 'tender_lots.l_tenderid = tenders.tenderid')
                ->where(['tender_lots.lotid' => $id])
                ->one();

        $this->purchase_name = $tender['purchase_name'];
        $this->t_csname = $tender['t_csname'];
        $this->t_csbin = $tender['t_csbin'];
        $this->origid = $tender['t_csorigid'];
        $this->tenderid = $tender['tenderid'];
        $this->ognid = $tender['t_csognid'];

        $contragent = (new \yii\db\Query())
                ->select(['csbin', 'csid'])
                ->from('csinfo')
                ->where(['csbin' => $this->t_csbin, 'cscactive'=>1])
                ->one();

        if ($contragent) {

            $this->csid = $contragent['csid'];
            Yii::$app->db->createCommand()->update('tenders', [
                't_csid' => $this->csid,
            ], 'tenderid = '.$this->tenderid)->execute();

            return true;

        }

        //ищем контрагента из песочницы

        $contragent2 = (new \yii\db\Query())
                ->select(['csbin', 'csid'])
                ->from(Yii::$app->params['cfg_sandbox_db'].'.csinfo')
                ->where(['csbin' => $this->t_csbin, 'cscactive'=>1])
                ->one();

        if ($contragent2) {

            $sandbox = new \sandBox();

            $this->csid = $contragent2['csid'];

            $sandbox->remove($this->csid);

            Yii::$app->db->createCommand()->update('tenders', [
                't_csid' => $this->csid,
            ], 'tenderid = '.$this->tenderid)->execute();
            return true;

        }

        return false;
    }

    public static function toggleTenderStatus()
    {
    	$tenderid = Yii::$app->request->post('tenderid');
    	$status = Yii::$app->request->post('status');
    	$userid = Yii::$app->auth->user()['userid'];

    	Yii::$app->db->createCommand()->update('tenders', [
                'application_status' => $status,
                'application_status_userid' => $userid,
                'application_status_date' => Yii::$app->params['currentTime'],
                'updated_at' => Yii::$app->params['currentTime'],
        ], 'tenderid = '.$tenderid)->execute();


        return true;
    }

    public static function getContragents()
    {
        return (new \yii\db\Query())
                ->select(['t_csbin', 't_csname'])
                ->from('tenders')
                ->distinct()
                ->all();
    }

    public static function reportAll()
    {   
        $sector = base64_decode(Yii::$app->request->get('sector'));
        $solution = base64_decode(Yii::$app->request->get('solution'));
        $source = base64_decode(Yii::$app->request->get('source'));
        $contragent = base64_decode(Yii::$app->request->get('contragent'));

        $lot_participate_true = (new \yii\db\Query())
                                ->select(['count(tender_participate_history.lotid) as lot_participate_count_true', 'tender_lots.l_tenderid'])
                                ->from('tender_participate_history')
                                ->join('inner join', 'tender_lots', 'tender_lots.lotid = tender_participate_history.lotid')
                                ->where(['tender_participate_history.isActive' => 1, 'tender_participate_history.isParticipate' => 1])
                                ->groupBy(['l_tenderid']);

        $lot_participate_false = (new \yii\db\Query())
                                ->select(['count(tender_participate_history.lotid) as lot_participate_count_false', 'tender_lots.l_tenderid'])
                                ->from('tender_participate_history')
                                ->join('inner join', 'tender_lots', 'tender_lots.lotid = tender_participate_history.lotid')
                                ->where(['tender_participate_history.isActive' => 1, 'tender_participate_history.isParticipate' => 0])
                                ->groupBy(['l_tenderid']);

        $tender_lot_dep = (new \yii\db\Query())
                ->select(['l_tenderid', 'GROUP_CONCAT(DISTINCT dname SEPARATOR ", ") AS depnames'])
                ->from('tender_lot_dep l')
                ->join('left join', 'contracts.deplist d', 'l.depid = d.depid')
                ->join('join', 'tender_lots', 'tender_lots.lotid = l.dep_lotid')
                ->join('join', 'tenders', 'tenders.tenderid = tender_lots.l_tenderid')
                ->groupBy(['l_tenderid']);

        $query = (new \yii\db\Query())
                        ->select('*')
                        ->from('tenders')
                        ->join('left join', 'tender_managers', 'tender_managers.tm_tenderid = tenders.tenderid')
                        ->join('left join', 'tender_sources', 'tender_sources.sourceid = tenders.source_get')
                        ->join('left join', ['lot_participate_true'=>$lot_participate_true], 'tenders.tenderid = lot_participate_true.l_tenderid')
                        ->join('left join', ['lot_participate_false'=>$lot_participate_false], 'tenders.tenderid = lot_participate_false.l_tenderid')
                        ->join('left join', ['tender_lot_dep'=>$tender_lot_dep], 'tenders.tenderid = tender_lot_dep.l_tenderid');

        if ($sector != 0) {
            $query->andWhere(['in', 'tenders.sector', $sector]);
        }

        if ($contragent != 0) {
             $query->andWhere(['tenders.t_csbin' => $contragent]);
        }

        if ($source != 0) {
            $query->andWhere(['in', 'tenders.source_get', $source]);
        }

        if ($solution == 1) {
            $query->andWhere(['>', 'lot_participate_count_true', 0]);
        }

        if ($solution == 2) {
            $query->andWhere([
                'and',
                ['>', 'lot_participate_count_false', 0],
                ['is', 'lot_participate_count_true', NULL]
            ]);
        }

         if ($solution == 3) {
            $query
                ->andWhere([
                    'and',
                     ['is', 'lot_participate_count_false', NULL],
                     ['is', 'lot_participate_count_true', NULL]
                ]);
        }

        return $query->all();
                       
    }   
}